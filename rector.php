<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/docs',
        __DIR__ . '/src',
    ]);

    $rectorConfig->symfonyContainerXml(__DIR__ . '/var/cache/dev/testsApp_KernelDevDebugContainer.xml');
    $rectorConfig->symfonyContainerPhp(__DIR__ . '/tests/symfony-container.php');

    //$rectorConfig->cacheClass(\Rector\Caching\ValueObject\Storage\FileCacheStorage::class);
    //$rectorConfig->cacheDirectory(__DIR__ . '/.cache/rector');

    // register a single rule
    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);
    $rectorConfig->disableParallel();

    //define sets of rules
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_82,
        \Rector\Symfony\Set\SymfonyLevelSetList::UP_TO_SYMFONY_44,
        \Rector\Doctrine\Set\DoctrineSetList::DOCTRINE_CODE_QUALITY,
        \Rector\PHPUnit\Set\PHPUnitLevelSetList::UP_TO_PHPUNIT_90,
    ]);

    // some routes are added twice if it remains activated
    // $rectorConfig->rule(\Rector\Symfony\Configs\Rector\ClassMethod\AddRouteAnnotationRector::class);

    // chill rules
    //$rectorConfig->rule(\Chill\Utils\Rector\Rector\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector::class);

    // skip some path...
    $rectorConfig->skip([
        // we need to discuss this: are we going to have FALSE in tests instead of an error ?
        \Rector\Php71\Rector\FuncCall\CountOnNullRector::class,

        // we must adapt service definition
        \Rector\Symfony\Symfony28\Rector\MethodCall\GetToConstructorInjectionRector::class,
        \Rector\Symfony\Symfony34\Rector\Closure\ContainerGetNameToTypeInTestsRector::class,
    ]);
};
