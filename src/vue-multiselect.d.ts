declare module "vue-multiselect" {
  import { defineComponent } from 'vue'

  export interface VueMultiselectProps<T> {
    options: T;
    searchable: boolean;
    trackBy: keyof T;
    label: keyof T;
  };

  const Component: ReturnType<typeof defineComponent<VueMultiselectProps<T>>>;

  export default Component
}
