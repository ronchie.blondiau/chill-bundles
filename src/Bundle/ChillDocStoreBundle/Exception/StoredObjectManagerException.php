<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Exception;

final class StoredObjectManagerException extends \Exception
{
    public static function errorDuringHttpRequest(\Throwable $exception): self
    {
        return new self('Error during HTTP request.', 500, $exception);
    }

    public static function invalidStatusCode(int $code): self
    {
        return new self(
            sprintf('Invalid status code received (%s).', $code)
        );
    }

    public static function unableToDecrypt(string $message): self
    {
        return new self(sprintf('Unable to decrypt content (reason: %s).', $message));
    }

    public static function unableToGetResponseContent(\Throwable $exception): self
    {
        return new self('Unable to get content from response.', 500, $exception);
    }
}
