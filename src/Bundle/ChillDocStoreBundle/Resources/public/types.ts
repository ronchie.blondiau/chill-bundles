import {DateTime} from "../../../ChillMainBundle/Resources/public/types";

export type StoredObjectStatus = "ready"|"failure"|"pending";

export interface StoredObject {
  id: number,

  /**
   * filename of the object in the object storage
   */
  filename: string,
  creationDate: DateTime,
  datas: object,
  iv: number[],
  keyInfos: object,
  title: string,
  type: string,
  uuid: string,
  status: StoredObjectStatus,
}

export interface StoredObjectStatusChange {
  id: number,
  filename: string,
  status: StoredObjectStatus,
  type: string,
}

/**
 * Function executed by the WopiEditButton component.
 */
export type WopiEditButtonExecutableBeforeLeaveFunction = {
  (): Promise<void>
}

