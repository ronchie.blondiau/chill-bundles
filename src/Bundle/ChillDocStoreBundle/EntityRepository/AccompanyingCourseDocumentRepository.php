<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\EntityRepository;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccompanyingCourseDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccompanyingCourseDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccompanyingCourseDocument[]    findAll()
 * @method AccompanyingCourseDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccompanyingCourseDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccompanyingCourseDocument::class);
    }

    // /**
    //  * @return AccompanyingCourseDocument[] Returns an array of AccompanyingCourseDocument objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
     */

    /*
    public function findOneBySomeField($value): ?AccompanyingCourseDocument
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
     */
}
