<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Service;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Exception\StoredObjectManagerException;

interface StoredObjectManagerInterface
{
    public function getLastModified(StoredObject $document): \DateTimeInterface;

    /**
     * Get the content of a StoredObject.
     *
     * @param StoredObject $document the document
     *
     * @return string the retrieved content in clear
     *
     * @throws StoredObjectManagerException if unable to read or decrypt the content
     */
    public function read(StoredObject $document): string;

    /**
     * Set the content of a StoredObject.
     *
     * @param StoredObject $document     the document
     * @param              $clearContent The content to store in clear
     *
     * @throws StoredObjectManagerException
     */
    public function write(StoredObject $document, string $clearContent): void;

    public function clearCache(): void;
}
