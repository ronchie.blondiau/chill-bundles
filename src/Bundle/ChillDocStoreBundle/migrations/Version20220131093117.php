<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Implementations of create and update traits for Document entity + template property added.
 */
final class Version20220131093117 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.person_document DROP CONSTRAINT FK_41DA53C5DA0FB8');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP CONSTRAINT FK_41DA53C3174800F');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP CONSTRAINT FK_41DA53C65FF1AEC');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP template_id');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP createdAt');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP updatedAt');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_doc.person_document DROP updatedBy_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F65DA0FB8');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F63174800F');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP CONSTRAINT FK_A45098F665FF1AEC');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP template_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP createdAt');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP updatedAt');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document DROP updatedBy_id');
    }

    public function getDescription(): string
    {
        return 'Implementations of create and update traits for Document entity + template property added';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD template_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_doc.accompanyingcourse_document.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_doc.accompanyingcourse_document.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F65DA0FB8 FOREIGN KEY (template_id) REFERENCES chill_docgen_template (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F63174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.accompanyingcourse_document ADD CONSTRAINT FK_A45098F665FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A45098F65DA0FB8 ON chill_doc.accompanyingcourse_document (template_id)');
        $this->addSql('CREATE INDEX IDX_A45098F63174800F ON chill_doc.accompanyingcourse_document (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_A45098F665FF1AEC ON chill_doc.accompanyingcourse_document (updatedBy_id)');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD template_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_doc.person_document.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_doc.person_document.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD CONSTRAINT FK_41DA53C5DA0FB8 FOREIGN KEY (template_id) REFERENCES chill_docgen_template (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD CONSTRAINT FK_41DA53C3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.person_document ADD CONSTRAINT FK_41DA53C65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_41DA53C5DA0FB8 ON chill_doc.person_document (template_id)');
        $this->addSql('CREATE INDEX IDX_41DA53C3174800F ON chill_doc.person_document (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_41DA53C65FF1AEC ON chill_doc.person_document (updatedBy_id)');
    }
}
