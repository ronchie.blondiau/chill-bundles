<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocStore;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210928182542 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP uuid');
    }

    public function getDescription(): string
    {
        return 'Create UUID column on StoredObject table.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD uuid UUID DEFAULT NULL');
        $this->addSql('UPDATE chill_doc.stored_object SET uuid=uuid_generate_v4()');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER uuid SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_49604E36D17F50A6 ON chill_doc.stored_object (uuid)');
    }
}
