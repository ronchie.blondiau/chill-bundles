<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Form\AccompanyingCourseDocumentType;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}/parcours/{course}/document")
 */
class DocumentAccompanyingCourseController extends AbstractController
{
    /**
     * DocumentAccompanyingCourseController constructor.
     */
    public function __construct(
        protected TranslatorInterface $translator,
        protected EventDispatcherInterface $eventDispatcher,
        protected AuthorizationHelper $authorizationHelper
    ) {
    }

    /**
     * @Route("/{id}/delete", name="chill_docstore_accompanying_course_document_delete")
     */
    public function delete(Request $request, AccompanyingPeriod $course, AccompanyingCourseDocument $document): Response
    {
        $this->denyAccessUnlessGranted(AccompanyingCourseDocumentVoter::DELETE, $document);

        $form = $this->createForm(FormType::class);
        $form->add('submit', SubmitType::class, ['label' => 'Delete']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->remove($document);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->translator->trans('The document is successfully removed'));

            if ($request->query->has('returnPath')) {
                return $this->redirect($request->query->get('returnPath'));
            }

            return $this->redirectToRoute('chill_docstore_generic-doc_by-period_index', ['id' => $course->getId()]);
        }

        return $this->render(
            '@ChillDocStore/AccompanyingCourseDocument/delete.html.twig',
            [
                'document' => $document,
                'delete_form' => $form->createView(),
                'accompanyingCourse' => $course,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="accompanying_course_document_edit", methods="GET|POST")
     */
    public function edit(Request $request, AccompanyingPeriod $course, AccompanyingCourseDocument $document): Response
    {
        $this->denyAccessUnlessGranted(AccompanyingCourseDocumentVoter::UPDATE, $document);

        $document->setUser($this->getUser());
        $document->setDate(new \DateTime('Now'));

        $form = $this->createForm(
            AccompanyingCourseDocumentType::class,
            $document
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->translator->trans('The document is successfully updated'));

            return $this->redirectToRoute(
                'accompanying_course_document_edit',
                ['id' => $document->getId(), 'course' => $course->getId()]
            );
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans('This form contains errors'));
        }

        return $this->render(
            '@ChillDocStore/AccompanyingCourseDocument/edit.html.twig',
            [
                'document' => $document,
                'form' => $form->createView(),
                'accompanyingCourse' => $course,
            ]
        );
    }

    /**
     * @Route("/new", name="accompanying_course_document_new", methods="GET|POST")
     */
    public function new(Request $request, AccompanyingPeriod $course): Response
    {
        if (null === $course) {
            throw $this->createNotFoundException('Accompanying period not found');
        }

        $document = new AccompanyingCourseDocument();
        $document->setUser($this->getUser());
        $document->setCourse($course);
        $document->setDate(new \DateTime('Now'));

        $this->denyAccessUnlessGranted(AccompanyingCourseDocumentVoter::CREATE, $document);

        $form = $this->createForm(AccompanyingCourseDocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->denyAccessUnlessGranted(
                'CHILL_ACCOMPANYING_COURSE_DOCUMENT_CREATE',
                $document,
                'creation of this activity not allowed'
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            $this->addFlash('success', $this->translator->trans('The document is successfully registered'));

            return $this->redirectToRoute('chill_docstore_generic-doc_by-period_index', ['id' => $course->getId()]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans('This form contains errors'));
        }

        return $this->render('@ChillDocStore/AccompanyingCourseDocument/new.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
            'accompanyingCourse' => $course,
        ]);
    }

    /**
     * @Route("/{id}", name="accompanying_course_document_show", methods="GET")
     */
    public function show(AccompanyingPeriod $course, AccompanyingCourseDocument $document): Response
    {
        $this->denyAccessUnlessGranted(AccompanyingCourseDocumentVoter::SEE_DETAILS, $document);

        return $this->render(
            '@ChillDocStore/AccompanyingCourseDocument/show.html.twig',
            ['document' => $document, 'accompanyingCourse' => $course]
        );
    }
}
