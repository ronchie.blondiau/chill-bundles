<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Repository;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

class AccompanyingCourseDocumentRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(private readonly EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(AccompanyingCourseDocument::class);
    }

    public function buildQueryByCourse(AccompanyingPeriod $course): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('d');

        $qb
            ->where($qb->expr()->eq('d.course', ':course'))
            ->setParameter('course', $course);

        return $qb;
    }

    public function countByCourse(AccompanyingPeriod $course): int
    {
        $qb = $this->buildQueryByCourse($course)->select('COUNT(d)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function find($id): ?AccompanyingCourseDocument
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?AccompanyingCourseDocument
    {
        return $this->findOneBy($criteria);
    }

    public function getClassName()
    {
        return AccompanyingCourseDocument::class;
    }
}
