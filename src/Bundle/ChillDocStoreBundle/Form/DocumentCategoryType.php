<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentCategoryType extends AbstractType
{
    private $chillBundlesFlipped;

    public function __construct($kernelBundles)
    {
        // TODO faire un service dans CHillMain
        foreach ($kernelBundles as $key => $value) {
            if (str_starts_with((string) $key, 'Chill')) {
                $this->chillBundlesFlipped[$value] = $key;
            }
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bundleId', ChoiceType::class, [
                'choices' => $this->chillBundlesFlipped,
                'disabled' => false,
            ])
            ->add('idInsideBundle', null, [
                'disabled' => true,
            ])
            ->add('documentClass', null, [
                'disabled' => false,
            ]) // cahcerh par default PersonDocument
            ->add('name', TranslatableStringFormType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentCategory::class,
        ]);
    }
}
