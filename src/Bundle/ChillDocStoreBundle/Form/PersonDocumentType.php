<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\Document;
use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Chill\MainBundle\Form\Type\ScopePickerType;
use Chill\MainBundle\Security\Resolver\CenterResolverDispatcher;
use Chill\MainBundle\Security\Resolver\ScopeResolverDispatcher;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonDocumentType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelperInterface $translatableStringHelper, private readonly ScopeResolverDispatcher $scopeResolverDispatcher, private readonly ParameterBagInterface $parameterBag, private readonly CenterResolverDispatcher $centerResolverDispatcher)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $document = $options['data'];
        $isScopeConcerned = $this->scopeResolverDispatcher->isConcerned($document);

        $builder
            ->add('title', TextType::class)
            ->add('description', ChillTextareaType::class, [
                'required' => false,
            ])
            ->add('object', StoredObjectType::class, [
                'error_bubbling' => true,
            ])
            ->add('date', ChillDateType::class)
            ->add('category', EntityType::class, [
                'placeholder' => 'Choose a document category',
                'class' => DocumentCategory::class,
                'query_builder' => static fn (EntityRepository $er) => $er->createQueryBuilder('c')
                    ->where('c.documentClass = :docClass')
                    ->setParameter('docClass', PersonDocument::class),
                'choice_label' => fn ($entity = null) => $entity ? $this->translatableStringHelper->localize($entity->getName()) : '',
            ]);

        if ($isScopeConcerned && $this->parameterBag->get('chill_main')['acl']['form_show_scopes']) {
            $builder->add('scope', ScopePickerType::class, [
                'center' => $this->centerResolverDispatcher->resolveCenter($document),
                'role' => $options['role'],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);

        $resolver->setRequired(['role'])
            ->setAllowedTypes('role', ['string']);
    }
}
