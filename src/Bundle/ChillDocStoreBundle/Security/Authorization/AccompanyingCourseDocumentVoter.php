<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Security\Authorization;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\VoterHelperFactoryInterface;
use Chill\MainBundle\Security\Authorization\VoterHelperInterface;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

class AccompanyingCourseDocumentVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    final public const CREATE = 'CHILL_ACCOMPANYING_COURSE_DOCUMENT_CREATE';

    final public const DELETE = 'CHILL_ACCOMPANYING_COURSE_DOCUMENT_DELETE';

    final public const SEE = 'CHILL_ACCOMPANYING_COURSE_DOCUMENT_SEE';

    final public const SEE_DETAILS = 'CHILL_ACCOMPANYING_COURSE_DOCUMENT_SEE_DETAILS';

    final public const UPDATE = 'CHILL_ACCOMPANYING_COURSE_DOCUMENT_UPDATE';

    protected VoterHelperInterface $voterHelper;

    public function __construct(
        protected LoggerInterface $logger,
        protected Security $security,
        VoterHelperFactoryInterface $voterHelperFactory
    ) {
        $this->voterHelper = $voterHelperFactory
            ->generate(self::class)
            ->addCheckFor(AccompanyingCourseDocument::class, $this->getRoles())
            ->addCheckFor(AccompanyingPeriod::class, [self::SEE, self::CREATE])
            ->build();
    }

    public function getRoles(): array
    {
        return [
            self::CREATE,
            self::SEE,
            self::SEE_DETAILS,
            self::UPDATE,
            self::DELETE,
        ];
    }

    public function getRolesWithHierarchy(): array
    {
        return ['accompanyingCourseDocument' => $this->getRoles()];
    }

    public function getRolesWithoutScope(): array
    {
        return [];
    }

    protected function supports($attribute, $subject): bool
    {
        return $this->voterHelper->supports($attribute, $subject);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }

        if ($subject instanceof AccompanyingPeriod) {
            if (AccompanyingPeriod::STEP_CLOSED === $subject->getStep()) {
                if (self::CREATE === $attribute) {
                    return false;
                }
            }
        } elseif ($subject instanceof AccompanyingCourseDocument) {
            if (!$this->security->isGranted(AccompanyingPeriodVoter::SEE, $subject->getCourse())) {
                return false;
            }

            if (
                AccompanyingPeriod::STEP_CLOSED === $subject->getCourse()->getStep()
                && \in_array($attribute, [self::CREATE, self::DELETE, self::UPDATE], true)
            ) {
                return false;
            }

            if (self::CREATE === $attribute && null !== $subject->getCourse()) {
                return $this->voterHelper->voteOnAttribute($attribute, $subject->getCourse(), $token);
            }
        }

        return $this->voterHelper->voteOnAttribute($attribute, $subject, $token);
    }
}
