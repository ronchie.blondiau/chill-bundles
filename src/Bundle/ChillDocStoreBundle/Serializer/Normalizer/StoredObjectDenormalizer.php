<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Serializer\Normalizer;

use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\DocStoreBundle\Repository\StoredObjectRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectToPopulateTrait;

class StoredObjectDenormalizer implements DenormalizerInterface
{
    use ObjectToPopulateTrait;

    public function __construct(private readonly StoredObjectRepository $storedObjectRepository)
    {
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $object = $this->extractObjectToPopulate(StoredObject::class, $context);

        if (null !== $object) {
            return $object;
        }

        return $this->storedObjectRepository->find($data['id']);
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        if (false === \is_array($data)) {
            return false;
        }

        if (false === \array_key_exists('id', $data)) {
            return false;
        }

        return StoredObject::class === $type;
    }
}
