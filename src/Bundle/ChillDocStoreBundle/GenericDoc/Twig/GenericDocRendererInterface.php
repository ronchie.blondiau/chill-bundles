<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc\Twig;

use Chill\DocStoreBundle\GenericDoc\GenericDocDTO;

interface GenericDocRendererInterface
{
    public function supports(GenericDocDTO $genericDocDTO, $options = []): bool;

    public function getTemplate(GenericDocDTO $genericDocDTO, $options = []): string;

    public function getTemplateData(GenericDocDTO $genericDocDTO, $options = []): array;
}
