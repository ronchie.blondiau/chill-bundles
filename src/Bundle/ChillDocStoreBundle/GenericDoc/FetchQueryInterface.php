<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\GenericDoc;

use Doctrine\DBAL\Types\Types;

interface FetchQueryInterface
{
    public function getSelectKeyString(): string;

    public function getSelectIdentifierJsonB(): string;

    /**
     * @return list<mixed>
     */
    public function getSelectIdentifierParams(): array;

    /**
     * @return list<Types::*>
     */
    public function getSelectIdentifiersTypes(): array;

    public function getSelectDate(): string;

    /**
     * @return list<mixed>
     */
    public function getSelectDateParams(): array;

    /**
     * @return list<Types::*>
     */
    public function getSelectDateTypes(): array;

    public function getFromQuery(): string;

    /**
     * @return list<mixed>
     */
    public function getFromQueryParams(): array;

    /**
     * @return list<Types::*>
     */
    public function getFromQueryTypes(): array;

    public function getWhereQuery(): string;

    /**
     * @return list<mixed>
     */
    public function getWhereQueryParams(): array;

    /**
     * @return list<Types::*>
     */
    public function getWhereQueryTypes(): array;
}
