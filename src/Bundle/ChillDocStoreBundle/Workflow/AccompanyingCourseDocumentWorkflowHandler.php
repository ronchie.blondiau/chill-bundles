<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocStoreBundle\Workflow;

use Chill\DocStoreBundle\Entity\AccompanyingCourseDocument;
use Chill\DocStoreBundle\Security\Authorization\AccompanyingCourseDocumentVoter;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriodParticipation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccompanyingCourseDocumentWorkflowHandler implements EntityWorkflowHandlerInterface
{
    private readonly EntityRepository $repository;

    /**
     * TODO: injecter le repository directement.
     */
    public function __construct(
        EntityManagerInterface $em,
        private readonly TranslatorInterface $translator
    ) {
        $this->repository = $em->getRepository(AccompanyingCourseDocument::class);
    }

    public function getDeletionRoles(): array
    {
        return [
            AccompanyingCourseDocumentVoter::DELETE,
        ];
    }

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $course = $this->getRelatedEntity($entityWorkflow)?->getCourse();
        $persons = [];

        if (null !== $course) {
            $persons = $course->getCurrentParticipations()->map(static fn (AccompanyingPeriodParticipation $participation) => $participation->getPerson())->toArray();
        }

        return [
            'persons' => array_values($persons),
        ];
    }

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        $doc = $this->getRelatedEntity($entityWorkflow);

        if (null === $doc) {
            return $this->translator->trans('workflow.Document deleted');
        }

        return $this->translator->trans('workflow.Document (n°%doc%)', ['%doc%' => $entityWorkflow->getRelatedEntityId()])
            .' - '.$doc->getTitle();
    }

    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?AccompanyingCourseDocument
    {
        return $this->repository->find($entityWorkflow->getRelatedEntityId());
    }

    /**
     * @param AccompanyingCourseDocument $object
     *
     * @return array[]
     */
    public function getRelatedObjects(object $object): array
    {
        return [
            ['entityClass' => AccompanyingCourseDocument::class, 'entityId' => $object->getId()],
        ];
    }

    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string
    {
        return null;
    }

    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array
    {
        $suggestedUsers = $entityWorkflow->getUsersInvolved();

        $referrer = $this->getRelatedEntity($entityWorkflow)->getCourse()->getUser();
        $suggestedUsers[spl_object_hash($referrer)] = $referrer;

        return $suggestedUsers;
    }

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        return '@ChillDocStore/AccompanyingCourseDocument/_workflow.html.twig';
    }

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        return [
            'entity_workflow' => $entityWorkflow,
            'document' => $this->getRelatedEntity($entityWorkflow),
        ];
    }

    public function isObjectSupported(object $object): bool
    {
        return $object instanceof AccompanyingCourseDocument;
    }

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return AccompanyingCourseDocument::class === $entityWorkflow->getRelatedEntityClass();
    }

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return false;
    }
}
