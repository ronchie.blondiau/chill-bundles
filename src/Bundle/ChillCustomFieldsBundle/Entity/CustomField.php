<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomField.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="customfield")
 *
 * @ORM\HasLifecycleCallbacks
 */
class CustomField
{
    final public const ONE_TO_MANY = 2;

    final public const ONE_TO_ONE = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\CustomFieldsBundle\Entity\CustomFieldsGroup",
     * inversedBy="customFields")
     */
    private ?CustomFieldsGroup $customFieldGroup = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     */
    private array $options = [];

    /**
     * @ORM\Column(type="float")
     */
    private ?float $ordering = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private false $required = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $type = null;

    /**
     * Get customFieldGroup.
     *
     * @return CustomFieldsGroup
     */
    public function getCustomFieldsGroup()
    {
        return $this->customFieldGroup;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @param mixed|null $locale
     *
     * @return array
     */
    public function getName($locale = null)
    {
        if ($locale) {
            if (isset($this->name[$locale])) {
                return $this->name[$locale];
            }

            foreach ($this->name as $name) {
                if (!empty($name)) {
                    return $name;
                }
            }

            return '';
        }

        return $this->name;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get order.
     *
     * @return float
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * alias for isRequired.
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->isRequired();
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns true if the custom field is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * return true if the field required.
     *
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return CustomField
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set customFieldGroup.
     *
     * @return CustomField
     */
    public function setCustomFieldsGroup(?CustomFieldsGroup $customFieldGroup = null)
    {
        $this->customFieldGroup = $customFieldGroup;

        return $this;
    }

    /**
     * Set name.
     *
     * @param array $name
     *
     * @return CustomField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set options.
     *
     * @return CustomField
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Set order.
     *
     * @param float $order
     *
     * @return CustomField
     */
    public function setOrdering($order)
    {
        $this->ordering = $order;

        return $this;
    }

    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return CustomField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
