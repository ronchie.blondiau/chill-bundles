<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Service;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Helpers for manipulating custom fields.
 *
 * Herlpers for getting a certain custom field, for getting the raw value
 * of a custom field and for rendering the value of a custom field.
 */
class CustomFieldsHelper
{
    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em       The entity manager
     * @param CustomFieldProvider    $provider The customfield provider that
     *                                         contains all the declared custom fields
     */
    public function __construct(private readonly EntityManagerInterface $em, private readonly CustomFieldProvider $provider)
    {
    }

    public function isEmptyValue(array $fields, CustomField $customField)
    {
        $slug = $customField->getSlug();
        $rawValue = $fields[$slug] ?? null;
        $customFieldType = $this->provider->getCustomFieldByType($customField->getType());

        $deserializedValue = $customFieldType->deserialize($rawValue, $customField);

        return $customFieldType->isEmptyValue($deserializedValue, $customField);
    }

    /**
     * Render the value of a custom field.
     *
     * @param array       $fields       the **raw** array, as stored in the db
     * @param CustomField $customField  the customField entity
     * @param string      $documentType the type of document in which the rendered value is displayed ('html' or 'csv')
     *
     * @return The representation of the value the customField
     *
     * @throws CustomFieldsHelperException if slug is missing
     */
    public function renderCustomField(array $fields, CustomField $customField, $documentType = 'html')
    {
        $slug = $customField->getSlug();
        $rawValue = $fields[$slug] ?? null;
        $customFieldType = $this->provider->getCustomFieldByType($customField->getType());

        return $customFieldType->render($rawValue, $customField, $documentType);
    }
}
