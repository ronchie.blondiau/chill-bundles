<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Tests\Config;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the option Customizables_entities.
 *
 * @internal
 *
 * @coversNothing
 */
final class ConfigCustomizablesEntitiesTest extends KernelTestCase
{
    /**
     * Test that the 'chill_custom_fields.customizables_entities' is filled
     * correctly with a minimal configuration.
     *
     * @internal use a custom config environment
     */
    public function testNotEmptyConfig()
    {
        self::bootKernel(['environment' => 'test_customizable_entities_test_not_empty_config']);
        $customizableEntities = self::$kernel->getContainer()
            ->getParameter('chill_custom_fields.customizables_entities');

        $this->assertIsArray($customizableEntities);
        $this->assertCount(2, $customizableEntities);

        foreach ($customizableEntities as $key => $config) {
            $this->assertIsArray($config);
            $this->assertArrayHasKey('name', $config);
            $this->assertArrayHasKey('class', $config);
        }
    }

    /**
     * Test that the config does work if the option
     * chill_custom_fields.customizables_entities IS NOT present.
     *
     * In this case, parameter 'chill_custom_fields.customizables_entities'
     * should be an empty array in container
     */
    public function testNotPresentInConfig()
    {
        self::bootKernel(['environment' => 'test']);
        $customizableEntities = self::$kernel->getContainer()
            ->getParameter('chill_custom_fields.customizables_entities');

        $this->assertIsArray($customizableEntities);
        $this->assertCount(1, $customizableEntities);
    }
}
