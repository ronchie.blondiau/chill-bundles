<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Tests\CustomFields;

use Chill\CustomFieldsBundle\CustomFields\CustomFieldText;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Tests\CustomFieldTestHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CustomFieldsTextTest extends WebTestCase
{
    use CustomFieldTestHelper;

    private ?object $customFieldProvider = null;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->customFieldProvider = self::$kernel->getContainer()
            ->get('chill.custom_field.provider');
    }

    public function testCustomFieldsTextExists()
    {
        $customField = $this->customFieldProvider->getCustomFieldByType('text');

        $this->assertInstanceOf(
            \Chill\CustomFieldsBundle\CustomFields\CustomFieldInterface::class,
            $customField
        );
        $this->assertInstanceOf(
            CustomFieldText::class,
            $customField
        );
    }

    public function testFormTextNew()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/fr/admin/customfield/new?type=text');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $form = $crawler->selectButton('custom_field_choice_submit')->form();
        $this->assertTrue($form->has('custom_field_choice[options][maxLength]'));
    }

    public function testPublicFormRenderingLengthLessThan256()
    {
        $customField = new CustomField();
        $customField->setType('text')
            ->setOptions([CustomFieldText::MAX_LENGTH => 255])
            ->setSlug('slug')
            ->setOrdering(10)
            ->setActive(true)
            ->setName(['en' => 'my label']);

        $crawler = $this->getCrawlerForField($customField);

        $this->assertCount(1, $crawler->filter('input[type=text]'));
        $this->assertCount(1, $crawler->filter("label:contains('my label')"));
    }

    public function testPublicFormRenderingLengthMoreThan25()
    {
        $customField = new CustomField();
        $customField->setType('text')
            ->setOptions([CustomFieldText::MAX_LENGTH => 256])
            ->setSlug('slug')
            ->setOrdering(10)
            ->setActive(true)
            ->setName(['en' => 'my label']);

        $crawler = $this->getCrawlerForField($customField);

        $this->assertCount(1, $crawler->filter('textarea'));
        $this->assertCount(1, $crawler->filter("label:contains('my label')"));
    }
}
