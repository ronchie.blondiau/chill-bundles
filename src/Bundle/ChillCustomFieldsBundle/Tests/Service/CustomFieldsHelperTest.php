<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFields\Tests\Service;

use Chill\CustomFieldsBundle\Entity\CustomField;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Tests for custom fields helper.
 *
 * @internal
 *
 * @coversNothing
 */
final class CustomFieldsHelperTest extends KernelTestCase
{
    private ?object $cfHelper = null;

    private CustomField $randomCFText;

    protected function setUp(): void
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();

        $this->cfHelper = $container->get('chill.custom_field.helper');

        $this->randomCFText = (new CustomField())
            ->setSlug('my-slug')
            ->setActive(true)
            ->setName(['fr' => 'my cf'])
            ->setOptions(['maxLength' => 1000])
            ->setType('text');
    }

    public function testIsEmptyValue()
    {
        // not empty value
        $data = [
            $this->randomCFText->getSlug() => 'Sample text',
        ];

        $this->assertFalse($this->cfHelper->isEmptyValue($data, $this->randomCFText));

        // empty value
        $data = [
            $this->randomCFText->getSlug() => '',
        ];

        $this->assertTrue($this->cfHelper->isEmptyValue($data, $this->randomCFText));
    }

    public function testRenderCustomField()
    {
        $data = [
            $this->randomCFText->getSlug() => 'Sample text',
        ];

        $text = $this->cfHelper->renderCustomField($data, $this->randomCFText);

        $this->assertContains('Sample text', $text);
    }
}
