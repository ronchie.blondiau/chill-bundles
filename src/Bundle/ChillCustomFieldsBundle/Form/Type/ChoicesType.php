<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ChoicesType extends AbstractType
{
    public function getBlockPrefix()
    {
        return 'cf_choices';
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}
