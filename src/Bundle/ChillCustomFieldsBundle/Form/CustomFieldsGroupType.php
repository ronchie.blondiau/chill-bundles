<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form;

use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomFieldsGroupType extends AbstractType
{
    public function __construct(
        private readonly array $customizableEntities,
        // TODO : add comment about this variable
        private readonly TranslatorInterface $translator
    ) {
    }

    // TODO : details about the function
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // prepare translation
        $entities = [];
        $customizableEntities = []; // TODO : change name too close than $this->customizableEntities

        foreach ($this->customizableEntities as $key => $definition) {
            $entities[$definition['class']] = $this->translator->trans($definition['name']);
            $customizableEntities[$definition['class']] = $definition;
        }

        $builder
            ->add('name', TranslatableStringFormType::class)
            ->add('entity', ChoiceType::class, [
                'choices' => array_combine(array_values($entities), array_keys($entities)),
            ]);

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            static function (FormEvent $event) use ($customizableEntities, $builder) {
                $form = $event->getForm();
                $group = $event->getData();

                // stop the function if entity is not set
                if (null === $group->getEntity()) {
                    return;
                }

                $optionBuilder = null;

                if (\count($customizableEntities[$group->getEntity()]['options']) > 0) {
                    $optionBuilder = $builder
                        ->getFormFactory()
                        ->createBuilderForProperty(CustomFieldsGroup::class, 'options')
                        ->create(
                            'options',
                            null,
                            [
                                'compound' => true,
                                'auto_initialize' => false,
                                'required' => false,
                            ]
                        );

                    foreach ($customizableEntities[$group->getEntity()]['options'] as $key => $option) {
                        $optionBuilder->add($key, $option['form_type'], $option['form_options']);
                    }
                }

                if ((null !== $optionBuilder) && $optionBuilder->count() > 0) {
                    $form->add($optionBuilder->getForm());
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomFieldsGroup::class,
        ]);
    }
}
