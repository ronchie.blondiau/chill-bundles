<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * This extension create the possibility to add some text
 * after the input.
 *
 * This can be used to print the units of the field, or some text.
 *
 * This class must be extended by Extension class specifics to each input.
 */
abstract class PostTextExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (\array_key_exists('post_text', $options)) {
            // set the post text variable to the view
            $view->vars['post_text'] = $options['post_text'];
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['post_text']);
    }
}
