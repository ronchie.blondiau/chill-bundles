<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CustomFieldsBundle\DataFixtures\ORM;

use Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice\Option;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

error_reporting(0);
/**
 * Load some Options.
 */
class LoadOption extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var \Faker\Generator
     */
    public $fakerEn;

    /**
     * @var \Faker\Generator
     */
    public $fakerFr;

    /**
     * @var \Faker\Generator
     */
    public $fakerNl;

    private int $counter = 0;

    public function __construct()
    {
        $this->fakerFr = \Faker\Factory::create('fr_FR');
        $this->fakerEn = \Faker\Factory::create('en_EN');
        $this->fakerNl = \Faker\Factory::create('nl_NL');
    }

    public function getOrder()
    {
        return 1000;
    }

    public function load(ObjectManager $manager)
    {
        echo "Loading Options \n";

        // load companies
        $this->loadingCompanies($manager);
        $this->loadingWords($manager);

        $manager->flush();
    }

    /**
     * @return Option
     */
    private function createChildOption(Option $parent, array $text)
    {
        ++$this->counter;

        return (new Option())
            ->setText($text)
            ->setParent($parent)
            ->setActive(true)
            ->setInternalKey($parent->getKey().'-'.$this->counter);
    }

    private function loadingCompanies(ObjectManager $manager)
    {
        echo "Loading companies \n";
        $companiesParents = [
            [
                'fr' => 'Grandes Entreprises',
                'nl' => 'Grotes Bedrijven',
                'en' => 'Big Companies',
            ],
            [
                'fr' => 'Moyennes Entreprises',
                'nl' => 'Middelbare Bedrijven',
                'en' => 'Middle Companies',
            ],
            [
                'fr' => 'Petites Entreprises',
                'nl' => 'Kleine Bedrijven',
                'en' => 'Little Companies',
            ],
        ];

        foreach ($companiesParents as $text) {
            $parent = (new Option())
                ->setText($text)
                ->setKey('company');
            $manager->persist($parent);

            // Load children
            $expected_nb_children = random_int(10, 50);

            for ($i = 0; $i < $expected_nb_children; ++$i) {
                $companyName = $this->fakerFr->company;

                $manager->persist(
                    $this->createChildOption($parent, [
                        'fr' => $companyName,
                        'nl' => $companyName,
                        'en' => $companyName,
                    ])
                );
            }
        }
    }

    private function loadingWords(ObjectManager $manager)
    {
        echo "Loading some words...\n";

        $parents = [
            [
                'fr' => 'Categorie 1',
                'nl' => 'Categorie 1',
                'en' => 'Category 1',
            ],
            [
                'fr' => 'Categorie 2',
                'nl' => 'Categorie 2',
                'en' => 'Category 2',
            ],
        ];

        foreach ($parents as $text) {
            $parent = (new Option())
                ->setText($text)
                ->setKey('word');
            $manager->persist($parent);

            // Load children
            $expected_nb_children = random_int(10, 50);

            for ($i = 0; $i < $expected_nb_children; ++$i) {
                $manager->persist($this->createChildOption($parent, [
                    'fr' => $this->fakerFr->word,
                    'nl' => $this->fakerNl->word,
                    'en' => $this->fakerEn->word,
                ]));
            }
        }
    }
}
