<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Event;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a moderator field.
 */
final class Version20190115140042 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add a moderator field to events';
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event DROP CONSTRAINT FK_FA320FC8D0AFA354');
        $this->addSql('DROP INDEX IDX_FA320FC8D0AFA354');
        $this->addSql('ALTER TABLE chill_event_event DROP moderator_id');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event ADD moderator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_event_event ADD CONSTRAINT FK_FA320FC8D0AFA354 FOREIGN KEY (moderator_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FA320FC8D0AFA354 ON chill_event_event (moderator_id)');
    }
}
