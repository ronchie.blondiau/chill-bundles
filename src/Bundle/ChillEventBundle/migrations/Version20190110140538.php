<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Event;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Switch event date to datetime.
 */
final class Version20190110140538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'switch event date to datetime';
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event ALTER date TYPE DATE');
        $this->addSql('ALTER TABLE chill_event_event ALTER date DROP DEFAULT');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_event_event ALTER date TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_event_event ALTER date DROP DEFAULT');
    }
}
