<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Menu;

use Chill\EventBundle\Security\Authorization\EventVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var \Chill\PersonBundle\Entity\Person $person */
        $person = $parameters['person'] ?? null;

        if ($this->authorizationChecker->isGranted(EventVoter::SEE, $person)) {
            $menu->addChild($this->translator->trans('Events participation'), [
                'route' => 'chill_event__list_by_person',
                'routeParameters' => [
                    'person_id' => $person->getId(),
                ],
            ])
                ->setExtras([
                    'order' => 500,
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
