<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Events', [
            'route' => 'chill_event_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 6500,
            ]);

        $menu->addChild('Event type', [
            'route' => 'chill_eventtype_admin',
        ])->setExtras(['order' => 6510]);

        $menu->addChild('Event status', [
            'route' => 'chill_event_admin_status',
        ])->setExtras(['order' => 6520]);

        $menu->addChild('Role', [
            'route' => 'chill_event_admin_role',
        ])->setExtras(['order' => 6530]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_event'];
    }
}
