<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class EventType.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_event_event_type")
 *
 * @ORM\HasLifecycleCallbacks
 */
class EventType
{
    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $name;

    /**
     * @var Collection<Role>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\EventBundle\Entity\Role",
     * mappedBy="type")
     */
    private Collection $roles;

    /**
     * @var Collection<Status>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\EventBundle\Entity\Status",
     * mappedBy="type")
     */
    private Collection $statuses;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->statuses = new ArrayCollection();
    }

    /**
     * Add role.
     *
     * @return EventType
     */
    public function addRole(Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Add status.
     *
     * @return EventType
     */
    public function addStatus(Status $status)
    {
        $this->statuses[] = $status;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get label.
     *
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get statuses.
     *
     * @return Collection
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * Remove role.
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Remove status.
     */
    public function removeStatus(Status $status)
    {
        $this->statuses->removeElement($status);
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return EventType
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set label.
     *
     * @param array $label
     *
     * @return EventType
     */
    public function setName($label)
    {
        $this->name = $label;

        return $this;
    }
}
