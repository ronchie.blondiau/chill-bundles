<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * Controller for the event configuration section (in admin section).
 */
class AdminController extends AbstractController
{
    /**
     * Event admin.
     *
     * @Route("/{_locale}/admin/event", name="chill_event_admin_index")
     */
    public function indexAdminAction()
    {
        return $this->render('@ChillEvent/Admin/index.html.twig');
    }
}
