<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Form\Type;

use Chill\EventBundle\Entity\EventType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of TranslatableEventType.
 */
class PickEventTypeType extends AbstractType
{
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(TranslatableStringHelper $helper)
    {
        $this->translatableStringHelper = $helper;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $helper = $this->translatableStringHelper;
        $resolver->setDefaults(
            [
                'class' => EventType::class,
                'query_builder' => static fn (EntityRepository $er) => $er->createQueryBuilder('et')
                    ->where('et.active = true'),
                'choice_label' => static fn (EventType $t) => $helper->localize($t->getName()),
                'choice_attrs' => static fn (EventType $t) => ['data-link-category' => $t->getId()],
            ]
        );
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
