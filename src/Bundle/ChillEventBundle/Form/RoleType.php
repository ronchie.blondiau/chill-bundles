<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\Form;

use Chill\EventBundle\Entity\EventType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

final class RoleType extends AbstractType
{
    public function __construct(private readonly TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TranslatableStringFormType::class)
            ->add('active')
            ->add('type', EntityType::class, [
                'class' => EventType::class,
                'choice_label' => fn (EventType $e) => $this->translatableStringHelper->localize($e->getName()),
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_eventbundle_role';
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \Chill\EventBundle\Entity\Role::class,
        ]);
    }
}
