<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\EventBundle\DataFixtures\ORM;

use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Entity\Participation;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Chill\MainBundle\Entity\Center;
use Chill\PersonBundle\Entity\Person;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load Events and Participation.
 */
class LoadParticipation extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_FR');
    }

    public function createEvents(Center $center, ObjectManager $manager)
    {
        $expectedNumber = 20;
        $events = [];

        for ($i = 0; $i < $expectedNumber; ++$i) {
            $event = (new Event())
                ->setDate($this->faker->dateTimeBetween('-2 years', '+6 months'))
                ->setName($this->faker->words(random_int(2, 4), true))
                ->setType($this->getReference(LoadEventTypes::$refs[array_rand(LoadEventTypes::$refs)]))
                ->setCenter($center)
                ->setCircle(
                    $this->getReference(
                        LoadScopes::$references[array_rand(LoadScopes::$references)]
                    )
                );
            $manager->persist($event);
            $events[] = $event;
        }

        return $events;
    }

    public function getOrder()
    {
        return 30010;
    }

    public function load(ObjectManager $manager)
    {
        $centers = $manager->getRepository(Center::class)
            ->findAll();

        foreach ($centers as $center) {
            $people = $manager->getRepository(Person::class)
                ->findBy(['center' => $center]);
            $events = $this->createEvents($center, $manager);

            /** @var Person $person */
            foreach ($people as $person) {
                $nb = random_int(0, 3);

                for ($i = 0; $i < $nb; ++$i) {
                    $event = $events[array_rand($events)];
                    $role = $event->getType()->getRoles()->get(
                        array_rand($event->getType()->getRoles()->toArray())
                    );
                    $status = $event->getType()->getStatuses()->get(
                        array_rand($event->getType()->getStatuses()->toArray())
                    );

                    $participation = (new Participation())
                        ->setPerson($person)
                        ->setRole($role)
                        ->setStatus($status)
                        ->setEvent($event);
                    $manager->persist($participation);
                }
            }
        }

        $manager->flush();
    }
}
