<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\DataFixtures\ORM;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Service\DocGenerator\AccompanyingPeriodContext;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Load DocGeneratorTemplate.
 */
class LoadDocGeneratorTemplate extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $templates = [
            [
                'name' => ['fr' => 'FORMULAIRE AEB'],
                'desc' => 'stocké sur openstack comedienbe',
                'file' => [
                    'filename' => 'pKNlhCrQDCRsAuC8vYHDKa',
                    'key' => '{"alg":"A256CBC","ext":true,"k":"_VihnD41-VDHlpS-ouwtbMPnu-OXVdtA7ENQWWtAQYM","key_ops":["encrypt","decrypt"],"kty":"oct"}',
                    'iv' => '[86,231,83,148,117,107,149,173,130,19,105,194,224,145,8,48]',
                    'type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                ],
                'context' => AccompanyingPeriodContext::class,
                'entity' => AccompanyingPeriod::class,
                'options' => ['mainPerson' => false, 'person1' => false, 'person2' => false],
            ], [
                'name' => ['fr' => 'AIDE ALIMENTAIRE'],
                'desc' => 'stocké sur openstack comedienbe',
                'file' => [
                    'filename' => 'pKNlhCrQDCRsAuC8vYHDKa',
                    'key' => '{"alg":"A256CBC","ext":true,"k":"_VihnD41-VDHlpS-ouwtbMPnu-OXVdtA7ENQWWtAQYM","key_ops":["encrypt","decrypt"],"kty":"oct"}',
                    'iv' => '[86,231,83,148,117,107,149,173,130,19,105,194,224,145,8,48]',
                    'type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                ],
                'context' => AccompanyingPeriodContext::class,
                'entity' => AccompanyingPeriod::class,
                'options' => ['mainPerson' => false, 'person1' => false, 'person2' => false],
            ],
        ];

        foreach ($templates as $template) {
            $newStoredObj = (new StoredObject())
                ->setFilename($template['file']['filename'])
                ->setKeyInfos(json_decode($template['file']['key'], true))
                ->setIv(json_decode($template['file']['iv'], true))
                ->setCreatedAt(new \DateTime('today'))
                ->setType($template['file']['type']);

            $manager->persist($newStoredObj);

            $newTemplate = (new DocGeneratorTemplate())
                ->setName($template['name'])
                ->setDescription($template['desc'])
                ->setFile($newStoredObj)
                ->setContext($template['context'])
                ->setEntity($template['entity'])
                ->setOptions($template['options']);

            $manager->persist($newTemplate);
        }
        $manager->flush();
    }
}
