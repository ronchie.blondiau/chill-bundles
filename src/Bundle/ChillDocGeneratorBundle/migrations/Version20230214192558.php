<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230214192558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add status, template_id and fix defaults on chill_doc.stored_object';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD template_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD status TEXT DEFAULT \'ready\' NOT NULL');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('UPDATE chill_doc.stored_object SET createdAt = creation_date');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD createdBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP creation_date;');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER type SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER title DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_doc.stored_object.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD CONSTRAINT FK_49604E365DA0FB8 FOREIGN KEY (template_id) REFERENCES chill_docgen_template (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD CONSTRAINT FK_49604E363174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_49604E365DA0FB8 ON chill_doc.stored_object (template_id)');
        $this->addSql('CREATE INDEX IDX_49604E363174800F ON chill_doc.stored_object (createdBy_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP CONSTRAINT FK_49604E365DA0FB8');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP CONSTRAINT FK_49604E363174800F');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP template_id');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP status');
        $this->addSql('ALTER TABLE chill_doc.stored_object ADD creation_date TIMESTAMP(0) DEFAULT NOW()');
        $this->addSql('UPDATE chill_doc.stored_object SET creation_date = createdAt');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP createdAt');
        $this->addSql('ALTER TABLE chill_doc.stored_object DROP createdBy_id');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER title SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_doc.stored_object ALTER type DROP DEFAULT');
    }
}
