<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\DocGenerator;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Creation of table for storing DocGenTemplate.
 */
final class Version20210805162522 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_docgen_template_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_docgen_template');
    }

    public function getDescription(): string
    {
        return 'Creation of table for storing DocGenTemplate';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_docgen_template_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_docgen_template (id INT NOT NULL, name JSON NOT NULL, description TEXT DEFAULT NULL, file VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
    }
}
