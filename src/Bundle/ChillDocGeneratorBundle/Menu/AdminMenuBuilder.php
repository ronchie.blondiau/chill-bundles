<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private readonly TranslatorInterface $translator, private readonly Security $security)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $menu->addChild($this->translator->trans('docgen.Document generation'), [
                'route' => 'chill_crud_docgen_template_index',
            ])->setExtras([
                'order' => 4020,
                'explain' => 'docgen.Manage templates and document generation',
            ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_docstore'];
    }
}
