<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * @template T of object
 *
 * @extends DocGeneratorContextInterface<T>
 */
interface DocGeneratorContextWithAdminFormInterface extends DocGeneratorContextInterface
{
    public function adminFormReverseTransform(array $data): array;

    public function adminFormTransform(array $data): array;

    public function buildAdminForm(FormBuilderInterface $builder): void;

    public function hasAdminForm(): bool;
}
