<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Context;

use Chill\DocGeneratorBundle\Entity\DocGeneratorTemplate;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @template T of object
 *
 * @extends DocGeneratorContextInterface<T>
 */
interface DocGeneratorContextWithPublicFormInterface extends DocGeneratorContextInterface
{
    /**
     * Generate the form that display.
     *
     * @param T $entity
     */
    public function buildPublicForm(FormBuilderInterface $builder, DocGeneratorTemplate $template, mixed $entity): void;

    /**
     * Fill the form with initial data.
     *
     * @param T $entity
     */
    public function getFormData(DocGeneratorTemplate $template, mixed $entity): array;

    /**
     * has form.
     *
     * @param T $entity
     */
    public function hasPublicForm(DocGeneratorTemplate $template, mixed $entity): bool;

    /**
     * Transform the data from the form into serializable data, storable into messenger's message.
     *
     * @param T $entity
     */
    public function contextGenerationDataNormalize(DocGeneratorTemplate $template, mixed $entity, array $data): array;

    /**
     * Reverse the data from the messenger's message into data usable for doc's generation.
     *
     * @param T $entity
     */
    public function contextGenerationDataDenormalize(DocGeneratorTemplate $template, mixed $entity, array $data): array;
}
