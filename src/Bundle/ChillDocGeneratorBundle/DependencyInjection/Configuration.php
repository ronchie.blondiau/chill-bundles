<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_doc_generator');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->arrayNode('driver')
            ->addDefaultsIfNotSet()
            ->children()
            ->enumNode('type')
            ->isRequired()
            ->values(['relatorio'])
            ->defaultValue('relatorio')
            ->end()
            ->arrayNode('relatorio')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('url')
            ->isRequired()
            ->defaultValue('http://relatorio:8888/')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
