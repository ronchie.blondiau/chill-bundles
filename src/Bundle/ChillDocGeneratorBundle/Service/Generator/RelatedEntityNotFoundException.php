<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\DocGeneratorBundle\Service\Generator;

class RelatedEntityNotFoundException extends \RuntimeException
{
    public function __construct(string $relatedEntityClass, int $relatedEntityId, ?\Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Related entity not found: %s, %s', $relatedEntityClass, $relatedEntityId),
            99_876_652,
            $previous
        );
    }
}
