<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;

final readonly class AdminMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private Security $security)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        // all the entries below must have ROLE_ADMIN permissions
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Budget', [
            'route' => 'chill_admin_budget',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 7050,
                'explain' => 'Budget resource and charge type configuration',
            ]);
        $menu
            ->addChild('admin.menu.Resource types', [
                'route' => 'chill_crud_resource_kind_index',
            ])
            ->setExtras([
                'order' => 7060,
            ]);
        $menu
            ->addChild('admin.menu.Charge types', [
                'route' => 'chill_crud_charge_kind_index',
            ])
            ->setExtras([
                'order' => 7070,
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_budget'];
    }
}
