<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Menu;

use Chill\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Entity\Person;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(protected AuthorizationCheckerInterface $authorizationChecker, protected TranslatorInterface $translator)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /** @var Person $person */
        $person = $parameters['person'];

        if ($this->authorizationChecker->isGranted(BudgetElementVoter::SEE, $person)) {
            $menu->addChild(
                $this->translator->trans('Budget'),
                [
                    'route' => 'chill_budget_elements_index',
                    'routeParameters' => ['id' => $person->getId()],
                ]
            )
                ->setExtra('order', 59);
        }
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
