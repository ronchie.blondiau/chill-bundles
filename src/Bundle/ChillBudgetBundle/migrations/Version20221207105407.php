<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class Version20221207105407 extends AbstractMigration implements ContainerAwareInterface
{
    public ContainerInterface $container;

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM chill_budget.resource_type;');
        $this->addSql('DELETE FROM chill_budget.charge_type;');
    }

    public function getDescription(): string
    {
        return 'Use new budget admin entities';
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema): void
    {
        $resources = $this->container->getParameter('chill_budget.resources');
        $charges = $this->container->getParameter('chill_budget.charges');

        foreach ($resources as $value) {
            $lang = $value['labels'][0]['lang'];
            $label = $value['labels'][0]['label'];
            $kind = $value['key'];
            $this->addSql(
                'INSERT INTO chill_budget.resource_type (id, isActive, name, ordering, kind) VALUES (
                        nextval(\'chill_budget.resource_type_id_seq\'), true, jsonb_build_object(:lang::text, :label::text), 0, :kind::text)',
                ['lang' => $lang, 'label' => $label, 'kind' => $kind],
                ['lang' => Types::STRING, 'label' => Types::STRING, 'kind' => Types::STRING]
            );
            $this->addSql(
                'UPDATE chill_budget.resource SET resource_id = resource_type.id
                          FROM chill_budget.resource_type WHERE resource.type = :kind AND resource_type.kind = resource.type;',
                ['kind' => $kind],
                ['kind' => Types::STRING]
            );
        }

        foreach ($charges as $value) {
            $lang = $value['labels'][0]['lang'];
            $label = $value['labels'][0]['label'];
            $kind = $value['key'];
            $this->addSql(
                'INSERT INTO chill_budget.charge_type VALUES (nextval(\'chill_budget.charge_type_id_seq\'), true,
                                             jsonb_build_object(:lang::text, :label::text), 0, :kind::text);',
                ['lang' => $lang, 'label' => $label, 'kind' => $kind],
                ['lang' => Types::STRING, 'label' => Types::STRING, 'kind' => Types::STRING]
            );
            $this->addSql(
                'UPDATE chill_budget.charge SET charge_id = charge_type.id
                          FROM chill_budget.charge_type WHERE charge.type = :kind AND charge_type.kind = charge.type;',
                ['kind' => $kind],
                ['kind' => Types::STRING]
            );
        }
    }
}
