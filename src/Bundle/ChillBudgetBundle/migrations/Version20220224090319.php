<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220224090319 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge DROP CONSTRAINT FK_5C99D2C3E79FF843');
        $this->addSql('ALTER TABLE chill_budget.charge DROP household_id');
        $this->addSql('ALTER TABLE chill_budget.resource DROP CONSTRAINT FK_5E0A5E97E79FF843');
        $this->addSql('ALTER TABLE chill_budget.resource DROP household_id');
    }

    public function getDescription(): string
    {
        return 'Add household to budget AbstractElement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge ADD household_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_budget.charge ADD CONSTRAINT FK_5C99D2C3E79FF843 FOREIGN KEY (household_id) REFERENCES chill_person_household (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5C99D2C3E79FF843 ON chill_budget.charge (household_id)');
        $this->addSql('ALTER TABLE chill_budget.resource ADD household_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_budget.resource ADD CONSTRAINT FK_5E0A5E97E79FF843 FOREIGN KEY (household_id) REFERENCES chill_person_household (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5E0A5E97E79FF843 ON chill_budget.resource (household_id)');
    }
}
