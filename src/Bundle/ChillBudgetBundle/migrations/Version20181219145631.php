<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * autorise les valeurs nulles dans "HELP".
 */
final class Version20181219145631 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge ALTER help SET NOT NULL');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_budget.charge ALTER help DROP NOT NULL');
    }
}
