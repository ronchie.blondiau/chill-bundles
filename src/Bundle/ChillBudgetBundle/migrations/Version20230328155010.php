<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230328155010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'budget elements: restore the previous type to resource/charge kind if applicable';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
            WITH type_to_id AS (
                SELECT DISTINCT charge.charge_id AS id, charge.type
                FROM chill_budget.charge
                WHERE type <> ''
            )
            UPDATE chill_budget.charge_type
                SET kind = type_to_id.type
                FROM type_to_id
                WHERE type_to_id.type <> '' AND type_to_id.id = charge_type.id
        SQL);

        $this->addSql(<<<'SQL'
            WITH type_to_id AS (
                SELECT DISTINCT resource.resource_id AS id, resource.type
                FROM chill_budget. resource
                WHERE type <> ''
            )
            UPDATE chill_budget.resource_type
                SET kind = type_to_id.type
                FROM type_to_id
                WHERE type_to_id.type <> '' AND type_to_id.id = resource_type.id
        SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE chill_budget.resource_type SET kind=md5(random()::text) WHERE kind = ''");
        $this->addSql("UPDATE chill_budget.charge_type SET kind=md5(random()::text) WHERE kind = ''");
    }
}
