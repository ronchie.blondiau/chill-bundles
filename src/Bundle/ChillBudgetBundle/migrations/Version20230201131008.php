<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Budget;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230201131008 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this shouldn't be undone.
    }

    public function getDescription(): string
    {
        return 'Fix the comment on tags column in resource and charge type';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN chill_budget.resource_type.tags IS \'(DC2Type:json)\'');
        $this->addSql('COMMENT ON COLUMN chill_budget.charge_type.tags IS \'(DC2Type:json)\'');
    }
}
