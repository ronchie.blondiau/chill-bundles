<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Entity;

use Chill\MainBundle\Entity\HasCentersInterface;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * Resource.
 *
 * @ORM\Table(name="chill_budget.resource")
 *
 * @ORM\Entity(repositoryClass="Chill\BudgetBundle\Repository\ResourceRepository")
 */
class Resource extends AbstractElement implements HasCentersInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=ResourceKind::class, inversedBy="AbstractElement")
     *
     * @ORM\JoinColumn
     */
    private ?ResourceKind $resource = null;

    public function __construct()
    {
        $this->setStartDate(new \DateTimeImmutable('today'));
    }

    public function getCenters(): array
    {
        if (null !== $this->getPerson()) {
            return [$this->getPerson()->getCenter()];
        }

        return $this->getHousehold()->getCurrentPersons()->map(static fn (Person $p) => $p->getCenter())->toArray();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getResource(): ?ResourceKind
    {
        return $this->resource;
    }

    public function isCharge(): bool
    {
        return false;
    }

    public function isResource(): bool
    {
        return true;
    }

    public function setResource(?ResourceKind $resource): self
    {
        $this->resource = $resource;

        return $this;
    }
}
