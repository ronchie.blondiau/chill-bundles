<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Calculator;

class CalculatorResult
{
    final public const TYPE_CURRENCY = 'currency';

    final public const TYPE_PERCENTAGE = 'percentage';

    final public const TYPE_RATE = 'rate';

    public $label;

    public float $result;

    public $type;
}
