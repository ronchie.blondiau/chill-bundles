<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\Repository;

use Chill\BudgetBundle\Entity\ResourceKind;
use Doctrine\Persistence\ObjectRepository;

interface ResourceKindRepositoryInterface extends ObjectRepository
{
    public function find($id): ?ResourceKind;

    /**
     * @return list<ResourceKind>
     */
    public function findAll(): array;

    /**
     * @return list<ResourceKind>
     */
    public function findAllActive(): array;

    /**
     * @return list<ResourceKind>
     */
    public function findAllByType(string $type): array;

    /**
     * @return list<ResourceKind>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    public function findOneBy(array $criteria): ?ResourceKind;

    public function findOneByKind(string $kind): ?ResourceKind;

    public function getClassName(): string;
}
