<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\BudgetBundle\DependencyInjection;

use Chill\BudgetBundle\Controller\Admin\ChargeKindController;
use Chill\BudgetBundle\Controller\Admin\ResourceKindController;
use Chill\BudgetBundle\Entity\ChargeKind;
use Chill\BudgetBundle\Entity\ResourceKind;
use Chill\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillBudgetExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services/form.yaml');
        $loader->load('services/repository.yaml');
        $loader->load('services/security.yaml');
        $loader->load('services/controller.yaml');
        $loader->load('services/templating.yaml');
        $loader->load('services/menu.yaml');
        $loader->load('services/calculator.yaml');
        $loader->load('services/services.yaml');

        $this->storeConfig('resources', $config, $container);
        $this->storeConfig('charges', $config, $container);
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoutes($container);
        $this->prependCruds($container);
    }

    /** (non-PHPdoc).
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prependRoutes(ContainerBuilder $container)
    {
        // add routes for custom bundle
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillBudgetBundle/config/routing.yaml',
                ],
            ],
        ]);
    }

    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                BudgetElementVoter::UPDATE => [BudgetElementVoter::SEE],
                BudgetElementVoter::CREATE => [BudgetElementVoter::SEE],
            ],
        ]);
    }

    protected function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => ChargeKind::class,
                    'name' => 'charge_kind',
                    'base_path' => '/admin/budget/charge-kind',
                    'form_class' => \Chill\BudgetBundle\Form\Admin\ChargeKindType::class,
                    'controller' => ChargeKindController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Charge/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Charge/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Charge/edit.html.twig',
                        ],
                    ],
                ],
                [
                    'class' => ResourceKind::class,
                    'name' => 'resource_kind',
                    'base_path' => '/admin/budget/resource-kind',
                    'form_class' => \Chill\BudgetBundle\Form\Admin\ResourceKindType::class,
                    'controller' => ResourceKindController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Resource/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Resource/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillBudget/Admin/Resource/edit.html.twig',
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function storeConfig($position, array $config, ContainerBuilder $container)
    {
        $container
            ->setParameter(sprintf('chill_budget.%s', $position), $config[$position]);
    }
}
