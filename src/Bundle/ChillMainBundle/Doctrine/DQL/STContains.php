<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;

/**
 * Geometry function 'ST_CONTAINS', added by postgis.
 */
class STContains extends FunctionNode
{
    private ?\Doctrine\ORM\Query\AST\Node $firstPart = null;

    private ?\Doctrine\ORM\Query\AST\Node $secondPart = null;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'ST_CONTAINS('.$this->firstPart->dispatch($sqlWalker).
            ', '.$this->secondPart->dispatch($sqlWalker).')';
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);

        $this->firstPart = $parser->StringPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);

        $this->secondPart = $parser->StringPrimary();

        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
