<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Usage : TO_CHAR(datetime, fmt).
 */
class ToChar extends FunctionNode
{
    private ?\Doctrine\ORM\Query\AST\ArithmeticExpression $datetime = null;

    private \Doctrine\ORM\Query\AST\Node|string|null $fmt = null;

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            'TO_CHAR(%s, %s)',
            $sqlWalker->walkArithmeticPrimary($this->datetime),
            $sqlWalker->walkArithmeticPrimary($this->fmt)
        );
    }

    public function parse(Parser $parser)
    {
        $parser->match(\Doctrine\ORM\Query\TokenType::T_IDENTIFIER);
        $parser->match(\Doctrine\ORM\Query\TokenType::T_OPEN_PARENTHESIS);
        $this->datetime = $parser->ArithmeticExpression();
        $parser->match(\Doctrine\ORM\Query\TokenType::T_COMMA);
        $this->fmt = $parser->StringExpression();
        $parser->match(\Doctrine\ORM\Query\TokenType::T_CLOSE_PARENTHESIS);
    }
}
