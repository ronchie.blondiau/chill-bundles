<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Doctrine\Migrations;

use Doctrine\Migrations\Version\Comparator;
use Doctrine\Migrations\Version\Version;

/**
 * Compare Version classes names from different namespaces and order them.
 *
 * Version are compared on the date's class.
 */
final class VersionComparator implements Comparator
{
    public function compare(Version $a, Version $b): int
    {
        return \strcmp($this->getNumber($a), $this->getNumber($b));
    }

    private function getNumber(Version $version): string
    {
        $names = \explode('\\', (string) $version);

        return end($names);
    }
}
