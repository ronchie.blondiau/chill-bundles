<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="chill_main_civility")
 *
 * @ORM\Entity
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={"chill_main_civility": Civility::class})
 */
class Civility
{
    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $abbreviation = [];

    /**
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({"read"})
     */
    private bool $active = true;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $name = [];

    /**
     * @ORM\Column(type="float", name="ordering", nullable=true, options={"default": 0.0})
     */
    private float $order = 0;

    public function getAbbreviation(): array
    {
        return $this->abbreviation;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function getOrder(): ?float
    {
        return $this->order;
    }

    public function setAbbreviation(array $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setOrder(float $order): self
    {
        $this->order = $order;

        return $this;
    }
}
