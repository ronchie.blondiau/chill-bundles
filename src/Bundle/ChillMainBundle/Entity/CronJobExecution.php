<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_main_cronjob_execution")
 */
class CronJobExecution
{
    final public const FAILURE = 100;

    final public const SUCCESS = 1;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $lastEnd = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private \DateTimeImmutable $lastStart;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": null})
     */
    private ?int $lastStatus = null;

    /**
     * @ORM\Column(type="json", options={"default": "'{}'::jsonb", "jsonb": true})
     */
    private array $lastExecutionData = [];

    public function __construct(/**
     * @ORM\Column(type="text", nullable=false)
     *
     * @ORM\Id
     */
        private string $key
    ) {
        $this->lastStart = new \DateTimeImmutable('now');
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getLastEnd(): \DateTimeImmutable
    {
        return $this->lastEnd;
    }

    public function getLastStart(): \DateTimeImmutable
    {
        return $this->lastStart;
    }

    public function getLastStatus(): ?int
    {
        return $this->lastStatus;
    }

    public function setLastEnd(?\DateTimeImmutable $lastEnd): CronJobExecution
    {
        $this->lastEnd = $lastEnd;

        return $this;
    }

    public function setLastStart(\DateTimeImmutable $lastStart): CronJobExecution
    {
        $this->lastStart = $lastStart;

        return $this;
    }

    public function setLastStatus(?int $lastStatus): CronJobExecution
    {
        $this->lastStatus = $lastStatus;

        return $this;
    }

    public function getLastExecutionData(): array
    {
        return $this->lastExecutionData;
    }

    public function setLastExecutionData(array $lastExecutionData): CronJobExecution
    {
        $this->lastExecutionData = $lastExecutionData;

        return $this;
    }
}
