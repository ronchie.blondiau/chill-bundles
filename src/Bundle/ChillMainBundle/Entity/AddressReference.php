<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Chill\MainBundle\Doctrine\Model\Point;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_main_address_reference", indexes={
 *
 *     @ORM\Index(name="address_refid", columns={"refId"})
 * },
 * uniqueConstraints={
 *
 *     @ORM\UniqueConstraint(name="chill_main_address_reference_unicity", columns={"refId", "source"})
 * })
 *
 * @ORM\HasLifecycleCallbacks
 */
class AddressReference
{
    /**
     * This is an internal column which is populated by database.
     *
     * This column will ease the search operations
     *
     * @ORM\Column(type="text", options={"default": ""})
     */
    private string $addressCanonical = '';

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @groups({"read"})
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @groups({"read"})
     */
    private ?\DateTimeImmutable $deletedAt = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @groups({"read"})
     */
    private string $municipalityCode = '';

    /**
     * A geospatial field storing the coordinates of the Address.
     *
     * @ORM\Column(type="point")
     *
     * @groups({"read"})
     */
    private ?Point $point = null;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\PostalCode")
     *
     * @groups({"read"})
     */
    private ?PostalCode $postcode = null;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @groups({"read"})
     */
    private string $refId = '';

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @groups({"read"})
     */
    private string $source = '';

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @groups({"read"})
     */
    private string $street = '';

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @groups({"read"})
     */
    private string $streetNumber = '';

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @groups({"read"})
     */
    private ?\DateTimeImmutable $updatedAt = null;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMunicipalityCode(): string
    {
        return $this->municipalityCode;
    }

    public function getPoint(): ?Point
    {
        return $this->point;
    }

    /**
     * Get postcode.
     */
    public function getPostcode(): ?PostalCode
    {
        return $this->postcode;
    }

    public function getRefId(): string
    {
        return $this->refId;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function hasPoint(): bool
    {
        return null !== $this->getPoint();
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function setMunicipalityCode(?string $municipalityCode): self
    {
        $this->municipalityCode = (string) $municipalityCode;

        return $this;
    }

    public function setPoint(?Point $point): self
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Set postcode.
     *
     * @return Address
     */
    public function setPostcode(?PostalCode $postcode = null)
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function setRefId(string $refId): self
    {
        $this->refId = $refId;

        return $this;
    }

    public function setSource(?string $source): self
    {
        $this->source = (string) $source;

        return $this;
    }

    public function setStreet(?string $street): self
    {
        $this->street = (string) $street;

        return $this;
    }

    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = (string) $streetNumber;

        return $this;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
