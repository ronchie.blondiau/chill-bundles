<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="chill_main_geographical_unit_layer", uniqueConstraints={
 *
 *     @ORM\UniqueConstraint(name="geographical_unit_layer_refid", columns={"refId"})
 * })
 *
 * @ORM\Entity
 */
class GeographicalUnitLayer
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json", nullable=false, options={"default": "[]"})
     *
     * @Serializer\Groups({"read"})
     */
    private array $name = [];

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @Serializer\Groups({"read"})
     */
    private string $refId = '';

    /**
     * @var Collection<GeographicalUnit>
     *
     * @ORM\OneToMany(targetEntity=GeographicalUnit::class, mappedBy="layer")
     */
    private Collection $units;

    public function __construct()
    {
        $this->units = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): array
    {
        return $this->name;
    }

    public function getRefId(): string
    {
        return $this->refId;
    }

    public function getUnits(): Collection
    {
        return $this->units;
    }

    public function setName(array $name): GeographicalUnitLayer
    {
        $this->name = $name;

        return $this;
    }
}
