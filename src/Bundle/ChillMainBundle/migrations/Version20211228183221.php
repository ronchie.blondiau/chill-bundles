<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211228183221 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('create unique index uniq_5bdc8067567988b4440f6072
            on chill_main_notification (relatedentityclass, relatedentityid)');
    }

    public function getDescription(): string
    {
        return 'remove unique index which prevent to notify twice an entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_5bdc8067567988b4440f6072');
    }
}
