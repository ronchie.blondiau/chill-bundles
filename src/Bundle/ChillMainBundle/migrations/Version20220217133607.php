<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Rename residential_address table.
 */
final class Version20220217133607 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE IF EXISTS chill_person_residential_address RENAME TO chill_main_residential_address;');
        $this->addSql('ALTER SEQUENCE IF EXISTS chill_person_residential_address_id_seq RENAME TO chill_main_residential_address_id_seq;');
    }

    public function getDescription(): string
    {
        return 'Rename residential_address table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE IF EXISTS chill_main_residential_address RENAME TO chill_person_residential_address;');
        $this->addSql('ALTER SEQUENCE IF EXISTS chill_main_residential_address_id_seq RENAME TO chill_person_residential_address_id_seq;');
    }
}
