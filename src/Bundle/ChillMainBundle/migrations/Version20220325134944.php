<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add 3 fields on AddressReference.
 */
final class Version20220325134944 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address_reference DROP createdAt');
        $this->addSql('ALTER TABLE chill_main_address_reference DROP deletedAt');
        $this->addSql('ALTER TABLE chill_main_address_reference DROP updatedAt');
        $this->addSql('DROP INDEX address_refid');
        $this->addSql('create index address_refid
                on chill_main_address_reference (refid)
                where ((refid)::text <> \'\'::text)');
    }

    public function getDescription(): string
    {
        return 'Add 3 fields on AddressReference';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address_reference ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ADD deletedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_main_address_reference.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_address_reference.deletedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_address_reference.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('DROP INDEX address_refid');
        $this->addSql('CREATE INDEX address_refid ON chill_main_address_reference (refId)');
    }
}
