<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add defaultFor to LocationType.
 */
final class Version20211123093355 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_A459B5CADD3E4105');
        $this->addSql('ALTER TABLE chill_main_location_type DROP defaultFor');
    }

    public function getDescription(): string
    {
        return 'Add defaultFor to LocationType';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_location_type ADD defaultFor VARCHAR(32) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A459B5CADD3E4105 ON chill_main_location_type (defaultFor)');
    }
}
