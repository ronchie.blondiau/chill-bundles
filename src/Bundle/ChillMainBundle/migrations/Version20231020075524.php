<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add phonenumber to user profile.
 */
final class Version20231020075524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add phonenumber to user profile';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ADD phonenumber VARCHAR(35) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN users.phonenumber IS \'(DC2Type:phone_number)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users DROP phonenumber');
    }
}
