<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230711152947 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add last execution data to cronjon execution table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_cronjob_execution ADD lastExecutionData JSONB DEFAULT \'{}\'::jsonb NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_cronjob_execution DROP COLUMN lastExecutionData');
    }
}
