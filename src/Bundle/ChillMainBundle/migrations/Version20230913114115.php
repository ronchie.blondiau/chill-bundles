<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230913114115 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // drop job_history
        $this->addSql('ALTER TABLE chill_main_user_job_history DROP CONSTRAINT user_job_history_endate_null_or_after_startdate');
        $this->addSql('ALTER TABLE chill_main_user_job_history DROP CONSTRAINT user_job_history_not_overlaps');
        $this->addSql('DROP SEQUENCE chill_main_user_job_history_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_main_user_job_history DROP CONSTRAINT FK_4E3BF4DDBE04EA9');
        $this->addSql('ALTER TABLE chill_main_user_job_history DROP CONSTRAINT FK_4E3BF4DDA76ED395');
        $this->addSql('DROP TABLE chill_main_user_job_history');

        $this->addSql('ALTER TABLE users ADD userjob_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT fk_1483a5e964b65c5b FOREIGN KEY (userjob_id) REFERENCES chill_main_user_job (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1483a5e964b65c5b ON users (userjob_id)');

        // drop scope_history
        $this->addSql('ALTER TABLE chill_main_user_scope_history DROP CONSTRAINT user_scope_history_endate_null_or_after_startdate');
        $this->addSql('ALTER TABLE chill_main_user_scope_history DROP CONSTRAINT user_scope_history_not_overlaps');
        $this->addSql('DROP SEQUENCE chill_main_user_scope_history_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_main_user_scope_history DROP CONSTRAINT FK_48B969D7682B5931');
        $this->addSql('ALTER TABLE chill_main_user_scope_history DROP CONSTRAINT FK_48B969D7A76ED395');
        $this->addSql('DROP TABLE chill_main_user_scope_history');

        $this->addSql('ALTER TABLE users ADD mainscope_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT fk_1483a5e9115e73f3 FOREIGN KEY (mainscope_id) REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1483a5e9115e73f3 ON users (mainscope_id)');
    }

    public function getDescription(): string
    {
        return 'Add new entities UserScopeHistory and UserJobHistory';
    }

    public function up(Schema $schema): void
    {
        // create scope_history
        $this->addSql('CREATE SEQUENCE chill_main_user_scope_history_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_user_scope_history ('
            .'id INT NOT NULL,'
            .'scope_id INT DEFAULT NULL,'
            .'user_id INT DEFAULT NULL,'
            .'endDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,'
            .'startDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,'
            .'PRIMARY KEY(id))');

        $this->addSql('CREATE INDEX IDX_48B969D7682B5931 ON chill_main_user_scope_history (scope_id)');
        $this->addSql('CREATE INDEX IDX_48B969D7A76ED395 ON chill_main_user_scope_history (user_id)');

        $this->addSql('COMMENT ON COLUMN chill_main_user_scope_history.endDate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_user_scope_history.startDate IS \'(DC2Type:datetime_immutable)\'');

        $this->addSql('ALTER TABLE chill_main_user_scope_history ADD CONSTRAINT FK_48B969D7682B5931 '
            .'FOREIGN KEY (scope_id) REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE chill_main_user_scope_history ADD CONSTRAINT FK_48B969D7A76ED395 '
            .'FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE chill_main_user_scope_history '
            .'ADD CONSTRAINT user_scope_history_not_overlaps '
            .'EXCLUDE USING GIST (user_id with =, tsrange(startDate, endDate) with &&) '
            .'DEFERRABLE INITIALLY DEFERRED');

        $this->addSql('ALTER TABLE chill_main_user_scope_history '
            .'ADD CONSTRAINT user_scope_history_endate_null_or_after_startdate '
            .'CHECK (startDate <= endDate OR endDate IS NULL)');

        // insert user scope_history datas
        $this->addSql('INSERT INTO chill_main_user_scope_history (id, startDate, endDate, user_id, scope_id) '
            .'SELECT nextval(\'chill_main_user_scope_history_id_seq\'), \'1970-01-01\'::date, NULL, users.id, mainscope_id '
            .'FROM users');

        // remove mainscope
        $this->addSql('ALTER TABLE users DROP CONSTRAINT fk_1483a5e9115e73f3');
        $this->addSql('ALTER TABLE users DROP mainscope_id');

        // create job_history
        $this->addSql('CREATE SEQUENCE chill_main_user_job_history_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_user_job_history ('
            .'id INT NOT NULL,'
            .'job_id INT DEFAULT NULL,'
            .'user_id INT DEFAULT NULL,'
            .'endDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,'
            .'startDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,'
            .'PRIMARY KEY(id))');

        $this->addSql('CREATE INDEX IDX_4E3BF4DDBE04EA9 ON chill_main_user_job_history (job_id)');
        $this->addSql('CREATE INDEX IDX_4E3BF4DDA76ED395 ON chill_main_user_job_history (user_id)');

        $this->addSql('COMMENT ON COLUMN chill_main_user_job_history.endDate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_main_user_job_history.startDate IS \'(DC2Type:datetime_immutable)\'');

        $this->addSql('ALTER TABLE chill_main_user_job_history ADD CONSTRAINT FK_4E3BF4DDBE04EA9 '
            .'FOREIGN KEY (job_id) REFERENCES chill_main_user_job (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE chill_main_user_job_history ADD CONSTRAINT FK_4E3BF4DDA76ED395 '
            .'FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE chill_main_user_job_history '
            .'ADD CONSTRAINT user_job_history_not_overlaps '
            .'EXCLUDE USING GIST (user_id with =, tsrange(startDate, endDate) with &&) '
            .'DEFERRABLE INITIALLY DEFERRED');

        $this->addSql('ALTER TABLE chill_main_user_job_history '
            .'ADD CONSTRAINT user_job_history_endate_null_or_after_startdate '
            .'CHECK (startDate <= endDate OR endDate IS NULL)');

        // insert user job_history datas
        $this->addSql('INSERT INTO chill_main_user_job_history (id, startDate, endDate, user_id, job_id) '
            .'SELECT nextval(\'chill_main_user_job_history_id_seq\'), \'1970-01-01\'::date, NULL, users.id, userjob_id '
            .'FROM users');

        // remove userjob
        $this->addSql('ALTER TABLE users DROP CONSTRAINT fk_1483a5e964b65c5b');
        $this->addSql('ALTER TABLE users DROP userjob_id');
    }
}
