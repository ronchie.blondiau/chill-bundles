<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add table for ChillMain/Notification.
 */
final class Version20210610140248 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_notification_addresses_user DROP CONSTRAINT FK_E52C5D2BEF1A9D84');
        $this->addSql('DROP SEQUENCE chill_main_notification_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_main_notification');
        $this->addSql('DROP TABLE chill_main_notification_addresses_user');
    }

    public function getDescription(): string
    {
        return 'Add table for ChillMain/Notification';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_main_notification_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_main_notification (id INT NOT NULL, sender_id INT NOT NULL, message TEXT NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, relatedEntityClass VARCHAR(255) NOT NULL, relatedEntityId INT NOT NULL, read JSONB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5BDC8067F624B39D ON chill_main_notification (sender_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5BDC8067567988B4440F6072 ON chill_main_notification (relatedEntityClass, relatedEntityId)');
        $this->addSql('COMMENT ON COLUMN chill_main_notification.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chill_main_notification_addresses_user (notification_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(notification_id, user_id))');
        $this->addSql('CREATE INDEX IDX_E52C5D2BEF1A9D84 ON chill_main_notification_addresses_user (notification_id)');
        $this->addSql('CREATE INDEX IDX_E52C5D2BA76ED395 ON chill_main_notification_addresses_user (user_id)');
        $this->addSql('ALTER TABLE chill_main_notification ADD CONSTRAINT FK_5BDC8067F624B39D FOREIGN KEY (sender_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_notification_addresses_user ADD CONSTRAINT FK_E52C5D2BEF1A9D84 FOREIGN KEY (notification_id) REFERENCES chill_main_notification (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_main_notification_addresses_user ADD CONSTRAINT FK_E52C5D2BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
