<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211229140308 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_notification DROP CONSTRAINT FK_5BDC806765FF1AEC');
        $this->addSql('DROP INDEX IDX_5BDC806765FF1AEC');
        $this->addSql('ALTER TABLE chill_main_notification DROP updatedAt');
        $this->addSql('ALTER TABLE chill_main_notification DROP updatedBy_id');
    }

    public function getDescription(): string
    {
        return 'Notification: add updated tracking information';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_notification ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('UPDATE chill_main_notification SET updatedAt="date"');
        $this->addSql('ALTER TABLE chill_main_notification ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_main_notification.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_main_notification ADD CONSTRAINT FK_5BDC806765FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5BDC806765FF1AEC ON chill_main_notification (updatedBy_id)');
    }
}
