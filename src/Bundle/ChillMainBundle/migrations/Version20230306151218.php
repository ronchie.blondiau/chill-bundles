<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230306151218 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'fix inconsistencies in chill_main_address_reference table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER refid TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER refid SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address_reference SET street = \'\' WHERE street IS NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetnumber TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetnumber SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address_reference SET streetnumber = \'\' WHERE streetnumber IS NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetnumber SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalitycode TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalitycode SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address_reference SET municipalitycode = \'\' WHERE municipalitycode IS NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalitycode SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source SET DEFAULT \'\'');
        $this->addSql('UPDATE chill_main_address_reference SET source = \'\' WHERE source IS NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('not double-checked');

        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalityCode TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalityCode DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER municipalityCode DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER refId TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER refId DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER source DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER street DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetNumber TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetNumber DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_main_address_reference ALTER streetNumber DROP NOT NULL');
    }
}
