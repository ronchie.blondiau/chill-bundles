<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221003132620 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX geographical_unit_layer_refid');
        $this->addSql('DROP INDEX geographical_unit_refid');
        $this->addSql('DROP INDEX chill_internal_geographical_unit_layer_geom_idx');
    }

    public function getDescription(): string
    {
        return 'Create indexes and unique constraints on geographical unit entities';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX geographical_unit_layer_refid ON chill_main_geographical_unit_layer (refId)');
        $this->addSql('CREATE UNIQUE INDEX geographical_unit_refid ON chill_main_geographical_unit (layer_id, unitRefId)');
        $this->addSql('CREATE INDEX chill_internal_geographical_unit_layer_geom_idx ON chill_main_geographical_unit USING GIST (layer_id, geom)');
    }
}
