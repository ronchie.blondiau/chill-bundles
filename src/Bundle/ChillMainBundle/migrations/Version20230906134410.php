<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add property to center.
 */
final class Version20230906134410 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add the isActive property to the Center entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE centers ADD isActive BOOLEAN DEFAULT true NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE centers DROP isActive');
    }
}
