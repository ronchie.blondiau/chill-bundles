<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211230003532 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_notification DROP title');
        $this->addSql('ALTER TABLE chill_main_notification ALTER sender_id SET NOT NULL');
        $this->addSql('ALTER TABLE chill_main_notification ALTER updatedAt DROP NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Add title and allow system notification (sender is null)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_notification ADD title TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_main_notification ALTER sender_id DROP NOT NULL');
        $this->addSql('UPDATE chill_main_notification set updatedat="date"');
        $this->addSql('ALTER TABLE chill_main_notification ALTER updatedat SET NOT NULL');
    }
}
