<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230111104315 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE regroupment_id_seq CASCADE');
        $this->addSql('ALTER TABLE regroupment_center DROP CONSTRAINT FK_2BCCE2F9EC6D1029');
        $this->addSql('ALTER TABLE regroupment_center DROP CONSTRAINT FK_2BCCE2F95932F377');
        $this->addSql('DROP TABLE regroupment');
        $this->addSql('DROP TABLE regroupment_center');
    }

    public function getDescription(): string
    {
        return 'Add regroupment admin entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE regroupment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE regroupment (id INT NOT NULL, name VARCHAR(15) DEFAULT \'\' NOT NULL, isActive BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE regroupment_center (regroupment_id INT NOT NULL, center_id INT NOT NULL, PRIMARY KEY(regroupment_id, center_id))');
        $this->addSql('CREATE INDEX IDX_2BCCE2F9EC6D1029 ON regroupment_center (regroupment_id)');
        $this->addSql('CREATE INDEX IDX_2BCCE2F95932F377 ON regroupment_center (center_id)');
        $this->addSql('ALTER TABLE regroupment_center ADD CONSTRAINT FK_2BCCE2F9EC6D1029 FOREIGN KEY (regroupment_id) REFERENCES regroupment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE regroupment_center ADD CONSTRAINT FK_2BCCE2F95932F377 FOREIGN KEY (center_id) REFERENCES centers (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
