<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add new fields to address, including a Point geometry field.
 */
final class Version20210420115006 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address RENAME COLUMN street TO streetaddress1;');
        $this->addSql('ALTER TABLE chill_main_address RENAME COLUMN streetNumber TO streetaddress2;');
        $this->addSql('ALTER TABLE chill_main_address DROP floor');
        $this->addSql('ALTER TABLE chill_main_address DROP corridor');
        $this->addSql('ALTER TABLE chill_main_address DROP steps');
        $this->addSql('ALTER TABLE chill_main_address DROP buildingName');
        $this->addSql('ALTER TABLE chill_main_address DROP flat');
        $this->addSql('ALTER TABLE chill_main_address DROP distribution');
        $this->addSql('ALTER TABLE chill_main_address DROP extra');
        $this->addSql('ALTER TABLE chill_main_address DROP validTo');
        $this->addSql('ALTER TABLE chill_main_address DROP point');
    }

    public function getDescription(): string
    {
        return 'Add a Point data type and modify the Address entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address RENAME COLUMN streetaddress1 TO street;');
        $this->addSql('ALTER TABLE chill_main_address RENAME COLUMN streetaddress2 TO streetNumber;');
        $this->addSql('ALTER TABLE chill_main_address ADD floor VARCHAR(16) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD corridor VARCHAR(16) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD steps VARCHAR(16) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD buildingName VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD flat VARCHAR(16) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD distribution VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD extra VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD validTo DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD point geometry(POINT,4326) DEFAULT NULL');
    }
}
