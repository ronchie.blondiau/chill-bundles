<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220114165950 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step ALTER transitionat SET NOT NULL');
    }

    public function getDescription(): string
    {
        return 'remove not null on transition at';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step ALTER transitionat DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_main_workflow_entity_step ALTER transitionat SET DEFAULT NULL');
    }
}
