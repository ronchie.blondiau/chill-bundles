<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230306142148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add columns refStatus and refStatusLastUpdate to Address';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ADD refStatus TEXT DEFAULT \'match\' NOT NULL');
        $this->addSql('ALTER TABLE chill_main_address ADD refStatusLastUpdate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL');

        // we must set the last status update to the address reference date to avoid inconsistencies
        $this->addSql('UPDATE chill_main_address a SET refStatusLastUpdate = COALESCE(r.updatedat, r.createdat, \'1970-01-01\'::timestamp) FROM chill_main_address_reference r WHERE a.addressreference_id = r.id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address DROP refStatus');
        $this->addSql('ALTER TABLE chill_main_address DROP refStatusLastUpdate');
    }
}
