<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add some fields to PostalCode.
 */
final class Version20211006151653 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_postal_code DROP refPostalCodeId');
        $this->addSql('ALTER TABLE chill_main_postal_code DROP postalCodeSource');
        $this->addSql('ALTER TABLE chill_main_postal_code DROP center');
    }

    public function getDescription(): string
    {
        return 'Add some fields to PostalCode';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_postal_code ADD refPostalCodeId VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_postal_code ADD postalCodeSource VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_main_postal_code ADD center geometry(POINT,4326) DEFAULT NULL');
    }
}
