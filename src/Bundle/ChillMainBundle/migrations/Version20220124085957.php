<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Alter some address fields + add confidential field on Address.
 */
final class Version20220124085957 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address DROP confidential');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor TYPE VARCHAR(16)');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat TYPE VARCHAR(16)');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor TYPE VARCHAR(16)');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps TYPE VARCHAR(16)');
    }

    public function getDescription(): string
    {
        return 'Alter some address fields + add confidential field on Address';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ADD confidential BOOLEAN DEFAULT FALSE');
        $this->addSql('ALTER TABLE chill_main_address ALTER floor TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER corridor TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER steps TYPE TEXT');
        $this->addSql('ALTER TABLE chill_main_address ALTER flat TYPE TEXT');
    }
}
