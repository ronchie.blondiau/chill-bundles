<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a field "isNoAddress" on addresses.
 */
final class Version20200422122715 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address DROP isNoAddress');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_main_address ADD isNoAddress BOOLEAN NOT NULL DEFAULT FALSE');
    }
}
