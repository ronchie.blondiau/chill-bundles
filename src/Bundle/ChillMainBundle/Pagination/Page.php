<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * a page is an element of a pagination.
 */
class Page implements PageInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    public function __construct(
        /**
         * the number of the current page.
         */
        protected int $number,
        /**
         * the number of item per page.
         */
        protected int $itemPerPage,
        UrlGeneratorInterface $urlGenerator,
        /**
         * The route for the current page.
         */
        protected string $route,
        /**
         * Parameters for the route to the current page.
         */
        protected array $routeParameters,
        /**
         * The number of items in the whole iteration.
         */
        protected int $totalItems
    ) {
        $this->urlGenerator = $urlGenerator;
    }

    public function generateUrl(): string
    {
        return $this->urlGenerator->generate($this->route, $this->routeParameters);
    }

    public function getFirstItemNumber(): int
    {
        return ($this->number - 1) * $this->itemPerPage;
    }

    public function getLastItemNumber(): int
    {
        $last = $this->number * $this->itemPerPage - 1;

        return $last < $this->totalItems ? $last : $this->totalItems;
    }

    public function getNumber(): int
    {
        return $this->number;
    }
}
