<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Pagination;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * Create paginator instances.
 */
final readonly class PaginatorFactory implements PaginatorFactoryInterface
{
    final public const DEFAULT_CURRENT_PAGE_KEY = 'page';

    final public const DEFAULT_ITEM_PER_NUMBER_KEY = 'item_per_page';

    final public const DEFAULT_PAGE_NUMBER = 1;

    public function __construct(
        /**
         * the request stack.
         */
        private RequestStack $requestStack,
        /**
         * the router and generator for url.
         */
        private RouterInterface $router,
        /**
         * the default item per page. This may be overriden by
         * the request or inside the paginator.
         */
        private int $itemPerPage = 20
    ) {
    }

    /**
     * create a paginator instance.
     *
     * The default route and route parameters are the current ones. If set,
     * thos route are overriden.
     *
     * @param string|null $route           the specific route to use in pages
     * @param array|null  $routeParameters the specific route parameters to use in pages
     */
    public function create(
        int $totalItems,
        ?string $route = null,
        ?array $routeParameters = null
    ): PaginatorInterface {
        return new Paginator(
            $totalItems,
            $this->getCurrentItemsPerPage(),
            $this->getCurrentPageNumber(),
            $route ?? $this->getCurrentRoute(),
            $routeParameters ?? $this->getCurrentRouteParameters(),
            $this->router,
            self::DEFAULT_CURRENT_PAGE_KEY,
            self::DEFAULT_ITEM_PER_NUMBER_KEY
        );
    }

    public function getCurrentItemsPerPage(): int
    {
        return $this->requestStack
            ->getCurrentRequest()
            ->query
            ->getInt(self::DEFAULT_ITEM_PER_NUMBER_KEY, $this->itemPerPage);
    }

    public function getCurrentPageFirstItemNumber(): int
    {
        return ($this->getCurrentPageNumber() - 1) *
            $this->getCurrentItemsPerPage();
    }

    public function getCurrentPageNumber(): int
    {
        return $this->requestStack
            ->getCurrentRequest()
            ->query
            ->getInt(self::DEFAULT_CURRENT_PAGE_KEY, self::DEFAULT_PAGE_NUMBER);
    }

    private function getCurrentRoute()
    {
        $request = $this->requestStack->getCurrentRequest();

        return $request->get('_route');
    }

    private function getCurrentRouteParameters()
    {
        return array_merge(
            $this->router->getContext()->getParameters(),
            // get the route parameters
            $this->requestStack
                ->getCurrentRequest()
                ->attributes->get('_route_params'),
            // get the query parameters
            $this->requestStack
                ->getCurrentRequest()->query->all()
        );
    }
}
