<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\PermissionsGroup;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

final class PermissionsGroupRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(PermissionsGroup::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?PermissionsGroup
    {
        return $this->repository->find($id, $lockMode, $lockVersion);
    }

    /**
     * @return PermissionsGroup[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return list<PermissionsGroup>
     */
    public function findAllOrderedAlphabetically(): array
    {
        $qb = $this->repository->createQueryBuilder('pg');

        return $qb->select(['pg', 'pg.name AS HIDDEN sort_name'])
            ->orderBy('sort_name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return PermissionsGroup[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?PermissionsGroup
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    public function getClassName()
    {
        return PermissionsGroup::class;
    }
}
