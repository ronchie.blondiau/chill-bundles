<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\Center;
use Doctrine\Persistence\ObjectRepository;

interface CenterRepositoryInterface extends ObjectRepository
{
    /**
     * Return all active centers.
     *
     * Note: this is a teaser: active will comes later on center entity
     *
     * @return Center[]
     */
    public function findActive(): array;
}
