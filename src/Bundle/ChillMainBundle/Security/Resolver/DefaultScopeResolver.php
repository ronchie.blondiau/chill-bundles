<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\HasScopesInterface;

class DefaultScopeResolver implements ScopeResolverInterface
{
    public static function getDefaultPriority(): int
    {
        return -256;
    }

    public function isConcerned($entity, ?array $options = []): bool
    {
        return $entity instanceof HasScopeInterface || $entity instanceof HasScopesInterface;
    }

    /**
     * @param HasScopeInterface|HasScopesInterface $entity
     */
    public function resolveScope($entity, ?array $options = [])
    {
        if ($entity instanceof HasScopeInterface) {
            return $entity->getScope();
        }

        if ($entity instanceof HasScopesInterface) {
            return $entity->getScopes();
        }

        throw new \UnexpectedValueException(sprintf('should be an instanceof %s or %s', HasScopesInterface::class, HasScopeInterface::class));
    }

    public function supports($entity, ?array $options = []): bool
    {
        return $entity instanceof HasScopeInterface || $entity instanceof HasScopesInterface;
    }
}
