<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Chill\MainBundle\Entity\Center;

/**
 * @deprecated Use CenterResolverManager and its interface CenterResolverManagerInterface
 */
interface CenterResolverDispatcherInterface
{
    /**
     * @param object $entity
     *
     * @return Center|Center[]|null
     */
    public function resolveCenter($entity, ?array $options = []);
}
