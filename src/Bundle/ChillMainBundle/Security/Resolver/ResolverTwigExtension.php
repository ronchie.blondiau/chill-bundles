<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Twig\TwigFilter;

final class ResolverTwigExtension extends \Twig\Extension\AbstractExtension
{
    public function __construct(private readonly CenterResolverManagerInterface $centerResolverDispatcher, private readonly ScopeResolverDispatcher $scopeResolverDispatcher)
    {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('chill_resolve_center', $this->resolveCenter(...)),
            new TwigFilter('chill_resolve_scope', $this->resolveScope(...)),
            new TwigFilter('chill_is_scope_concerned', $this->isScopeConcerned(...)),
        ];
    }

    /**
     * @return bool
     */
    public function isScopeConcerned($entity, ?array $options = [])
    {
        return $this->scopeResolverDispatcher->isConcerned($entity, $options);
    }

    /**
     * @return Center|Center[]|null
     */
    public function resolveCenter(mixed $entity, ?array $options = [])
    {
        return $this->centerResolverDispatcher->resolveCenters($entity, $options);
    }

    /**
     * @return array|\Chill\MainBundle\Entity\Scope|\Chill\MainBundle\Entity\Scope[]
     */
    public function resolveScope($entity, ?array $options = []): array|\Chill\MainBundle\Entity\Scope
    {
        return $this->scopeResolverDispatcher->resolveScope();
    }
}
