<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasCentersInterface;

class DefaultCenterResolver implements CenterResolverInterface
{
    public static function getDefaultPriority(): int
    {
        return -256;
    }

    /**
     * @param HasCenterInterface $entity
     */
    public function resolveCenter($entity, ?array $options = [])
    {
        if ($entity instanceof HasCenterInterface) {
            return $entity->getCenter();
        }

        if ($entity instanceof HasCentersInterface) {
            return $entity->getCenters();
        }

        throw new \UnexpectedValueException('should be an instanceof');
    }

    public function supports($entity, ?array $options = []): bool
    {
        return $entity instanceof HasCenterInterface || $entity instanceof HasCentersInterface;
    }
}
