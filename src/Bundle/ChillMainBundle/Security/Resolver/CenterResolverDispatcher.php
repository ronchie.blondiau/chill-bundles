<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Resolver;

final readonly class CenterResolverDispatcher implements CenterResolverDispatcherInterface
{
    /**
     * @param \Chill\MainBundle\Security\Resolver\CenterResolverInterface[] $resolvers
     */
    public function __construct(private iterable $resolvers = [])
    {
    }

    public function resolveCenter($entity, ?array $options = [])
    {
        trigger_deprecation(
            'ChillMainBundle',
            'dev-master',
            '
            Use the service CenterResolverManager through the interface CenterResolverManagerInterface.
            The new method "CenterResolverManagerInterface::resolveCenters(): array" is available and the typing
            has been improved in order to avoid mixing types.
            '
        );

        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($entity, $options)) {
                return $resolver->resolveCenter($entity, $options);
            }
        }

        return null;
    }
}
