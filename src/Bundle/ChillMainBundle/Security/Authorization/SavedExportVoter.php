<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Chill\MainBundle\Entity\SavedExport;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SavedExportVoter extends Voter
{
    final public const DELETE = 'CHLL_MAIN_EXPORT_SAVED_DELETE';

    final public const EDIT = 'CHLL_MAIN_EXPORT_SAVED_EDIT';

    final public const GENERATE = 'CHLL_MAIN_EXPORT_SAVED_GENERATE';

    private const ALL = [
        self::DELETE,
        self::EDIT,
        self::GENERATE,
    ];

    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof SavedExport && \in_array($attribute, self::ALL, true);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::DELETE, self::EDIT, self::GENERATE => $subject->getUser() === $token->getUser(),
            default => throw new \UnexpectedValueException('attribute not supported: '.$attribute),
        };
    }
}
