<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ChillExportVoter extends Voter
{
    final public const EXPORT = 'chill_export';

    private readonly VoterHelperInterface $helper;

    public function __construct(VoterHelperFactoryInterface $voterHelperFactory)
    {
        $this->helper = $voterHelperFactory
            ->generate(self::class)
            ->addCheckFor(null, [self::EXPORT])
            ->build();
    }

    protected function supports($attribute, $subject): bool
    {
        return $this->helper->supports($attribute, $subject);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return $this->helper->voteOnAttribute($attribute, $subject, $token);
    }
}
