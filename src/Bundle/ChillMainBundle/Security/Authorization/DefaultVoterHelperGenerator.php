<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security\Authorization;

final class DefaultVoterHelperGenerator implements VoterGeneratorInterface
{
    private array $configuration = [];

    public function __construct(private readonly AuthorizationHelper $authorizationHelper)
    {
    }

    public function addCheckFor(?string $class, array $attributes): self
    {
        $this->configuration[] = [$attributes, $class];

        return $this;
    }

    public function build(): VoterHelperInterface
    {
        return new DefaultVoterHelper(
            $this->authorizationHelper,
            $this->configuration
        );
    }
}
