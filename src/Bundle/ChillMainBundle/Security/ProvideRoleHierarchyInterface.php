<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Security;

/**
 * Give a hierarchy for the role.
 *
 * This hierarchy allow to sort roles, which is useful in UI
 */
interface ProvideRoleHierarchyInterface extends ProvideRoleInterface
{
    /**
     * Return an array of roles, where keys are the hierarchy, and values
     * an array of roles.
     *
     * Example:
     *
     * ```
     * [ 'Title' => [ 'CHILL_FOO_SEE', 'CHILL_FOO_UPDATE' ] ]
     * ```
     *
     * @return array<string, array<int, string>> Where keys are the hierarchy, and values an array of roles: `[ 'title' => [ 'CHILL_FOO_SEE', 'CHILL_FOO_UPDATE' ] ]`
     */
    public function getRolesWithHierarchy(): array;
}
