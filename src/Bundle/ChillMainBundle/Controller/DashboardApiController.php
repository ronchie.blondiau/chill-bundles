<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\NewsItemRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final readonly class DashboardApiController
{
    public function __construct(
        private NewsItemRepository $newsItemRepository,
    ) {
    }

    /**
     * Get user dashboard config (not yet based on user id and still hardcoded for now).
     *
     * @Route("/api/1.0/main/dashboard-config-item.json", methods={"get"})
     */
    public function getDashboardConfiguration(): JsonResponse
    {
        $data = [];

        if (0 < $this->newsItemRepository->countCurrentNews()) {
            // show news only if we have news
            // NOTE: maybe this should be done in the frontend...
            $data[] =
                [
                    'position' => 'top-left',
                    'id' => 1,
                    'type' => 'news',
                    'metadata' => [
                        // arbitrary data that will be store "some time"
                        'only_unread' => false,
                    ],
                ];
        }

        return new JsonResponse($data, JsonResponse::HTTP_OK, []);
    }
}
