<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class UserJobScopeHistoriesController extends AbstractController
{
    public function __construct(
        private readonly Environment $engine,
    ) {
    }

    /**
     * @Route("/{_locale}/admin/main/user/{id}/job-scope-history", name="admin_user_job_scope_history")
     */
    public function indexAction(User $user): Response
    {
        $jobHistories = $user->getUserJobHistoriesOrdered();
        $scopeHistories = $user->getMainScopeHistoriesOrdered();

        return new Response(
            $this->engine->render(
                '@ChillMain/User/history.html.twig',
                [
                    'user' => $user,
                    'jobHistories' => $jobHistories,
                    'scopeHistories' => $scopeHistories,
                ]
            )
        );
    }
}
