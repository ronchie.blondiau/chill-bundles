<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Form\UserPhonenumberType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserProfileController extends AbstractController
{
    public function __construct(
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * User profile that allows editing of phonenumber and visualization of certain data.
     *
     * @Route("/{_locale}/main/user/my-profile", name="chill_main_user_profile")
     */
    public function __invoke(Request $request)
    {
        $user = $this->getUser();
        $editForm = $this->createPhonenumberEditForm($user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $phonenumber = $editForm->get('phonenumber')->getData();

            $user->setPhonenumber($phonenumber);

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->translator->trans('user.profile.Phonenumber successfully updated!'));

            return $this->redirectToRoute('chill_main_user_profile');
        }

        return $this->render('@ChillMain/User/profile.html.twig', [
            'user' => $user,
            'form' => $editForm->createView(),
        ]);
    }

    private function createPhonenumberEditForm(UserInterface $user): FormInterface
    {
        return $this->createForm(
            UserPhonenumberType::class,
            $user,
        )
            ->add('submit', SubmitType::class, ['label' => $this->translator->trans('Save')]);
    }
}
