<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends CRUDController
{
    protected function createEntity(string $action, Request $request): object
    {
        $entity = parent::createEntity($action, $request);

        $entity->setAvailableForUsers(true);

        return $entity;
    }

    protected function customizeQuery(string $action, Request $request, $query): void
    {
        $query->where('e.availableForUsers = TRUE');
    }

    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        return $query->addOrderBy('e.name', 'DESC');
    }
}
