<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\Form\AbsenceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AbsenceController extends AbstractController
{
    /**
     * @Route(
     *     "/{_locale}/absence",
     *     name="chill_main_user_absence_index",
     *     methods={"GET", "POST"}
     * )
     */
    public function setAbsence(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(AbsenceType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('chill_main_user_absence_index');
        }

        return $this->render('@ChillMain/Menu/absence.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/absence/unset",
     *     name="chill_main_user_absence_unset",
     *     methods={"GET", "POST"}
     * )
     */
    public function unsetAbsence(Request $request)
    {
        $user = $this->getUser();

        $user->setAbsenceStart(null);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('chill_main_user_absence_index');
    }
}
