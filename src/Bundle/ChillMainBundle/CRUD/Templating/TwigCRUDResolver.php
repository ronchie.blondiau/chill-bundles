<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\CRUD\Templating;

use Chill\MainBundle\CRUD\Resolver\Resolver;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigCRUDResolver
 * Twig filters to display data in crud template.
 */
class TwigCRUDResolver extends AbstractExtension
{
    /**
     * @var Resolver
     */
    protected $resolver;

    /**
     * TwigCRUDResolver constructor.
     */
    public function __construct(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @param null $action
     *
     * @return string
     */
    public function getConfig($configKey, $crudName, $action = null)
    {
        return $this->resolver->getConfigValue($configKey, $crudName, $action);
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction(
                'chill_crud_config',
                $this->getConfig(...),
                ['is_safe' => 'html']
            ),
            new TwigFunction(
                'chill_crud_action_exists',
                $this->hasAction(...),
                []
            ),
        ];
    }

    /**
     * @return bool
     */
    public function hasAction($crudName, $action)
    {
        return $this->resolver->hasAction($crudName, $action);
    }
}
