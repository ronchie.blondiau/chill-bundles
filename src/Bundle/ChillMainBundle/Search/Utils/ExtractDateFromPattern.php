<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search\Utils;

class ExtractDateFromPattern
{
    private const DATE_PATTERN = [
        ['([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))', 'Y-m-d'], // 1981-05-12
        ['((0[1-9]|[12]\\d|3[01])\\/(0[1-9]|1[0-2])\\/([12]\\d{3}))', 'd/m/Y'], // 15/12/1980
        ['((0[1-9]|[12]\\d|3[01])-(0[1-9]|1[0-2])-([12]\\d{3}))', 'd-m-Y'], // 15/12/1980
    ];

    public function extractDates(string $subject): SearchExtractionResult
    {
        $dates = [];
        $filteredSubject = $subject;

        foreach (self::DATE_PATTERN as [$pattern, $format]) {
            $matches = [];
            \preg_match_all($pattern, $filteredSubject, $matches);

            foreach ($matches[0] as $match) {
                $date = \DateTimeImmutable::createFromFormat($format, $match);

                if (false !== $date) {
                    $dates[] = $date;
                    // filter string: remove what is found
                    $filteredSubject = \trim(\strtr($filteredSubject, [$match => '']));
                }
            }
        }

        return new SearchExtractionResult($filteredSubject, $dates);
    }
}
