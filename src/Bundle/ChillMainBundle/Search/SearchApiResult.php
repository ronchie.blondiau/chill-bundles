<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

use Symfony\Component\Serializer\Annotation as Serializer;

class SearchApiResult
{
    private mixed $result;

    public function __construct(private readonly float $relevance)
    {
    }

    /**
     * @Serializer\Groups({"read"})
     */
    public function getRelevance(): float
    {
        return $this->relevance;
    }

    /**
     * @Serializer\Groups({"read"})
     */
    public function getResult()
    {
        return $this->result;
    }

    public function setResult(mixed $result): self
    {
        $this->result = $result;

        return $this;
    }
}
