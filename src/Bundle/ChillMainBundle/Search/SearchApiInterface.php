<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

interface SearchApiInterface
{
    public function getResult(string $key, array $metadata, float $pertinence);

    public function prepare(array $metadatas): void;

    public function provideQuery(string $pattern, array $parameters): SearchApiQuery;

    public function supportsResult(string $key, array $metadatas): bool;

    public function supportsTypes(string $pattern, array $types, array $parameters): bool;
}
