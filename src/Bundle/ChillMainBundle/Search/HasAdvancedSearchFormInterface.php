<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

use Symfony\Component\Form\FormBuilderInterface;

interface HasAdvancedSearchFormInterface extends SearchInterface
{
    public function buildForm(FormBuilderInterface $builder);

    public function convertFormDataToQuery(array $data);

    public function convertTermsToFormData(array $terms);

    public function getAdvancedSearchTitle(): string;
}
