<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Search;

/**
 * Throw by search provider when the search name is not found.
 */
class UnknowSearchNameException extends \Exception
{
    private $name;

    public function __construct($name)
    {
        parent::__construct("No module search supports with the name {$name}");
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
