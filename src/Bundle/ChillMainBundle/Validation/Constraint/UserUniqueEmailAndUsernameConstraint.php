<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validation\Constraint;

use Chill\MainBundle\Validation\Validator\UserUniqueEmailAndUsername;
use Symfony\Component\Validator\Constraint;

class UserUniqueEmailAndUsernameConstraint extends Constraint
{
    public $messageDuplicateEmail = 'A user with the same or a close email already exists';

    public $messageDuplicateUsername = 'A user with the same or a close username already exists';

    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }

    public function validatedBy()
    {
        return UserUniqueEmailAndUsername::class;
    }
}
