<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Templating;

use Chill\MainBundle\Templating\ChillMarkdownRenderExtension;
use PHPUnit\Framework\TestCase;

/**
 * Test the service ChillMarkdownRenderExtension.
 *
 * @internal we do not want to test the markdown transformation. We just want to
 * test that the markdown is correctly transformed into html, and that the html
 * is safe.
 *
 * @coversNothing
 */
final class ChillMarkdownRenderExtensionTest extends TestCase
{
    private const SIMPLE_HTML = <<<'HTML'
        <h1>test</h1>
        <p>Text.</p>
        HTML;

    private const SIMPLE_MARKDOWN = <<<'MD'
        # test

        Text.
        MD;

    private const UNAUTHORIZED_HTML = <<<'HTML'
        <p>&lt;script&gt;alert(&quot;ok&quot;);&lt;/script&gt;</p>
        HTML;

    private const UNAUTHORIZED_MARKDOWN = <<<'MD'
        <script>alert("ok");</script>
        MD;

    /**
     * Test that the markdown input is transformed into html.
     */
    public function testRendering()
    {
        $extension = new ChillMarkdownRenderExtension();

        $this->assertEquals(
            self::SIMPLE_HTML,
            $extension->renderMarkdownToHtml(self::SIMPLE_MARKDOWN)
        );
    }

    /**
     * Test that the output of the markdown content is sanitized.
     */
    public function testSecurity()
    {
        $extension = new ChillMarkdownRenderExtension();

        $this->assertEquals(
            self::UNAUTHORIZED_HTML,
            $extension->renderMarkdownToHtml(self::UNAUTHORIZED_MARKDOWN)
        );
    }
}
