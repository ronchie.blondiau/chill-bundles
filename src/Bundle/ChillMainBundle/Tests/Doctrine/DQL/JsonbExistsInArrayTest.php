<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Doctrine\DQL;

use Chill\MainBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class JsonbExistsInArrayTest extends KernelTestCase
{
    private EntityManagerInterface $em;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->em = self::$container->get(EntityManagerInterface::class);
    }

    public function testDQLFunctionWorks()
    {
        $result = $this->em
            ->createQuery('SELECT JSONB_EXISTS_IN_ARRAY(u.attributes, :param) FROM '.User::class.' u')
            ->setParameter('param', 'fr')
            ->getResult();

        $this->assertIsArray($result);
    }
}
