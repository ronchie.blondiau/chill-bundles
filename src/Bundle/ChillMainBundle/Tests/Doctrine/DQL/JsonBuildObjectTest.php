<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Doctrine\DQL;

use Chill\MainBundle\Entity\Address;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class JsonBuildObjectTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->entityManager = self::$container->get(EntityManagerInterface::class);
    }

    /**
     * @dataProvider provideQueries
     */
    public function testQuery(string $sql, array $params, array $paramType): void
    {
        $query = $this->entityManager->createQuery($sql);

        foreach ($params as $k => $v) {
            $query->setParameter($k, $v, $paramType[$k]);
        }
        $query->setMaxResults(1);

        $result = $query->getResult(AbstractQuery::HYDRATE_ARRAY);

        self::assertIsArray($result);
    }

    public function provideQueries(): iterable
    {
        yield ['SELECT JSON_BUILD_OBJECT(1, 2, 3, 4) FROM '.Address::class.' a', [], []];
        yield ["SELECT JSON_BUILD_OBJECT('st', a.street, 'sn', a.streetNumber) FROM ".Address::class.' a', [], []];
        // next query make the test fails. But we do not need it for now.
        // yield ["SELECT JSON_BUILD_OBJECT(a.street, :param), LOWER(:param) FROM " . Address::class . " a", ['param' => 1], ['param' => Types::INTEGER]];
    }
}
