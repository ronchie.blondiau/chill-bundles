<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Form\Type;

use Chill\MainBundle\Entity\GroupCenter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Form\CenterType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PickCenterTypeTest extends TypeTestCase
{
    /**
     * Test that a user which can reach multiple center
     * make CenterType render as "entity" type.
     */
    public function testUserCanReachMultipleCenters(): never
    {
        $this->markTestSkipped();
        // prepare user
        $centerA = $this->prepareCenter(1, 'centerA');
        $centerB = $this->prepareCenter(2, 'centerB');
        $groupCenterA = (new GroupCenter())
            ->setCenter($centerA);
        $groupCenterB = (new GroupCenter())
            ->setCenter($centerB);
        $user = (new User())
            ->addGroupCenter($groupCenterA)
            ->addGroupCenter($groupCenterB);

        $type = $this->prepareType($user);

        $this->assertEquals(EntityType::class, $type->getParent());
    }

    /**
     * Test that a user which can reach only one center
     * render as an hidden field.
     */
    public function testUserCanReachMultipleSameCenter(): never
    {
        $this->markTestSkipped();
        // prepare user
        $center = $this->prepareCenter(1, 'center');
        $groupCenterA = (new GroupCenter())
            ->setCenter($center);
        $groupCenterB = (new GroupCenter())
            ->setCenter($center);
        $user = (new User())
            ->addGroupCenter($groupCenterA)
            ->addGroupCenter($groupCenterB);

        $type = $this->prepareType($user);

        $this->assertEquals(HiddenType::class, $type->getParent());
    }

    /**
     * Test that a user which can reach only one center
     * render as an hidden field.
     */
    public function testUserCanReachSingleCenter(): never
    {
        $this->markTestSkipped();
        // prepare user
        $center = $this->prepareCenter(1, 'center');
        $groupCenter = (new GroupCenter())
            ->setCenter($center);
        $user = (new User())
            ->addGroupCenter($groupCenter);

        $type = $this->prepareType($user);

        $this->assertEquals(HiddenType::class, $type->getParent());
    }

    /**
     * prepare a mocked center, with and id and name given.
     *
     * @param int    $id
     * @param string $name
     *
     * @return \Chill\MainBundle\Entity\Center
     */
    private function prepareCenter($id, $name)
    {
        $prophet = new \Prophecy\Prophet();

        $prophecyCenter = $prophet->prophesize();
        $prophecyCenter->willExtend('\\'.\Chill\MainBundle\Entity\Center::class);
        $prophecyCenter->getId()->willReturn($id);
        $prophecyCenter->getName()->willReturn($name);

        return $prophecyCenter->reveal();
    }

    /**
     * prepare the type with mocked center transformer and token storage.
     *
     * @param User $user the user for wich the form will be prepared
     *
     * @return CenterType
     */
    private function prepareType(User $user)
    {
        $prophet = new \Prophecy\Prophet();

        // create a center transformer
        $centerTransformerProphecy = $prophet->prophesize();
        $centerTransformerProphecy
            ->willExtend(\Chill\MainBundle\Form\Type\DataTransformer\CenterTransformer::class);
        $transformer = $centerTransformerProphecy->reveal();

        $tokenProphecy = $prophet->prophesize();
        $tokenProphecy
            ->willImplement('\\'.\Symfony\Component\Security\Core\Authentication\Token\TokenInterface::class);
        $tokenProphecy->getUser()->willReturn($user);
        $token = $tokenProphecy->reveal();

        $tokenStorageProphecy = $prophet->prophesize();
        $tokenStorageProphecy
            ->willExtend(\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage::class);
        $tokenStorageProphecy->getToken()->willReturn($token);
        $tokenStorage = $tokenStorageProphecy->reveal();

        return new CenterType($tokenStorage, $transformer);
    }
}
