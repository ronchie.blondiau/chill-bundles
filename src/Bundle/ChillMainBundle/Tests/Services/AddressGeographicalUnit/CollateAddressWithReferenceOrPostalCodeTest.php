<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Services\AddressGeographicalUnit;

use Chill\MainBundle\Service\AddressGeographicalUnit\CollateAddressWithReferenceOrPostalCode;
use Doctrine\DBAL\Connection;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class CollateAddressWithReferenceOrPostalCodeTest extends KernelTestCase
{
    private Connection $connection;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->connection = self::$container->get(Connection::class);
    }

    public function testRun(): void
    {
        $collator = new CollateAddressWithReferenceOrPostalCode(
            $this->connection,
            new NullLogger()
        );

        $result = $collator(0);

        self::assertGreaterThan(0, $result);
    }
}
