<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Util;

use Chill\MainBundle\Util\CountriesInfo;
use PHPUnit\Framework\TestCase;

/**
 * tests for CountriesInfos.
 *
 * @internal
 *
 * @coversNothing
 */
final class CountriesInfoTest extends TestCase
{
    public function getGetContinentsCodes()
    {
        $continents = CountriesInfo::getContinentsCodes();

        $this->assertContains('EU', $continents);
    }

    public function testGetArrayCountriesData()
    {
        $data = CountriesInfo::getArrayCountriesData();

        $this->assertNotNull($data);
        $this->assertContains([
            'AS', 'AF', 'AFG', '004', 'Afghanistan, Islamic Republic of',
        ], $data);
    }

    public function testGetCountryCodeByContinents()
    {
        $countries = CountriesInfo::getCountriesCodeByContinent('EU');

        $this->assertContains('BE', $countries);
        $this->assertContains('FR', $countries);
        $this->assertContains('GB', $countries);
    }

    public function testGetCountryData()
    {
        $raw = CountriesInfo::getCountriesData();

        $this->assertStringStartsWith(
            'AS AF AFG 004 Afghanistan, Islamic Republic of',
            $raw
        );
    }
}
