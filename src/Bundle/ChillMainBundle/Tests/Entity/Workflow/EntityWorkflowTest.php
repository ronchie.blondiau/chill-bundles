<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Entity\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class EntityWorkflowTest extends TestCase
{
    public function testIsFinalizeWith1Steps()
    {
        $entityWorkflow = new EntityWorkflow();

        $entityWorkflow->setStep('final');
        $entityWorkflow->getCurrentStep()->setIsFinal(true);

        $this->assertTrue($entityWorkflow->isFinal());
    }

    public function testIsFinalizeWith4Steps()
    {
        $entityWorkflow = new EntityWorkflow();

        $this->assertFalse($entityWorkflow->isFinal());

        $entityWorkflow->setStep('two');

        $this->assertFalse($entityWorkflow->isFinal());

        $entityWorkflow->setStep('previous_final');

        $this->assertFalse($entityWorkflow->isFinal());

        $entityWorkflow->getCurrentStep()->setIsFinal(true);
        $entityWorkflow->setStep('final');

        $this->assertTrue($entityWorkflow->isFinal());
    }

    public function testIsFreeze()
    {
        $entityWorkflow = new EntityWorkflow();

        $this->assertFalse($entityWorkflow->isFreeze());

        $entityWorkflow->setStep('step_one');

        $this->assertFalse($entityWorkflow->isFreeze());

        $entityWorkflow->setStep('step_three');

        $this->assertFalse($entityWorkflow->isFreeze());

        $entityWorkflow->setStep('freezed');
        $entityWorkflow->getCurrentStep()->setFreezeAfter(true);

        $this->assertTrue($entityWorkflow->isFreeze());

        $entityWorkflow->setStep('after_freeze');

        $this->assertTrue($entityWorkflow->isFreeze());

        $this->assertTrue($entityWorkflow->getCurrentStep()->isFreezeAfter());
    }
}
