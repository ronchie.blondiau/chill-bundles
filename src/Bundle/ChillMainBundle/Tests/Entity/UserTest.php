<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Entity;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserJob;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class UserTest extends TestCase
{
    use ProphecyTrait;

    public function testMainScopeHistory()
    {
        $user = new User();
        $scopeA = new Scope();
        $scopeB = new Scope();

        $user->setMainScope($scopeA);
        $user->setMainScope($scopeB);

        // 1. check getMainScope get now scopeB, not scopeA
        self::assertSame($scopeB, $user->getMainScope());

        // 2. get scopeA history, check endDate is not null
        self::assertNotNull(
            $user
                ->getMainScopeHistories()
                ->filter(fn (User\UserScopeHistory $userScopeHistory) => $userScopeHistory->getScope() === $scopeA)
                ->first()->getEndDate()
        );
    }

    public function testUserJobHistory()
    {
        $user = new User();
        $jobA = new UserJob();
        $jobB = new UserJob();

        $user->setUserJob($jobA);
        $user->setUserJob($jobB);

        // 1. check getUserJob get now jobB, not jobA
        self::assertSame($jobB, $user->getUserJob());

        // 2. get jobA history, check endDate is not null
        self::assertNotNull(
            $user
                ->getUserJobHistories()
                ->filter(fn (User\UserJobHistory $userJobHistory) => $userJobHistory->getJob() === $jobA)
                ->first()->getEndDate()
        );
    }
}
