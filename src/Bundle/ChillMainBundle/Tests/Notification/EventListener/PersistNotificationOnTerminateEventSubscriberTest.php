<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Notification\EventListener;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\EventListener\PersistNotificationOnTerminateEventSubscriber;
use Chill\MainBundle\Notification\NotificationPersister;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersistNotificationOnTerminateEventSubscriberTest extends TestCase
{
    use ProphecyTrait;

    public function testNotificationIsPersisted()
    {
        $persister = new NotificationPersister();
        $em = $this->prophesize(EntityManagerInterface::class);
        $em->persist(Argument::type(Notification::class))->shouldBeCalledTimes(1);
        $em->flush()->shouldBeCalledTimes(1);
        $event = $this->prophesize(TerminateEvent::class);
        $event->isMasterRequest()->willReturn(true);

        $eventSubscriber = new PersistNotificationOnTerminateEventSubscriber($em->reveal(), $persister);

        $notification = new Notification();
        $persister->persist($notification);

        $eventSubscriber->onKernelTerminate($event->reveal());
    }
}
