<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Entity\Workflow\EntityWorkflowStep;
use Chill\MainBundle\Workflow\EventSubscriber\NotificationOnTransition;
use Chill\MainBundle\Workflow\Helper\MetadataExtractor;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Call\Call;
use Prophecy\Exception\Prediction\FailedPredictionException;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Marking;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class NotificationOnTransitionTest extends TestCase
{
    use ProphecyTrait;

    public function testOnCompleteSendNotification(): void
    {
        $dest = new User();
        $currentUser = new User();
        $workflowProphecy = $this->prophesize(WorkflowInterface::class);
        $workflow = $workflowProphecy->reveal();
        $entityWorkflow = new EntityWorkflow();
        $entityWorkflow
            ->setWorkflowName('workflow_name')
            ->setRelatedEntityClass(\stdClass::class)
            ->setRelatedEntityId(1);
        // force an id to entityWorkflow:
        $reflection = new \ReflectionClass($entityWorkflow);
        $id = $reflection->getProperty('id');
        $id->setAccessible(true);
        $id->setValue($entityWorkflow, 1);

        $step = new EntityWorkflowStep();
        $entityWorkflow->addStep($step);
        $step->addDestUser($dest)
            ->setCurrentStep('to_state');

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->persist(Argument::type(Notification::class))->should(
            static function ($args) use ($dest) {
                /** @var Call[] $args */
                if (1 !== \count($args)) {
                    throw new FailedPredictionException('no notification sent');
                }

                $notification = $args[0]->getArguments()[0];

                if (!$notification instanceof Notification) {
                    throw new FailedPredictionException('persist is not a notification');
                }

                if (!$notification->getAddressees()->contains($dest)) {
                    throw new FailedPredictionException('the dest is not notified');
                }
            }
        );

        $engine = $this->prophesize(\Twig\Environment::class);
        $engine->render(Argument::type('string'), Argument::type('array'))
            ->willReturn('dummy text');

        $extractor = $this->prophesize(MetadataExtractor::class);
        $extractor->buildArrayPresentationForPlace(Argument::type(EntityWorkflow::class), Argument::any())
            ->willReturn([]);
        $extractor->buildArrayPresentationForWorkflow(Argument::any())
            ->willReturn([]);

        $registry = $this->prophesize(Registry::class);
        $registry->get(Argument::type(EntityWorkflow::class), Argument::type('string'))
            ->willReturn($workflow);

        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($currentUser);

        $notificationOnTransition = new NotificationOnTransition(
            $em->reveal(),
            $engine->reveal(),
            $extractor->reveal(),
            $security->reveal(),
            $registry->reveal()
        );

        $event = new Event($entityWorkflow, new Marking(), new Transition('dummy_transition', ['from_state'], ['to_state']), $workflow);

        $notificationOnTransition->onCompletedSendNotification($event);
    }
}
