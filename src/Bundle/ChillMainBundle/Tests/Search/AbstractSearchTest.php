<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Search;

/**
 * Description of AbstractSearch.
 *
 * @internal
 *
 * @coversNothing
 */
final class AbstractSearchTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Chill\MainBundle\Search\AbstractSearch
     */
    private \Chill\MainBundle\Search\AbstractSearch&\PHPUnit\Framework\MockObject\MockObject $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockForAbstractClass(\Chill\MainBundle\Search\AbstractSearch::class);
    }

    public function testParseDateRegular()
    {
        $date = $this->stub->parseDate('2014-05-01');

        $this->assertEquals('2014', $date->format('Y'));
        $this->assertEquals('05', $date->format('m'));
        $this->assertEquals('01', $date->format('d'));
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testRecompose(): never
    {
        $this->markTestSkipped();
    }
}
