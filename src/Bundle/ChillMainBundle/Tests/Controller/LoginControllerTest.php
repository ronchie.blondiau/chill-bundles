<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 *
 * @coversNothing
 */
final class LoginControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = self::createClient();

        // load login page and submit form
        $crawler = $client->request('GET', '/login');
        $this->assertTrue($client->getResponse()->isSuccessful());

        $buttonCrawlerNode = $crawler->selectButton('login');
        $form = $buttonCrawlerNode->form();

        $client->submit($form, [
            '_username' => 'center a_social',
            '_password' => 'password',
        ]);

        // the response is a redirection
        $this->assertTrue($client->getResponse()->isRedirect());

        // the response is not a login page, but on a new page
        $this->assertDoesNotMatchRegularExpression('/\/login$/', $client->getResponse()
            ->headers
            ->get('location'));

        // on the home page, there must be a logout link
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/');

        $this->assertMatchesRegularExpression('/center a_social/', $client->getResponse()
            ->getContent());
        $logoutLinkFilter = $crawler->filter('a:contains("Se déconnecter")');

        // check there is > 0 logout link
        $this->assertGreaterThan(0, $logoutLinkFilter->count(), 'check that a logout link is present');

        // click on logout link
        $client->followRedirects(false);
        $client->click($crawler->selectLink('Se déconnecter')->link());

        $this->assertTrue($client->getResponse()->isRedirect());
        $client->followRedirect(); // redirect to login page

        // check we are back on login page
        $this->assertMatchesRegularExpression('/\/login$/', $client->getResponse()
            ->headers
            ->get('location'));
    }
}
