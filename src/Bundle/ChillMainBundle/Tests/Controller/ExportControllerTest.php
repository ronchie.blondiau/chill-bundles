<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the export.
 *
 * @internal
 *
 * @coversNothing
 */
final class ExportControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => 'center a_social',
            'PHP_AUTH_PW' => 'password',
            'HTTP_ACCEPT_LANGUAGE' => 'fr_FR',
        ]);

        $client->request('GET', '/fr/exports/');

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            'assert the list is shown'
        );
    }
}
