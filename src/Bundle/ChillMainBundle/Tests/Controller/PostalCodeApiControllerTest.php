<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PostalCodeApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    public function testSearch()
    {
        $client = $this->getClientAuthenticated();

        $client->request(
            'GET',
            '/api/1.0/main/postal-code/search.json',
            ['q' => 'fontenay le comte']
        );

        $this->assertResponseIsSuccessful();

        $data = \json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals('Fontenay Le Comte', $data['results'][0]['name']);

        // test response with invalid search pattern
        $client->request(
            'GET',
            '/api/1.0/main/postal-code/search.json',
            ['q' => '']
        );
        $this->assertResponseStatusCodeSame(400);
    }
}
