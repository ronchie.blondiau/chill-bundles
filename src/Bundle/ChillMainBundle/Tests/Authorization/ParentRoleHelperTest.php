<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Tests\Authorization;

use Chill\MainBundle\Security\ParentRoleHelper;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class ParentRoleHelperTest extends KernelTestCase
{
    private ParentRoleHelper $parentRoleHelper;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->parentRoleHelper = self::$container->get(ParentRoleHelper::class);
    }

    public function testGetReachableRoles()
    {
        // this test will be valid until the role hierarchy for person is changed.
        // this is not perfect but spare us a mock

        $parentRoles = $this->parentRoleHelper->getParentRoles(PersonVoter::SEE);

        $this->assertCount(3, $parentRoles);
        $this->assertContains(PersonVoter::CREATE, $parentRoles);
        $this->assertContains(PersonVoter::UPDATE, $parentRoles);
        $this->assertContains(PersonVoter::SEE, $parentRoles);
    }

    public function testIsRoleReached()
    {
        $this->assertTrue($this->parentRoleHelper->isRoleReached(PersonVoter::SEE, PersonVoter::CREATE));
        $this->assertFalse($this->parentRoleHelper->isRoleReached(PersonVoter::SEE, 'foo'));
    }
}
