<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\Notification;

class NotificationPersister implements NotificationPersisterInterface
{
    private array $waitingNotifications = [];

    /**
     * @return array|Notification[]
     */
    public function getWaitingNotifications(): array
    {
        return $this->waitingNotifications;
    }

    public function persist(Notification $notification): void
    {
        $this->waitingNotifications[] = $notification;
    }
}
