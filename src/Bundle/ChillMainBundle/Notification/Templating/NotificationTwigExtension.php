<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Notification\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotificationTwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('chill_list_notifications', [NotificationTwigExtensionRuntime::class, 'listNotificationsFor'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
            new TwigFunction('chill_count_notifications', [NotificationTwigExtensionRuntime::class, 'countNotificationsFor'], [
                'is_safe' => [],
            ]),
            new TwigFunction('chill_counter_notifications', [NotificationTwigExtensionRuntime::class, 'counterNotificationFor'], [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]),
        ];
    }
}
