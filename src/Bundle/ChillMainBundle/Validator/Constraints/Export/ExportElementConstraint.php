<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validator\Constraints\Export;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint which will check the ExportElement.
 *
 * @see ExportElementConstraintValidator
 */
class ExportElementConstraint extends Constraint
{
    public $element;

    public function getRequiredOptions()
    {
        return ['element'];
    }

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }

    public function validatedBy()
    {
        return ExportElementConstraintValidator::class;
    }
}
