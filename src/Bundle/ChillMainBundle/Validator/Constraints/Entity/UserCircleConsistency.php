<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Validator\Constraints\Entity;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserCircleConsistency extends Constraint
{
    public $getUserFunction = 'getUser';

    public $message = '{{ username }} is not allowed to see entities published in this circle';

    public $path = 'circle';

    public $role;

    public function getDefaultOption()
    {
        return 'role';
    }

    public function getRequiredOptions()
    {
        return ['role'];
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
