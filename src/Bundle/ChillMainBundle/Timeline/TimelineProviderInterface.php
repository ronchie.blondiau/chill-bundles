<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Timeline;

/**
 * Interface for service providing info to timeline.
 *
 * Services implementing those interface must be tagged like this :
 *
 * ```
 * services:
 *    my_timeline:
 *       class: My\Class
 *       tags:
 *          #a first 'person' context :
 *          - { name: timeline, context: person }
 *          # a second 'center' context :
 *          - { name: timeline, context: center }
 * ```
 *
 * The bundle which will call the timeline will document available context and
 * the arguments provided by the context.
 */
interface TimelineProviderInterface
{
    /**
     * provide data to build a SQL SELECT query to fetch entities.
     *
     * The TimeLineBuilder will create a full SELECT query and append
     * the query into an UNION of SELECT queries. This permit to fetch
     * all entities from different table in a single query.
     *
     * The associative array MUST have the following key :
     * - `id` : the name of the id column
     * - `type`: a string to indicate the type
     * - `date`: the name of the datetime column, used to order entities by date
     * - `FROM` (in capital) : the FROM clause. May contains JOIN instructions
     *
     * Those key are optional:
     * - `WHERE` (in capital) : the WHERE clause.
     *
     * Where relevant, the data must be quoted to avoid SQL injection.
     *
     * `$context` and `$args` are defined by the bundle which will call the timeline
     * rendering.
     *
     * @param string  $context
     * @param mixed[] $args    the argument to the context
     *
     * @return string[]
     *
     * @throw  \LogicException if the context is not supported
     */
    public function fetchQuery($context, array $args);

    /**
     * fetch entities from db into an associative array. The keys **MUST BE**
     * the id.
     *
     * All ids returned by all SELECT queries
     * (@see TimeLineProviderInterface::fetchQuery) and with the type
     * supported by the provider (@see TimelineProviderInterface::supportsType)
     * will be passed as argument.
     *
     * @param array $ids an array of id
     *
     * @return mixed[] an associative array of entities, with id as key
     */
    public function getEntities(array $ids);

    /**
     * return an associative array with argument to render the entity
     * in an html template, which will be included in the timeline page.
     *
     * The result must have the following key :
     *
     * - `template` : the template FQDN
     * - `template_data`: the data required by the template
     *
     *
     * Example:
     *
     * ```
     * array(
     *    'template'      => '@ChillMy/timeline/template.html.twig',
     *    'template_data' => array(
     *             'accompanyingPeriod' => $entity,
     *              'person' => $args['person']
     *         )
     *    );
     * ```
     *
     * `$context` and `$args` are defined by the bundle which will call the timeline
     * rendering.
     *
     * @param type $entity
     * @param type $context
     *
     * @return mixed[]
     *
     * @throws \LogicException if the context is not supported
     */
    public function getEntityTemplate($entity, $context, array $args);

    /**
     * Indicate if the result type may be handled by the service.
     *
     * @param string $type the key present in the SELECT query
     *
     * @return bool
     */
    public function supportsType($type);
}
