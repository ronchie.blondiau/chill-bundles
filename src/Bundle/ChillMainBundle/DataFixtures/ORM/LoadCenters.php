<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DataFixtures\ORM;

use Chill\MainBundle\Entity\Center;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadCenters extends AbstractFixture implements OrderedFixtureInterface
{
    public static $centers = [
        [
            'name' => 'Center A',
            'ref' => 'centerA',
        ],
        [
            'name' => 'Center B',
            'ref' => 'centerB',
        ],
    ];

    public static $refs = [];

    public function getOrder()
    {
        return 100;
    }

    public function load(ObjectManager $manager)
    {
        foreach (static::$centers as $new) {
            $center = new Center();
            $center->setName($new['name']);

            $manager->persist($center);
            $this->addReference($new['ref'], $center);
            static::$refs[] = $new['ref'];
        }

        $manager->flush();
    }
}
