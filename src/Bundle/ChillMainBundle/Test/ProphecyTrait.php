<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Test;

/**
 * a trait to prepare prophecy.
 *
 * **Usage : ** You must set up trait with `setUpTrait` before use
 * and use tearDownTrait after usage.
 *
 * @deprecated use @see{\Prophecy\PhpUnit\ProphecyTrait} instead
 *
 * @codeCoverageIgnore
 *
 * @deprecated use @class{Prophecy\PhpUnit\ProphecyTrait} instead
 */
trait ProphecyTrait
{
    /**
     * @var \Prophecy\Prophet()
     */
    private $prophet;

    /**
     * @return \Prophecy\Prophet
     */
    public function getProphet()
    {
        if (null === $this->prophet) {
            $this->prophet = new \Prophecy\Prophet();
        }

        return $this->prophet;
    }
}
