<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Extends choice to allow adding select2 library on widget.
 */
class Select2EntityType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->replaceDefaults(
            ['attr' => ['class' => 'select2 ']]
        );
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
