<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChillPhoneNumberType extends AbstractType
{
    private readonly string $defaultCarrierCode;

    private readonly PhoneNumberUtil $phoneNumberUtil;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->defaultCarrierCode = $parameterBag->get('chill_main')['phone_helper']['default_carrier_code'];
        $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('default_region', $this->defaultCarrierCode)
            ->setDefault('format', PhoneNumberFormat::NATIONAL)
            ->setDefault('type', \libphonenumber\PhoneNumberType::FIXED_LINE_OR_MOBILE);
    }

    public function getParent()
    {
        return PhoneNumberType::class;
    }
}
