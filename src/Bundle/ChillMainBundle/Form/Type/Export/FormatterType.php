<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type\Export;

use Chill\MainBundle\Export\ExportManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormatterType extends AbstractType
{
    /**
     * @var ExportManager
     */
    protected $exportManager;

    public function __construct(ExportManager $manager)
    {
        $this->exportManager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formatter = $this->exportManager->getFormatter($options['formatter_alias']);

        $formatter->buildForm(
            $builder,
            $options['export_alias'],
            $options['aggregator_aliases']
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['formatter_alias', 'export_alias',
            'aggregator_aliases', ]);
    }
}
