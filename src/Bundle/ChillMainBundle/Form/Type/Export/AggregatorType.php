<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type\Export;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AggregatorType extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exportManager = $options['export_manager'];
        $aggregator = $exportManager->getAggregator($options['aggregator_alias']);

        $builder
            ->add('enabled', CheckboxType::class, [
                'value' => true,
                'required' => false,
            ]);

        $filterFormBuilder = $builder->create('form', FormType::class, [
            'compound' => true,
            'required' => false,
            'error_bubbling' => false,
        ]);
        $aggregator->buildForm($filterFormBuilder);

        $builder->add($filterFormBuilder);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('aggregator_alias')
            ->setRequired('export_manager')
            ->setDefault('compound', true)
            ->setDefault('error_bubbling', false);
    }
}
