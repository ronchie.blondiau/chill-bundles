<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Display the date in a date picker.
 *
 * Extends the symfony `Symfony\Component\Form\Extension\Core\Type\DateType`
 * to automatically create a date picker.
 */
class ChillDateTimeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('date_widget', 'single_text')
            ->setDefault('time_widget', 'choice')
            ->setDefault('minutes', range(0, 59, 5))
            ->setDefault('hours', range(8, 22))
            ->setDefault('html5', true);
    }

    public function getParent()
    {
        return DateTimeType::class;
    }
}
