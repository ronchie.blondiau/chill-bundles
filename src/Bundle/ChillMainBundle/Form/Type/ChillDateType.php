<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Display the date in a date picker.
 *
 * Extends the symfony `Symfony\Component\Form\Extension\Core\Type\DateType`
 * to automatically create a date picker.
 */
class ChillDateType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('widget', 'single_text')
            ->setDefault('html5', true);
    }

    public function getParent()
    {
        return DateType::class;
    }
}
