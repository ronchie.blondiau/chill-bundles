<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Type;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Form\DataMapper\AddressDataMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A type to create/update Address entity.
 *
 * Options:
 *
 * - `has_valid_from` (boolean): show if an entry "has valid from" must be
 * shown.
 * - `null_if_empty` (boolean): replace the address type by null if the street
 * or the postCode is empty. This is useful when the address is not required and
 * embedded in another form.
 */
class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', TextType::class, [
                'required' => !$options['has_no_address'], // true if has no address is false
            ])
            ->add('streetNumber', TextType::class, [
                'required' => false,
            ])
            ->add('postCode', PostalCodeType::class, [
                'label' => 'Postal code',
                'placeholder' => 'Choose a postal code',
                'required' => !$options['has_no_address'], // true if has no address is false
            ]);

        if ($options['has_valid_from']) {
            $builder
                ->add(
                    'validFrom',
                    ChillDateType::class,
                    [
                        'required' => true,
                    ]
                );
        }

        if ($options['has_no_address']) {
            $builder
                ->add('isNoAddress', ChoiceType::class, [
                    'required' => true,
                    'choices' => [
                        'address.consider homeless' => true,
                        'address.real address' => false,
                    ],
                    'label' => 'address.address_homeless',
                ]);
        }

        if (true === $options['null_if_empty']) {
            $builder->setDataMapper(new AddressDataMapper());
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', Address::class)
            ->setDefined('has_valid_from')
            ->setAllowedTypes('has_valid_from', 'bool')
            ->setDefault('has_valid_from', true)
            ->setDefined('has_no_address')
            ->setDefault('has_no_address', false)
            ->setAllowedTypes('has_no_address', 'bool')
            ->setDefined('null_if_empty')
            ->setDefault('null_if_empty', false)
            ->setAllowedTypes('null_if_empty', 'bool');
    }
}
