<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Form\Type\ChillCollectionType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class NotificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Title',
                'required' => true,
            ])
            ->add('addressees', PickUserDynamicType::class, [
                'multiple' => true,
                'required' => false,
            ])
            ->add('message', ChillTextareaType::class, [
                'required' => false,
            ])
            ->add('addressesEmails', ChillCollectionType::class, [
                'label' => 'notification.dest by email',
                'help' => 'notification.dest by email help',
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => EmailType::class,
                'button_add_label' => 'notification.Add an email',
                'button_remove_label' => 'notification.Remove an email',
                'empty_collection_explain' => 'notification.Any email',
                'entry_options' => [
                    'constraints' => [
                        new NotNull(), new NotBlank(), new Email(),
                    ],
                    'label' => 'Email',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('class', Notification::class);
    }
}
