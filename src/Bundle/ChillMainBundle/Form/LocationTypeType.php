<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form;

use Chill\MainBundle\Entity\LocationType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

final class LocationTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TranslatableStringFormType::class,
            [
                'label' => 'Name',
            ]
        )
            ->add(
                'availableForUsers',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                ]
            )
            ->add(
                'editableByUsers',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                ]
            )
            ->add(
                'addressRequired',
                ChoiceType::class,
                [
                    'choices' => [
                        'optional' => LocationType::STATUS_OPTIONAL,
                        'required' => LocationType::STATUS_REQUIRED,
                        'never' => LocationType::STATUS_NEVER,
                    ],
                    'expanded' => true,
                ]
            )
            ->add(
                'contactData',
                ChoiceType::class,
                [
                    'choices' => [
                        'optional' => LocationType::STATUS_OPTIONAL,
                        'required' => LocationType::STATUS_REQUIRED,
                        'never' => LocationType::STATUS_NEVER,
                    ],
                    'expanded' => true,
                ]
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                ]
            )
            ->add(
                'defaultFor',
                ChoiceType::class,
                [
                    'choices' => [
                        'none' => null,
                        'person' => LocationType::DEFAULT_FOR_PERSON,
                        'thirdparty' => LocationType::DEFAULT_FOR_3PARTY,
                    ],
                    'expanded' => true,
                ]
            );
    }
}
