<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Form\Event;

use Symfony\Component\Form\FormBuilderInterface;

class CustomizeFormEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const NAME = 'chill_main.customize_form';

    public function __construct(protected string $type, protected FormBuilderInterface $builder)
    {
    }

    public function getBuilder(): FormBuilderInterface
    {
        return $this->builder;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
