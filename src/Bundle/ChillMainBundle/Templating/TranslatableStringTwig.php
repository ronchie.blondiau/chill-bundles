<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TranslatableStringTwig extends AbstractExtension
{
    use ContainerAwareTrait;

    /**
     * TranslatableStringTwig constructor.
     */
    public function __construct(private readonly TranslatableStringHelper $helper)
    {
    }

    /**
     * Returns a list of filters to add to the existing list.
     *
     * (non-PHPdoc)
     *
     * @see Twig_Extension::getFilters()
     */
    public function getFilters()
    {
        return [
            new TwigFilter(
                'localize_translatable_string',
                $this->localize(...)
            ), ];
    }

    /**
     * Returns the name of the extension.
     *
     * @return The name of the extension
     */
    public function getName()
    {
        return 'chill_main_localize';
    }

    public function localize(array $translatableStrings)
    {
        return $this->helper
            ->localize($translatableStrings);
    }
}
