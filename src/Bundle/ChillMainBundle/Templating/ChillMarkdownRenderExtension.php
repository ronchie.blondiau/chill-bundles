<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Render markdown.
 */
final class ChillMarkdownRenderExtension extends AbstractExtension
{
    /**
     * @var \Parsedown
     */
    protected $parsedown;

    public function __construct()
    {
        $this->parsedown = new \Parsedown();
        $this->parsedown->setSafeMode(true);
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('chill_markdown_to_html', $this->renderMarkdownToHtml(...), [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderMarkdownToHtml(?string $var): string
    {
        return $this->parsedown->parse((string) $var);
    }
}
