<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\UI;

use Symfony\Component\Security\Core\User\UserInterface;

interface NotificationCounterInterface
{
    /**
     * Add a number of notification.
     */
    public function addNotification(UserInterface $u): int;
}
