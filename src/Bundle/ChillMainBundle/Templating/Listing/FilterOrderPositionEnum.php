<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Templating\Listing;

enum FilterOrderPositionEnum: string
{
    case SearchBox = 'search_box';
    case Checkboxes = 'checkboxes';
    case DateRange = 'date_range';
    case EntityChoice = 'entity_choice';
    case SingleCheckbox = 'single_checkbox';
    case UserPicker = 'user_picker';
}
