<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\Exception\HandlerNotFoundException;
use Symfony\Component\Workflow\Registry;

class EntityWorkflowManager
{
    /**
     * @param \Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface[] $handlers
     */
    public function __construct(private readonly iterable $handlers, private readonly Registry $registry)
    {
    }

    public function getHandler(EntityWorkflow $entityWorkflow, array $options = []): EntityWorkflowHandlerInterface
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($entityWorkflow, $options)) {
                return $handler;
            }
        }

        throw new HandlerNotFoundException();
    }

    public function getSupportedWorkflows(EntityWorkflow $entityWorkflow): array
    {
        return $this->registry->all($entityWorkflow);
    }
}
