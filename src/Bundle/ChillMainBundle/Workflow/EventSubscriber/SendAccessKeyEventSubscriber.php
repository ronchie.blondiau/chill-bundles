<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Workflow\EntityWorkflowStep;
use Chill\MainBundle\Workflow\EntityWorkflowManager;
use Chill\MainBundle\Workflow\Helper\MetadataExtractor;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Workflow\Registry;

class SendAccessKeyEventSubscriber
{
    public function __construct(private readonly \Twig\Environment $engine, private readonly MetadataExtractor $metadataExtractor, private readonly Registry $registry, private readonly EntityWorkflowManager $entityWorkflowManager, private readonly MailerInterface $mailer)
    {
    }

    public function postPersist(EntityWorkflowStep $step): void
    {
        $entityWorkflow = $step->getEntityWorkflow();

        $place = $this->metadataExtractor->buildArrayPresentationForPlace($entityWorkflow);
        $workflow = $this->metadataExtractor->buildArrayPresentationForWorkflow(
            $this->registry->get($entityWorkflow, $entityWorkflow->getWorkflowName())
        );
        $handler = $this->entityWorkflowManager->getHandler($entityWorkflow);

        foreach ($entityWorkflow->futureDestEmails as $emailAddress) {
            $context = [
                'entity_workflow' => $entityWorkflow,
                'dest' => $emailAddress,
                'place' => $place,
                'workflow' => $workflow,
                'entityTitle' => $handler->getEntityTitle($entityWorkflow),
            ];

            $email = new Email();
            $email
                ->addTo($emailAddress)
                ->subject($this->engine->render('@ChillMain/Workflow/workflow_send_access_key_title.fr.txt.twig', $context))
                ->text($this->engine->render('@ChillMain/Workflow/workflow_send_access_key.fr.txt.twig', $context));

            $this->mailer->send($email);
        }
    }
}
