<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\EventSubscriber;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Workflow\Helper\MetadataExtractor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Registry;

class NotificationOnTransition implements EventSubscriberInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly \Twig\Environment $engine, private readonly MetadataExtractor $metadataExtractor, private readonly Security $security, private readonly Registry $registry)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.completed' => ['onCompletedSendNotification', 2048],
        ];
    }

    /**
     * Send a notification to:.
     *
     * * the dests of the new step;
     * * the users which subscribed to workflow, on each step, or on final
     *
     * **Warning** take care that this method must be executed **after** the dest users are added to
     * the step (@see{EntityWorkflowStep::addDestUser}). Currently, this is done during
     *
     * @see{EntityWorkflowTransitionEventSubscriber::addDests}.
     */
    public function onCompletedSendNotification(Event $event): void
    {
        if (!$event->getSubject() instanceof EntityWorkflow) {
            return;
        }

        /** @var EntityWorkflow $entityWorkflow */
        $entityWorkflow = $event->getSubject();

        /** @var array<string, User> $dests array of unique values, where keys is the object's hash */
        $dests = [];

        foreach (array_merge(
            // the subscriber to each step
            $entityWorkflow->getSubscriberToStep()->toArray(),
            // the subscriber to final, only if final
            $entityWorkflow->isFinal() ? $entityWorkflow->getSubscriberToFinal()->toArray() : [],
            // the dests for the current step
            $entityWorkflow->getCurrentStep()->getDestUser()->toArray(),
            // the cc users for the current step
            $entityWorkflow->getCurrentStep()->getCcUser()->toArray()
        ) as $dest) {
            $dests[spl_object_hash($dest)] = $dest;
        }

        $place = $this->metadataExtractor->buildArrayPresentationForPlace($entityWorkflow);
        $workflow = $this->metadataExtractor->buildArrayPresentationForWorkflow(
            $this->registry->get($entityWorkflow, $entityWorkflow->getWorkflowName())
        );

        foreach ($dests as $subscriber) {
            if (
                $this->security->getUser() === $subscriber
            ) {
                continue;
            }

            $context = [
                'entity_workflow' => $entityWorkflow,
                'dest' => $subscriber,
                'place' => $place,
                'workflow' => $workflow,
                'is_dest' => \in_array($subscriber->getId(), array_map(static fn (User $u) => $u->getId(), $entityWorkflow->futureDestUsers), true),
            ];

            $notification = new Notification();
            $notification
                ->setRelatedEntityId($entityWorkflow->getId())
                ->setRelatedEntityClass(EntityWorkflow::class)
                ->setTitle($this->engine->render('@ChillMain/Workflow/workflow_notification_on_transition_completed_title.fr.txt.twig', $context))
                ->setMessage($this->engine->render('@ChillMain/Workflow/workflow_notification_on_transition_completed_content.fr.txt.twig', $context))
                ->addAddressee($subscriber);
            $this->entityManager->persist($notification);
        }
    }
}
