<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow\Validator;

use Symfony\Component\Validator\Constraint;

class StepDestValid extends Constraint
{
    public string $messageDestNotAllowed = 'workflow.As the step is final, no dest are allowed';

    public string $messageRequireDest = 'workflow.As the step is not final, dest are required';

    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }
}
