<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Symfony\Component\Workflow\SupportStrategy\WorkflowSupportStrategyInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class RelatedEntityWorkflowSupportsStrategy implements WorkflowSupportStrategyInterface
{
    public function supports(WorkflowInterface $workflow, $subject): bool
    {
        if (!$subject instanceof EntityWorkflow) {
            return false;
        }

        foreach ($workflow->getMetadataStore()->getWorkflowMetadata()['related_entity'] as $relatedEntityClass) {
            if ($subject->getRelatedEntityClass() === $relatedEntityClass) {
                return true;
            }
        }

        return false;
    }
}
