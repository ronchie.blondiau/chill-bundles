<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Entity\Country;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Intl\Countries;

class LoadCountriesCommand extends Command
{
    /**
     * LoadCountriesCommand constructor.
     */
    public function __construct(private readonly EntityManager $entityManager, private $availableLanguages)
    {
        parent::__construct();
    }

    public static function prepareCountryList($languages)
    {
        $countryCodes = Countries::getCountryCodes();
        $countryEntities = [];

        foreach ($countryCodes as $code) {
            $names = [];

            foreach ($languages as $language) {
                $names[$language] = Countries::getName($code, $language);
            }

            $country = new Country();
            $country->setName($names)->setCountryCode($code);
            $countryEntities[] = $country;
        }

        return $countryEntities;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('chill:main:countries:populate')
            ->setDescription('Load or update countries in db. This command does not delete existing countries, '.
                'but will update names according to available languages');
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $countries = static::prepareCountryList($this->availableLanguages);
        $em = $this->entityManager;

        foreach ($countries as $country) {
            $countryStored = $em->getRepository(Country::class)
                ->findOneBy(['countryCode' => $country->getCountryCode()]);

            if (null === $countryStored) {
                $em->persist($country);
            } else {
                $countryStored->setName($country->getName());
            }
        }

        $em->flush();

        return 0;
    }
}
