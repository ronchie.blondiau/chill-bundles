<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Service\Import\AddressReferenceBEFromBestAddress;
use Chill\MainBundle\Service\Import\PostalCodeBEFromBestAddress;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadAddressesBEFromBestAddressCommand extends Command
{
    public function __construct(
        private readonly AddressReferenceBEFromBestAddress $addressImporter,
        private readonly PostalCodeBEFromBestAddress $postalCodeBEFromBestAddressImporter
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chill:main:address-ref-from-best-addresses')
            ->addArgument('lang', InputArgument::REQUIRED, "Language code, for example 'fr'")
            ->addArgument('list', InputArgument::IS_ARRAY, "The list to add, for example 'full', or 'extract' (dev) or '1xxx' (brussel CP)")
            ->setDescription('Import BE addresses from BeST Address (see https://osoc19.github.io/best/)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->postalCodeBEFromBestAddressImporter->import();

        $this->addressImporter->import($input->getArgument('lang'), $input->getArgument('list'));

        return 0;
    }
}
