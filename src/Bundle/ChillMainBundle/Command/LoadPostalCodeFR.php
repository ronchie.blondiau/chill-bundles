<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Service\Import\PostalCodeFRFromOpenData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadPostalCodeFR extends Command
{
    public function __construct(private readonly PostalCodeFRFromOpenData $loader)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this->setName('chill:main:postal-code:load:FR')
            ->setDescription('Load France\'s postal code from online open data');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->loader->import();

        return 0;
    }
}
