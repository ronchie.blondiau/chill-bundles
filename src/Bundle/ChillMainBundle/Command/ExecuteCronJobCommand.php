<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Command;

use Chill\MainBundle\Cron\CronManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteCronJobCommand extends Command
{
    public function __construct(
        private readonly CronManagerInterface $cronManager
    ) {
        parent::__construct('chill:cron-job:execute');
    }

    protected function configure()
    {
        $this
            ->setDescription('Execute the cronjob(s) given as argument, or one cronjob scheduled by system.')
            ->setHelp("If no job is specified, the next available cronjob will be executed by system.\nThis command should be execute every 15 minutes (more or less)")
            ->addArgument('job', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'one or more job to force execute (by default, all jobs are executed)', [])
            ->addUsage('');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ([] === $input->getArgument('job')) {
            $this->cronManager->run();

            return 0;
        }

        foreach ($input->getArgument('job') as $jobName) {
            $this->cronManager->run($jobName);
        }

        return 0;
    }
}
