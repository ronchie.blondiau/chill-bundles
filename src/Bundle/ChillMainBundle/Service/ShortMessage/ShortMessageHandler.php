<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\ShortMessage;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @AsMessageHandler
 */
class ShortMessageHandler implements MessageHandlerInterface
{
    public function __construct(private readonly ShortMessageTransporterInterface $messageTransporter)
    {
    }

    public function __invoke(ShortMessage $message): void
    {
        $this->messageTransporter->send($message);
    }
}
