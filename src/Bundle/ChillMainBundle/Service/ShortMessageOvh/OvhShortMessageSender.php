<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Service\ShortMessageOvh;

use Chill\MainBundle\Service\ShortMessage\ShortMessage;
use Chill\MainBundle\Service\ShortMessage\ShortMessageSenderInterface;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Ovh\Api;
use Psr\Log\LoggerInterface;

class OvhShortMessageSender implements ShortMessageSenderInterface
{
    public function __construct(
        private readonly Api $api,
        // for DI, must remains as first argument
        private readonly string $serviceName,
        // for di, must remains as second argument
        private readonly string $sender,
        // for DI, must remains as third argument
        private readonly LoggerInterface $logger,
        private readonly PhoneNumberUtil $phoneNumberUtil
    ) {
    }

    public function send(ShortMessage $shortMessage): void
    {
        $receiver = $this->phoneNumberUtil->format($shortMessage->getPhoneNumber(), PhoneNumberFormat::E164);

        $response = $this->api->post(
            strtr('/sms/{serviceName}/jobs', ['{serviceName}' => $this->serviceName]),
            [
                'message' => $shortMessage->getContent(),
                'receivers' => [$receiver],
                'sender' => $this->sender,
                'noStopClause' => true,
                'coding' => '7bit',
                'charset' => 'UTF-8',
                'priority' => $shortMessage->getPriority(),
            ]
        );

        $improved = array_merge([
            'validReceiversI' => implode(',', $response['validReceivers']),
            'idsI' => implode(',', $response['ids']),
        ], $response);

        $this->logger->warning('[sms] a sms was sent', $improved);
    }
}
