import 'es6-promise/auto';
import { createStore } from 'vuex';

const debug = process.env.NODE_ENV !== 'production';

const store = createStore({
    strict: debug,
    state: {},
    getters: {},
    mutations: {},
    actions: {},
});

export { store };
