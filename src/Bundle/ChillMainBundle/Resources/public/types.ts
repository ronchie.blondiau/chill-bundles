export interface DateTime {
  datetime: string;
  datetime8601: string
}

export interface Civility {
  id: number;
  // TODO
}

export interface Job {
  id: number;
  type: "user_job";
  label: {
    "fr": string; // could have other key. How to do that in ts ?
  }
}

export interface Center {
  id: number;
  type: "center";
  name: string;
}

export interface Scope {
  id: number;
  type: "scope";
  name: {
    "fr": string
  }
}

export interface User {
  type: "user";
  id: number;
  username: string;
  text: string;
  text_without_absence: string;
  email: string;
  user_job: Job;
  label: string;
  // todo: mainCenter; mainJob; etc..
}

export interface UserAssociatedInterface {
  type: "user";
  id: number;
};

export type TranslatableString = {
  fr?: string;
  nl?: string;
}

export interface Postcode {
  id: number;
  name: string;
  code: string;
  center: Point;
}

export type Point = {
  type: "Point";
  coordinates: [lat: number, lon: number];
}

export interface Country {
  id: number;
  name: TranslatableString;
  code: string;
}

export type AddressRefStatus = 'match'|'to_review'|'reviewed';

export interface Address {
  type: "address";
  address_id: number;
  text: string;
  street: string;
  streetNumber: string;
  postcode: Postcode;
  country: Country;
  floor: string | null;
  corridor: string | null;
  steps: string | null;
  flat: string | null;
  buildingName: string | null;
  distribution: string | null;
  extra: string | null;
  confidential: boolean;
  lines: string[];
  addressReference: AddressReference | null;
  validFrom: DateTime;
  validTo: DateTime | null;
  point: Point | null;
  refStatus: AddressRefStatus;
  isNoAddress: boolean;
}

export interface AddressWithPoint extends Address {
  point: Point
}

export interface AddressReference {
  id: number;
  createdAt: DateTime | null;
  deletedAt: DateTime | null;
  municipalityCode: string;
  point: Point;
  postcode: Postcode;
  refId: string;
  source: string;
  street: string;
  streetNumber: string;
  updatedAt: DateTime | null;
}

export interface SimpleGeographicalUnit {
  id: number;
  layerId: number;
  unitName: string;
  unitRefId: string;
}

export interface GeographicalUnitLayer {
  id: number;
  name: TranslatableString;
  refId: string;
}

export interface Location {
  type: "location";
  id: number;
  active: boolean;
  address: Address | null;
  availableForUsers: boolean;
  createdAt: DateTime | null;
  createdBy: User | null;
  updatedAt: DateTime | null;
  updatedBy: User | null;
  email: string | null
  name: string;
  phonenumber1: string | null;
  phonenumber2: string | null;
  locationType: LocationType;
}

export interface LocationAssociated {
  type: "location";
  id: number;
}

export interface LocationType {
  type: "location-type";
  id: number;
  active: boolean;
  addressRequired: "optional" | "required";
  availableForUsers: boolean;
  editableByUsers: boolean;
  contactData: "optional" | "required";
  title: TranslatableString;
}

export interface NewsItemType {
    id: number;
    title: string;
    content: string;
    startDate: DateTime;
    endDate: DateTime | null;
}
