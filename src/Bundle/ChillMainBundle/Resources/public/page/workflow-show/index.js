import {ShowHide} from 'ChillMainAssets/lib/show_hide/show_hide.js';

window.addEventListener('DOMContentLoaded', function() {
    let
        divTransitions = document.querySelector('#transitions'),
        futureDestUsersContainer = document.querySelector('#futureDests')
    ;

    if (null !== divTransitions) {
        new ShowHide({
            load_event: null,
            froms: [divTransitions],
            container: [futureDestUsersContainer],
            test: function(divs, arg2, arg3) {
                for (let div of divs) {
                    for (let input of div.querySelectorAll('input')) {
                        if (input.checked) {
                            if (input.dataset.toFinal === "1") {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
                }

                return true;
            },
        });
    }

    let
        transitionFilterContainer = document.querySelector('#transitionFilter'),
        transitions = document.querySelector('#transitions')
    ;

    if (null !== transitionFilterContainer) {
        transitions.querySelectorAll('.form-check').forEach(function(row) {

            const isForward = row.querySelector('input').dataset.isForward;

            new ShowHide({
                load_event: null,
                froms: [transitionFilterContainer],
                container: row,
                test: function (containers) {
                    for (let container of containers) {
                        for (let input of container.querySelectorAll('input')) {
                            if (input.checked) {
                                return isForward === input.value;
                            }
                        }
                    }
                },
                toggle_callback: function (c, dir) {
                    for (let div of c) {
                        let input = div.querySelector('input');
                        if ('hide' === dir) {
                            input.checked = false;
                            input.disabled = true;
                        } else {
                            input.disabled = false;
                        }
                    }
                },
            });
        });
    }

});
