<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\Loader;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;

/**
 * Import routes from bundles.
 *
 * Routes must be defined in configuration, add an entry
 * under `chill_main.routing.resources`
 */
class ChillRoutesLoader extends Loader
{
    public function __construct(private readonly array $routes)
    {
        parent::__construct();
    }

    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();

        foreach ($this->routes as $routeResource) {
            $collection->addCollection(
                $this->import($routeResource, null)
            );
        }

        return $collection;
    }

    public function supports($resource, $type = null)
    {
        return 'chill_routes' === $type;
    }
}
