<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminNewsMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('admin.dashboard.title', [
            'route' => 'chill_main_dashboard_admin',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 9000,
            ]);

        $menu->addChild('admin.dashboard.news', [
            'route' => 'chill_crud_news_item_index',
        ])->setExtras(['order' => 9000]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_news_item'];
    }
}
