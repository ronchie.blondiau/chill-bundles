<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Notification\Counter\NotificationByUserCounter;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\MainBundle\Workflow\Counter\WorkflowByUserCounter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private readonly NotificationByUserCounter $notificationByUserCounter, private readonly WorkflowByUserCounter $workflowByUserCounter, private readonly Security $security, private readonly TranslatorInterface $translator, protected ParameterBagInterface $parameterBag, private readonly RequestStack $requestStack)
    {
    }

    public function buildMenu($menuId, \Knp\Menu\MenuItem $menu, array $parameters)
    {
        $user = $this->security->getUser();

        if ($user instanceof User) {
            $menu->addChild($this->translator->trans('user.profile.title'), [
                'route' => 'chill_main_user_profile',
            ])
                ->setExtras([
                    'order' => -11_111_111,
                    'icon' => 'user',
                ]);

            if (null !== $user->getCurrentLocation()) {
                $locationTextMenu = $user->getCurrentLocation()->getName();
            } else {
                $locationTextMenu = 'Set a location';
            }

            $menu
                ->addChild(
                    $locationTextMenu,
                    [
                        'route' => 'chill_main_user_currentlocation_edit',
                        'routeParameters' => [
                            'returnPath' => $this->requestStack->getCurrentRequest()->getRequestUri(),
                        ],
                    ]
                )
                ->setExtras([
                    'order' => -9_999_999,
                    'icon' => 'map-marker',
                ]);

            $nbNotifications = $this->notificationByUserCounter->countUnreadByUser($user);

            // TODO add an icon? How exactly? For example a clock icon...
            $menu
                ->addChild($this->translator->trans('absence.Set absence date'), [
                    'route' => 'chill_main_user_absence_index',
                ])
                ->setExtras([
                    'order' => -8_888_888,
                ]);

            $menu
                ->addChild(
                    $this->translator->trans('notification.My notifications with counter', ['nb' => $nbNotifications]),
                    ['route' => 'chill_main_notification_my']
                )
                ->setExtras([
                    'order' => 600,
                    'icon' => 'envelope',
                    'counter' => $nbNotifications,
                ]);

            $workflowCount = $this->workflowByUserCounter->getCountUnreadByUser($user);

            $menu
                ->addChild(
                    $this->translator->trans('workflow.My workflows with counter', ['wc' => $workflowCount]),
                    ['route' => 'chill_main_workflow_list_dest']
                )
                ->setExtras([
                    'order' => 700,
                ]);

            if ($this->parameterBag->get('chill_main.access_user_change_password')) {
                $menu
                    ->addChild(
                        'Change password',
                        ['route' => 'change_my_password']
                    )
                    ->setExtras([
                        'order' => 99_999_999_998,
                    ]);
            }
        }

        $menu
            ->addChild(
                'Logout',
                [
                    'route' => 'logout',
                ]
            )
            ->setExtras([
                'order' => 99_999_999_999,
                'icon' => 'power-off',
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['user'];
    }
}
