<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\MainBundle\Security\Authorization\ChillExportVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SectionMenuBuilder.
 */
class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * SectionMenuBuilder constructor.
     */
    public function __construct(protected AuthorizationCheckerInterface $authorizationChecker, protected TranslatorInterface $translator, protected ParameterBagInterface $parameterBag)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        $menu->addChild($this->translator->trans('Homepage'), [
            'route' => 'chill_main_homepage',
        ])
            ->setExtras([
                'icons' => ['home'],
                'order' => 0,
            ]);

        if ($this->parameterBag->get('chill_main.access_global_history')) {
            $menu->addChild($this->translator->trans('Global timeline'), [
                'route' => 'chill_center_timeline',
            ])
                ->setExtras(
                    [
                        'order' => 10,
                    ]
                );
        }

        if ($this->authorizationChecker->isGranted(ChillExportVoter::EXPORT)) {
            $menu->addChild($this->translator->trans('Export Menu'), [
                'route' => 'chill_main_export_index',
            ])
                ->setExtras([
                    'icons' => ['upload'],
                    'order' => 20,
                ]);
        }

        $menu->addChild($this->translator->trans('news.menu'), [
            'route' => 'chill_main_news_items_history',
        ])
            ->setExtras([
                'icons' => ['newspaper-o'],
                'order' => 5,
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
