<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminUserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        protected ParameterBagInterface $parameterBag
    ) {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        // all the entries below must have ROLE_ADMIN permissions
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('Users and permissions', [
            'route' => 'chill_main_user_admin',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 1000,
            ]);

        $menu->addChild('Center list', [
            'route' => 'chill_crud_center_index',
        ])->setExtras(['order' => 1010]);

        $menu->addChild('Regroupements des centres', [
            'route' => 'chill_crud_regroupment_index',
        ])->setExtras(['order' => 1015]);

        $menu->addChild('List circles', [
            'route' => 'admin_scope',
        ])->setExtras(['order' => 1020]);

        if ($this->parameterBag->get('chill_main.access_permissions_group_list')) {
            $menu->addChild('Permissions group list', [
                'route' => 'admin_permissionsgroup',
            ])->setExtras(['order' => 1030]);
        }

        $menu->addChild('crud.admin_user.index.title', [
            'route' => 'chill_crud_admin_user_index',
        ])->setExtras(['order' => 1040]);

        $menu->addChild('User jobs', [
            'route' => 'chill_crud_admin_user_job_index',
        ])->setExtras(['order' => 1050]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_user'];
    }
}
