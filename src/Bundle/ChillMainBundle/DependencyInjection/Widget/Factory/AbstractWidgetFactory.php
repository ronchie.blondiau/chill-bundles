<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\Widget\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Allow to easily create WidgetFactory.
 *
 * Extending this factory, the widget will be created using the already defined
 * service created "as other services" in your configuration (the most frequent
 * way is using `services.yml` file.
 *
 * If you need to create different service based upon place, position or
 * definition, you should implements directly
 * `Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface`
 */
abstract class AbstractWidgetFactory implements WidgetFactoryInterface
{
    /**
     * {@inheritdoc}
     *
     * Will create the definition by returning the definition from the `services.yml`
     * file (or `services.xml` or `what-you-want.yml`).
     *
     * @see \Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface::createDefinition()
     */
    public function createDefinition(ContainerBuilder $containerBuilder, $place, $order, array $config)
    {
        return $containerBuilder->getDefinition(
            $this
                ->getServiceId($containerBuilder, $place, $order, $config)
        );
    }
}
