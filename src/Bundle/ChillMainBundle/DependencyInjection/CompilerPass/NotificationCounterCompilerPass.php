<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Chill\MainBundle\Templating\UI\CountNotificationUser;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class NotificationCounterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(CountNotificationUser::class)) {
            throw new \LogicException('The service '.CountNotificationUser::class.' should be defined');
        }

        $notificationCounterDefinition = $container->getDefinition(CountNotificationUser::class);

        foreach ($container->findTaggedServiceIds('chill.count_notification.user') as $id => $tags) {
            $notificationCounterDefinition
                ->addMethodCall('addNotificationCounter', [new Reference($id)]);
        }
    }
}
