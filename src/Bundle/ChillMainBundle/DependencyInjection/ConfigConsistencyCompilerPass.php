<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Description of ConfigConsistencyCompilerPass.
 */
class ConfigConsistencyCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $availableLanguages = $container
            ->getParameter('chill_main.available_languages');
        $methodCallsTranslator = $container
            ->findDefinition('translator.default')
            ->getMethodCalls();

        $fallbackLocales = [];

        foreach ($methodCallsTranslator as $call) {
            if ('setFallbackLocales' === $call[0]) {
                $fallbackLocales = array_merge(
                    $fallbackLocales,
                    $call[1][0]
                );
            }
        }

        if (0 === \count($fallbackLocales)) {
            throw new \LogicException('the fallback locale are not defined. The framework config should not allow this.');
        }

        $diff = array_diff($fallbackLocales, $availableLanguages);

        if (\count($diff) > 0) {
            throw new \RuntimeException(sprintf('The chill_main.available_languages parameter does not contains fallback locales. The languages %s are missing.', implode(', ', $diff)));
        }
    }
}
