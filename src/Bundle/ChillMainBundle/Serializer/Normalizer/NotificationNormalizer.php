<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Normalizer;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class NotificationNormalizer implements NormalizerAwareInterface, NormalizerInterface
{
    use NormalizerAwareTrait;

    public function __construct(private readonly NotificationHandlerManager $notificationHandlerManager, private readonly EntityManagerInterface $entityManager, private readonly Security $security)
    {
    }

    /**
     * @param Notification $object
     *
     * @return array|\ArrayObject|bool|float|int|string|void|null
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $entity = $this->entityManager
            ->getRepository($object->getRelatedEntityClass())
            ->find($object->getRelatedEntityId());

        return [
            'type' => 'notification',
            'id' => $object->getId(),
            'addressees' => $this->normalizer->normalize($object->getAddressees(), $format, $context),
            'date' => $this->normalizer->normalize($object->getDate(), $format, $context),
            'isRead' => $object->isReadBy($this->security->getUser()),
            'message' => $object->getMessage(),
            'relatedEntityClass' => $object->getRelatedEntityClass(),
            'relatedEntityId' => $object->getRelatedEntityId(),
            'sender' => $this->normalizer->normalize($object->getSender(), $format, $context),
            'title' => $object->getTitle(),
            'entity' => null !== $entity ? $this->normalizer->normalize($entity, $format, $context) : null,
        ];
    }

    public function supportsNormalization($data, ?string $format = null)
    {
        return $data instanceof Notification && 'json' === $format;
    }
}
