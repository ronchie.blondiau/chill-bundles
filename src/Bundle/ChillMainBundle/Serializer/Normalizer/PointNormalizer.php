<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Serializer\Normalizer;

use Chill\MainBundle\Doctrine\Model\Point;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PointNormalizer implements DenormalizerInterface
{
    public function denormalize($data, $type, $format = null, array $context = [])
    {
        if (!\is_array($data)) {
            throw new InvalidArgumentException('point data is not an array. It should be an array of 2 coordinates.');
        }

        if (2 !== \count($data)) {
            throw new InvalidArgumentException('point data is not an array of 2 elements. It should be an array of 2 coordinates.');
        }

        return Point::fromLonLat($data[0], $data[1]);
    }

    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return Point::class === $type;
    }
}
