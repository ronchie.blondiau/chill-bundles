<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

/**
 * Interface to provide export elements dynamically.
 *
 * The typical use case is providing exports or aggregators depending on
 * dynamic data. Example: providing exports for reports, reports depending
 * on data stored in database.
 */
interface ExportElementsProviderInterface
{
    /**
     * @return ExportElementInterface[]
     */
    public function getExportElements();
}
