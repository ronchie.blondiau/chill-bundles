<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\MainBundle\Export;

use Doctrine\ORM\QueryBuilder;

/**
 * Modifiers modify the export's query.
 *
 * Known subclasses : AggregatorInterface and FilterInterface
 */
interface ModifierInterface extends ExportElementInterface
{
    /**
     * The role required for executing this Modifier.
     *
     * If null, will used the ExportInterface::requiredRole role from
     * the current executing export.
     *
     * @return string|null A role required to execute this ModifiersInterface
     */
    public function addRole(): ?string;

    /**
     * Alter the query initiated by the export, to add the required statements
     * (`GROUP BY`, `SELECT`, `WHERE`).
     *
     * @param QueryBuilder $qb   the QueryBuilder initiated by the Export (and eventually modified by other Modifiers)
     * @param mixed[]      $data the data from the Form (builded by buildForm)
     */
    public function alterQuery(QueryBuilder $qb, $data);

    /**
     * On which type of Export this ModifiersInterface may apply.
     *
     * @return string the type on which the Modifiers apply
     */
    public function applyOn();
}
