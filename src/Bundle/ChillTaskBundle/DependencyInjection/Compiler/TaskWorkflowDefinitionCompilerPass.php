<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\DependencyInjection\Compiler;

use Chill\TaskBundle\Event\Lifecycle\TaskLifecycleEvent;
use Chill\TaskBundle\Templating\UI\CountNotificationTask;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class TaskWorkflowDefinitionCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(TaskWorkflowManager::class)) {
            throw new \LogicException('The service '.TaskWorkflowManager::class.' is not registered');
        }

        $workflowManagerDefinition = $container->getDefinition(TaskWorkflowManager::class);
        $counterDefinition = $container->getDefinition(CountNotificationTask::class);
        $lifecycleDefinition = $container->getDefinition(TaskLifecycleEvent::class);

        foreach ($container->findTaggedServiceIds('chill_task.workflow_definition') as $id => $tags) {
            // registering the definition to manager
            $workflowManagerDefinition
                ->addMethodCall('addDefinition', [new Reference($id)]);
            // adding a listener for currentStatus changes
            $definition = $container->getDefinition($id);
            $workflowManagerDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.entered', $definition->getClass()::getAssociatedWorkflowName()),
                    'method' => 'onTaskStateEntered',
                    'priority' => -255,
                ]);
            $counterDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.entered', $definition->getClass()::getAssociatedWorkflowName()),
                    'method' => 'resetCacheOnNewStates',
                    'priority' => 0,
                ]);
            $lifecycleDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.transition', $definition->getClass()::getAssociatedWorkflowName()),
                    'method' => 'onTransition',
                    'priority' => 0,
                ]);
        }
    }
}
