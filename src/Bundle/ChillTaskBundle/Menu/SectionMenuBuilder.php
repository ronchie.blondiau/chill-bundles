<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    public $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (false === $this->authorizationChecker->isGranted(TaskVoter::SHOW)) {
            return;
        }

        $menu->addChild(
            $this->translator->trans('Tasks'),
            [
                'route' => 'chill_task_singletask_list', [
                    'routeParameters' => [
                        'hide_form' => false,
                    ],
                ], ]
        )
            ->setExtras([
                'order' => 50,
                'icon' => 'exclamation-triangle',
                'entryclass' => 'user_menu__entry--warning-entry',
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
