<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\TaskBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Chill\TaskBundle\Templating\UI\CountNotificationTask;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    public $authorizationChecker;

    /**
     * @var CountNotificationTask
     */
    public $counter;

    /**
     * @var TokenStorageInterface
     */
    public $tokenStorage;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    public function __construct(
        CountNotificationTask $counter,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->counter = $counter;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (false === $this->authorizationChecker->isGranted(TaskVoter::SHOW)) {
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();
        $ended = $this->counter->countNotificationEnded($user);
        $warning = $this->counter->countNotificationWarning($user);

        if (0 < $ended) {
            $this->addItemInMenu(
                $menu,
                'nb tasks over deadline',
                $ended,
                -15,
                ['new', 'in_progress'],
                ['alert']
            );
        }

        if (0 < $warning) {
            $this->addItemInMenu(
                $menu,
                'nb tasks near deadline',
                $warning,
                -14,
                ['new', 'in_progress'],
                ['warning']
            );
        }

        $menu->addChild('My tasks', [
            'route' => 'chill_task_singletask_my_tasks',
        ])
            ->setExtras([
                'order' => -10,
                'icon' => 'tasks',
            ]);
    }

    public static function getMenuIds(): array
    {
        return ['user'];
    }

    protected function addItemInMenu(MenuItem $menu, $message, $number, $order, array $states = [], array $status = [])
    {
        if (0 < $number) {
            $menu->addChild(
                $this->translator->trans($message, ['nb' => $number]),
                [
                    'route' => 'chill_task_singletask_my_tasks',
                    'routeParameters' => [
                        'f' => [
                            'checkboxes' => [
                                'states' => $states,
                                'status' => $status,
                            ],
                        ],
                    ],
                ]
            )
                ->setExtras([
                    'order' => $order,
                    'icon' => 'exclamation-triangle',
                    'entryclass' => 'user_menu__entry--warning-entry',
                ]);
        }
    }
}
