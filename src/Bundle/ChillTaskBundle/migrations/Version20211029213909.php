<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Task;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211029213909 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_task.by_end_date');
    }

    public function getDescription(): string
    {
        return 'Add index for task state and end date';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX by_end_date ON chill_task.single_task (end_date DESC NULLS FIRST)');
    }
}
