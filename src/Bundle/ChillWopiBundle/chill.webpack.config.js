// this file loads all assets from the Chill person bundle
module.exports = function(encore, entries) {
    encore.addEntry('page_wopi_editor', __dirname + '/src/Resources/public/page/editor/index.js');
    encore.addEntry('mod_reload_page', __dirname + '/src/Resources/public/module/pending/index');
};
