<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Chill\WopiBundle\Controller\Editor;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routes) {
    $routes
        ->add('chill_wopi_file_edit', '/edit/{fileId}')
        ->controller(Editor::class);

    $routes
        ->add('chill_wopi_object_convert', '/convert/{uuid}')
        ->controller(Chill\WopiBundle\Controller\Convert::class);
};
