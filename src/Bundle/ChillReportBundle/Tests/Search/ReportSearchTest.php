<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Tests\Search;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test for report search.
 *
 * @internal
 *
 * @coversNothing
 */
final class ReportSearchTest extends WebTestCase
{
    public function testNamedSearch()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', [
            'q' => '@report '.(new \DateTime('tomorrow'))->format('Y-m-d'), // there should be any result for future. And future is tomorrow
            'name' => 'report',
        ]);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testSearchByDate()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', [
            'q' => '@report date:2015-01-05',
        ]);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertMatchesRegularExpression('/Situation de logement/i', $crawler->text());
    }

    public function testSearchDoubleDate()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', [
            'q' => '@report date:2014-05-01 2014-05-06',
        ]);

        $this->assertGreaterThan(0, $crawler->filter('.error')->count());
    }

    public function testSearchEmtpy()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', [
            'q' => '@report ',
        ]);

        $this->assertGreaterThan(0, $crawler->filter('.error')->count());
    }

    public function testSearchExpectedDefault()
    {
        $client = $this->getAuthenticatedClient();

        $crawler = $client->request('GET', '/fr/search', [
            'q' => '@report 2015-01-05',
        ]);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertMatchesRegularExpression('/Situation de logement/i', $crawler->text());
    }

    /**
     * Test that the user do not see unauthorized results.
     *
     * We test for that that :
     * - we do not see any unauthorized scope mention
     */
    public function testUsersDoNotSeeUnauthorizedResults()
    {
        $clientSocial = $this->getAuthenticatedClient();
        $clientAdministrative = $this->getAuthenticatedClient('center a_administrative');

        $params = ['q' => '@report date:2015-01-05'];

        $crawlerSocial = $clientSocial->request('GET', '/fr/search', $params);
        $crawlerAdministrative = $clientAdministrative->request('GET', '/fr/search', $params);

        $this->assertStringNotContainsString('social', $crawlerAdministrative->filter('.content')
            ->text());
        $this->assertStringNotContainsString('administrative', $crawlerAdministrative->filter('.content')
            ->text());
    }

    /**
     * @return \Symfony\Component\BrowserKit\AbstractBrowser
     */
    private function getAuthenticatedClient(mixed $username = 'center a_social')
    {
        return self::createClient([], [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW' => 'password',
        ]);
    }
}
