<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Security\Authorization;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\ReportBundle\Entity\Report;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ReportVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    final public const CREATE = 'CHILL_REPORT_CREATE';

    final public const LISTS = 'CHILL_REPORT_LISTS';

    final public const SEE = 'CHILL_REPORT_SEE';

    final public const UPDATE = 'CHILL_REPORT_UPDATE';

    /**
     * @var AuthorizationHelper
     */
    protected $helper;

    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }

    public function getRoles(): array
    {
        return [self::CREATE, self::UPDATE, self::SEE, self::LISTS];
    }

    public function getRolesWithHierarchy(): array
    {
        return ['Report' => $this->getRoles()];
    }

    public function getRolesWithoutScope(): array
    {
        return [self::LISTS];
    }

    protected function supports($attribute, $subject)
    {
        if ($subject instanceof Report) {
            return \in_array($attribute, [
                self::CREATE, self::UPDATE, self::SEE,
            ], true);
        }

        if ($subject instanceof Center) {
            return self::LISTS === $attribute;
        }
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }

        return $this->helper->userHasAccess($token->getUser(), $subject, $attribute);
    }
}
