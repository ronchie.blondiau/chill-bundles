<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Export\Filter;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Doctrine\ORM\Query\Expr;

class ReportDateFilter implements FilterInterface
{
    public function __construct(private readonly RollingDateConverterInterface $rollingDateConverter)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(\Doctrine\ORM\QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->between(
            'report.date',
            ':report_date_filter_date_from',
            ':report_date_filter_date_to'
        );

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter(
            'report_date_filter_date_from',
            $this->rollingDateConverter->convert($data['date_from'])
        );
        $qb->setParameter(
            'report_date_filter_date_to',
            $this->rollingDateConverter->convert($data['date_to'])
        );
    }

    public function applyOn()
    {
        return 'report';
    }

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder)
    {
        $builder->add('date_from', PickRollingDateType::class, [
            'label' => 'Report is after this date',
        ]);

        $builder->add('date_to', PickRollingDateType::class, [
            'label' => 'Report is before this date',
        ]);
    }

    public function getFormDefaultData(): array
    {
        return ['date_from' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START), 'date_to' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function describeAction($data, $format = 'string')
    {
        return ['Filtered by report\'s date: '
            .'between %date_from% and %date_to%', [
                '%date_from%' => $this->rollingDateConverter->convert($data['date_from'])->format('d-m-Y'),
                '%date_to%' => $this->rollingDateConverter->convert($data['date_to'])->format('d-m-Y'),
            ], ];
    }

    public function getTitle()
    {
        return 'Filter by report\'s date';
    }
}
