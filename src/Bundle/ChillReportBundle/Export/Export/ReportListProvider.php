<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\Export\Export;

use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\MainBundle\Export\ExportElementsProviderInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\ReportBundle\Entity\Report;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ReportListProvider implements ExportElementsProviderInterface
{
    /**
     * @var CustomFieldProvider
     */
    protected $customFieldProvider;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(
        EntityManagerInterface $em,
        TranslatableStringHelper $translatableStringHelper,
        TranslatorInterface $translator,
        CustomFieldProvider $customFieldProvider
    ) {
        $this->em = $em;
        $this->translatableStringHelper = $translatableStringHelper;
        $this->translator = $translator;
        $this->customFieldProvider = $customFieldProvider;
    }

    public function getExportElements()
    {
        $groups = $this->em->getRepository(CustomFieldsGroup::class)
            ->findBy(['entity' => Report::class]);
        $reports = [];

        foreach ($groups as $group) {
            $reports[$group->getId()] = new ReportList(
                $group,
                $this->translatableStringHelper,
                $this->translator,
                $this->customFieldProvider,
                $this->em
            );
        }

        return $reports;
    }
}
