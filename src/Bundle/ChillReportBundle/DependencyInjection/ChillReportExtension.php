<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ReportBundle\DependencyInjection;

use Chill\CustomFieldsBundle\Form\Type\LinkedCustomFieldsType;
use Chill\MainBundle\DependencyInjection\MissingBundleException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillReportExtension extends Extension implements PrependExtensionInterface
{
    /**
     * Declare the entity Report, as a customizable entity (can add custom fields).
     */
    public function declareReportAsCustomizable(ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');

        if (!isset($bundles['ChillCustomFieldsBundle'])) {
            throw new MissingBundleException('ChillCustomFieldsBundle');
        }

        $container->prependExtensionConfig(
            'chill_custom_fields',
            ['customizables_entities' => [
                [
                    'class' => \Chill\ReportBundle\Entity\Report::class,
                    'name' => 'ReportEntity',
                    'options' => [
                        'summary_fields' => [
                            'form_type' => LinkedCustomFieldsType::class,
                            'form_options' => [
                                'multiple' => true,
                                'expanded' => false,
                            ],
                        ],
                    ], ],
            ],
            ]
        );
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
        $loader->load('services/fixtures.yaml');
        $loader->load('services/export.yaml');
        $loader->load('services/controller.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->declareReportAsCustomizable($container);
        $this->declareRouting($container);
        $this->prependRoleHierarchy($container);
    }

    protected function prependRoleHierarchy(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                'CHILL_REPORT_UPDATE' => ['CHILL_REPORT_SEE'],
                'CHILL_REPORT_CREATE' => ['CHILL_REPORT_SEE'],
            ],
        ]);
    }

    /**
     * declare routes from report bundle.
     */
    private function declareRouting(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillReportBundle/config/routes.yaml',
                ],
            ],
        ]);
    }
}
