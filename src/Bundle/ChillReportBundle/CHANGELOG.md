
Version 1.5.1
=============

- [list export] fix error "all custom fields are shown"

Version 1.5.2
=============

- add privacy events to report list / view
- add privacy events to report edit / update 

Master branch
=============


 

