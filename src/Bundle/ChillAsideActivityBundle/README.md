# Aside activities module

This module offers the possibility to add extra curricular activities per user (eg. administrative tasks, formation, meetings) not immediately related to client support.
