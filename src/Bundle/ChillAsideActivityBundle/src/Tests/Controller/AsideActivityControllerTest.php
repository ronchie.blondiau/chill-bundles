<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Tests\Controller;

use Chill\AsideActivityBundle\Entity\AsideActivity;
use Chill\MainBundle\Test\PrepareClientTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class AsideActivityControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    public function generateAsideActivityId(): iterable
    {
        self::bootKernel();

        $qb = self::$container->get(EntityManagerInterface::class)
            ->createQueryBuilder();

        $asideActivityIds = $qb
            ->select('DISTINCT asideactivity.id')
            ->from(AsideActivity::class, 'asideactivity')
            ->innerJoin('asideactivity.agent', 'agent')
            ->where($qb->expr()->eq('agent.username', ':center_name'))
            ->setParameter('center_name', 'center a_social')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();

        \shuffle($asideActivityIds);

        yield [\array_pop($asideActivityIds)['id']];

        yield [\array_pop($asideActivityIds)['id']];

        yield [\array_pop($asideActivityIds)['id']];

        self::ensureKernelShutdown();
    }

    /**
     * @dataProvider generateAsideActivityId
     */
    public function testEditWithoutUsers(int $asideActivityId)
    {
        self::ensureKernelShutdown();
        $client = $this->getClientAuthenticated();
        $client->request('GET', "/fr/asideactivity/{$asideActivityId}/edit");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testIndexWithoutUsers()
    {
        self::ensureKernelShutdown();
        $client = $this->getClientAuthenticated();
        $client->request('GET', '/fr/asideactivity');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testNewWithoutUsers()
    {
        self::ensureKernelShutdown();
        $client = $this->getClientAuthenticated();
        $client->request('GET', '/fr/asideactivity/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
