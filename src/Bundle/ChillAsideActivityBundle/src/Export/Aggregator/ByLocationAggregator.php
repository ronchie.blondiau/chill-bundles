<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Aggregator;

use Chill\AsideActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\LocationRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByLocationAggregator implements AggregatorInterface
{
    public function __construct(private readonly LocationRepository $locationRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.Aside activity localisation';
            }
            if (null === $value || '' === $value || null === $l = $this->locationRepository->find($value)) {
                return '';
            }

            return $l->getName();
        };
    }

    public function getQueryKeys($data): array
    {
        return ['by_aside_activity_location_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.Group by aside activity location';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $qb->addSelect('IDENTITY(aside.location) AS by_aside_activity_location_aggregator')
            ->addGroupBy('by_aside_activity_location_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }
}
