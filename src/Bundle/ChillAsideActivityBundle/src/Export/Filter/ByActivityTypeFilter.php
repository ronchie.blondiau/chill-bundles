<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Export\Filter;

use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Chill\AsideActivityBundle\Export\Declarations;
use Chill\AsideActivityBundle\Repository\AsideActivityCategoryRepository;
use Chill\AsideActivityBundle\Templating\Entity\CategoryRender;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class ByActivityTypeFilter implements FilterInterface
{
    public function __construct(
        private readonly CategoryRender $categoryRender,
        private readonly TranslatableStringHelperInterface $translatableStringHelper,
        private readonly AsideActivityCategoryRepository $asideActivityTypeRepository
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $clause = $qb->expr()->in('aside.type', ':types');

        $qb->andWhere($clause);
        $qb->setParameter('types', $data['types']);
    }

    public function applyOn(): string
    {
        return Declarations::ASIDE_ACTIVITY_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('types', EntityType::class, [
                'class' => AsideActivityCategory::class,
                'choices' => $this->asideActivityTypeRepository->findAllActive(),
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'attr' => [
                    'class' => 'select2',
                ],
                'choice_label' => function (AsideActivityCategory $category) {
                    $options = [];

                    return $this->categoryRender->renderString($category, $options);
                },
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $types = array_map(
            fn (AsideActivityCategory $t): string => $this->translatableStringHelper->localize($t->getTitle()),
            $data['types'] instanceof Collection ? $data['types']->toArray() : $data['types']
        );

        return ['export.filter.Filtered by aside activity type: only %type%', [
            '%type%' => implode(', ', $types),
        ]];
    }

    public function getTitle(): string
    {
        return 'export.filter.Filter by aside activity type';
    }
}
