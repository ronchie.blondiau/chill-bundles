<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_aside_activity');

        $treeBuilder->getRootNode()
            ->children()
            ->arrayNode('form')
            ->canBeEnabled()
            ->children()
            ->arrayNode('time_duration')
            ->isRequired()
            ->defaultValue(
                [
                    ['label' => '1 minutes',  'seconds' => 60],
                    ['label' => '2 minutes',  'seconds' => 120],
                    ['label' => '3 minutes',  'seconds' => 180],
                    ['label' => '4 minutes',  'seconds' => 240],
                    ['label' => '5 minutes',  'seconds' => 300],
                    ['label' => '10 minutes', 'seconds' => 600],
                    ['label' => '15 minutes', 'seconds' => 900],
                    ['label' => '20 minutes', 'seconds' => 1200],
                    ['label' => '25 minutes', 'seconds' => 1500],
                    ['label' => '30 minutes', 'seconds' => 1800],
                    ['label' => '45 minutes', 'seconds' => 2700],
                    ['label' => '1 hour',     'seconds' => 3600],
                    ['label' => '1 hour 15',  'seconds' => 4500],
                    ['label' => '1 hour 30',  'seconds' => 5400],
                    ['label' => '1 hour 45',  'seconds' => 6300],
                    ['label' => '2 hours',    'seconds' => 7200],
                    ['label' => '2 hours 30', 'seconds' => 9000],
                    ['label' => '3 hours',    'seconds' => 10800],
                    ['label' => '3 hours 30', 'seconds' => 12600],
                    ['label' => '4 hours',    'seconds' => 14400],
                    ['label' => '4 hours 30', 'seconds' => 16200],
                    ['label' => '5 hours',    'seconds' => 18000],
                    ['label' => '5 hours 30', 'seconds' => 19800],
                    ['label' => '6 hours',    'seconds' => 21600],
                    ['label' => '6 hours 30', 'seconds' => 23400],
                    ['label' => '7 hours',    'seconds' => 25200],
                    ['label' => '7 hours 30', 'seconds' => 27000],
                    ['label' => '8 hours',    'seconds' => 28800],
                    ['label' => '8 hours 30', 'seconds' => 30600],
                    ['label' => '9 hours',    'seconds' => 32400],
                    ['label' => '9 hours 30', 'seconds' => 34200],
                    ['label' => '10 hours',   'seconds' => 36000],
                    ['label' => '1/2 day',    'seconds' => 14040],
                    ['label' => '1 day',      'seconds' => 28080],
                    ['label' => '1 1/2 days',   'seconds' => 42120],
                    ['label' => '2 days',     'seconds' => 56160],
                    ['label' => '2 1/2 days',   'seconds' => 70200],
                    ['label' => '3 days',     'seconds' => 84240],
                    ['label' => '3 1/2 days',   'seconds' => 98280],
                    ['label' => '4 days',     'seconds' => 112320],
                    ['label' => '4 1/2 days',   'seconds' => 126360],
                    ['label' => '5 days',     'seconds' => 140400],
                    ['label' => '5 1/2 days',   'seconds' => 154440],
                    ['label' => '6 days',     'seconds' => 168480],
                    ['label' => '6 1/2 days',   'seconds' => 182520],
                    ['label' => '7 days',     'seconds' => 196560],
                    ['label' => '7 1/2 days',   'seconds' => 210600],
                    ['label' => '8 days',     'seconds' => 224640],
                    ['label' => '8 1/2 days',   'seconds' => 238680],
                    ['label' => '9 days',     'seconds' => 252720],
                    ['label' => '9 1/2 days',   'seconds' => 266760],
                    ['label' => '10 days',    'seconds' => 280800],
                    ['label' => '10 1/2days',  'seconds' => 294840],
                    ['label' => '11 days',    'seconds' => 308880],
                    ['label' => '11 1/2 days',  'seconds' => 322920],
                    ['label' => '12 days',    'seconds' => 336960],
                    ['label' => '12 1/2 days',  'seconds' => 351000],
                    ['label' => '13 days',    'seconds' => 365040],
                    ['label' => '13 1/2 days',  'seconds' => 379080],
                    ['label' => '14 days',    'seconds' => 393120],
                    ['label' => '14 1/2 days',  'seconds' => 407160],
                    ['label' => '15 days',    'seconds' => 421200],
                    ['label' => '15 1/2 days',  'seconds' => 435240],
                    ['label' => '16 days',    'seconds' => 449280],
                    ['label' => '16 1/2 days',  'seconds' => 463320],
                    ['label' => '17 days',    'seconds' => 477360],
                    ['label' => '17 1/2 days',  'seconds' => 491400],
                    ['label' => '18 days',    'seconds' => 505440],
                    ['label' => '18 1/2 days',  'seconds' => 519480],
                    ['label' => '19 days',    'seconds' => 533520],
                    ['label' => '19 1/2 days',  'seconds' => 547560],
                    ['label' => '20 days',    'seconds' => 561600],
                    ['label' => '20 1/2 days',  'seconds' => 575640],
                    ['label' => '21 days',    'seconds' => 580680],
                    ['label' => '21 1/2 days',  'seconds' => 603720],
                    ['label' => '22 days',    'seconds' => 617760],
                    ['label' => '22 1/2 days',  'seconds' => 631800],
                    ['label' => '23 days',    'seconds' => 645840],
                    ['label' => '23 1/2 days',  'seconds' => 659880],
                    ['label' => '24 days',    'seconds' => 673920],
                    ['label' => '24 1/2 days',  'seconds' => 687960],
                    ['label' => '25 days',    'seconds' => 702000],
                    ['label' => '25 1/2 days',  'seconds' => 716040],
                    ['label' => '26 days',    'seconds' => 730080],
                    ['label' => '26 1/2 days',  'seconds' => 744120],
                    ['label' => '27 days',    'seconds' => 758160],
                    ['label' => '27 1/2 days',  'seconds' => 772200],
                    ['label' => '28 days',    'seconds' => 786240],
                    ['label' => '28 1/2 days',  'seconds' => 800280],
                    ['label' => '29 days',    'seconds' => 814320],
                    ['label' => '29 1/2 days',  'seconds' => 828360],
                    ['label' => '30 days',    'seconds' => 842400],
                ]
            )
            ->info('The intervals of time to show in activity form')
            ->prototype('array')
            ->children()
            ->scalarNode('seconds')
            ->info('The number of seconds of this duration. Must be an integer.')
            ->cannotBeEmpty()
            ->validate()
            ->ifTrue(static fn ($data) => !\is_int($data))->thenInvalid('The value %s is not a valid integer')
            ->end()
            ->end()
            ->scalarNode('label')
            ->cannotBeEmpty()
            ->info('The label to show into fields')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
