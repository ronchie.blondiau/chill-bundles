<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Form;

use Chill\AsideActivityBundle\Entity\AsideActivityCategory;
use Chill\AsideActivityBundle\Templating\Entity\CategoryRender;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

final class AsideActivityCategoryType extends AbstractType
{
    public function __construct(private readonly CategoryRender $categoryRender)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TranslatableStringFormType::class,
            [
                'label' => 'Nom',
            ]
        )
            ->add('parent', EntityType::class, [
                'class' => AsideActivityCategory::class,
                'required' => false,
                'label' => 'Parent',
                'choice_label' => function (AsideActivityCategory $category) {
                    $options = [];

                    return $this->categoryRender->renderString($category, $options);
                },
            ])
            ->add('ordering', NumberType::class)
            ->add(
                'isActive',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                ]
            );
    }
}
