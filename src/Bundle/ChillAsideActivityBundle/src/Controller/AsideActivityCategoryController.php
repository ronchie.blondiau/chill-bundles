<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AsideActivityBundle.
 */
class AsideActivityCategoryController extends CRUDController
{
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        /* @var QueryBuilder $query */
        $query->addOrderBy('e.ordering', 'ASC');

        return $query;
    }
}
