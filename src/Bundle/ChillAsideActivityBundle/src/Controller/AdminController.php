<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\AsideActivityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController.
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/{_locale}/admin/aside-activity", name="chill_aside_activity_admin")
     */
    public function indexAdminAction()
    {
        return $this->render('@ChillAsideActivity/Admin/index.html.twig');
    }
}
