import {createApp} from 'vue';
import {_createI18n} from 'ChillMainAssets/vuejs/_js/i18n';
import {store} from './store';
import {personMessages} from 'ChillPersonAssets/vuejs/_js/i18n'
import App from './App.vue';
import VueToast from "vue-toast-notification";

const i18n = _createI18n(personMessages);

const app = createApp({
    template: `<app></app>`,
})
    .use(store)
    .use(i18n)
    .use(VueToast, {
        position: "bottom-right",
        type: "error",
        duration: 10000,
        dismissible: true,
    })
    .component('app', App)
    .mount('#accompanying_course_work_create');
