export const lightGreen = '#43b29d';
export const darkGreen = '#368e7e';
export const lightBrown = '#a2ac80';
export const darkBrown = '#929d69';
export const lightBlue = '#8d9dab';
export const darkBlue = '#718596';