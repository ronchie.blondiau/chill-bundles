const visMessages = {
    fr: {
        add_persons: {
            title: "Ajouter des usagers",
            suggested_counter: "Pas de résultats | 1 résultat | {count} résultats",
            selected_counter: " 1 sélectionné | {count} sélectionnés",
            search_some_persons: "Rechercher des personnes..",
        },
        visgraph: {
            Course: 'Parcours',
            Household: 'Ménage',
            Holder: 'Titulaire',
            Legend: 'Calques',
            concerned: 'concerné',
            // both: 'neutre, non binaire',
            woman: 'féminin',
            man: 'masculin',
            undefined: "genre non précisé",
            years: 'ans',
            click_to_expand: 'cliquez pour étendre',
            add_relationship_link: "Créer un lien de filiation",
            edit_relationship_link: "Modifier le lien de filiation",
            delete_relationship_link: "Êtes-vous sûr ?",
            delete_confirmation_text: "Vous allez supprimer le lien entre ces 2 usagers.",
            reverse_relation: "Inverser la relation",
            relation_from_to_like: "{2} de {1}", // disable {0}
            between: "entre",
            and: "et",
            add_link: "Créer un lien de filiation",
            add_person: "Ajouter une personne",
            create_link_help: "Pour créer un lien de filiation, cliquez d'abord sur un usager, puis sur un second ; précisez ensuite la nature du lien dans le formulaire d'édition.",
            refresh: "Rafraîchir",
            screenshot: "Prendre une photo",
            choose_relation: "Choisissez le lien de parenté",
            relationship_household: "Filiation du ménage",
        },
        item: {
            type_person: "Usager",
            type_user: "TMS",
            type_thirdparty: "Tiers professionnel",
            type_household: "Ménage"
        },
        person: {
            firstname: "Prénom",
            lastname: "Nom",
            born: (ctx) => {
                if (ctx.gender === 'man') {
                    return 'Né le';
                } else if (ctx.gender === 'woman') {
                    return 'Née le';
                } else {
                    return 'Né·e le';
                }
            },
            center_id: "Identifiant du centre",
            center_type: "Type de centre",
            center_name: "Territoire", // vendée
            phonenumber: "Téléphone",
            mobilenumber: "Mobile",
            altnames: "Autres noms",
            email: "Courriel",
            gender: {
                title: "Genre",
                placeholder: "Choisissez le genre de l'usager",
                woman: "Féminin",
                man: "Masculin",
                both: "Neutre, non binaire",
                undefined: "Non renseigné",
                unknown: "Non renseigné"
            }
        },
        error_only_one_person: "Une seule personne peut être sélectionnée !",
        edit: 'Éditer',
        del: 'Supprimer',
        back: 'Revenir en arrière',
        addNode: 'Ajouter un noeuds',
        addEdge: 'Ajouter un lien de filiation',
        editNode: 'Éditer le noeuds',
        editEdge: 'Éditer le lien',
        addDescription: 'Cliquez dans un espace vide pour créer un nouveau nœud.',
        edgeDescription: 'Cliquez sur un usager et faites glisser le lien vers un autre usager pour les connecter.',
        editEdgeDescription: 'Cliquez sur les points de contrôle et faites-les glisser vers un nœud pour les relier.',
        createEdgeError: 'Il est impossible de relier des arêtes à un cluster.',
        deleteClusterError: 'Les clusters ne peuvent pas être supprimés.',
        editClusterError: 'Les clusters ne peuvent pas être modifiés.'
    },
    en: {
        edit: 'Edit',
        del: 'Delete selected',
        back: 'Back',
        addNode: 'Add Node',
        addEdge: 'Add Link',
        editNode: 'Edit Switch',
        editEdge: 'Edit Link',
        addDescription: 'Click in an empty space to place a new node.',
        edgeDescription: 'Click on a node and drag the link to another node to connect them.',
        editEdgeDescription: 'Click on the control points and drag them to a node to connect to it.',
        createEdgeError: 'Cannot link edges to a cluster.',
        deleteClusterError: 'Clusters cannot be deleted.',
        editClusterError: 'Clusters cannot be edited.'

    }
}

export {
    visMessages
}
