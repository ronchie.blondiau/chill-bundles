import { darkGreen, lightBlue, lightBrown, lightGreen } from './colors';
import { visMessages } from './i18n';

/**
 * Vis-network initial data/configuration script
 * Notes:
 *   Use window.network and window.options to avoid conflict between vue and vis
 *   cfr. https://github.com/almende/vis/issues/2524#issuecomment-307108271
 */

window.network = {}

window.options = {
    locale: 'fr',
    locales: visMessages,
    /*
    */
    configure: {
        enabled: false,
        filter: 'physics',
        showButton: true
    },
    physics: {
        enabled: false,
        barnesHut: {
            theta: 0.5,
            gravitationalConstant: -2000,
            centralGravity: 0.08, //// 0.3
            springLength: 220, //// 95
            springConstant: 0.04,
            damping: 0.09,
            avoidOverlap: 0
        },
        forceAtlas2Based: {
            theta: 0.5,
            gravitationalConstant: -50,
            centralGravity: 0.01,
            springLength: 100,
            springConstant: 0.08,
            damping: 0.75,
            avoidOverlap: 0.00
        },
        repulsion: {
            centralGravity: 0.2,
            springLength: 200,
            springConstant: 0.05,
            nodeDistance: 100,
            damping: 0.09
        },
        hierarchicalRepulsion: {
            centralGravity: 0.0,
            springLength: 100,
            springConstant: 0.01,
            nodeDistance: 120,
            damping: 0.09,
            avoidOverlap: 0
        },
        maxVelocity: 50,
        minVelocity: 0.1,
        solver: 'forceAtlas2Based', //'barnesHut', //
        stabilization: {
            enabled: true,
            iterations: 1000,
            updateInterval: 100,
            onlyDynamicEdges: false,
            fit: true
        },
        timestep: 0.5,
        adaptiveTimestep: true,
        wind: { x: 0, y: 0 }
    },
    interaction: {
        hover: true,
        multiselect: true,
        navigationButtons: false,
    },
    manipulation: {
        enabled: false,
        initiallyActive: false,
        addNode: false,
        deleteNode: false
    },
    nodes: {
        borderWidth: 1,
        borderWidthSelected: 3,
        font: {
            multi: 'md'
        }
    },
    edges: {
        font: {
            color: '#b0b0b0',
            size: 14,
            face: 'arial',
            background: 'none',
            strokeWidth: 2, // px
            strokeColor: '#ffffff',
            align: 'middle',
            multi: false,
            vadjust: 0,
        },
        scaling:{
            label: true,
        },
        smooth: true,
    },
    groups: {
        person: {
            shape: 'box',
            shapeProperties: {
                borderDashes: false,
                borderRadius: 3,
            },
            color: {
                border: '#b0b0b0',
                background: lightGreen,
                highlight: {
                    border: '#216458',
                    background: darkGreen,
                },
                hover: {
                    border: '#216458',
                    background: darkGreen,
                }
            },
            opacity: 0.9,
            shadow:{
                enabled: true,
                color: 'rgba(0,0,0,0.5)',
                size:10,
                x:5,
                y:5,
            },
        },
        household: {
            color: lightBrown,
        },
        accompanying_period: {
            color: lightBlue,
        },
    }
}

/**
 * @param gender
 * @returns {string}
 */
const getGender = (gender) => {
    switch (gender) {
        case 'both':
            return visMessages.fr.visgraph.both
        case 'woman':
            return visMessages.fr.visgraph.woman
        case 'man':
            return visMessages.fr.visgraph.man
        case 'unknown':
            return visMessages.fr.visgraph.unknown
        default:
            return visMessages.fr.visgraph.undefined
    }
}

/**
 * TODO only one abstract function (-> getAge() is repeated in PersonRenderBox.vue)
 * @param person
 * @returns {string|null}
 */
const getAge = (person) => {
    if (person.age) {
        return person.age + ' ' + visMessages.fr.visgraph.years;
    }
    return ''
}

/**
 * Return member position in household
 * @param member
 * @returns string
 */
const getHouseholdLabel = (member) => {
    let position = member.position.label.fr
    let holder = member.holder ? ` ${visMessages.fr.visgraph.Holder}` : ''
    return position + holder
}

/**
 * Return edge width for member (depends of position in household)
 * @param member
 * @returns integer (width)
 */
const getHouseholdWidth = (member) => {
    if (member.holder) {
        return 5
    }
    if (member.shareHousehold) {
        return 2
    }
    return 1
}

/**
 * Return direction edge
 * @param relationship
 * @returns string
 */
const getRelationshipDirection = (relationship) => {
    return (!relationship.reverse) ? 'to' : 'from'
}

/**
 * Return label edge
 *  !! always set label in title direction (arrow is reversed, see in previous method) !!
 * @param relationship
 * @returns string
 */
const getRelationshipLabel = (relationship) => {
    return relationship.relation.title.fr
}

/**
 * Return title edge
 * @param relationship
 * @returns string
 */
const getRelationshipTitle = (relationship) => {
    return (!relationship.reverse) ?
        relationship.relation.title.fr + ': ' + relationship.fromPerson.text + '\n' + relationship.relation.reverseTitle.fr + ': ' + relationship.toPerson.text :
        relationship.relation.title.fr + ': ' + relationship.toPerson.text + '\n' + relationship.relation.reverseTitle.fr + ': ' + relationship.fromPerson.text
}

/**
 * Split string id and return type|id substring
 * @param id
 * @param position
 * @returns string|integer
 */
const splitId = (id, position) => {
    //console.log(id, position)
    switch (position) {
        case 'type':                    // return 'accompanying_period'
            return /(.+)_/.exec(id)[1]
        case 'id':                      // return 124
            return parseInt(id.toString()
                .split("_")
                .pop())
        case 'link':
            return id.split("-")[0]     // return first segment
        default:
            throw 'position undefined'
    }
}

export {
    getGender,
    getAge,
    getHouseholdLabel,
    getHouseholdWidth,
    getRelationshipDirection,
    getRelationshipLabel,
    getRelationshipTitle,
    splitId
}
