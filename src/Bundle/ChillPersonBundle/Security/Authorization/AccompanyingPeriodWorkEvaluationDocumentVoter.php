<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Security\Authorization;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter for AccompanyingPeriodWorkEvaluationDocument.
 *
 * Delegates to the sames authorization than for Evalution
 */
class AccompanyingPeriodWorkEvaluationDocumentVoter extends Voter
{
    final public const SEE = 'CHILL_MAIN_ACCOMPANYING_PERIOD_WORK_EVALUATION_DOCUMENT_SHOW';

    public function __construct(private readonly AccessDecisionManagerInterface $accessDecisionManager)
    {
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof AccompanyingPeriodWorkEvaluationDocument
            && self::SEE === $attribute;
    }

    /**
     * @param string                                   $attribute
     * @param AccompanyingPeriodWorkEvaluationDocument $subject
     *
     * @return bool|void
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::SEE => $this->accessDecisionManager->decide(
                $token,
                [AccompanyingPeriodWorkEvaluationVoter::SEE],
                $subject->getAccompanyingPeriodWorkEvaluation()
            ),
            default => throw new \UnexpectedValueException("The attribute {$attribute} is not supported"),
        };
    }
}
