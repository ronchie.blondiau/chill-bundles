// this file loads all assets from the Chill person bundle
module.exports = function(encore, entries)
{
    entries.push(__dirname + '/Resources/public/chill/index.js');

    // Aliases are used when webpack is trying to resolve modules path
    encore.addAliases({
        ChillPersonAssets: __dirname + '/Resources/public'
    });

    encore.addEntry('vue_household_members_editor', __dirname + '/Resources/public/vuejs/HouseholdMembersEditor/index.js');
    encore.addEntry('vue_accourse', __dirname + '/Resources/public/vuejs/AccompanyingCourse/index.js');
    encore.addEntry('vue_accourse_work_create', __dirname + '/Resources/public/vuejs/AccompanyingCourseWorkCreate/index.js');
    encore.addEntry('vue_accourse_work_edit', __dirname + '/Resources/public/vuejs/AccompanyingCourseWorkEdit/index.js');
    encore.addEntry('vue_visgraph', __dirname + '/Resources/public/vuejs/VisGraph/index.js');
    encore.addEntry('vue_export_action_goal_result', __dirname + '/Resources/public/vuejs/ExportFormActionGoalResult/index.js');

    encore.addEntry('mod_set_referrer', __dirname + '/Resources/public/mod/AccompanyingPeriod/setReferrer.js');

    encore.addEntry('page_household_edit_metadata', __dirname + '/Resources/public/page/household_edit_metadata/index.js');
    encore.addEntry('page_person', __dirname + '/Resources/public/page/person/index.js');
    encore.addEntry('page_accompanying_course_index_person_locate', __dirname + '/Resources/public/page/accompanying_course_index/person_locate.js');
    encore.addEntry('page_accompanying_course_index_masonry', __dirname + '/Resources/public/page/accompanying_course_index/masonry.js');
    encore.addEntry('page_person_resource_showhide_input', __dirname + '/Resources/public/page/person_resource/showhide-input.js');
    encore.addEntry('page_suggest_names', __dirname + '/Resources/public/page/person/suggest-names.js');
    encore.addEntry('page_create_person', __dirname + '/Resources/public/page/person/create-person.js');
};
