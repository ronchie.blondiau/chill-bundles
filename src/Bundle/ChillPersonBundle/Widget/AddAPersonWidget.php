<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\Templating\Widget\WidgetInterface;
use Twig\Environment;

/**
 * Add a button "add a person".
 */
class AddAPersonWidget implements WidgetInterface
{
    public function render(
        Environment $env,
        $place,
        array $context,
        array $config
    ) {
        return $env->render('@ChillPerson/Widget/homepage_add_a_person.html.twig');
    }
}
