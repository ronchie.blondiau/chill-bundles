
Version 1.5.1
=============

- Improve import of person to allow multiple centers by file ;
- Launch an event on person import ;
- Allow person to have a `null` gender ;
- Allow filters and aggregator to handle null gender ;
- remove inexistant `person.css` file
- fix bug in accompanying person validation

Version 1.5.2
==============

- Add an column with fullname canonical (lowercase and unaccent) to persons entity ;
- Add a trigram index on fullname canonical ;
- Add a "similar person matcher", which allow to detect person with similar names when adding a person ;
- Add a research of persons by fuzzy name, returning result with a similarity of 0.15 ;

Thanks to @matla :-)

Version 1.5.3
=============

- add filtering on accompanying period 
- fix problems in gender filter

Version 1.5.4
=============

- add filenumber in person header

Version 1.5.5
=============

- Fix bug in accompanying period filter

Version 1.5.6
=============

- Update address validation
- Add command to move person and all data of a person to a new one, and delete the old one.

Version 1.5.7
=============

- fix error on macro renderPerson / withLink not taken into account
- add a link between accompanying person and user
- add an icon when the file is opened / closed in result list, and in person rendering macro
- improve command to move person and all data: allow to delete some entities during move and add events

Version 1.5.8
=============

-  add search by phonenumber, with a custom SearchInterface
   
   This can be activated or desactivated by config:

   ```
   chill_person:
       enabled:              true
       search:
           enabled:              true

           # enable search by phone. 'always' show the result on every result. 'on-domain' will show the result only if the domain is given in the search box. 'never' disable this feature
           search_by_phone:      on-domain # One of "always"; "on-domain"; "never"
   ```
-  format phonenumber using twilio (if available) ;
-  add `record_actions` in person search result list: users can click on a little eye to open person page ;
-  add new fields (email, mobilenumber, gender) into importPeopleFromCSV command
- configure asset using a function


Version 1.5.9
=============

- create CRUD 
- add the ability to add alt names to persons 
- [UI] set action button bottom of edit form according to crud template
- [closing motive] add an hierarchy for closing motives ;
- [closing motive] Add an admin section for closing motives ;

<<<<<<< HEAD
Version 1.5.10
==============

- [closing motive] display closing motive in remark

Version 1.5.11
==============

- Fix versioning constraint to chill main

Version 1.5.12
==============

- [addresses] add a homeless to person's addresses, and this information into 
    person list

Version 1.5.13
==============

- [CRUD] add step delete
- [CRUD] improve index view in person CRUD
- [CRUD] filter by basis on person by default in EntityPersonCRUDController
- [CRUD] override relevant part of the main CRUD template
- [CRUD] fix redirection on person view: add a `person_id` to every page redirected.

Version 1.5.14
==============

- [Accompanying period list] Fix period label in list
- [Accompanying period list] Fix label of closing motive
- [Person details] Add an "empty" statement on place of birth
- [Person list] Add a lock/unlock icon instead of open/closed folder in result list;
- [Admin closing motive] Remove links to Closing motive View;
- [Admin closing motive] Improve icons for active in list of closing motive;
