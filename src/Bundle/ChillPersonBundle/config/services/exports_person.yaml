services:
    _defaults:
        autoconfigure: true
        autowire: true

    ## Indicators
    Chill\PersonBundle\Export\Export\CountPerson:
        tags:
            - { name: chill.export, alias: count_person }

    Chill\PersonBundle\Export\Export\CountPersonWithAccompanyingCourse:
        tags:
            - { name: chill.export, alias: count_person_with_accompanying_course }

    Chill\PersonBundle\Export\Export\ListPerson:
        tags:
            - { name: chill.export, alias: list_person }

    Chill\PersonBundle\Export\Helper\ListPersonHelper:
        arguments:
            $customPersonHelpers: !tagged_iterator chill_person.list_person_customizer

    Chill\PersonBundle\Export\Export\ListPersonHavingAccompanyingPeriod:
        tags:
            - { name: chill.export, alias: list_person_with_acp }

    Chill\PersonBundle\Export\Export\ListPersonWithAccompanyingPeriodDetails:
        tags:
            - { name: chill.export, alias: list_person_with_acp_details }

    Chill\PersonBundle\Export\Export\ListAccompanyingPeriod:
        tags:
            - { name: chill.export, alias: list_acp }

    chill.person.export.list_person.duplicate:
        class: Chill\PersonBundle\Export\Export\ListPersonDuplicate
        arguments:
            - "@doctrine.orm.entity_manager"
            - "@translator"
            - "@router"
            - '%chill_main.notifications%'
        tags:
            - { name: chill.export, alias: list_person_duplicate }

    ## Filters
    chill.person.export.filter_gender:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\GenderFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_gender_filter }

    chill.person.export.filter_age:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\AgeFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_age_filter }

    chill.person.export.filter_birthdate:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\BirthdateFilter
        tags:
            - { name: chill.export_filter, alias: person_birthdate_filter }

    chill.person.export.filter_deathdate:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\DeathdateFilter
        tags:
            - { name: chill.export_filter, alias: person_deathdate_filter }

    chill.person.export.filter_dead_or_alive:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\DeadOrAliveFilter
        tags:
            - { name: chill.export_filter, alias: person_dead_or_alive_filter }

    chill.person.export.filter_nationality:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\NationalityFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_nationality_filter }

    chill.person.export.filter_residential_address_at_user:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\ResidentialAddressAtUserFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_residential_address_at_user_filter }

    chill.person.export.filter_residential_address_at_thirdparty:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\ResidentialAddressAtThirdpartyFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_residential_address_at_thirdparty_filter }

    chill.person.export.filter_marital_status:
        class: Chill\PersonBundle\Export\Filter\PersonFilters\MaritalStatusFilter
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_marital_status_filter }

    Chill\PersonBundle\Export\Filter\PersonFilters\GeographicalUnitFilter:
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_geog_filter }

    Chill\PersonBundle\Export\Filter\PersonFilters\AddressRefStatusFilter:
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_filter, alias: person_address_ref_status_filter }

    Chill\PersonBundle\Export\Filter\PersonFilters\ByHouseholdCompositionFilter:
        tags:
            - { name: chill.export_filter, alias: person_by_household_composition_filter }

    Chill\PersonBundle\Export\Filter\PersonFilters\WithoutHouseholdComposition:
        tags:
            - { name: chill.export_filter, alias: person_without_household_composition_filter }

    Chill\PersonBundle\Export\Filter\PersonFilters\WithParticipationBetweenDatesFilter:
        tags:
            - { name: chill.export_filter, alias: person_with_participation_between_dates_filter }

    ## Aggregators
    chill.person.export.aggregator_nationality:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\NationalityAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_nationality_aggregator }

    chill.person.export.aggregator_country_of_birth:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\CountryOfBirthAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_country_of_birth_aggregator }

    chill.person.export.aggregator_gender:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\GenderAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_gender_aggregator }

    chill.person.export.aggregator_age:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\AgeAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_age_aggregator }

    chill.person.export.aggregator_marital_status:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\MaritalStatusAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_marital_status_aggregator }

    chill.person.export.aggregator_household_position:
        class: Chill\PersonBundle\Export\Aggregator\PersonAggregators\HouseholdPositionAggregator
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_household_position_aggregator }

    Chill\PersonBundle\Export\Aggregator\PersonAggregators\GeographicalUnitAggregator:
        autowire: true
        autoconfigure: true
        tags:
            - { name: chill.export_aggregator, alias: person_geog_aggregator }

    Chill\PersonBundle\Export\Aggregator\PersonAggregators\ByHouseholdCompositionAggregator:
        tags:
            - { name: chill.export_aggregator, alias: person_household_compo_aggregator }

    Chill\PersonBundle\Export\Aggregator\PersonAggregators\CenterAggregator:
        tags:
            - { name: chill.export_aggregator, alias: person_center_aggregator }

    Chill\PersonBundle\Export\Aggregator\PersonAggregators\PostalCodeAggregator:
        tags:
            - { name: chill.export_aggregator, alias: person_postal_code_aggregator }
