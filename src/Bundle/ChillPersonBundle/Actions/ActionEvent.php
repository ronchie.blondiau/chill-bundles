<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Actions;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event triggered when an entity attached to a person is removed.
 */
class ActionEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const DELETE = 'CHILL_PERSON.DELETE_ASSOCIATED_ENTITY';

    final public const MOVE = 'CHILL_PERSON.MOVE_ASSOCIATED_ENTITY';

    /**
     * @var string[]
     */
    protected $postSql = [];

    /**
     * @var string[]
     */
    protected $preSql = [];

    /**
     * @param string  $entity
     * @param mixed[] $metadata
     * @param int     $personId
     * @param string  $sqlStatement
     */
    public function __construct(
        protected $personId,
        /**
         * the FQDN class name as recorded in doctrine.
         */
        protected $entity,
        /**
         * the sql statement.
         */
        protected $sqlStatement,
        /**
         * an array of key value data to describe the movement.
         */
        protected $metadata = []
    ) {
    }

    /**
     * Add Sql which will be executed **after** the delete statement.
     *
     * @param type $postSql
     *
     * @return $this
     */
    public function addPostSql(string $postSql)
    {
        $this->postSql[] = $postSql;

        return $this;
    }

    /*
     * Add Sql which will be executed **before** the delete statement
     */
    public function addPreSql(string $preSql)
    {
        $this->preSql[] = $preSql;

        return $this;
    }

    /**
     * get the entity name, as recorded in doctrine.
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function getPersonId(): int
    {
        return $this->personId;
    }

    /**
     * @return string[]
     */
    public function getPostSql(): array
    {
        return $this->postSql;
    }

    /**
     * @return string[]
     */
    public function getPreSql(): array
    {
        return $this->preSql;
    }

    public function getSqlStatement()
    {
        return $this->sqlStatement;
    }
}
