<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Entity\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class SocialActionTest extends TestCase
{
    public function testGetDescendantsWithThisForActions()
    {
        $parentA = new SocialAction();
        $childA = (new SocialAction())->setParent($parentA);
        $grandChildA = (new SocialAction())->setParent($childA);
        $grandGrandChildA = (new SocialAction())->setParent($grandChildA);
        $unrelatedA = new SocialAction();

        $parentB = new SocialAction();
        $childB = (new SocialAction())->setParent($parentB);
        $grandChildB = (new SocialAction())->setParent($childB);
        $grandGrandChildB = (new SocialAction())->setParent($grandChildB);
        $unrelatedB = new SocialAction();

        $actual = SocialAction::getDescendantsWithThisForActions([$parentA, $parentB]);

        $this->assertContains($parentA, $actual);
        $this->assertContains($parentB, $actual);
        $this->assertContains($childA, $actual);
        $this->assertContains($childB, $actual);
        $this->assertContains($grandChildA, $actual);
        $this->assertContains($grandChildB, $actual);
        $this->assertContains($grandGrandChildA, $actual);
        $this->assertContains($grandGrandChildB, $actual);
        $this->assertCount(8, $actual);
        $this->assertNotContains($unrelatedA, $actual);
        $this->assertNotContains($unrelatedB, $actual);
    }
}
