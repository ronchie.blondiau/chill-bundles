<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Bundle\ChillPersonBundle\Tests\Entity\Household;

use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Household\Position;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class HouseholdMemberTest extends TestCase
{
    public function testPositionDoNotSharehousehold()
    {
        $position = (new Position())
            ->setShareHousehold(false);
        $membership = (new HouseholdMember())
            ->setPosition($position);

        $this->assertFalse($membership->getShareHousehold());
    }

    public function testPositionSharehousehold()
    {
        $position = (new Position())
            ->setShareHousehold(true);
        $membership = (new HouseholdMember())
            ->setPosition($position);

        $this->assertTrue($membership->getShareHousehold());
    }
}
