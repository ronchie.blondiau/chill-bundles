<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListPersonWithAccompanyingPeriodDetails;
use Chill\PersonBundle\Export\Helper\FilterListAccompanyingPeriodHelperInterface;
use Chill\PersonBundle\Export\Helper\ListAccompanyingPeriodHelper;
use Chill\PersonBundle\Export\Helper\ListPersonHelper;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
class ListPersonWithAccompanyingPeriodDetailsTest extends AbstractExportTest
{
    use ProphecyTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    public function getExport()
    {
        $listPersonHelper = self::$container->get(ListPersonHelper::class);
        $listAccompanyingPeriodHelper = self::$container->get(ListAccompanyingPeriodHelper::class);
        $entityManager = self::$container->get(EntityManagerInterface::class);
        $rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $filterHelper = $this->prophesize(FilterListAccompanyingPeriodHelperInterface::class);

        yield new ListPersonWithAccompanyingPeriodDetails(
            $listPersonHelper,
            $listAccompanyingPeriodHelper,
            $entityManager,
            $rollingDateConverter,
            $filterHelper->reveal()
        );
    }

    public function getFormData()
    {
        return [
            ['address_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getModifiersCombination()
    {
        return [
            [Declarations::PERSON_TYPE, Declarations::ACP_TYPE],
        ];
    }
}
