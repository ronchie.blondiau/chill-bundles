<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\AvgDurationAPWorkPersonAssociatedOnAccompanyingPeriod;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkRepository;

/**
 * @internal
 *
 * @coversNothing
 */
class AvgDurationAPWorkPersonAssociatedOnAccompanyingPeriodTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $repository = self::$container->get(AccompanyingPeriodWorkRepository::class);

        yield new AvgDurationAPWorkPersonAssociatedOnAccompanyingPeriod($this->getParameters(true), $repository);
        yield new AvgDurationAPWorkPersonAssociatedOnAccompanyingPeriod($this->getParameters(false), $repository);
    }

    public function getFormData()
    {
        return [];
    }

    public function getModifiersCombination()
    {
        return [
            [
                Declarations::SOCIAL_WORK_ACTION_TYPE,
                Declarations::ACP_TYPE,
                Declarations::PERSON_TYPE,
            ]];
    }
}
