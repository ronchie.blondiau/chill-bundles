<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\CountHouseholdInPeriod;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CountHouseholdInPeriodTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);
        $rollingDate = self::$container->get(RollingDateConverterInterface::class);

        yield new CountHouseholdInPeriod($em, $rollingDate, $this->getParameters(true));
        yield new CountHouseholdInPeriod($em, $rollingDate, $this->getParameters(false));
    }

    public function getFormData()
    {
        return [
            ['calc_date' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getModifiersCombination()
    {
        return [
            [
                Declarations::HOUSEHOLD_TYPE,
                Declarations::ACP_TYPE,
                Declarations::PERSON_TYPE,
            ]];
    }
}
