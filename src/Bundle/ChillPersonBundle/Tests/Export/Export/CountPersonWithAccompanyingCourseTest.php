<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\CountPersonWithAccompanyingCourse;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CountPersonWithAccompanyingCourseTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);

        yield new CountPersonWithAccompanyingCourse($em, $this->getParameters(true));
        yield new CountPersonWithAccompanyingCourse($em, $this->getParameters(false));
    }

    public function getFormData()
    {
        return [[]];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::ACP_TYPE]];
    }
}
