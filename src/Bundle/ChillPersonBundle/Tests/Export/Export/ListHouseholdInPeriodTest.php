<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Export;

use Chill\MainBundle\Export\Helper\AggregateStringHelper;
use Chill\MainBundle\Export\Helper\ExportAddressHelper;
use Chill\MainBundle\Export\Helper\TranslatableStringExportLabelHelper;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\ListHouseholdInPeriod;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ListHouseholdInPeriodTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $addressHelper = self::$container->get(ExportAddressHelper::class);
        $aggregateStringHelper = self::$container->get(AggregateStringHelper::class);
        $entityManager = self::$container->get(EntityManagerInterface::class);
        $rollingDateConverter = self::$container->get(RollingDateConverterInterface::class);
        $translatableStringHelper = self::$container->get(TranslatableStringExportLabelHelper::class);

        yield new ListHouseholdInPeriod(
            $addressHelper,
            $aggregateStringHelper,
            $entityManager,
            $rollingDateConverter,
            $translatableStringHelper,
            $this->getParameters(true),
        );

        yield new ListHouseholdInPeriod(
            $addressHelper,
            $aggregateStringHelper,
            $entityManager,
            $rollingDateConverter,
            $translatableStringHelper,
            $this->getParameters(false),
        );
    }

    public function getFormData()
    {
        return [['calc_date' => new RollingDate(RollingDate::T_TODAY)]];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::PERSON_TYPE]];
    }
}
