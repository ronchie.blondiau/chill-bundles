<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\Export;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\MainBundle\Test\Export\AbstractExportTest;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Export\Export\StatAccompanyingCourseDuration;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class StatAccompanyingCourseDurationTest extends AbstractExportTest
{
    private StatAccompanyingCourseDuration $export;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->export = self::$container->get('chill.person.export.avg_accompanyingcourse_duration');
    }

    public function getExport()
    {
        $em = self::$container->get(EntityManagerInterface::class);
        $rollingDateconverter = self::$container->get(RollingDateConverterInterface::class);

        yield new StatAccompanyingCourseDuration($em, $rollingDateconverter, $this->getParameters(true));
        yield new StatAccompanyingCourseDuration($em, $rollingDateconverter, $this->getParameters(false));
    }

    public function getFormData(): array
    {
        return [
            ['closingdate_rolling' => new RollingDate(RollingDate::T_TODAY)],
        ];
    }

    public function getModifiersCombination()
    {
        return [[Declarations::ACP_TYPE]];
    }
}
