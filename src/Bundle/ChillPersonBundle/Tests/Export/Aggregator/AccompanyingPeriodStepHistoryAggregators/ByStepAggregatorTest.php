<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodStepHistory;
use Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators\ByStepAggregator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class ByStepAggregatorTest extends AbstractAggregatorTest
{
    public function getAggregator()
    {
        $translator = new class () implements TranslatorInterface {
            public function trans(string $id, array $parameters = [], ?string $domain = null, ?string $locale = null)
            {
                return $id;
            }
        };

        return new ByStepAggregator($translator);
    }

    public function getFormData()
    {
        return [[]];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $qb = $em->createQueryBuilder()
            ->select('COUNT(DISTINCT acpstephistory.id) As export_result')
            ->from(AccompanyingPeriodStepHistory::class, 'acpstephistory')
            ->join('acpstephistory.period', 'acp');

        return [
            $qb,
        ];
    }
}
