<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Repository\GeographicalUnitLayerLayerRepository;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators\GeographicalUnitStatAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class GeographicalUnitStatAggregatorTest extends AbstractAggregatorTest
{
    private GeographicalUnitStatAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get('chill.person.export.aggregator_geographicalunitstat');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $repository = self::$container->get(GeographicalUnitLayerLayerRepository::class);
        $levels = $repository->findAll();

        return [
            ['date_calc' => new RollingDate(RollingDate::T_TODAY), 'level' => $levels],
        ];
    }

    public function getQueryBuilders(): array
    {
        if (null === self::$kernel) {
            self::bootKernel();
        }

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
