<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\PersonAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Export\Aggregator\PersonAggregators\AgeAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class AgeAggregatorTest extends AbstractAggregatorTest
{
    private AgeAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get('chill.person.export.aggregator_age');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData()
    {
        return [
            [
                'date_age_calculation' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container
            ->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(person.id)')
                ->from(Person::class, 'person'),
        ];
    }
}
