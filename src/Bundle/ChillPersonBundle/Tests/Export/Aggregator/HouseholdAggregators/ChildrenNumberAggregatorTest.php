<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Aggregator\HouseholdAggregators;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Aggregator\HouseholdAggregators\ChildrenNumberAggregator;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ChildrenNumberAggregatorTest extends AbstractAggregatorTest
{
    private ChildrenNumberAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get('chill.person.export.aggregator_household_childrennumber');
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData(): array
    {
        return [
            [
                'on_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(acp.id)')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join('acp.participations', 'acppart')
                ->join('acppart.person', 'partperson')
                ->join('partperson.householdParticipations', 'member')
                ->join('member.household', 'household')
                ->join('household.compositions', 'composition'),
        ];
    }
}
