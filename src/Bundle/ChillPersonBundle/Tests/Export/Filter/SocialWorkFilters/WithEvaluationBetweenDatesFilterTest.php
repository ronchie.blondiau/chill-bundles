<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\SocialWorkFilters\AccompanyingPeriodWorkWithEvaluationBetweenDatesFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class WithEvaluationBetweenDatesFilterTest extends AbstractFilterTest
{
    private AccompanyingPeriodWorkWithEvaluationBetweenDatesFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(AccompanyingPeriodWorkWithEvaluationBetweenDatesFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        return [
            [
                'start_date' => new RollingDate(RollingDate::T_MONTH_CURRENT_START),
                'end_date' => new RollingDate(RollingDate::T_TODAY),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
