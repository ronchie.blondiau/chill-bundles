<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\UserJob;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\SocialWorkFilters\CreatorJobFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CreatorJobFilterTest extends AbstractFilterTest
{
    private CreatorJobFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(CreatorJobFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $jobs = $em->createQuery('SELECT j FROM '.UserJob::class.' j')
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'jobs' => new ArrayCollection($jobs),
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
