<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\HandlingThirdPartyFilter;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\ThirdPartyBundle\Templating\Entity\ThirdPartyRender;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class HandlingThirdPartyFilterTest extends AbstractFilterTest
{
    private ThirdPartyRender $thirdPartyRender;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->thirdPartyRender = self::$container->get(ThirdPartyRender::class);
    }

    public function getFilter()
    {
        return new HandlingThirdPartyFilter($this->thirdPartyRender);
    }

    public function getFormData()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        $thirdParties = $em->createQuery('SELECT tp FROM '.ThirdParty::class.' tp')
            ->setMaxResults(2)
            ->getResult();

        return [
            [
                'handling_3parties' => $thirdParties,
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
