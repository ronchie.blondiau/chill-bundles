<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\SocialWorkFilters\CreatorScopeFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CreatorScopeFilterTest extends AbstractFilterTest
{
    private CreatorScopeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(CreatorScopeFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $scopes = $em->createQuery('SELECT s FROM '.Scope::class.' s')
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'scopes' => $scopes,
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
