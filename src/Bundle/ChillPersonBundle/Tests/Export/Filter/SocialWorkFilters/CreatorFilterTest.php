<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Tests\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Export\Filter\SocialWorkFilters\CreatorFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class CreatorFilterTest extends AbstractFilterTest
{
    private CreatorFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(CreatorFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        $creators = $em->createQuery('SELECT u FROM '.User::class.' u')
            ->setMaxResults(1)
            ->getResult();

        return [
            [
                'creators' => $creators,
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acpw.id')
                ->from(AccompanyingPeriodWork::class, 'acpw'),
        ];
    }
}
