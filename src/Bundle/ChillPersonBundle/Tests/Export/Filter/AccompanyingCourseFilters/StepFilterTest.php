<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\StepFilterOnDate;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class StepFilterTest extends AbstractFilterTest
{
    private StepFilterOnDate $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(StepFilterOnDate::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): iterable
    {
        foreach ([
            ['accepted_steps_multi' => [AccompanyingPeriod::STEP_DRAFT]],
            ['accepted_steps_multi' => [AccompanyingPeriod::STEP_CONFIRMED]],
            ['accepted_steps_multi' => [AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_LONG]],
            ['accepted_steps_multi' => [AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_SHORT]],
            ['accepted_steps_multi' => [AccompanyingPeriod::STEP_CLOSED]],
        ] as $d) {
            yield ['calc_date' => new RollingDate(RollingDate::T_TODAY), ...$d];
        }
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from(AccompanyingPeriod::class, 'acp'),
        ];
    }
}
