<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\ReferrerFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ReferrerFilterTest extends AbstractFilterTest
{
    private ReferrerFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(ReferrerFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self:self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $u) {
            $data[] = ['accepted_referrers' => $u, 'date_calc' => new RollingDate(RollingDate::T_TODAY)];
        }

        return $data;
    }

    public function getQueryBuilders(): iterable
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        yield $em->createQueryBuilder()
            ->from(AccompanyingPeriod::class, 'acp')
            ->select('acp.id');

        $qb = $em->createQueryBuilder();
        $qb
            ->from(AccompanyingPeriod\AccompanyingPeriodWork::class, 'acpw')
            ->join('acpw.accompanyingPeriod', 'acp')
            ->join('acp.participations', 'acppart')
            ->join('acppart.person', 'person')
        ;

        $qb->select('COUNT(DISTINCT acpw.id) as export_result');

        yield $qb;
    }
}
