<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters\ConfidentialFilter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class ConfidentialFilterTest extends AbstractFilterTest
{
    private ConfidentialFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.person.export.filter_confidential');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        return [
            ['accepted_confidentials' => true],
            ['accepted_confidentials' => false],
        ];
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('acp.id')
                ->from('ChillPersonBundle:AccompanyingPeriod', 'acp'),
        ];
    }
}
