<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\PersonAltName;
use Chill\PersonBundle\EventListener\PersonEventListener;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonCreateEventTest extends TestCase
{
    public function generateAltNames(): iterator
    {
        yield ['vinCENT', 'VINCENT'];

        yield ['jean-marie', 'JEAN-MARIE'];

        yield ['fastré', 'FASTRÉ'];

        yield ['émile', 'ÉMILE'];
    }

    public function generateNames(): iterator
    {
        yield ['émelie-marie', 'Émelie-Marie', 'lenaerts', 'LENAERTS'];

        yield ['jean-marie', 'Jean-Marie', 'lenaerts', 'LENAERTS'];

        yield ['vinCENT', 'Vincent', 'fastré', 'FASTRÉ'];

        yield ['Vincent', 'Vincent', 'van Gogh', 'VAN GOGH'];

        yield ['André marie', 'André Marie', 'Bah', 'BAH'];
    }

    /**
     * @dataProvider generateAltNames
     */
    public function testAltNamesOnPrePersist(mixed $altname, mixed $altnameExpected)
    {
        $listener = new PersonEventListener();

        $personAltname = new PersonAltName();

        $personAltname->setLabel($altname);

        $listener->prePersistAltName($personAltname);

        $this->assertEquals($altnameExpected, $personAltname->getLabel());
    }

    /**
     * @dataProvider generateNames
     */
    public function testOnPrePersist(mixed $firstname, mixed $firstnameExpected, mixed $lastname, mixed $lastnameExpected)
    {
        $listener = new PersonEventListener();

        $person = new Person();

        $person->setFirstName($firstname);
        $person->setLastName($lastname);

        $listener->prePersistPerson($person);

        $this->assertEquals($firstnameExpected, $person->getFirstName());
        $this->assertEquals($lastnameExpected, $person->getLastName());
    }
}
