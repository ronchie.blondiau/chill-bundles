<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Repository;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Repository\CenterRepositoryInterface;
use Chill\MainBundle\Repository\CountryRepository;
use Chill\MainBundle\Security\Authorization\AuthorizationHelperInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\PersonACLAwareRepository;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonACLAwareRepositoryTest extends KernelTestCase
{
    use ProphecyTrait;

    private CenterRepositoryInterface $centerRepository;

    private CountryRepository $countryRepository;

    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->entityManager = self::$container->get(EntityManagerInterface::class);
        $this->countryRepository = self::$container->get(CountryRepository::class);
        $this->centerRepository = self::$container->get(CenterRepositoryInterface::class);
    }

    public function testCountByCriteria()
    {
        $user = new User();

        $authorizationHelper = $this->prophesize(AuthorizationHelperInterface::class);
        $authorizationHelper->getReachableCenters(Argument::exact($user), Argument::exact(PersonVoter::SEE))
            ->willReturn($this->centerRepository->findAll());

        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        $repository = new PersonACLAwareRepository(
            $security->reveal(),
            $this->entityManager,
            $this->countryRepository,
            $authorizationHelper->reveal()
        );

        $number = $repository->countBySearchCriteria('diallo');

        $this->assertGreaterThan(0, $number);
    }

    public function testFindByCriteria()
    {
        $user = new User();

        $authorizationHelper = $this->prophesize(AuthorizationHelperInterface::class);
        $authorizationHelper->getReachableCenters(Argument::exact($user), Argument::exact(PersonVoter::SEE))
            ->willReturn($this->centerRepository->findAll());

        $security = $this->prophesize(Security::class);
        $security->getUser()->willReturn($user);

        $repository = new PersonACLAwareRepository(
            $security->reveal(),
            $this->entityManager,
            $this->countryRepository,
            $authorizationHelper->reveal()
        );

        $results = $repository->findBySearchCriteria(0, 5, false, 'diallo');

        $this->assertGreaterThan(0, \count($results));
        $this->assertContainsOnlyInstancesOf(Person::class, $results);

        foreach ($results as $person) {
            $this->assertStringContainsString('diallo', strtolower($person->getFirstName().' '.$person->getLastName()));
        }
    }
}
