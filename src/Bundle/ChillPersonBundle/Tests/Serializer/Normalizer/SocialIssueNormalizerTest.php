<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Serializer\Normalizer;

use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class SocialIssueNormalizerTest extends KernelTestCase
{
    private NormalizerInterface $normalizer;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->normalizer = self::$container->get(NormalizerInterface::class);
    }

    public function testNormalization()
    {
        $si = new SocialIssue();

        $normalized = $this->normalizer->normalize(
            $si,
            'json',
            ['groups' => ['read']]
        );

        $this->assertIsArray($normalized);
        $this->assertArrayHasKey('type', $normalized);
        $this->assertEquals('social_issue', $normalized['type']);
    }
}
