<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Serializer\Normalizer;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWork;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkGoal;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class AccompanyingPeriodWorkDocGenNormalizerTest extends KernelTestCase
{
    private NormalizerInterface $normalizer;

    protected function setUp(): void
    {
        parent::bootKernel();
        $this->normalizer = self::$container->get(NormalizerInterface::class);
    }

    public function testNormalizationNull()
    {
        $actual = $this->normalizer->normalize(null, 'docgen', [
            'docgen:expects' => AccompanyingPeriodWork::class,
            AbstractNormalizer::GROUPS => ['docgen:read'],
        ]);

        $expected = [
            'id' => '',
        ];

        $this->assertIsArray($actual);
        $this->markTestSkipped('specification still not finalized');
        $this->assertEqualsCanonicalizing(array_keys($expected), array_keys($actual));

        foreach ($expected as $key => $item) {
            if ('@ignored' === $item) {
                continue;
            }

            $this->assertEquals($item, $actual[$key]);
        }
    }

    public function testNormalize()
    {
        $work = new AccompanyingPeriodWork();
        $work
            ->addPerson((new Person())->setFirstName('hello')->setLastName('name'))
            ->addGoal($g = new AccompanyingPeriodWorkGoal())
            ->addResult($r = new Result())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setCreatedBy($user = new User())
            ->setUpdatedBy($user);
        $g->addResult($r)->setGoal($goal = new Goal());
        $goal->addResult($r);

        $actual = $this->normalizer->normalize($work, 'docgen', [
            'docgen:expects' => AccompanyingPeriodWork::class,
            AbstractNormalizer::GROUPS => ['docgen:read'],
        ]);

        $expected = [
            'id' => 0,
        ];

        $this->assertIsArray($actual);
        $this->markTestSkipped('specification still not finalized');
        $this->assertEqualsCanonicalizing(array_keys($expected), array_keys($actual));

        foreach ($expected as $key => $item) {
            if (0 === $item) {
                continue;
            }

            $this->assertEquals($item, $actual[$key]);
        }
    }
}
