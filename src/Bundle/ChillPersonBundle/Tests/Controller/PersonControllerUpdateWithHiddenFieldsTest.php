<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

// ini_set('memory_limit', '-1');

use Chill\PersonBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the edition of persons.
 *
 * As I am logged in as "center a_social"
 *
 * @internal
 *
 * @coversNothing
 */
final class PersonControllerUpdateWithHiddenFieldsTest extends WebTestCase
{
    /**
     * @var string The url using for editing the person's information
     */
    private string $editUrl;

    private ?object $em = null;

    private ?Person $person = null;

    /**
     * @var string The url using for seeing the person's information
     */
    private string $viewUrl;

    /**
     * Prepare client and create a random person.
     */
    protected function setUp(): void
    {
        self::bootKernel(['environment' => 'test_with_hidden_fields']);

        $this->em = self::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');

        $center = $this->em->getRepository(\Chill\MainBundle\Entity\Center::class)
            ->findOneBy(['name' => 'Center A']);

        $this->person = (new Person())
            ->setLastName('My Beloved')
            ->setFirstName('Jesus')
            ->setCenter($center)
            ->setGender(Person::MALE_GENDER);

        $this->em->persist($this->person);
        $this->em->flush();

        $this->editUrl = '/en/person/'.$this->person->getId().'/general/edit';
        $this->viewUrl = '/en/person/'.$this->person->getId().'/general';

        $this->client = self::createClient(
            [
                'environment' => 'test_with_hidden_fields',
            ],
            [
                'PHP_AUTH_USER' => 'center a_social',
                'PHP_AUTH_PW' => 'password',
            ]
        );
    }

    protected function tearDown(): void
    {
        $this->refreshPerson();
        $this->em->remove($this->person);
        $this->em->flush();
    }

    /**
     * Test the edit page are accessible.
     */
    public function testEditPageIsSuccessful()
    {
        $this->client->request('GET', $this->editUrl);
        $this->assertTrue(
            $this->client->getResponse()->isSuccessful(),
            'The person edit form is accessible'
        );
    }

    /**
     * Test the edition of a field.
     *
     * Given I fill the field with $value
     * And I submit the form
     * Then I am redirected to the 'general' page
     * And the person is updated in the db
     *
     * @dataProvider validTextFieldsProvider
     *
     * @param string $field
     * @param string $value
     */
    public function testEditTextField($field, $value, \Closure $callback)
    {
        $crawler = $this->client->request('GET', $this->editUrl);

        $form = $crawler->selectButton('Submit')
            ->form();
        // transform countries into value if needed
        switch ($field) {
            case 'nationality':
            case 'countryOfBirth':
                if (null !== $value) {
                    $country = $this->em->getRepository(\Chill\MainBundle\Entity\Country::class)
                        ->findOneByCountryCode($value);
                    $transformedValue = $country->getId();
                } else {
                    $transformedValue = null;
                }

                break;

            default:
                $transformedValue = $value;
        }

        $form->get('chill_personbundle_person['.$field.']')
            ->setValue($transformedValue);

        $this->client->submit($form);
        $this->refreshPerson();

        $this->assertTrue(
            $this->client->getResponse()->isRedirect($this->viewUrl),
            'the page is redirected to general view'
        );
        $this->assertEquals(
            $value,
            $callback($this->person),
            'the value '.$field.' is updated in db'
        );

        $crawler = $this->client->followRedirect();
        $this->assertGreaterThan(
            0,
            $crawler->filter('.success')->count(),
            'a element .success is shown'
        );

        if ('birthdate' === $field || 'memo' === $field || 'countryOfBirth' === $field || 'nationality' === $field
                || 'gender' === $field) {
            // we do not perform test on the web page contents.
        } else {
            $this->assertGreaterThan(0, $crawler->filter('html:contains("'.$value.'")')->count());
        }
    }

    /**
     * Test the configurable fields are absent.
     *
     * @group configurable_fields
     */
    public function testHiddenFielsAreAbsent()
    {
        $crawler = $this->client->request('GET', $this->editUrl);

        $configurables = ['placeOfBirth', 'phonenumber', 'email',
            'countryOfBirth', 'nationality', 'spokenLanguages', 'maritalStatus', ];
        $form = $crawler->selectButton('Submit')->form(); // ;

        foreach ($configurables as $key) {
            $this->assertFalse($form->has('chill_personbundle_person['.$key.']'));
        }
    }

    /**
     * provide valid values to test, with field name and
     * a function to find the value back from person entity.
     *
     * @return mixed[]
     */
    public function validTextFieldsProvider()
    {
        return [
            ['firstName', 'random Value', static fn (Person $person) => $person->getFirstName()],
            ['lastName', 'random Value', static fn (Person $person) => $person->getLastName()],
            ['birthdate', '15-12-1980', static fn (Person $person) => $person->getBirthdate()->format('d-m-Y')],
            ['memo', 'jfkdlmq jkfldmsq jkmfdsq', static fn (Person $person) => $person->getMemo()],
            ['birthdate', '', static fn (Person $person) => $person->getBirthdate()],
            ['gender', Person::FEMALE_GENDER, static fn (Person $person) => $person->getGender()],
        ];
    }

    /**
     * Reload the person from the db.
     */
    protected function refreshPerson()
    {
        $this->person = $this->em->getRepository(Person::class)
            ->find($this->person->getId());
    }

    private function getVeryLongText()
    {
        return <<<'EOT'
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse molestie at enim id auctor. Vivamus malesuada elit ipsum, ac mollis ex facilisis sit amet. Phasellus accumsan, quam ut aliquet accumsan, augue ligula consequat erat, condimentum iaculis orci magna egestas eros. In vel blandit sapien. Duis ut dui vitae tortor iaculis malesuada vitae vitae lorem. Morbi efficitur dolor orci, a rhoncus urna blandit quis. Aenean at placerat dui, ut tincidunt nulla. In ultricies tempus ligula ac rutrum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce urna nibh, placerat vel auctor sed, maximus quis magna. Vivamus quam ante, consectetur vel feugiat quis, aliquet id ante. Integer gravida erat dignissim ante commodo mollis. Donec imperdiet mauris elit, nec blandit dolor feugiat ut. Proin iaculis enim ut tortor pretium commodo. Etiam aliquet hendrerit dolor sed fringilla. Vestibulum facilisis nibh tincidunt dui egestas, vitae congue mi imperdiet. Duis vulputate ultricies lectus id cursus. Fusce bibendum sem dignissim, bibendum purus quis, mollis ex. Cras ac est justo. Duis congue mattis ipsum, vitae sagittis justo dictum sit amet. Duis aliquam pharetra sem, non laoreet ante laoreet ac. Mauris ornare mi tempus rutrum consequat.
            EOT;
    }
}
