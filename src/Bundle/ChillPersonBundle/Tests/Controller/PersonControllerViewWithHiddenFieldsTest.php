<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

use Chill\PersonBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonControllerViewWithHiddenFieldsTest extends WebTestCase
{
    private ?object $em = null;

    private ?Person $person = null;

    /**
     * @var string The url to view the person details
     */
    private readonly string $viewUrl;

    protected function setUp(): void
    {
        /*
         * self::bootKernel(['environment' => 'test_with_hidden_fields']);
         *
         * $this->em = self::$kernel->getContainer()
         * ->get('doctrine.orm.entity_manager');
         *
         * $center = $this->em->getRepository(\Chill\MainBundle\Entity\Center::class)
         * ->findOneBy(['name' => 'Center A']);
         *
         * $this->person = (new Person())
         * ->setLastName('Tested Person')
         * ->setFirstName('Réginald')
         * ->setCenter($center)
         * ->setGender(Person::MALE_GENDER);
         *
         * $this->em->persist($this->person);
         * $this->em->flush();
         *
         * $this->viewUrl = '/en/person/' . $this->person->getId() . '/general';
         */
    }

    protected function tearDown(): void
    {
        /*
        $this->refreshPerson();
        $this->em->remove($this->person);
        $this->em->flush();
        */
    }

    /**
     * Test if the  view page is accessible.
     *
     * @group configurable_fields
     */
    public function testViewPerson(): never
    {
        $this->markTestSkipped('This configuration does not allow multiple environnements');
        $client = self::createClient(
            ['environment' => 'test_with_hidden_fields'],
            [
                'PHP_AUTH_USER' => 'center a_social',
                'PHP_AUTH_PW' => 'password',
                'HTTP_ACCEPT_LANGUAGE' => 'fr',
            ]
        );

        $crawler = $client->request('GET', $this->viewUrl);
        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Tested Person")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Réginald")')->count());
        $this->assertNotContains('Email addresses', $crawler->text());
        $this->assertNotContains('Phonenumber', $crawler->text());
        $this->assertNotContains('Langues parlées', $crawler->text());
        $this->assertNotContains(/* Etat */ 'civil', $crawler->text());
    }

    /**
     * Reload the person from the db.
     */
    protected function refreshPerson()
    {
        $this->person = $this->em->getRepository(Person::class)
            ->find($this->person->getId());
    }
}
