<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Controller;

use Chill\MainBundle\Test\PrepareClientTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 *
 * @coversNothing
 */
final class SocialIssueApiControllerTest extends WebTestCase
{
    use PrepareClientTrait;

    protected function tearDown(): void
    {
        self::ensureKernelShutdown();
    }

    /**
     * @depends testList
     */
    public function testItem(array $data): void
    {
        $socialIssues = $data['results'];
        shuffle($socialIssues);
        $socialIssue = \array_pop($socialIssues);

        $client = $this->getClientAuthenticated();
        $client->request(Request::METHOD_GET, sprintf('/api/1.0/person/social-work/social-issue/%d.json', $socialIssue['id']));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('type', $data);
    }

    public function testList(): array
    {
        $client = $this->getClientAuthenticated();
        $client->request(Request::METHOD_GET, '/api/1.0/person/social-work/social-issue.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $data = \json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertGreaterThan(0, $data['count']);
        $this->assertGreaterThan(0, \count($data['results']));

        return $data;
    }
}
