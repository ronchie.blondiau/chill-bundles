<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Validator\Person;

use Chill\PersonBundle\Validator\Constraints\Person\Birthdate;
use Chill\PersonBundle\Validator\Constraints\Person\BirthdateValidator;
use Symfony\Component\Clock\MockClock;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * Test the behaviour of BirthdayValidator.
 *
 * @internal
 *
 * @coversNothing
 */
final class BirthdateValidatorTest extends ConstraintValidatorTestCase
{
    public function testTomorrowInvalid()
    {
        $bornAfter = new \DateTime('2023-08-31');
        $this->validator->validate($bornAfter, $this->createConstraint());
        $this->buildViolation('msg')
            ->setParameter('%date%', (new \DateTime('2023-08-29'))->format('d-m-Y'))
            ->setCode('3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4')
            ->assertRaised();
    }

    public function testValidateTodayInvalid()
    {
        $bornToday = new \DateTime('2023-08-30');
        $this->validator->validate($bornToday, $this->createConstraint());
        $this->buildViolation('msg')
            ->setParameter('%date%', (new \DateTime('2023-08-29'))->format('d-m-Y'))
            ->setCode('3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4')
            ->assertRaised();
    }

    public function testValidateTooEarlyDate(): void
    {
        $bornLongTimeAgo = new \DateTime('1871-03-18');
        $this->validator->validate($bornLongTimeAgo, $this->createConstraint());

        $this->buildViolation('below')
            ->setParameter('%date%', (new \DateTime('1873-08-30'))->format('d-m-Y'))
            ->setCode('3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4')
            ->assertRaised();
    }

    public function testValidateYesterdayValid()
    {
        $bornYesterday = new \DateTime('2023-08-29');
        $this->validator->validate($bornYesterday, $this->createConstraint());
        $this->assertNoViolation();
    }

    protected function createValidator()
    {
        return new BirthdateValidator(
            new ParameterBag(['chill_person' => ['validation' => ['birthdate_not_after' => 'P1D']]]),
            new MockClock(new \DateTimeImmutable('2023-08-30T14:12:00'))
        );
    }

    private function createConstraint()
    {
        return new Birthdate([
            'message' => 'msg',
            'belowMessage' => 'below',
        ]);
    }
}
