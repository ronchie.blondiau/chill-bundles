<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Validator\Person;

use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Security\Resolver\CenterResolverManagerInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Validator\Constraints\Person\PersonHasCenter;
use Chill\PersonBundle\Validator\Constraints\Person\PersonHasCenterValidator;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonHasCenterValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    public function testValidateRequired()
    {
        $constraint = $this->getConstraint();
        $personHasCenter = (new Person())->setCenter(new Center());
        $personNoCenter = new Person();

        $this->validator->validate($personHasCenter, $constraint);
        $this->assertNoViolation();

        $this->validator->validate($personNoCenter, $constraint);
        $this->buildViolation('msg')
            ->atPath('property.path.center')
            ->assertRaised();
    }

    protected function createValidator()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag
            ->method('get')
            ->with($this->equalTo('chill_person'))
            ->willReturn([
                'validation' => [
                    'center_required' => true,
                ],
            ]);

        $prophecy = $this->prophesize(CenterResolverManagerInterface::class);

        $prophecy->resolveCenters(Argument::type(Person::class), Argument::any())->will(static function ($args) {
            $center = $args[0]->getCenter();

            if ($center instanceof Center) {
                return [$center];
            }

            return [];
        });

        return new PersonHasCenterValidator($parameterBag, $prophecy->reveal());
    }

    protected function getConstraint()
    {
        return new PersonHasCenter([
            'message' => 'msg',
        ]);
    }
}
