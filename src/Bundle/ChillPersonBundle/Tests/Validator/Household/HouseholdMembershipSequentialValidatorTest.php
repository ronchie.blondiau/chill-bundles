<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Tests\Validator\Household;

use Chill\PersonBundle\Entity\Household\Household;
use Chill\PersonBundle\Entity\Household\HouseholdMember;
use Chill\PersonBundle\Entity\Household\Position;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Templating\Entity\PersonRender;
use Chill\PersonBundle\Validator\Constraints\Household\HouseholdMembershipSequential;
use Chill\PersonBundle\Validator\Constraints\Household\HouseholdMembershipSequentialValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class HouseholdMembershipSequentialValidatorTest extends ConstraintValidatorTestCase
{
    public function testEmptyPerson()
    {
        $constraint = $this->getConstraint();

        $person = new Person();

        $this->validator->validate($person, $constraint);

        $this->assertNoViolation();
    }

    public function testMembershipCovering()
    {
        $constraint = $this->getConstraint();

        $person = new Person();
        $household = new Household();
        $position = (new Position())
            ->setShareHousehold(true);
        $membership = (new HouseholdMember())
            ->setPosition($position)
            ->setStartDate(new \DateTimeImmutable('2010-01-01'))
            ->setPerson($person);
        $membership = (new HouseholdMember())
            ->setPosition($position)
            ->setStartDate(new \DateTimeImmutable('2011-01-01'))
            ->setPerson($person);

        $this->validator->validate($person, $constraint);

        $this->buildViolation('msg')
            ->setParameters([
                '%person_name%' => 'name',
                '%from%' => '01-01-2011',
                '%nbHousehold%' => 2,
            ])
            ->assertRaised();
    }

    public function testMembershipCoveringNoShareHousehold()
    {
        $constraint = $this->getConstraint();

        $person = new Person();
        $household = new Household();
        $position = (new Position())
            ->setShareHousehold(false);
        $membership = (new HouseholdMember())
            ->setPosition($position)
            ->setStartDate(new \DateTimeImmutable('2010-01-01'))
            ->setPerson($person);
        $membership = (new HouseholdMember())
            ->setPosition($position)
            ->setStartDate(new \DateTimeImmutable('2011-01-01'))
            ->setPerson($person);

        $this->validator->validate($person, $constraint);

        $this->assertNoViolation();
    }

    protected function createValidator()
    {
        $render = $this->createMock(PersonRender::class);
        $render->method('renderString')
            ->willReturn('name');

        return new HouseholdMembershipSequentialValidator($render);
    }

    protected function getConstraint()
    {
        return new HouseholdMembershipSequential([
            'message' => 'msg',
        ]);
    }
}
