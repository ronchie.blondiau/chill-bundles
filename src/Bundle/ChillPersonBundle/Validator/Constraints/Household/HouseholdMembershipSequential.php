<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\Household;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class HouseholdMembershipSequential extends Constraint
{
    public $message = 'household_membership.Person with membership covering';

    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT];
    }
}
