<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Validator\Constraints\Person;

use Symfony\Component\Validator\Constraint;

/**
 * Create a constraint on birth date: the birthdate after today are not allowed.
 *
 * It is possible to add a delay before today, expressed as described in
 * interval_spec : http://php.net/manual/en/dateinterval.construct.php
 * (this interval_spec itself is based on ISO8601 :
 *  https://en.wikipedia.org/wiki/ISO_8601#Durations)
 *
 * @Annotation
 */
class Birthdate extends Constraint
{
    final public const BIRTHDATE_INVALID_CODE = '3f42fd96-0b2d-11ec-8cf3-0f3b1b1ca1c4';

    public $message = 'The birthdate must be before %date%';

    public $belowMessage = 'The birthdate must be after %date%';
}
