<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Test;

use Chill\MainBundle\Entity\Center;
use Chill\PersonBundle\Entity\Person;

trait PreparePersonTrait
{
    /**
     * prepare a person.
     *
     * Properties added are :
     * - firstname
     * - lastname
     * - gender
     *
     * This person should not be persisted in a database
     *
     * @return Person
     */
    protected function preparePerson(Center $center)
    {
        return (new Person())
            ->setCenter($center)
            ->setFirstName('test firstname')
            ->setLastName('default lastname')
            ->setGender(Person::MALE_GENDER);
    }
}
