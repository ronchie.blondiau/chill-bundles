<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

class HouseholdCompositionTypeApiController extends ApiController
{
    /**
     * @param QueryBuilder $query
     */
    protected function customizeQuery(string $action, Request $request, $query): void
    {
        match ($action) {
            '_index' => $query->andWhere($query->expr()->eq('e.active', "'TRUE'")),
            default => throw new \UnexpectedValueException('unexepcted action: '.$action),
        };
    }
}
