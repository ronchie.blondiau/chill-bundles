<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Repository\Relationships\RelationshipRepository;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RelationshipApiController extends ApiController
{
    public function __construct(private readonly ValidatorInterface $validator, private readonly RelationshipRepository $repository)
    {
    }

    /**
     * @ParamConverter("person", options={"id": "person_id"})
     */
    public function getRelationshipsByPerson(Person $person)
    {
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);

        $relationships = $this->repository->findByPerson($person);

        return $this->json($relationships, Response::HTTP_OK, [], ['groups' => ['read']]);
    }
}
