<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Repository\AccompanyingPeriodRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserAccompanyingPeriodController extends AbstractController
{
    public function __construct(private readonly AccompanyingPeriodRepository $accompanyingPeriodRepository, private readonly PaginatorFactory $paginatorFactory)
    {
    }

    /**
     * @Route("/{_locale}/person/accompanying-periods/my", name="chill_person_accompanying_period_user")
     */
    public function listAction(Request $request): Response
    {
        $active = $request->query->getBoolean('active', true);
        $steps = match ($active) {
            true => [
                AccompanyingPeriod::STEP_CONFIRMED,
                AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_LONG,
                AccompanyingPeriod::STEP_CONFIRMED_INACTIVE_SHORT,
            ],
            false => [
                AccompanyingPeriod::STEP_CLOSED,
            ]
        };

        $total = $this->accompanyingPeriodRepository->countBy(['user' => $this->getUser(), 'step' => $steps]);
        $pagination = $this->paginatorFactory->create($total);
        $accompanyingPeriods = $this->accompanyingPeriodRepository->findBy(
            ['user' => $this->getUser(), 'step' => $steps],
            ['openingDate' => 'DESC'],
            $pagination->getItemsPerPage(),
            $pagination->getCurrentPageFirstItemNumber()
        );

        return $this->render('@ChillPerson/AccompanyingPeriod/user_periods_list.html.twig', [
            'accompanyingPeriods' => $accompanyingPeriods,
            'pagination' => $pagination,
            'active' => $active,
        ]);
    }

    /**
     * @Route("/{_locale}/person/accompanying-periods/my/drafts", name="chill_person_accompanying_period_draft_user")
     */
    public function listDraftsAction(): Response
    {
        $total = $this->accompanyingPeriodRepository->countBy(['user' => $this->getUser(), 'step' => 'DRAFT']);
        $pagination = $this->paginatorFactory->create($total);
        $accompanyingPeriods = $this->accompanyingPeriodRepository->findBy(
            ['createdBy' => $this->getUser(), 'step' => 'DRAFT'],
            ['id' => 'DESC'],
            $pagination->getItemsPerPage(),
            $pagination->getCurrentPageFirstItemNumber()
        );

        return $this->render('@ChillPerson/AccompanyingPeriod/user_draft_periods_list.html.twig', [
            'accompanyingPeriods' => $accompanyingPeriods,
            'pagination' => $pagination,
        ]);
    }
}
