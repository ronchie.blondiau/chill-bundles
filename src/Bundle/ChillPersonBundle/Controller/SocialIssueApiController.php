<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class SocialIssueApiController extends ApiController
{
    protected function customizeQuery(string $action, Request $request, $query): void
    {
        $query->where(
            $query->expr()->orX(
                $query->expr()->gt('e.desactivationDate', ':now'),
                $query->expr()->isNull('e.desactivationDate')
            )
        );
        $query->setParameter('now', new \DateTimeImmutable());
    }

    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator, $_format)
    {
        $query->addOrderBy('e.ordering', 'ASC');

        return $query;
    }
}
