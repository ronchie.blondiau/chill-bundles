<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

class OpeningApiController extends ApiController
{
    protected function customizeQuery(string $action, Request $request, $qb): void
    {
        $qb->where($qb->expr()->gt('e.noActiveAfter', ':now'))
            ->orWhere($qb->expr()->isNull('e.noActiveAfter'));
        $qb->setParameter('now', new \DateTime('now'));
    }
}
