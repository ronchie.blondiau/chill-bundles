<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class HouseholdMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(TranslatorInterface $translator, private readonly Security $security)
    {
        $this->translator = $translator;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters): void
    {
        /** @var \Chill\PersonBundle\Entity\Household\Household $household */
        $household = $parameters['household'];

        $menu->addChild($this->translator->trans('household.Household summary'), [
            'route' => 'chill_person_household_summary',
            'routeParameters' => [
                'household_id' => $household->getId(),
            ], ])
            ->setExtras(['order' => 10]);

        $menu->addChild($this->translator->trans('household.Relationship'), [
            'route' => 'chill_person_household_relationship',
            'routeParameters' => [
                'household_id' => $household->getId(),
            ], ])
            ->setExtras(['order' => 15]);

        $menu->addChild($this->translator->trans('household_composition.Compositions'), [
            'route' => 'chill_person_household_composition_index',
            'routeParameters' => [
                'id' => $household->getId(),
            ], ])
            ->setExtras(['order' => 17]);

        if ($this->security->isGranted(AccompanyingPeriodVoter::SEE, $parameters['household'])) {
            $menu->addChild($this->translator->trans('household.Accompanying period'), [
                'route' => 'chill_person_household_accompanying_period',
                'routeParameters' => [
                    'household_id' => $household->getId(),
                ], ])
                ->setExtras(['order' => 20]);
        }

        $menu->addChild($this->translator->trans('household.Addresses'), [
            'route' => 'chill_person_household_addresses',
            'routeParameters' => [
                'household_id' => $household->getId(),
            ], ])
            ->setExtras(['order' => 30]);
    }

    public static function getMenuIds(): array
    {
        return ['household'];
    }
}
