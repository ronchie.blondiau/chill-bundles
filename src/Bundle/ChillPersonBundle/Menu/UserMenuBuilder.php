<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    public $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $menu->addChild('My accompanying periods', [
                'route' => 'chill_person_accompanying_period_user',
            ])
                ->setExtras([
                    'order' => 20,
                    'icon' => 'tasks',
                ]);
            $menu->addChild('My accompanying periods in draft', [
                'route' => 'chill_person_accompanying_period_draft_user',
            ])
                ->setExtras([
                    'order' => 30,
                    'icon' => 'tasks',
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['user'];
    }
}
