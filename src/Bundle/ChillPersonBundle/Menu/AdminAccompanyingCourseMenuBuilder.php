<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminAccompanyingCourseMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $menu->addChild('person_admin.accompanying_period', [
            'route' => 'chill_accompanying-course_admin_index',
        ])
            ->setAttribute('class', 'list-group-item-header')
            ->setExtras([
                'order' => 2200,
            ]);

        $menu->addChild('person_admin.closing motives', [
            'route' => 'chill_crud_closing_motive_index',
        ])->setExtras(['order' => 2210]);

        $menu->addChild('person_admin.origin', [
            'route' => 'chill_crud_origin_index',
        ])->setExtras(['order' => 2210]);
    }

    public static function getMenuIds(): array
    {
        return ['admin_section', 'admin_accompanying_course'];
    }
}
