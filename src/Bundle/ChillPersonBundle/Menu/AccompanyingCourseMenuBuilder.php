<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Workflow\Registry;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AccompanyingCourseMenuBuilder.
 */
class AccompanyingCourseMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(TranslatorInterface $translator, protected Registry $registry, protected Security $security)
    {
        $this->translator = $translator;
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters): void
    {
        /** @var AccompanyingPeriod $period */
        $period = $parameters['accompanyingCourse'];

        $menu->addChild($this->translator->trans('Resume Accompanying Course'), [
            'route' => 'chill_person_accompanying_course_index',
            'routeParameters' => [
                'accompanying_period_id' => $period->getId(),
            ], ])
            ->setExtras(['order' => 10]);

        if ($this->security->isGranted(AccompanyingPeriodVoter::EDIT, $period)) {
            $menu->addChild($this->translator->trans('Edit Accompanying Course'), [
                'route' => 'chill_person_accompanying_course_edit',
                'routeParameters' => [
                    'accompanying_period_id' => $period->getId(),
                ], ])
                ->setExtras(['order' => 20]);
        }

        if (AccompanyingPeriod::STEP_DRAFT === $period->getStep()) {
            // no more menu items if the period is draft
            return;
        }

        if ($this->security->isGranted(AccompanyingPeriodVoter::SEE_DETAILS, $period)) {
            /*
                $menu->addChild($this->translator->trans('Accompanying Course History'), [
                    'route' => 'chill_person_accompanying_course_history',
                    'routeParameters' => [
                        'accompanying_period_id' => $period->getId(),
                    ], ])
                    ->setExtras(['order' => 30]);
             */

            $menu->addChild($this->translator->trans('Accompanying Course Comment'), [
                'route' => 'chill_person_accompanying_period_comment_list',
                'routeParameters' => [
                    'accompanying_period_id' => $period->getId(),
                ], ])
                ->setExtras(['order' => 50]);
        }

        if ($this->security->isGranted(AccompanyingPeriodWorkVoter::SEE, $period)) {
            $menu->addChild($this->translator->trans('Accompanying Course Action'), [
                'route' => 'chill_person_accompanying_period_work_list',
                'routeParameters' => [
                    'id' => $period->getId(),
                ], ])
                ->setExtras(['order' => 40]);
        }

        $workflow = $this->registry->get($period, 'accompanying_period_lifecycle');

        if ($this->security->isGranted(AccompanyingPeriodVoter::EDIT, $period)) {
            if ($workflow->can($period, 'close')) {
                $menu->addChild($this->translator->trans('Close Accompanying Course'), [
                    'route' => 'chill_person_accompanying_course_close',
                    'routeParameters' => [
                        'accompanying_period_id' => $period->getId(),
                    ], ])
                    ->setExtras(['order' => 99999]);
            }
        }

        if (null !== $period->getClosingDate()
            && $this->security->isGranted(AccompanyingPeriodVoter::RE_OPEN_COURSE, $period)) {
            $menu->addChild($this->translator->trans('Re-open accompanying course'), [
                'route' => 'chill_person_accompanying_course_reopen',
                'routeParameters' => [
                    'accompanying_period_id' => $period->getId(),
                ], ])
                ->setExtras(['order' => 99998]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['accompanyingCourse'];
    }
}
