<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodVoter;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SectionMenuBuilder.
 */
class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     * SectionMenuBuilder constructor.
     */
    public function __construct(protected ParameterBagInterface $parameterBag, private readonly Security $security, protected TranslatorInterface $translator)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->security->isGranted(PersonVoter::CREATE) && $this->parameterBag->get('chill_person.create_person_allowed')) {
            $menu->addChild($this->translator->trans('Add a person'), [
                'route' => 'chill_person_new',
            ])
                ->setExtras([
                    'order' => 10,
                    'icons' => ['plus'],
                ]);
        }

        if ($this->parameterBag->get('chill_person.create_parcours_allowed')) {
            $menu->addChild($this->translator->trans('Create an accompanying course'), [
                'route' => 'chill_person_accompanying_course_new',
            ])
                ->setExtras([
                    'order' => 11,
                    'icons' => ['plus'],
                ]);
        }

        if ($this->security->isGranted(AccompanyingPeriodVoter::REASSIGN_BULK, null)) {
            $menu->addChild($this->translator->trans('reassign.Bulk reassign'), [
                'route' => 'chill_course_list_reassign',
            ])
                ->setExtras([
                    'order' => 40,
                    'icons' => [],
                ]);
        }

        if ($this->security->getUser() instanceof User && $this->security->isGranted('ROLE_USER')) {
            $menu
                ->addChild('Régulation', [
                    'route' => 'chill_person_course_list_regulation',
                ])
                ->setExtras([
                    'order' => 150,
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
