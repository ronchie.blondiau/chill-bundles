<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class AccompanyingPeriodWorkEvaluationUpdateQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'cpapw.accompanyingperiod_id';
    }

    public function getRelatedEntityColumn(): string
    {
        return AccompanyingPeriodWorkEvaluationDocument::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'doc.id';
    }

    public function getUserIdColumn(): string
    {
        return 'doc.updatedby_id';
    }

    public function getDateTimeColumn(): string
    {
        return 'doc.updatedAt';
    }

    public function getMetadataColumn(): string
    {
        return "'{}'::jsonb";
    }

    public function getDiscriminator(): string
    {
        return 'accompanying_period_work_evaluation_document_updated_at';
    }

    public function getFromStatement(): string
    {
        return 'chill_person_accompanying_period_work_evaluation_document doc
            JOIN chill_person_accompanying_period_work_evaluation e ON doc.accompanyingperiodworkevaluation_id = e.id
            JOIN chill_person_accompanying_period_work cpapw ON cpapw.id = e.accompanyingperiodwork_id';
    }

    public function getWhereClause(): string
    {
        return 'doc.updatedAt IS NOT NULL';
    }
}
