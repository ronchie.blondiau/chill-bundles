<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class AccompanyingPeriodStartQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'a.id';
    }

    public function getRelatedEntityColumn(): string
    {
        return AccompanyingPeriod::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'a.id';
    }

    public function getUserIdColumn(): string
    {
        return 'NULL';
    }

    public function getDateTimeColumn(): string
    {
        return 'a.openingDate';
    }

    public function getDiscriminator(): string
    {
        return 'accompanying_period_start';
    }

    public function getMetadataColumn(): string
    {
        return '\'{}\'::jsonb';
    }

    public function getFromStatement(): string
    {
        return 'chill_person_accompanying_period a';
    }

    public function getWhereClause(): string
    {
        return '';
    }
}
