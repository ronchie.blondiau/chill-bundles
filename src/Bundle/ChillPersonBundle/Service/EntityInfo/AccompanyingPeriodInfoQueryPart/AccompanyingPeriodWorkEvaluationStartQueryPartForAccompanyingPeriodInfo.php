<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class AccompanyingPeriodWorkEvaluationStartQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'cpapw.accompanyingperiod_id';
    }

    public function getRelatedEntityColumn(): string
    {
        return AccompanyingPeriodWorkEvaluation::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'e.id';
    }

    public function getUserIdColumn(): string
    {
        return 'cpapwr.user_id';
    }

    public function getDateTimeColumn(): string
    {
        return 'e.startDate';
    }

    public function getMetadataColumn(): string
    {
        return "'{}'::jsonb";
    }

    public function getDiscriminator(): string
    {
        return 'accompanying_period_work_evaluation_start';
    }

    public function getFromStatement(): string
    {
        return 'chill_person_accompanying_period_work_evaluation e
            JOIN chill_person_accompanying_period_work cpapw ON cpapw.id = e.accompanyingperiodwork_id
            LEFT JOIN chill_person_accompanying_period_work_referrer cpapwr ON cpapw.id = cpapwr.accompanyingperiodwork_id AND daterange(cpapwr.startDate, cpapwr.endDate) @> e.startDate';
    }

    public function getWhereClause(): string
    {
        return 'e.startDate IS NOT NULL';
    }
}
