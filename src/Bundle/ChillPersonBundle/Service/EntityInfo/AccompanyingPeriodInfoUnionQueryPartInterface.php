<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Service\EntityInfo;

interface AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string;

    /**
     * @return class-string
     */
    public function getRelatedEntityColumn(): string;

    public function getRelatedEntityIdColumn(): string;

    public function getUserIdColumn(): string;

    public function getDateTimeColumn(): string;

    public function getDiscriminator(): string;

    public function getMetadataColumn(): string;

    public function getFromStatement(): string;

    public function getWhereClause(): string;
}
