<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Load closing motives into database.
 */
class LoadAccompanyingPeriodClosingMotive extends AbstractFixture implements OrderedFixtureInterface
{
    public static $closingMotives = [
        'nothing_to_do' => [
            'name' => [
                'fr' => 'Plus rien à faire',
                'en' => 'Nothing to do',
                'nl' => 'nieks meer te doen',
            ],
        ],
        'did_not_come_back' => [
            'name' => [
                'fr' => "N'est plus revenu",
                'en' => "Did'nt come back",
                'nl' => 'Niet teruggekomen',
            ],
        ],
        'no_more_money' => [
            'active' => false,
            'name' => [
                'fr' => "Plus d'argent",
                'en' => 'No more money',
                'nl' => 'Geen geld',
            ],
        ],
    ];

    public static $references = [];

    public function getOrder()
    {
        return 9500;
    }

    public function load(ObjectManager $manager)
    {
        foreach (static::$closingMotives as $ref => $new) {
            $motive = new ClosingMotive();
            $motive->setName($new['name'])
                ->setActive($new['active'] ?? true);

            $manager->persist($motive);
            $this->addReference($ref, $motive);
            echo "Adding ClosingMotive {$ref}\n";
        }

        $manager->flush();
    }
}
