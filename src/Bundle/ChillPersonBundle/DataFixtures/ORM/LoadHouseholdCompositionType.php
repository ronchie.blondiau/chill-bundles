<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\DataFixtures\ORM;

use Chill\PersonBundle\Entity\Household\HouseholdCompositionType;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

class LoadHouseholdCompositionType extends AbstractFixture implements FixtureGroupInterface
{
    final public const TYPES = [
        ['fr' => 'Couple avec enfant(s)'],
        ['fr' => 'Couple sans enfant'],
        ['fr' => 'Mère seule'],
        ['fr' => 'Père seul'],
        ['fr' => 'Mère isolée'],
        ['fr' => 'Père isolé'],
        ['fr' => 'Homme seul'],
        ['fr' => 'Femme seule'],
    ];

    public static function getGroups(): array
    {
        return ['composition-type'];
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::TYPES as $type) {
            $manager->persist(
                (new HouseholdCompositionType())
                    ->setLabel($type)
            );
        }

        $manager->flush();
    }
}
