<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\AccompanyingPeriod\Lifecycle;

use Chill\PersonBundle\Entity\AccompanyingPeriod;

/**
 * Message which will request a change in the step of accompanying period.
 */
class AccompanyingPeriodStepChangeRequestMessage
{
    private int $periodId;

    public function __construct(
        AccompanyingPeriod|int $period,
        private readonly string $transition,
    ) {
        if (is_int($period)) {
            $this->periodId = $period;
        } else {
            if (null !== $id = $period->getId()) {
                $this->periodId = $id;
            }

            throw new \LogicException('This AccompanyingPeriod does not have and id yet');
        }
    }

    public function getPeriodId(): int
    {
        return $this->periodId;
    }

    public function getTransition(): string
    {
        return $this->transition;
    }
}
