<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Serializer\Normalizer;

use Chill\PersonBundle\Entity\AccompanyingPeriod\Origin;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * @internal we keep this normalizer, because the property 'text' may be replace by a rendering in the future
 */
final class AccompanyingPeriodOriginNormalizer implements NormalizerInterface
{
    /**
     * @param Origin      $origin
     * @param string|null $format
     */
    public function normalize($origin, $format = null, array $context = [])
    {
        return [
            'type' => 'origin',
            'id' => $origin->getId(),
            'label' => $origin->getLabel(),
            'text' => $origin->getLabel(),
        ];
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof Origin && 'json' === $format;
    }
}
