<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Timeline;

use Chill\PersonBundle\Entity\AccompanyingPeriod;

/**
 * Provide information for opening periods to timeline.
 */
class TimelineAccompanyingPeriodOpening extends AbstractTimelineAccompanyingPeriod
{
    public function fetchQuery($context, array $args)
    {
        $metadata = $this->em
            ->getClassMetadata(AccompanyingPeriod::class);

        $query = $this->basicFetchQuery($context, $args);

        $query->setKey('accompanying_period_opening')
            ->setDate($metadata->getColumnName('openingDate'));

        return $query;
    }

    public function getEntityTemplate($entity, $context, array $args)
    {
        return $this->getBasicEntityTemplate(
            '@ChillPerson/Timeline/opening_period.html.twig',
            $entity,
            $context,
            $args
        );
    }

    public function supportsType($type)
    {
        return 'accompanying_period_opening' === $type;
    }
}
