<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Notification;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Repository\AccompanyingPeriodRepository;

final readonly class AccompanyingPeriodNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private AccompanyingPeriodRepository $accompanyingPeriodRepository)
    {
    }

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillPerson/AccompanyingPeriod/showInNotification.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return [
            'notification' => $notification,
            'period' => $this->accompanyingPeriodRepository->find($notification->getRelatedEntityId()),
        ];
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return AccompanyingPeriod::class === $notification->getRelatedEntityClass();
    }
}
