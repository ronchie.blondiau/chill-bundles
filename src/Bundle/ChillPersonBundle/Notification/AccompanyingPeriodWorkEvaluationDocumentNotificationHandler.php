<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Notification;

use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;

final readonly class AccompanyingPeriodWorkEvaluationDocumentNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private AccompanyingPeriodWorkEvaluationDocumentRepository $accompanyingPeriodWorkEvaluationDocumentRepository)
    {
    }

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillPerson/AccompanyingCourseWork/showEvaluationDocumentInNotification.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return [
            'notification' => $notification,
            'document' => $doc = $this->accompanyingPeriodWorkEvaluationDocumentRepository->find($notification->getRelatedEntityId()),
            'evaluation' => $doc?->getAccompanyingPeriodWorkEvaluation(),
        ];
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluationDocument::class === $notification->getRelatedEntityClass();
    }
}
