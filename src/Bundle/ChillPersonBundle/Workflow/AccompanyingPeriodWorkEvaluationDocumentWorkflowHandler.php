<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Workflow;

use Chill\MainBundle\Entity\Workflow\EntityWorkflow;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Chill\MainBundle\Workflow\EntityWorkflowHandlerInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocument;
use Chill\PersonBundle\Repository\AccompanyingPeriod\AccompanyingPeriodWorkEvaluationDocumentRepository;
use Chill\PersonBundle\Security\Authorization\AccompanyingPeriodWorkEvaluationDocumentVoter;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccompanyingPeriodWorkEvaluationDocumentWorkflowHandler implements EntityWorkflowHandlerInterface
{
    public function __construct(private readonly AccompanyingPeriodWorkEvaluationDocumentRepository $repository, private readonly TranslatableStringHelperInterface $translatableStringHelper, private readonly TranslatorInterface $translator)
    {
    }

    public function getDeletionRoles(): array
    {
        return [
            '_',
        ];
    }

    public function getEntityData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $doc = $this->getRelatedEntity($entityWorkflow);

        if (null === $doc) {
            return [
                'persons' => [],
            ];
        }

        return [
            'persons' => $doc->getAccompanyingPeriodWorkEvaluation()
                ->getAccompanyingPeriodWork()->getPersons(),
        ];
    }

    public function getEntityTitle(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        $doc = $this->getRelatedEntity($entityWorkflow);

        if (null === $doc) {
            return $this->translator->trans('workflow.doc for evaluation deleted');
        }

        return $this->translator->trans(
            'workflow.Doc for evaluation (n°%eval%)',
            ['%eval%' => $entityWorkflow->getRelatedEntityId()]
        ).' ('.$this->translatableStringHelper->localize($doc->getAccompanyingPeriodWorkEvaluation()
            ->getEvaluation()->getTitle()).') '.$doc->getTitle();
    }

    public function getRelatedEntity(EntityWorkflow $entityWorkflow): ?AccompanyingPeriodWorkEvaluationDocument
    {
        return $this->repository->find($entityWorkflow->getRelatedEntityId());
    }

    /**
     * @param AccompanyingPeriodWorkEvaluationDocument $object
     *
     * @return array[]
     */
    public function getRelatedObjects(object $object): array
    {
        return [
            ['entityClass' => AccompanyingPeriodWorkEvaluationDocument::class, $object->getId()],
        ];
    }

    public function getRoleShow(EntityWorkflow $entityWorkflow): ?string
    {
        return AccompanyingPeriodWorkEvaluationDocumentVoter::SEE;
    }

    public function getSuggestedUsers(EntityWorkflow $entityWorkflow): array
    {
        $suggestedUsers = $entityWorkflow->getUsersInvolved();

        $referrer = $this->getRelatedEntity($entityWorkflow)
            ->getAccompanyingPeriodWorkEvaluation()
            ->getAccompanyingPeriodWork()
            ->getAccompanyingPeriod()
            ->getUser();

        $suggestedUsers[spl_object_hash($referrer)] = $referrer;

        return $suggestedUsers;
    }

    public function getTemplate(EntityWorkflow $entityWorkflow, array $options = []): string
    {
        return '@ChillPerson/Workflow/_evaluation_document.html.twig';
    }

    public function getTemplateData(EntityWorkflow $entityWorkflow, array $options = []): array
    {
        $doc = $this->getRelatedEntity($entityWorkflow);

        return [
            'entity_workflow' => $entityWorkflow,
            'evaluation' => null !== $doc ? $doc->getAccompanyingPeriodWorkEvaluation() : $doc,
            'doc' => $doc,
        ];
    }

    public function isObjectSupported(object $object): bool
    {
        return $object instanceof AccompanyingPeriodWorkEvaluationDocument;
    }

    public function supports(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return AccompanyingPeriodWorkEvaluationDocument::class === $entityWorkflow->getRelatedEntityClass();
    }

    public function supportsFreeze(EntityWorkflow $entityWorkflow, array $options = []): bool
    {
        return false;
    }
}
