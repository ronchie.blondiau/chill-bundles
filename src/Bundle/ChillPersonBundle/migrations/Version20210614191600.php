<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add comments and expecting birth to household.
 */
final class Version20210614191600 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household DROP comment_members');
        $this->addSql('ALTER TABLE chill_person_household DROP waiting_for_birth');
        $this->addSql('ALTER TABLE chill_person_household DROP waiting_for_birth_date');
    }

    public function getDescription(): string
    {
        return 'Add comments and expecting birth to household';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household ADD comment_members TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household ADD waiting_for_birth BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household ADD waiting_for_birth_date DATE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_household.waiting_for_birth_date IS \'(DC2Type:date_immutable)\'');
    }
}
