<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create the relationship and relation entity.
 */
final class Version20211025141226 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_relationships DROP CONSTRAINT FK_23D47C513256915B');
        $this->addSql('DROP SEQUENCE chill_person_relations_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_person_relationships_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_person_relations');
        $this->addSql('DROP TABLE chill_person_relationships');
    }

    public function getDescription(): string
    {
        return 'Create the relationship and relation entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_relations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_person_relationships_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_relations (id INT NOT NULL, title JSON DEFAULT NULL, reverseTitle JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_person_relationships (id INT NOT NULL, relation_id INT NOT NULL, reverse BOOLEAN NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, fromPerson_id INT NOT NULL, toPerson_id INT NOT NULL, createdBy_id INT NOT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_23D47C51CBA59C1E ON chill_person_relationships (fromPerson_id)');
        $this->addSql('CREATE INDEX IDX_23D47C514013E22A ON chill_person_relationships (toPerson_id)');
        $this->addSql('CREATE INDEX IDX_23D47C513256915B ON chill_person_relationships (relation_id)');
        $this->addSql('CREATE INDEX IDX_23D47C513174800F ON chill_person_relationships (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_23D47C5165FF1AEC ON chill_person_relationships (updatedBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_person_relationships.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_relationships.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_relationships ADD CONSTRAINT FK_23D47C51CBA59C1E FOREIGN KEY (fromPerson_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_relationships ADD CONSTRAINT FK_23D47C514013E22A FOREIGN KEY (toPerson_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_relationships ADD CONSTRAINT FK_23D47C513256915B FOREIGN KEY (relation_id) REFERENCES chill_person_relations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_relationships ADD CONSTRAINT FK_23D47C513174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_relationships ADD CONSTRAINT FK_23D47C5165FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
