<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

final class Version20220215135509 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function down(Schema $schema): void
    {
        throw new \Exception('You should not do that.');
    }

    public function getDescription(): string
    {
        return 'Update phone numbers for person';
    }

    public function up(Schema $schema): void
    {
        $carrier_code = $this->container
            ->getParameter('chill_main')['phone_helper']['default_carrier_code'];

        if (null === $carrier_code) {
            throw new \RuntimeException('no carrier code');
        }

        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber TYPE TEXT');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_person ALTER phonenumber DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_person.phonenumber IS NULL');

        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber TYPE TEXT');
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_person ALTER mobilenumber DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_person.mobilenumber IS NULL');

        $this->addSql(
            'UPDATE chill_person_person SET '.
            $this->buildMigrationPhonenumberClause($carrier_code, 'phonenumber').
            ', '.
            $this->buildMigrationPhoneNumberClause($carrier_code, 'mobilenumber')
        );

        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber TYPE TEXT');
        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_phone ALTER phonenumber DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_phone.phonenumber IS NULL');

        $this->addSql(
            'UPDATE chill_person_phone SET '.
            $this->buildMigrationPhoneNumberClause($carrier_code, 'phonenumber')
        );
    }

    private function buildMigrationPhoneNumberClause(string $defaultCarriercode, string $field): string
    {
        $util = PhoneNumberUtil::getInstance();

        $countryCode = $util->getCountryCodeForRegion($defaultCarriercode);

        return sprintf('%s=CASE
                WHEN %s = \'\' THEN NULL
                WHEN LEFT(%s, 1) = \'0\'
                    THEN \'+%s\' || replace(replace(substr(%s, 2), \'(0)\', \'\'), \' \', \'\')
                ELSE replace(replace(%s, \'(0)\', \'\'),\' \', \'\')
            END', $field, $field, $field, $countryCode, $field, $field);
    }
}
