<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Set comment in household as Embedded Comment.
 */
final class Version20210615074857 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household RENAME comment_members_comment TO comment_members');
        $this->addSql('ALTER TABLE chill_person_household ALTER comment_members SET DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_person_household ALTER comment_members SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household DROP comment_members_comment');
        $this->addSql('ALTER TABLE chill_person_household DROP comment_members_userId');
        $this->addSql('ALTER TABLE chill_person_household DROP comment_members_date');
    }

    public function getDescription(): string
    {
        return 'replace comment in household as embedded comment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household RENAME comment_members TO comment_members_comment');
        $this->addSql('ALTER TABLE chill_person_household ALTER COLUMN comment_members_comment DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household ALTER COLUMN comment_members_comment SET DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_household ADD comment_members_userId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_household ADD comment_members_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_household ADD CONSTRAINT fk_household_comment_embeddable_user FOREIGN KEY (comment_members_userId) REFERENCES users (id)');
    }
}
