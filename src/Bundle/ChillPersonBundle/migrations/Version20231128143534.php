<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231128143534 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set a default for the content column of accompanying_period_comment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE chill_person_accompanying_period_comment SET content='' WHERE content IS NULL");
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ALTER COLUMN content SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ALTER COLUMN content SET DEFAULT \'\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ALTER COLUMN content DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_comment ALTER COLUMN content DROP NOT NULL');
    }
}
