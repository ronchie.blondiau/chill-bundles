<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a link between social issues and accompanying periods.
 */
final class Version20210518075908 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE chill_person_accompanying_period_social_issues');
    }

    public function getDescription(): string
    {
        return 'Add a link between social issue and accompanying period';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE chill_person_accompanying_period_social_issues (accompanyingperiod_id INT NOT NULL, socialissue_id INT NOT NULL, PRIMARY KEY(accompanyingperiod_id, socialissue_id))');
        $this->addSql('CREATE INDEX IDX_CAFE078F550B0C53 ON chill_person_accompanying_period_social_issues (accompanyingperiod_id)');
        $this->addSql('CREATE INDEX IDX_CAFE078FA549916C ON chill_person_accompanying_period_social_issues (socialissue_id)');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_social_issues ADD CONSTRAINT FK_CAFE078F550B0C53 FOREIGN KEY (accompanyingperiod_id) REFERENCES chill_person_accompanying_period (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_social_issues ADD CONSTRAINT FK_CAFE078FA549916C FOREIGN KEY (socialissue_id) REFERENCES chill_person_social_issue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
