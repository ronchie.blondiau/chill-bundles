<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create class AccompanyingPeriodWorkEvaluation.
 */
final class Version20210729163023 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_person_accompanying_period_work_evaluation_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_person_accompanying_period_work_evaluation');
    }

    public function getDescription(): string
    {
        return 'create class and tables for AccompanyingPeriodWorkEvaluation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_accompanying_period_work_evaluation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_person_accompanying_period_work_evaluation (id INT NOT NULL, evaluation_id INT DEFAULT NULL, startDate DATE DEFAULT NULL, endDate DATE DEFAULT NULL, maxDate DATE DEFAULT NULL, warningInterval INTERVAL DEFAULT NULL, createdAt DATE DEFAULT NULL, updatedAt DATE DEFAULT NULL, accompanyingPeriodWork_id INT DEFAULT NULL, createdBy_id INT DEFAULT NULL, updatedBy_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_741A3A0BC55C1209 ON chill_person_accompanying_period_work_evaluation (accompanyingPeriodWork_id)');
        $this->addSql('CREATE INDEX IDX_741A3A0B456C5646 ON chill_person_accompanying_period_work_evaluation (evaluation_id)');
        $this->addSql('CREATE INDEX IDX_741A3A0B3174800F ON chill_person_accompanying_period_work_evaluation (createdBy_id)');
        $this->addSql('CREATE INDEX IDX_741A3A0B65FF1AEC ON chill_person_accompanying_period_work_evaluation (updatedBy_id)');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.maxDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.warningInterval IS \'(DC2Type:dateinterval)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.createdAt IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_evaluation.updatedAt IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation ADD CONSTRAINT FK_741A3A0BC55C1209 FOREIGN KEY (accompanyingPeriodWork_id) REFERENCES chill_person_accompanying_period_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation ADD CONSTRAINT FK_741A3A0B456C5646 FOREIGN KEY (evaluation_id) REFERENCES chill_person_social_work_evaluation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation ADD CONSTRAINT FK_741A3A0B3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_evaluation ADD CONSTRAINT FK_741A3A0B65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
