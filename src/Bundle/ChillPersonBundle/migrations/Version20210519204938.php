<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add updatedAt, updatedBy, createdAt on accompanying period.
 */
final class Version20210519204938 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP createdAt');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP updatedAt');
        $this->addSql('ALTER TABLE chill_person_accompanying_period DROP updatedBy_id');
    }

    public function getDescription(): string
    {
        return 'Add updatedAt, updatedBy, createdAt on accompanying period';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD updatedBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period ADD CONSTRAINT FK_E260A86865FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E260A86865FF1AEC ON chill_person_accompanying_period (updatedBy_id)');
    }
}
