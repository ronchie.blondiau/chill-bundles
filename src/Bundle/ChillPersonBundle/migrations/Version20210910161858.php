<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Optimize trigram index on person fullname: create index for both center_id and fullname.
 */
final class Version20210910161858 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX fullnamecanonical_trgm_idx');
        $this->addSql('CREATE INDEX fullnameCanonical_trgm_idx ON chill_person_person USING GIST (fullnameCanonical gist_trgm_ops)');
    }

    public function getDescription(): string
    {
        return 'Optimize trigram index on person fullname: create index for both center_id and fullname';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX fullnamecanonical_trgm_idx');
        $this->addSql('CREATE INDEX fullnameCanonical_trgm_idx ON chill_person_person USING GIST (center_id, fullnameCanonical gist_trgm_ops)');
    }
}
