<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211119211101 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX person_birthdate');
        $this->addSql('DROP INDEX phonenumber');
    }

    public function getDescription(): string
    {
        return 'add indexes for searching by birthdate';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX person_birthdate ON chill_person_person (birthdate)');
        $this->addSql('CREATE INDEX phonenumber ON chill_person_phone USING GIST (phonenumber gist_trgm_ops)');
    }
}
