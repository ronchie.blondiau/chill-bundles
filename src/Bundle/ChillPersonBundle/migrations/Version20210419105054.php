<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Rename sequence "closing motive".
 */
final class Version20210419105054 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_closingmotive_id_seq '
            .'RENAME TO chill_person_closingmotive_id_seq');
    }

    public function getDescription(): string
    {
        return 'rename sequence "closing motive"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_closingmotive_id_seq RENAME TO '
            .'chill_person_accompanying_period_closingmotive_id_seq');
    }
}
