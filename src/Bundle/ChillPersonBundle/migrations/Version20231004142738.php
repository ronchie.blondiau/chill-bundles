<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231004142738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'keep an history of accompanying period work referrer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE chill_person_accompanying_period_work_referrer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT fk_3619f5ebb99f6060');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT FK_3619F5EBA76ED395');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT chill_person_accompanying_period_work_referrer_pkey');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD id INT NOT NULL DEFAULT nextval(\'chill_person_accompanying_period_work_referrer_id_seq\')');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD endDate DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD startDate DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER accompanyingperiodwork_id DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER user_id DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT FK_3619F5EBC55C1209 FOREIGN KEY (accompanyingPeriodWork_id) REFERENCES chill_person_accompanying_period_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT FK_3619F5EBA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD PRIMARY KEY (id)');
        $this->addSql('ALTER INDEX idx_3619f5ebb99f6060 RENAME TO IDX_3619F5EBC55C1209');

        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL;');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD createdBy_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD updatedBy_id INT DEFAULT NULL;');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work_referrer.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT FK_3619F5EB3174800F FOREIGN KEY (createdBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT FK_3619F5EB65FF1AEC FOREIGN KEY (updatedBy_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
        $this->addSql('CREATE INDEX IDX_3619F5EB3174800F ON chill_person_accompanying_period_work_referrer (createdBy_id);');
        $this->addSql('CREATE INDEX IDX_3619F5EB65FF1AEC ON chill_person_accompanying_period_work_referrer (updatedBy_id);');

        // set a default date on startDate
        $this->addSql('UPDATE chill_person_accompanying_period_work_referrer r SET startdate=w.startdate FROM chill_person_accompanying_period_work w WHERE w.id = r.accompanyingperiodwork_id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER startDate SET NOT NULL');

        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT cpapwr_start_date_before_end CHECK (startdate <= enddate OR enddate IS NULL)');

        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer
            ADD CONSTRAINT acc_period_work_ref_history_not_overlaps
            EXCLUDE USING GIST (accompanyingperiodwork_id with =, user_id with =, tsrange(startdate, enddate) with &&)
            DEFERRABLE INITIALLY DEFERRED');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE chill_person_accompanying_period_work_referrer_id_seq CASCADE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT FK_3619F5EBC55C1209');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT fk_3619f5eba76ed395');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT cpapwr_start_date_before_end');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT acc_period_work_ref_history_not_overlaps');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP CONSTRAINT chill_person_accompanying_period_work_referrer_pkey');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP endDate');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer DROP startDate');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ALTER accompanyingPeriodWork_id SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT fk_3619f5ebb99f6060 FOREIGN KEY (accompanyingperiodwork_id) REFERENCES chill_person_accompanying_period_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD CONSTRAINT fk_3619f5eba76ed395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work_referrer ADD PRIMARY KEY (accompanyingperiodwork_id, user_id)');
        $this->addSql('ALTER INDEX idx_3619f5ebc55c1209 RENAME TO idx_3619f5ebb99f6060');
    }
}
