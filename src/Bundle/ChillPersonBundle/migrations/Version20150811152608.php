<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Migration for adapting the Person Bundle to the 'cahier de charge' :
 * - RENAMING :
 * - - date_of_birth TO birthdate
 * - - genre to gender.
 */
class Version20150811152608 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE person RENAME COLUMN birthdate TO date_of_birth');
        $this->addSql('ALTER TABLE person RENAME COLUMN gender TO genre');
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE person RENAME COLUMN date_of_birth TO birthdate');
        $this->addSql('ALTER TABLE person RENAME COLUMN genre TO gender');
    }
}
