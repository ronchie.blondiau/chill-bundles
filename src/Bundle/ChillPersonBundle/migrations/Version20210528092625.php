<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * prefix table concerning household with 'chill_person' and add constraints.
 */
final class Version20210528092625 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('the down method is not implemented');
    }

    public function getDescription(): string
    {
        return 'prefix table concerning household with \'chill_person\' and add constraints';
    }

    public function up(Schema $schema): void
    {
        // we need to rename constraint, drop them first, recreate them after
        $this->addSql('ALTER TABLE householdmembers DROP CONSTRAINT fk_4d1fb288e79ff843');
        $this->addSql('ALTER TABLE householdmembers DROP CONSTRAINT fk_4d1fb288217bbb47');

        // rename tables
        $this->addSql('ALTER TABLE householdmembers RENAME TO chill_person_household_members');
        $this->addSql('ALTER TABLE household RENAME TO chill_person_household');

        // rename sequences
        $this->addSql('ALTER SEQUENCE household_id_seq RENAME TO chill_person_household_id_seq');
        $this->addSql('ALTER SEQUENCE householdmembers_id_seq RENAME TO chill_person_household_members_id_seq');

        // recreate constraints
        $this->addSql('ALTER TABLE chill_person_household_members ADD CONSTRAINT FK_EEF5DED7217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_person_household_members ADD CONSTRAINT FK_EEF5DED7E79FF843 FOREIGN KEY (household_id) REFERENCES chill_person_household (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        // create constraint 'householdmembers not overlaps'
        $this->addSql('ALTER TABLE chill_person_household_members ADD CHECK (startdate < enddate)');
        $this->addSql('ALTER TABLE chill_person_household_members ADD CONSTRAINT '.
            'household_members_not_overlaps EXCLUDE USING GIST(
              -- extension btree_gist required to include comparaison with integer
              person_id WITH =,
              daterange(startdate, enddate) WITH &&
           ) WHERE (sharedhousehold IS TRUE)
           INITIALLY DEFERRED');

        // rename constraints
        $this->addSql('ALTER TABLE public.chill_person_household_to_addresses DROP CONSTRAINT fk_7109483e79ff843');
        $this->addSql('ALTER TABLE chill_person_household_to_addresses ADD CONSTRAINT FK_C28AF063E79FF843 FOREIGN KEY (household_id) REFERENCES chill_person_household (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        // rename indexes
        $this->addSql('ALTER INDEX idx_7109483e79ff843 RENAME TO IDX_C28AF063E79FF843');
        $this->addSql('ALTER INDEX idx_7109483f5b7af75 RENAME TO IDX_C28AF063F5B7AF75');
        $this->addSql('ALTER INDEX idx_4d1fb288e79ff843 RENAME TO IDX_EEF5DED7E79FF843');
        $this->addSql('ALTER INDEX idx_4d1fb288217bbb47 RENAME TO IDX_EEF5DED7217BBB47');
    }
}
