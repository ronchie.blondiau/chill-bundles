<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a contactInfo and a mobilenumber columns on person. Change the email column.
 */
final class Version20180518144221 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_person_person DROP contactInfo');
        $this->addSql('ALTER TABLE chill_person_person DROP mobilenumber');
        $this->addSql('ALTER TABLE chill_person_person ALTER email SET NOT NULL');
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_person_person ADD contactInfo TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD mobilenumber TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER email DROP NOT NULL');
    }
}
