<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * set label for origin as json.
 */
final class Version20210419105940 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this will keep the '"' at first and last character, but is acceptable
        $this->addSql('ALTER TABLE chill_person_accompanying_period_origin '
            .'ALTER label TYPE VARCHAR(255) USING label->\'fr\'::text');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_origin '
            .'ALTER label DROP DEFAULT');
    }

    public function getDescription(): string
    {
        return 'set label for origin as json';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_origin '
            .'ALTER label TYPE JSON USING json_build_object(\'fr\', label)::jsonb');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_origin '
            .'ALTER label DROP DEFAULT');
    }
}
