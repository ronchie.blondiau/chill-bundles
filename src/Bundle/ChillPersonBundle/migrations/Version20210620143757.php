<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * add updated information to accompanying period work.
 */
final class Version20210620143757 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work DROP updatedAt');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work DROP updatedBy_id');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER createdAt TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER createdAt DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER startDate TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER startDate DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER endDate TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER endDate DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.createdat IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.startdate IS NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.enddate IS NULL');
    }

    public function getDescription(): string
    {
        return 'add updated information to accompanying period work';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ADD updatedBy_id INT NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER createdat TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER createdat DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER startdate TYPE DATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER startdate DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER enddate TYPE DATE');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER enddate DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_work ALTER enddate SET DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.updatedAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.createdAt IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.startDate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_person_accompanying_period_work.endDate IS \'(DC2Type:datetime_immutable)\'');
    }
}
