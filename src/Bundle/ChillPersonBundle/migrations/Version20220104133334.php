<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220104133334 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource ADD comment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource DROP comment');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource ADD CONSTRAINT fk_dc78989ff8697d13 FOREIGN KEY (comment_id) REFERENCES chill_person_accompanying_period_comment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_dc78989ff8697d13 ON chill_person_accompanying_period_resource (comment_id)');
    }

    public function getDescription(): string
    {
        return 'Replace accompanyingCourse Resources comment by a string';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource DROP CONSTRAINT fk_dc78989ff8697d13');
        $this->addSql('DROP INDEX idx_dc78989ff8697d13');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource ADD comment TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_accompanying_period_resource DROP comment_id');
    }
}
