<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add Civility to Person.
 */
final class Version20211108100849 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person DROP CONSTRAINT FK_BF210A1423D6A298');
        $this->addSql('DROP INDEX IDX_BF210A1423D6A298');
        $this->addSql('ALTER TABLE chill_person_person DROP civility_id');
    }

    public function getDescription(): string
    {
        return 'Add Civility to Person';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_person ADD civility_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD CONSTRAINT FK_BF210A1423D6A298 FOREIGN KEY (civility_id) REFERENCES chill_main_civility (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BF210A1423D6A298 ON chill_person_person (civility_id)');
    }
}
