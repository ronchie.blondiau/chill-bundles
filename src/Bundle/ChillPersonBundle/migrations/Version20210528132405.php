<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Household members: allow startdate and enddate to be null.
 */
final class Version20210528132405 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_members ALTER startDate SET NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household_members ALTER endDate SET NOT NULL');
    }

    public function getDescription(): string
    {
        return 'Household members: allow startdate and enddate to be null';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_person_household_members ALTER startdate DROP NOT NULL');
        $this->addSql('ALTER TABLE chill_person_household_members ALTER enddate DROP NOT NULL');
    }
}
