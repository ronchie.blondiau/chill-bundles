<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Person;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211119215630 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("CREATE OR REPLACE VIEW view_chill_person_current_address AS
            SELECT
                cphm.person_id  AS person_id,
                cma.id AS address_id,
                CASE WHEN cphm.startdate > COALESCE(cma.validfrom, '-infinity'::date) THEN cphm.startdate ELSE cma.validfrom END AS valid_from,
                CASE WHEN COALESCE(cphm.enddate, 'infinity'::date) < COALESCE(cma.validto, 'infinity'::date) THEN cphm.enddate ELSE cma.validto END AS valid_to
            FROM chill_person_household_members AS cphm
                     LEFT JOIN chill_person_household_to_addresses AS cphta ON cphta.household_id = cphm.household_id
                     LEFT JOIN chill_main_address AS cma ON cphta.address_id = cma.id
            WHERE
                  cphm.sharedhousehold IS TRUE
            AND
                  current_date between cphm.startdate AND coalesce(enddate, 'infinity'::date)
            AND
                  current_date between cma.validfrom AND coalesce(validto, 'infinity'::date)
      ");
    }

    public function getDescription(): string
    {
        return 'update computation of view_chill_person_current_address: do not take enddate into account';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE OR REPLACE VIEW view_chill_person_current_address AS
            SELECT
                cphm.person_id  AS person_id,
                cma.id AS address_id,
                CASE WHEN cphm.startdate > COALESCE(cma.validfrom, '-infinity'::date) THEN cphm.startdate ELSE cma.validfrom END AS valid_from,
                CASE WHEN COALESCE(cphm.enddate, 'infinity'::date) < COALESCE(cma.validto, 'infinity'::date) THEN cphm.enddate ELSE cma.validto END AS valid_to
            FROM chill_person_household_members AS cphm
                     JOIN chill_person_household_to_addresses AS cphta ON cphta.household_id = cphm.household_id
                     JOIN chill_main_address AS cma ON cphta.address_id = cma.id
            WHERE
                  cphm.sharedhousehold IS TRUE
            AND
                  daterange(cphm.startdate, cphm.enddate, '[)') @> current_date
            AND
                  daterange(cma.validfrom, cma.validto, '[)') @> current_date
      ");
    }
}
