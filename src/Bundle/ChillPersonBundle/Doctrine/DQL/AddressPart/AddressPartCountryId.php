<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Doctrine\DQL\AddressPart;

use Chill\PersonBundle\Doctrine\DQL\AddressPart;

class AddressPartCountryId extends AddressPart
{
    public function getPart()
    {
        return 'country_id';
    }
}
