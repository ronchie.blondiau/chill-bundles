<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingPeriodStepHistoryFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use src\Bundle\ChillPersonBundle\Tests\Export\Filter\AccompanyingPeriodStepHistoryFilters\ByDateFilterTest;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ByDateFilter
 * Implements the FilterInterface.
 *
 * This class represents a filter that filters data based on a date range on the start date.
 * It allows the user to select a start date and an end date to filter the data.
 *
 * @see ByDateFilterTest
 */
final readonly class ByDateFilter implements FilterInterface
{
    public function __construct(
        private RollingDateConverterInterface $rollingDateConverter
    ) {
    }

    public function getTitle()
    {
        return 'export.filter.step_history.by_date.title';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('start_date', PickRollingDateType::class, [
                'label' => 'export.filter.step_history.by_date.start_date_label',
            ])
            ->add('end_date', PickRollingDateType::class, [
                'label' => 'export.filter.step_history.by_date.end_date_label',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return [
            'start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START),
            'end_date' => new RollingDate(RollingDate::T_TODAY),
        ];
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'exports.filter.step_history.by_date.description',
            [
                'start_date' => $this->rollingDateConverter->convert($data['start_date']),
                'end_date' => $this->rollingDateConverter->convert($data['end_date']),
            ],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $startDate = 'acp_step_history_by_date_start_filter';
        $endDate = 'acp_step_history_by_date_end_filter';

        $qb
            ->andWhere(
                "acpstephistory.startDate >= :{$startDate} AND (acpstephistory.endDate < :{$endDate} OR acpstephistory.endDate IS NULL)"
            )
            ->setParameter($startDate, $this->rollingDateConverter->convert($data['start_date']))
            ->setParameter($endDate, $this->rollingDateConverter->convert($data['end_date']));
    }

    public function applyOn()
    {
        return Declarations::ACP_STEP_HISTORY;
    }
}
