<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class ClosingMotiveFilter implements FilterInterface
{
    public function __construct(private readonly TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->in('acp.closingMotive', ':closingmotive');

        if ($where instanceof Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('closingmotive', $data['accepted_closingmotives']);
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_closingmotives', EntityType::class, [
            'class' => ClosingMotive::class,
            'choice_label' => fn (ClosingMotive $cm) => $this->translatableStringHelper->localize($cm->getName()),
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $motives = [];

        foreach ($data['accepted_closingmotives'] as $k => $v) {
            $motives[] = $this->translatableStringHelper->localize($v->getName());
        }

        return [
            'Filtered by closingmotive: only %closingmotives%', [
                '%closingmotives%' => implode(', ', $motives),
            ], ];
    }

    public function getTitle(): string
    {
        return 'Filter by closing motive';
    }
}
