<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\AccompanyingCourseFilters;

use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User\UserScopeHistory;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Repository\ScopeRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UserScopeFilter implements FilterInterface
{
    private const PREFIX = 'acp_filter_main_scope';

    public function __construct(
        private readonly ScopeRepositoryInterface $scopeRepository,
        private readonly TranslatableStringHelper $translatableStringHelper,
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->join(
                'acp.userHistories',
                "{$p}_userHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_userHistory.accompanyingPeriod", 'acp.id'),
                    $qb->expr()->andX(
                        $qb->expr()->gte('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_userHistory.endDate"),
                            $qb->expr()->lt('COALESCE(acp.closingDate, CURRENT_TIMESTAMP())', "{$p}_userHistory.endDate")
                        )
                    )
                )
            )
            ->join(
                UserScopeHistory::class,
                "{$p}_scopeHistory",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_scopeHistory.user", "{$p}_userHistory.user"),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_scopeHistory.startDate", "{$p}_userHistory.startDate"),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_scopeHistory.endDate"),
                            $qb->expr()->gt("{$p}_scopeHistory.endDate", "{$p}_userHistory.startDate")
                        )
                    )
                )
            )
            ->andWhere($qb->expr()->in("{$p}_scopeHistory.scope", ":{$p}_scopes"))
            ->setParameter(
                "{$p}_scopes",
                $data['scopes'],
            )
        ;
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('scopes', EntityType::class, [
                'class' => Scope::class,
                'choices' => $this->scopeRepository->findAllActive(),
                'choice_label' => fn (Scope $s) => $this->translatableStringHelper->localize($s->getName()),
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            'export.filter.course.by_user_scope.Filtered by user main scope: only %scope%', [
                '%scope%' => implode(
                    ', ',
                    array_map(
                        fn (Scope $s) => $this->translatableStringHelper->localize($s->getName()),
                        $data['scopes'] instanceof Collection ? $data['scopes']->toArray() : $data['scopes']
                    )
                ),
            ],
        ];
    }

    public function getFormDefaultData(): array
    {
        return [
            'scopes' => [],
        ];
    }

    public function getTitle(): string
    {
        return 'export.filter.course.by_user_scope.Filter by user scope';
    }
}
