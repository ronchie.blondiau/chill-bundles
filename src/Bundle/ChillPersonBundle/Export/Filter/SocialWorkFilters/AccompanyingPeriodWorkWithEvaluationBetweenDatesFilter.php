<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Filter\SocialWorkFilters;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Service\RollingDate\RollingDateConverterInterface;
use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodWorkEvaluation;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class AccompanyingPeriodWorkWithEvaluationBetweenDatesFilter implements FilterInterface
{
    public function __construct(
        private RollingDateConverterInterface $rollingDateConverter,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder): void
    {
        $builder
            ->add('start_date', PickRollingDateType::class, [
                'label' => 'export.filter.work.evaluation_between_dates.start_date',
            ])
            ->add('end_date', PickRollingDateType::class, [
                'label' => 'export.filter.work.evaluation_between_dates.end_date',
            ]);
    }

    public function getFormDefaultData(): array
    {
        return ['start_date' => new RollingDate(RollingDate::T_YEAR_CURRENT_START), 'end_date' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function getTitle(): string
    {
        return 'export.filter.work.evaluation_between_dates.title';
    }

    public function describeAction($data, $format = 'string'): array
    {
        return [
            'export.filter.work.evaluation_between_dates.description',
            [
                '%startDate%' => null !== $data['start_date'] ? $this->rollingDateConverter->convert($data['start_date'])->format('d-m-Y') : '',
                '%endDate%' => null !== $data['end_date'] ? $this->rollingDateConverter->convert($data['end_date'])->format('d-m-Y') : '',
            ],
        ];
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data): void
    {
        $s = 'workeval_between_filter_start';
        $e = 'workeval_between_filter_end';

        $qb->andWhere(
            $qb->expr()->exists(
                'SELECT 1 FROM '.AccompanyingPeriodWorkEvaluation::class." workeval_between_filter_workeval WHERE workeval_between_filter_workeval.createdAt BETWEEN :{$s} AND :{$e} AND IDENTITY(workeval_between_filter_workeval.accompanyingPeriodWork) = acpw.id"
            )
        )
            ->setParameter($s, $this->rollingDateConverter->convert($data['start_date']))
            ->setParameter($e, $this->rollingDateConverter->convert($data['end_date']));
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }
}
