<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfidentialAggregator implements AggregatorInterface
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect('acp.confidential AS confidential_aggregator');
        $qb->addGroupBy('confidential_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Confidentiality';
            }

            return match ($value) {
                true => $this->translator->trans('is confidential'),
                false => $this->translator->trans('is not confidential'),
                default => throw new \LogicException(sprintf('The value %s is not valid', $value)),
            };
        };
    }

    public function getQueryKeys($data): array
    {
        return ['confidential_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by confidential';
    }
}
