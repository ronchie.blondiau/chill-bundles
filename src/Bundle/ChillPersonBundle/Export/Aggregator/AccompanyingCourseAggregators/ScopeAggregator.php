<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\ScopeRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ScopeAggregator implements AggregatorInterface
{
    public function __construct(private ScopeRepository $scopeRepository, private TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acpscope', $qb->getAllAliases(), true)) {
            $qb->leftJoin('acp.scopes', 'acpscope');
        }

        $qb->addSelect('acpscope.id as scope_aggregator');
        $qb->addGroupBy('scope_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Scope';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            if (null === $s = $this->scopeRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize(
                $s->getName()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['scope_aggregator'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course.by_scope.Group course by scope';
    }
}
