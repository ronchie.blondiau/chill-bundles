<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\SocialIssueRepository;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class SocialIssueAggregator implements AggregatorInterface
{
    public function __construct(private SocialIssueRepository $issueRepository, private SocialIssueRender $issueRender)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acpsocialissue', $qb->getAllAliases(), true)) {
            // we will see accompanying period linked with social issues
            $qb->join('acp.socialIssues', 'acpsocialissue');
        }

        $qb->addSelect('acpsocialissue.id as socialissue_aggregator');
        $qb->addGroupBy('socialissue_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Social issues';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $i = $this->issueRepository->find($value);

            return $this->issueRender->renderString($i, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['socialissue_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by social issue';
    }
}
