<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class RequestorAggregator implements AggregatorInterface
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acppart', $qb->getAllAliases(), true)) {
            $qb->join('acp.participations', 'acppart');
        }

        $qb->addSelect("
            ( CASE
                WHEN acp.requestorPerson IS NOT NULL
                THEN
                    ( CASE
                        WHEN acp.requestorPerson = acppart.person
                        THEN 'is person concerned'
                        ELSE 'is other person'
                    END )
                ELSE
                    ( CASE
                        WHEN acp.requestorThirdParty IS NOT NULL
                        THEN 'is thirdparty'
                        ELSE 'no requestor'
                    END )
            END ) AS requestor_aggregator
        ");

        $qb->addGroupBy('requestor_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Requestor';
            }

            return $this->translator->trans($value);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['requestor_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by requestor';
    }
}
