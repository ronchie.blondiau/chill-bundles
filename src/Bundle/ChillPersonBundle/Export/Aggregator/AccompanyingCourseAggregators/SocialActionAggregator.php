<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingCourseAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class SocialActionAggregator implements AggregatorInterface
{
    public function __construct(private SocialActionRender $actionRender, private SocialActionRepository $actionRepository)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('acpw', $qb->getAllAliases(), true)) {
            // here, we will only see accompanying period linked with a socialAction
            $qb->join('acp.works', 'acpw');
        }

        $qb->addSelect('IDENTITY(acpw.socialAction) AS socialaction_aggregator');
        $qb->addGroupBy('socialaction_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value) {
            if ('_header' === $value) {
                return 'Social action';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            if (null === $sa = $this->actionRepository->find($value)) {
                return '';
            }

            return $this->actionRender->renderString($sa, []);
        };
    }

    public function getQueryKeys($data): array
    {
        return ['socialaction_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group by social action';
    }
}
