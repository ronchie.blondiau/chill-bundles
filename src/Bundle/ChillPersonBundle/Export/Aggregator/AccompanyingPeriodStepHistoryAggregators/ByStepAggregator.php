<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators;

use Chill\MainBundle\Export\AggregatorInterface;
use Chill\PersonBundle\Export\Declarations;
use Chill\PersonBundle\Tests\Export\Aggregator\AccompanyingPeriodStepHistoryAggregators\ByStepAggregatorTest;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @see ByStepAggregatorTest
 */
final readonly class ByStepAggregator implements AggregatorInterface
{
    private const KEY = 'acpstephistory_step_agg';

    public function __construct(
        private TranslatorInterface $translator
    ) {
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing in this form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function (?string $step): string {
            if ('_header' === $step) {
                return 'export.aggregator.step_history.by_step.header';
            }

            if (null === $step || '' === $step) {
                return '';
            }

            return $this->translator->trans('accompanying_period.'.$step);
        };
    }

    public function getQueryKeys($data)
    {
        return [
            self::KEY,
        ];
    }

    public function getTitle()
    {
        return 'export.aggregator.step_history.by_step.title';
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->addSelect('acpstephistory.step AS '.self::KEY)
            ->addGroupBy(self::KEY);
    }

    public function applyOn()
    {
        return Declarations::ACP_STEP_HISTORY;
    }
}
