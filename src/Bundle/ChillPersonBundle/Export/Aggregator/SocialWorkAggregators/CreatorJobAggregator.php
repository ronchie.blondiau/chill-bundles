<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Aggregator\SocialWorkAggregators;

use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserJobRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class CreatorJobAggregator implements AggregatorInterface
{
    private const PREFIX = 'acpw_aggr_creator_job';

    public function __construct(
        private readonly UserJobRepository $jobRepository,
        private readonly TranslatableStringHelper $translatableStringHelper
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_history",
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq("{$p}_history.user", 'acpw.createdBy'),
                    $qb->expr()->andX(
                        $qb->expr()->lte("{$p}_history.startDate", 'acpw.createdAt'),
                        $qb->expr()->orX(
                            $qb->expr()->isNull("{$p}_history.endDate"),
                            $qb->expr()->gt("{$p}_history.endDate", 'acpw.createdAt')
                        )
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_history.job) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::SOCIAL_WORK_ACTION_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, mixed $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.course_work.by_creator_job.Creator\'s job';
            }

            if (null === $value || '' === $value || null === $j = $this->jobRepository->find($value)) {
                return '';
            }

            return $this->translatableStringHelper->localize(
                $j->getLabel()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.course_work.by_creator_job.title';
    }
}
