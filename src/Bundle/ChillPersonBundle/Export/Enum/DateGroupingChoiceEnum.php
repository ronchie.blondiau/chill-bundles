<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Export\Enum;

enum DateGroupingChoiceEnum: string
{
    case MONTH = 'YYYY-MM';
    case WEEK = 'YYYY-IW';
    case YEAR = 'YYYY';
}
