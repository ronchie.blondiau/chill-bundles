<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Privacy;

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2015, Champs Libres Cooperative SCRLFS,
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Chill\PersonBundle\Entity\Person;

/**
 * Class PrivacyEvent.
 *
 * Array $args expects arguments with the following keys: 'element_class', 'element_id', 'action'
 * By default, action is set to 'show'
 */
class PrivacyEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    final public const PERSON_PRIVACY_EVENT = 'chill_person.privacy_event';

    private array $persons = [];

    /**
     * PrivacyEvent constructor.
     */
    public function __construct(private readonly Person $person, private readonly array $args = ['action' => 'show'])
    {
    }

    public function addPerson(Person $person)
    {
        $this->persons[] = $person;

        return $this;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return array $persons
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @return bool
     */
    public function hasPersons()
    {
        return \count($this->persons) >= 1;
    }
}
