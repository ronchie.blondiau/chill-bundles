<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\Household;

use Chill\MainBundle\Entity\AddressReference;
use Chill\PersonBundle\Entity\Household\Household;

interface HouseholdACLAwareRepositoryInterface
{
    public function countByAddressReference(AddressReference $addressReference): int;

    /**
     * @return array|Household[]
     */
    public function findByAddressReference(
        AddressReference $addressReference,
        ?int $firstResult = 0,
        ?int $maxResult = 50
    ): array;
}
