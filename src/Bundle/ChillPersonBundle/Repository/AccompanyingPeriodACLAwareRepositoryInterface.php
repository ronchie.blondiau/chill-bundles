<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\MainBundle\Entity\Location;
use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\UserJob;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;

interface AccompanyingPeriodACLAwareRepositoryInterface
{
    /**
     * @param array|UserJob[]  $jobs
     * @param array|Scope[]    $services
     * @param array|Location[] $administrativeLocations
     */
    public function countByUnDispatched(array $jobs, array $services, array $administrativeLocations): int;

    /**
     * @param array|PostalCode[] $postalCodes
     */
    public function countByUserAndPostalCodesOpenedAccompanyingPeriod(?User $user, array $postalCodes): int;

    /**
     * @return array<AccompanyingPeriod>
     */
    public function findByPerson(
        Person $person,
        string $role,
        ?array $orderBy = [],
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @param array|UserJob[]  $jobs                    if empty, does not take this argument into account
     * @param array|Scope[]    $services                if empty, does not take this argument into account
     * @param array|Location[] $administrativeLocations
     *
     * @return list<AccompanyingPeriod>
     */
    public function findByUnDispatched(array $jobs, array $services, array $administrativeLocations, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array|PostalCode[] $postalCodes
     *
     * @return list<AccompanyingPeriod>
     */
    public function findByUserAndPostalCodesOpenedAccompanyingPeriod(?User $user, array $postalCodes, array $orderBy = [], int $limit = 0, int $offset = 50): array;
}
