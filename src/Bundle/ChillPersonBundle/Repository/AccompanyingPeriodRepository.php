<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository;

use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

final class AccompanyingPeriodRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(AccompanyingPeriod::class);
    }

    public function countBy(array $criteria): int
    {
        return $this->repository->count($criteria);
    }

    public function countByRecentUserHistory(User $user, \DateTimeImmutable $since): int
    {
        $qb = $this->buildQueryByRecentUserHistory($user, $since);

        return $qb->select('count(a)')->getQuery()->getSingleScalarResult();
    }

    public function createQueryBuilder(string $alias, ?string $indexBy = null): QueryBuilder
    {
        return $this->repository->createQueryBuilder($alias, $indexBy);
    }

    public function find($id): ?AccompanyingPeriod
    {
        return $this->repository->find($id);
    }

    /**
     * @return AccompanyingPeriod[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @return array|AccompanyingPeriod[]
     */
    public function findByRecentUserHistory(User $user, \DateTimeImmutable $since, ?int $limit = 20, ?int $offset = 0): array
    {
        $qb = $this->buildQueryByRecentUserHistory($user, $since);

        return $qb->select('a')
            ->addOrderBy('userHistory.startDate', 'DESC')
            ->getQuery()
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();
    }

    public function findConfirmedByUser(User $user)
    {
        $qb = $this->createQueryBuilder('ap');
        $qb->where($qb->expr()->eq('ap.user', ':user'))
            ->andWhere('ap.step', 'CONFIRMED')
            ->setParameter('user', $user);

        return $qb;
    }

    public function findOneBy(array $criteria): ?AccompanyingPeriod
    {
        return $this->findOneBy($criteria);
    }

    public function getClassName()
    {
        return AccompanyingPeriod::class;
    }

    private function buildQueryByRecentUserHistory(User $user, \DateTimeImmutable $since): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('a');

        $qb
            ->join('a.userHistories', 'userHistory')
            ->where($qb->expr()->eq('a.user', ':user'))
            ->andWhere($qb->expr()->neq('a.step', "'".AccompanyingPeriod::STEP_DRAFT."'"))
            ->andWhere($qb->expr()->gte('userHistory.startDate', ':since'))
            ->andWhere($qb->expr()->isNull('userHistory.endDate'))
            ->setParameter('user', $user)
            ->setParameter('since', $since);

        return $qb;
    }
}
