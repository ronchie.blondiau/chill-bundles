<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\AccompanyingPeriod;

use Chill\PersonBundle\Entity\AccompanyingPeriod\AccompanyingPeriodInfo;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Clock\ClockInterface;

readonly class AccompanyingPeriodInfoRepository implements AccompanyingPeriodInfoRepositoryInterface
{
    private EntityRepository $entityRepository;

    public function __construct(
        private ClockInterface $clock,
        private EntityManagerInterface $em,
    ) {
        $this->entityRepository = $em->getRepository($this->getClassName());
    }

    public function findAccompanyingPeriodIdInactiveAfter(\DateInterval $interval, array $statuses = []): array
    {
        $query = $this->em->createQuery();
        $baseDql = 'SELECT DISTINCT IDENTITY(ai.accompanyingPeriod) FROM '.AccompanyingPeriodInfo::class.' ai JOIN ai.accompanyingPeriod a WHERE NOT EXISTS
            (SELECT 1 FROM '.AccompanyingPeriodInfo::class.' aiz WHERE aiz.infoDate > :after AND IDENTITY(aiz.accompanyingPeriod) = IDENTITY(ai.accompanyingPeriod))';

        if ([] !== $statuses) {
            $dql = $baseDql.' AND a.step IN (:statuses)';
            $query->setParameter('statuses', $statuses);
        } else {
            $dql = $baseDql;
        }

        return $query->setDQL($dql)
            ->setParameter('after', $this->clock->now()->sub($interval))
            ->getSingleColumnResult();
    }

    public function findAccompanyingPeriodIdActiveSince(\DateInterval $interval, array $statuses = []): array
    {
        $query = $this->em->createQuery();
        $baseDql = 'SELECT DISTINCT IDENTITY(ai.accompanyingPeriod) FROM '.AccompanyingPeriodInfo::class.' ai
            JOIN ai.accompanyingPeriod a WHERE ai.infoDate > :after';

        if ([] !== $statuses) {
            $dql = $baseDql.' AND a.step IN (:statuses)';
            $query->setParameter('statuses', $statuses);
        } else {
            $dql = $baseDql;
        }

        return $query->setDQL($dql)
            ->setParameter('after', $this->clock->now()->sub($interval))
            ->getSingleColumnResult();
    }

    public function find($id): never
    {
        throw new \LogicException('Calling an accompanying period info by his id does not make sense');
    }

    public function findAll(): array
    {
        return $this->entityRepository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->entityRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?AccompanyingPeriodInfo
    {
        return $this->entityRepository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return AccompanyingPeriodInfo::class;
    }
}
