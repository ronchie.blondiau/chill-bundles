<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Repository\SocialWork;

use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

final class SocialActionRepository implements ObjectRepository
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(SocialAction::class);
    }

    public function createQueryBuilder(string $alias, ?string $indexBy = null): QueryBuilder
    {
        return $this->repository->createQueryBuilder($alias, $indexBy);
    }

    public function find($id): ?SocialAction
    {
        return $this->repository->find($id);
    }

    /**
     * @return array<int, SocialAction>
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array|SocialAction[]
     */
    public function findAllActive(): array
    {
        return $this->buildQueryWithDesactivatedDateCriteria()->getQuery()->getResult();
    }

    /**
     * @param mixed|null $limit
     * @param mixed|null $offset
     *
     * @return array<int, SocialAction>
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?SocialAction
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return SocialAction::class;
    }

    private function buildQueryWithDesactivatedDateCriteria(): QueryBuilder
    {
        $qb = $this->repository->createQueryBuilder('sa');

        $qb->where('sa.desactivationDate is null')
            ->orWhere('sa.desactivationDate > :now')
            ->orderBy('sa.ordering', 'ASC')
            ->setParameter('now', new \DateTime('now'));

        return $qb;
    }
}
