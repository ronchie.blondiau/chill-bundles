<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Person;

use Chill\MainBundle\Entity\Center;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * Associate a Person with the current center.
 *
 * The process of selecting the current center is done on database side,
 * using a SQL view.
 *
 * @ORM\Entity(readOnly=true)
 *
 * @ORM\Table(name="view_chill_person_person_center_history_current")
 *
 * @psalm-internal Chill\PersonBundle\Entity
 */
class PersonCenterCurrent
{
    /**
     * @ORM\ManyToOne(targetEntity=Center::class)
     */
    private Center $center;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity=Person::class, inversedBy="centerCurrent")
     */
    private Person $person;

    /**
     * @ORM\Column(type="date_immutable", nullable=false)
     */
    private \DateTimeImmutable $startDate;

    /**
     * Populate the properties person, center, start and end date from history.
     *
     * The creator and updatedby are not filled.
     *
     * @internal Should not be instantied, unless inside Person entity
     */
    public function __construct(PersonCenterHistory $history)
    {
        $this->person = $history->getPerson();
        $this->center = $history->getCenter();
        $this->startDate = $history->getStartDate();
        $this->endDate = $history->getEndDate();
        $this->id = $history->getId();
    }

    public function getCenter(): Center
    {
        return $this->center;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    /**
     * The id will be the same as the current @see{PersonCenterHistory::class}.
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }
}
