<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Person;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_resource_kind")
 */
class PersonResourceKind
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive = true;

    /**
     * @ORM\Column(type="json", length=255)
     *
     * @Serializer\Groups({"docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function getTitle(): ?array
    {
        return $this->title;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function setTitle(array $title): self
    {
        $this->title = $title;

        return $this;
    }
}
