<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Person;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\MainBundle\Entity\Center;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * Associate a Person with a Center. The association may change on date intervals.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_person_center_history")
 */
class PersonCenterHistory implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __construct(
        /**
         * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="centerHistory")
         */
        private ?Person $person = null,
        /**
         * @ORM\ManyToOne(targetEntity=Center::class)
         */
        private ?Center $center = null,
        /**
         * @ORM\Column(type="date_immutable", nullable=false)
         */
        private ?\DateTimeImmutable $startDate = null
    ) {
    }

    public function getCenter(): ?Center
    {
        return $this->center;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setCenter(?Center $center): self
    {
        $this->center = $center;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }
}
