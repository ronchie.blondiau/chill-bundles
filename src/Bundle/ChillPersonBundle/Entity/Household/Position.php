<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\Household;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_household_position")
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={
 *     "household_position": Position::class
 * })
 */
class Position
{
    /**
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({ "read" })
     */
    private bool $allowHolder = false;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $label = [];

    /**
     * @ORM\Column(type="float")
     *
     * @Serializer\Groups({"read"})
     */
    private float $ordering = 0.00;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({"read"})
     */
    private bool $shareHouseHold = true;

    public function getAllowHolder(): bool
    {
        return $this->allowHolder;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    public function getShareHousehold(): bool
    {
        return $this->shareHouseHold;
    }

    public function isAllowHolder(): bool
    {
        return $this->getAllowHolder();
    }

    public function setAllowHolder(bool $allowHolder): self
    {
        $this->allowHolder = $allowHolder;

        return $this;
    }

    public function setLabel(array $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function setOrdering(float $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function setShareHousehold(bool $shareHouseHold): self
    {
        $this->shareHouseHold = $shareHouseHold;

        return $this;
    }
}
