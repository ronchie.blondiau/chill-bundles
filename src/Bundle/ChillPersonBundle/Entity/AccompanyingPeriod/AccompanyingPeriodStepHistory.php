<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table("chill_person_accompanying_period_step_history")
 */
class AccompanyingPeriodStepHistory implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=AccompanyingPeriod::class)
     */
    private AccompanyingPeriod $period;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $step;

    /**
     * @ORM\ManyToOne(targetEntity=ClosingMotive::class)
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?ClosingMotive $closingMotive = null;

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriod(): AccompanyingPeriod
    {
        return $this->period;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getStep(): string
    {
        return $this->step;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @internal use AccompanyingPeriod::addLocationHistory
     */
    public function setPeriod(AccompanyingPeriod $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function setStep(string $step): AccompanyingPeriodStepHistory
    {
        $this->step = $step;

        return $this;
    }

    public function getClosingMotive(): ?ClosingMotive
    {
        return $this->closingMotive;
    }

    public function setClosingMotive(?ClosingMotive $closingMotive): self
    {
        $this->closingMotive = $closingMotive;

        return $this;
    }
}
