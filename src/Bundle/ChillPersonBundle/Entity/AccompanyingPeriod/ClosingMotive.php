<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * ClosingMotive give an explanation why we closed the Accompanying period.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_accompanying_period_closingmotive")
 */
class ClosingMotive
{
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * Child Accompanying periods.
     *
     * @var Collection<ClosingMotive>
     *
     * @ORM\OneToMany(
     *     targetEntity="Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive",
     * mappedBy="parent")
     */
    private Collection $children;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $name = [];

    /**
     * @ORM\Column(type="float")
     */
    private float $ordering = 0.0;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive",
     * inversedBy="children")
     */
    private ?ClosingMotive $parent = null;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private bool $isCanceledAccompanyingPeriod = false;

    /**
     * ClosingMotive constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function addChildren(ClosingMotive $child): ClosingMotive
    {
        if ($this->children->contains($child)) {
            return $this;
        }

        $this->children->add($child);
        $child->setParent($this)->setIsCanceledAccompanyingPeriod($this->getIsCanceledAccompanyingPeriod());

        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    public function getOrdering(): float
    {
        return $this->ordering;
    }

    /**
     * @return ClosingMotive
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getIsCanceledAccompanyingPeriod(): bool
    {
        return $this->isCanceledAccompanyingPeriod;
    }

    public function hasParent(): bool
    {
        return null !== $this->parent;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function isChild(): bool
    {
        return null !== $this->parent;
    }

    public function isLeaf(): bool
    {
        return 0 === $this->children->count();
    }

    public function isParent(): bool
    {
        return $this->children->count() > 0;
    }

    public function isCanceledAccompanyingPeriod(): bool
    {
        return $this->isCanceledAccompanyingPeriod;
    }

    public function removeChildren(ClosingMotive $child): ClosingMotive
    {
        if ($this->children->removeElement($child)) {
            $child->setParent(null);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        if (false === $this->active) {
            foreach ($this->getChildren() as $child) {
                $child->setActive(false);
            }
        }

        return $this;
    }

    public function setChildren(Collection $children): ClosingMotive
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Set name.
     *
     * @param array $name
     *
     * @return ClosingMotive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return $this
     */
    public function setOrdering(float $ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function setParent(?ClosingMotive $parent): ClosingMotive
    {
        $this->parent = $parent;

        if (null !== $parent) {
            // $parent->addChildren($this);
        }

        return $this;
    }

    public function setIsCanceledAccompanyingPeriod(bool $isCanceledAP): ClosingMotive
    {
        $this->isCanceledAccompanyingPeriod = $isCanceledAP;

        foreach ($this->getChildren() as $child) {
            $child->setIsCanceledAccompanyingPeriod($isCanceledAP);
        }

        return $this;
    }
}
