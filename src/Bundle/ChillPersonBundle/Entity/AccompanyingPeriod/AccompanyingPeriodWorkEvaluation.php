<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table("chill_person_accompanying_period_work_evaluation")
 *
 * @Serializer\DiscriminatorMap(typeProperty="type", mapping={
 *     "accompanying_period_work_evaluation": AccompanyingPeriodWorkEvaluation::class,
 * })
 */
class AccompanyingPeriodWorkEvaluation implements TrackCreationInterface, TrackUpdateInterface
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity=AccompanyingPeriodWork::class,
     *     inversedBy="accompanyingPeriodWorkEvaluations"
     * )
     *
     * @Serializer\Groups({"read:evaluation:include-work"})
     *
     * @Serializer\Context(normalizationContext={"groups": {"read:accompanyingPeriodWork:light"}}, groups={"read:evaluation:include-work"})
     */
    private ?AccompanyingPeriodWork $accompanyingPeriodWork = null;

    /**
     * @ORM\Column(type="text", nullable=false, options={"default": ""})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private string $comment = '';

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=User::class
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?User $createdBy = null;

    /**
     * **Note on deserialization/denormalization**: denormalization of documents is handled by.
     *
     * @see{Chill\PersonBundle\Serializer\Normalizer\AccompanyingPeriodWorkEvaluationDenormalizer}
     *
     * @ORM\OneToMany(
     *     targetEntity=AccompanyingPeriodWorkEvaluationDocument::class,
     *     mappedBy="accompanyingPeriodWorkEvaluation",
     *     cascade={"remove", "persist"},
     *     orphanRemoval=true
     * )
     *
     * @ORM\OrderBy({"createdAt": "DESC", "id": "DESC"})
     *
     * @Serializer\Groups({"read"})
     *
     * @var Collection<AccompanyingPeriodWorkEvaluationDocument>
     */
    private Collection $documents;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Evaluation::class
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?Evaluation $evaluation = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * This is a workaround for client, to allow them to assign arbitrary data
     * dedicated to their job.
     *
     * This data is not persisted into database, but will appears on the data
     * normalized during the same request (like PUT/PATCH request)
     *
     * @Serializer\Groups({"read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private $key;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?\DateTimeImmutable $maxDate = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?\DateTimeImmutable $startDate = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=User::class
     * )
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?User $updatedBy = null;

    /**
     * @ORM\Column(type="dateinterval", nullable=true, options={"default": null})
     *
     * @Serializer\Groups({"read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?\DateInterval $warningInterval = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Groups({"read", "docgen:read"})
     * @Serializer\Groups({"write"})
     * @Serializer\Groups({"accompanying_period_work_evaluation:create"})
     */
    private ?int $timeSpent = null;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function addDocument(AccompanyingPeriodWorkEvaluationDocument $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setAccompanyingPeriodWorkEvaluation($this);
        }

        return $this;
    }

    public function getAccompanyingPeriodWork(): ?AccompanyingPeriodWork
    {
        return $this->accompanyingPeriodWork;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @return Collection<AccompanyingPeriodWorkEvaluationDocument>
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Arbitrary data, used for client.
     */
    public function getKey()
    {
        return $this->key;
    }

    public function getMaxDate(): ?\DateTimeImmutable
    {
        return $this->maxDate;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @Serializer\Groups({"docgen:read"})
     */
    public function getWarningDate(): ?\DateTimeImmutable
    {
        if (null === $this->getEndDate() || null === $this->getWarningInterval()) {
            return null;
        }

        return $this->getEndDate()->sub($this->getWarningInterval());
    }

    public function getWarningInterval(): ?\DateInterval
    {
        return $this->warningInterval;
    }

    public function getTimeSpent(): ?int
    {
        return $this->timeSpent;
    }

    public function removeDocument(AccompanyingPeriodWorkEvaluationDocument $document): self
    {
        $this->documents->removeElement($document);
        $document->setAccompanyingPeriodWorkEvaluation(null);

        return $this;
    }

    public function setAccompanyingPeriodWork(?AccompanyingPeriodWork $accompanyingPeriodWork): AccompanyingPeriodWorkEvaluation
    {
        if (
            $accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $this->accompanyingPeriodWork instanceof AccompanyingPeriodWork
            && $this->accompanyingPeriodWork->getId() !== $accompanyingPeriodWork->getId()
        ) {
            throw new \RuntimeException('Changing the accompanyingPeriodWork is not allowed');
        }

        $this->accompanyingPeriodWork = $accompanyingPeriodWork;

        return $this;
    }

    public function setComment(string $comment): AccompanyingPeriodWorkEvaluation
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @param \DateTimeImmutable|null $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setCreatedBy(?User $createdBy): AccompanyingPeriodWorkEvaluation
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): AccompanyingPeriodWorkEvaluation
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setTimeSpent(?int $timeSpent): self
    {
        $this->timeSpent = $timeSpent;

        return $this;
    }

    public function setEvaluation(?Evaluation $evaluation): AccompanyingPeriodWorkEvaluation
    {
        if (
            ($evaluation instanceof Evaluation
                && $this->evaluation instanceof Evaluation
                && $evaluation->getId() !== $this->evaluation->getId())
            || ($this->evaluation instanceof Evaluation
                && null === $evaluation)
        ) {
            $cl = AccompanyingPeriodWorkEvaluation::class;

            throw new \LogicException("once set, an {$cl}  cannot
                change or remove the linked Evaluation::class");
        }

        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Arbitrary data, used for client.
     */
    public function setKey(mixed $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function setMaxDate(?\DateTimeImmutable $maxDate): AccompanyingPeriodWorkEvaluation
    {
        $this->maxDate = $maxDate;

        return $this;
    }

    public function setStartDate(?\DateTimeImmutable $startDate): AccompanyingPeriodWorkEvaluation
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @param \DateTimeImmutable|null $updatedAt
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setUpdatedBy(?User $updatedBy): AccompanyingPeriodWorkEvaluation
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function setWarningInterval(?\DateInterval $warningInterval): AccompanyingPeriodWorkEvaluation
    {
        $this->warningInterval = $warningInterval;

        return $this;
    }
}
