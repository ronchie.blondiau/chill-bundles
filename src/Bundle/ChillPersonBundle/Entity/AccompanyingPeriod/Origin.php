<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_accompanying_period_origin")
 *
 * @Serializer\DiscriminatorMap(
 *     typeProperty="type",
 *     mapping={
 *         "origin": Origin::class
 *     })
 */
class Origin
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     *
     * @Serializer\Groups({"read", "docgen:read"})
     *
     * @Serializer\Context({"is-translatable": true}, groups={"docgen:read"})
     */
    private array $label = [];

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     *
     * @Serializer\Groups({"read"})
     */
    private ?\DateTimeImmutable $noActiveAfter = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getNoActiveAfter(): ?\DateTimeImmutable
    {
        return $this->noActiveAfter;
    }

    public function setLabel(array $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function setNoActiveAfter(?\DateTimeImmutable $noActiveAfter): self
    {
        $this->noActiveAfter = $noActiveAfter;

        return $this;
    }
}
