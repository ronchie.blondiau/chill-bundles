<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table("chill_person_accompanying_period_user_history")
 */
class UserHistory implements TrackCreationInterface
{
    use TrackCreationTrait;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, options={"default": null})
     */
    private ?\DateTimeImmutable $endDate = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    public function __construct(
        /**
         * @ORM\ManyToOne(targetEntity=AccompanyingPeriod::class, inversedBy="userHistories")
         *
         * @ORM\JoinColumn(nullable=false)
         */
        private ?AccompanyingPeriod $accompanyingPeriod,
        /**
         * @ORM\ManyToOne(targetEntity=User::class)
         *
         * @ORM\JoinColumn(nullable=false)
         */
        private User $user,

        /**
         * @ORM\Column(type="datetime_immutable", nullable=false, options={"default": "now()"})
         */
        private \DateTimeImmutable $startDate = new \DateTimeImmutable('now')
    ) {
    }

    public function getAccompanyingPeriod(): AccompanyingPeriod
    {
        return $this->accompanyingPeriod;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): UserHistory
    {
        $this->endDate = $endDate;

        return $this;
    }
}
