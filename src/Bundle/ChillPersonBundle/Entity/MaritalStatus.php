<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaritalStatus.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="chill_person_marital_status")
 *
 * @ORM\HasLifecycleCallbacks
 */
class MaritalStatus
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="string", length=7)
     */
    private ?string $id;

    /**
     * @ORM\Column(type="json")
     */
    private array $name;

    public function __construct()
    {
        $this->id = substr(md5(uniqid()), 0, 7);
    }

    /**
     * Get id.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string array
     */
    public function getName(): array
    {
        return $this->name;
    }

    /**
     * Set id.
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set name.
     *
     * @param string array $name
     */
    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
