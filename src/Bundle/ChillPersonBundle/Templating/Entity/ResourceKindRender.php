<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\ChillEntityRenderInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\Person\PersonResourceKind;

/**
 * @implements ChillEntityRenderInterface<PersonResourceKind>
 */
final readonly class ResourceKindRender implements ChillEntityRenderInterface
{
    public function __construct(private TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function renderBox($entity, array $options): string
    {
        return
            '<span class="resource">'.
            $this->translatableStringHelper->localize(
                $entity->getTitle()
            ).
            '</span>';
    }

    public function renderString($entity, array $options): string
    {
        $title = '';

        if (null !== $entity->getTitle()) {
            return $this->translatableStringHelper->localize($entity->getTitle());
        }

        return $title;
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof PersonResourceKind;
    }
}
