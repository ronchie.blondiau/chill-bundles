<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\PersonBundle\Form\Type\PickPersonDynamicType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonFindManuallyDuplicateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('person', PickPersonDynamicType::class, [
                'label' => 'Find duplicate',
                'mapped' => false,
            ])
            ->add('direction', HiddenType::class, [
                'data' => 'starting',
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_personbundle_person_find_manually_duplicate';
    }
}
