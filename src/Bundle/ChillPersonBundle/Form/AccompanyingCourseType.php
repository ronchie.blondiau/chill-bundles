<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\PersonBundle\Form\Type\ClosingMotivePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AccompanyingCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'closingDate',
            ChillDateType::class,
            [
                'required' => true,
            ]
        );

        $builder->add('closingMotive', ClosingMotivePickerType::class);
    }
}
