<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\Type;

use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\PersonBundle\Repository\SocialWork\SocialIssueRepository;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PickSocialIssueType extends AbstractType
{
    public function __construct(private readonly SocialIssueRender $issueRender, private readonly SocialIssueRepository $issueRepository)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'class' => SocialIssue::class,
                'choices' => $this->issueRepository->findAllActive(),
                'choice_label' => fn (SocialIssue $si) => $this->issueRender->renderString($si, []),
                'placeholder' => 'Pick a social issue',
                'required' => false,
                'attr' => ['class' => 'select2'],
                'label' => 'Social issues',
                'multiple' => false,
            ])
            ->setAllowedTypes('multiple', ['bool']);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
