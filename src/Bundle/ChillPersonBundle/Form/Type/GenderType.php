<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\Type;

use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A type to select the civil union state.
 */
class GenderType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $a = [
            Person::MALE_GENDER => Person::MALE_GENDER,
            Person::FEMALE_GENDER => Person::FEMALE_GENDER,
            Person::BOTH_GENDER => Person::BOTH_GENDER,
        ];

        $resolver->setDefaults([
            'choices' => $a,
            'expanded' => true,
            'multiple' => false,
            'placeholder' => null,
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
