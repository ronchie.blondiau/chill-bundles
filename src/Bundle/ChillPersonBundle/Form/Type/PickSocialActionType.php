<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\Type;

use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Repository\SocialWork\SocialActionRepository;
use Chill\PersonBundle\Templating\Entity\SocialActionRender;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PickSocialActionType extends AbstractType
{
    public function __construct(private readonly SocialActionRender $actionRender, private readonly SocialActionRepository $actionRepository)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'class' => SocialAction::class,
                'choices' => $this->actionRepository->findAllActive(),
                'choice_label' => fn (SocialAction $sa) => $this->actionRender->renderString($sa, []),
                'placeholder' => 'Pick a social action',
                'required' => false,
                'attr' => ['class' => 'select2'],
                'label' => 'Social actions',
                'multiple' => false,
            ])
            ->setAllowedTypes('multiple', ['bool']);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
