<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\SocialWork;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\DateIntervalType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Chill\PersonBundle\Entity\SocialWork\Goal;
use Chill\PersonBundle\Entity\SocialWork\Result;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SocialActionType.
 */
class SocialActionType extends AbstractType
{
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TranslatableStringFormType::class, [
                'label' => 'Nom',
            ])
            ->add('issue', EntityType::class, [
                'class' => SocialIssue::class,
                'label' => 'socialAction.socialIssue',
                'choice_label' => fn (SocialIssue $issue) => $this->translatableStringHelper->localize($issue->getTitle()),
            ])
            ->add('parent', EntityType::class, [
                'class' => SocialAction::class,
                'required' => false,
                'choice_label' => fn (SocialAction $issue) => $this->translatableStringHelper->localize($issue->getTitle()),
            ])
            ->add('ordering', NumberType::class, [
                'required' => true,
                'scale' => 6,
            ])
            ->add('results', EntityType::class, [
                'class' => Result::class,
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choice_label' => fn (Result $r) => $this->translatableStringHelper->localize($r->getTitle()),
            ])

            ->add('goals', EntityType::class, [
                'class' => Goal::class,
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choice_label' => fn (Goal $g) => $this->translatableStringHelper->localize($g->getTitle()),
            ])

            ->add('evaluations', EntityType::class, [
                'class' => Evaluation::class,
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choice_label' => fn (Evaluation $e) => $this->translatableStringHelper->localize($e->getTitle()),
            ])

            ->add('defaultNotificationDelay', DateIntervalType::class, [
                'label' => 'socialAction.defaultNotificationDelay',
                'required' => false,
            ])
            ->add('desactivationDate', ChillDateType::class, [
                'required' => false,
                'label' => 'goal.desactivationDate',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', SocialIssue::class);
    }
}
