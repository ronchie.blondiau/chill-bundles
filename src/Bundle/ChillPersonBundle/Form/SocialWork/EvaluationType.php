<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form\SocialWork;

use Chill\MainBundle\Form\Type\DateIntervalType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\SocialWork\Evaluation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EvaluationType.
 */
class EvaluationType extends AbstractType
{
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TranslatableStringFormType::class, [
                'label' => 'Nom',
            ])
            ->add('url', UrlType::class, [
                'label' => 'evaluation.url',
                'required' => false,
            ])
            ->add('delay', DateIntervalType::class, [
                'label' => 'evaluation.delay',
                'required' => false,
            ])
            ->add('notificationDelay', DateIntervalType::class, [
                'label' => 'evaluation.notificationDelay',
                'required' => false,
            ])
            ->add('active', ChoiceType::class, [
                'label' => 'active',
                'choices' => [
                    'active' => true,
                    'inactive' => false,
                ],
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', Evaluation::class);
    }
}
