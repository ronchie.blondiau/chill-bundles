<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\ChillTextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HouseholdMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', ChillDateType::class, [
                'label' => 'household.Start date',
                'input' => 'datetime_immutable',
            ]);

        if (null !== $options['data']->getPosition()) {
            if (!$options['data']->getPosition()->getShareHousehold()) {
                $builder->add('endDate', ChillDateType::class, [
                    'label' => 'household.End date',
                    'input' => 'datetime_immutable',
                ]);
            }
        }

        $builder
            ->add('comment', ChillTextareaType::class, [
                'label' => 'household.Comment',
                'required' => false,
            ]);
    }
}
