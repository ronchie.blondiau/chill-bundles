<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\PersonBundle\Form;

use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Chill\PersonBundle\Form\Type\ClosingMotivePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ClosingMotiveType.
 */
class ClosingMotiveType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TranslatableStringFormType::class, [
                'label' => 'Nom',
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Actif ?',
                'required' => false,
            ])
            ->add('ordering', NumberType::class, [
                'label' => 'Ordre d\'apparition',
                'required' => true,
                'scale' => 5,
            ])
            ->add('parent', ClosingMotivePickerType::class, [
                'label' => 'Parent',
                'required' => false,
                'placeholder' => 'closing_motive.any parent',
                'multiple' => false,
                'only_leaf' => false,
            ])
            ->add('isCanceledAccompanyingPeriod', CheckboxType::class, [
                'label' => $this->translator->trans('Consider canceled'),
                'required' => false,
                'help' => $this->translator->trans('Canceled parcours help'),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', ClosingMotive::class);
    }
}
