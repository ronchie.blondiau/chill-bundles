<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Controller for activity configuration.
 */
class AdminController extends AbstractController
{
    /**
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activity", name="chill_activity_admin_index")
     */
    public function indexActivityAction()
    {
        return $this->render('@ChillActivity/Admin/layout_activity.html.twig');
    }

    /**
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activity_redirect_to_main", name="chill_admin_aside_activity_redirect_to_admin_index", options={null})
     * @\Symfony\Component\Routing\Annotation\Route(path="/{_locale}/admin/activity_redirect_to_main", name="chill_admin_activity_redirect_to_admin_index")
     */
    public function redirectToAdminIndexAction()
    {
        return $this->redirectToRoute('chill_main_admin_central');
    }
}
