<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class AdminActivityTypeCategoryController extends CRUDController
{
    /**
     * @param \Doctrine\ORM\QueryBuilder|mixed $query
     *
     * @return \Doctrine\ORM\QueryBuilder|mixed
     */
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        /* @var \Doctrine\ORM\QueryBuilder $query */
        return $query->orderBy('e.ordering', 'ASC');
    }
}
