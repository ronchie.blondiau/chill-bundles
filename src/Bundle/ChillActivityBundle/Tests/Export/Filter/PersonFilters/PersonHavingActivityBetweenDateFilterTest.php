<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter\PersonFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Entity\ActivityReason;
use Chill\ActivityBundle\Export\Filter\PersonFilters\PersonHavingActivityBetweenDateFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class PersonHavingActivityBetweenDateFilterTest extends AbstractFilterTest
{
    private PersonHavingActivityBetweenDateFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get('chill.activity.export.person_having_an_activity_between_date_filter');
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(ActivityReason::class, 'ar')
            ->select('ar')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'date_from' => \DateTime::createFromFormat('Y-m-d', '2021-07-01'),
                'date_to' => \DateTime::createFromFormat('Y-m-d', '2022-07-01'),
                'reasons' => [$a],
            ];
        }

        return $data;
    }

    public function getQueryBuilders(): array
    {
        if (null === self::$kernel) {
            self::bootKernel();
        }

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity')
                ->join('activity.person', 'person'),
        ];
    }
}
