<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\SocialWork\SocialAction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class BySocialActionFilterTest extends AbstractFilterTest
{
    private \Chill\ActivityBundle\Export\Filter\ACPFilters\BySocialActionFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(\Chill\ActivityBundle\Export\Filter\ACPFilters\BySocialActionFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(SocialAction::class, 'sa')
            ->select('sa')
            ->getQuery()
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'accepted_socialactions' => new ArrayCollection([$a]),
            ];
        }

        return $data;
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity')
                ->join('activity.socialActions', 'actsocialaction'),
        ];
    }
}
