<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Entity\ActivityType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @internal
 *
 * @coversNothing
 */
final class ActivityTypeFilterTest extends AbstractFilterTest
{
    private \Chill\ActivityBundle\Export\Filter\ACPFilters\ActivityTypeFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(\Chill\ActivityBundle\Export\Filter\ACPFilters\ActivityTypeFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(ActivityType::class, 'at')
            ->select('at')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'accepted_activitytypes' => [],
                'date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'date_before' => new RollingDate(RollingDate::T_TODAY),
            ];
            $data[] = [
                'accepted_activitytypes' => new ArrayCollection([$a]),
                'date_after' => null,
                'date_before' => null,
            ];
            $data[] = [
                'accepted_activitytypes' => [$a],
                'date_after' => null,
                'date_before' => null,
            ];
            $data[] = [
                'accepted_activitytypes' => [$a],
                'date_after' => new RollingDate(RollingDate::T_YEAR_PREVIOUS_START),
                'date_before' => new RollingDate(RollingDate::T_TODAY),
            ];
            $data[] = [
                'accepted_activitytypes' => [],
                'date_after' => null,
                'date_before' => null,
            ];
        }

        return $data;
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(AccompanyingPeriod::class, 'acp')
                ->join(Activity::class, 'activity', Expr\Join::WITH, 'activity.accompanyingPeriod = acp')
                ->join('activity.activityType', 'acttype'),
        ];
    }
}
