<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Filter\PersonsFilter;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Templating\Entity\PersonRenderInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
class PersonsFilterTest extends AbstractFilterTest
{
    private PersonRenderInterface $personRender;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->personRender = self::$container->get(PersonRenderInterface::class);
    }

    public function getFilter()
    {
        return new PersonsFilter($this->personRender);
    }

    public function getFormData()
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $persons = $em->createQuery('SELECT p FROM '.Person::class.' p ')
            ->setMaxResults(2)
            ->getResult();

        self::ensureKernelShutdown();

        return [
            [
                'accepted_persons' => $persons,
            ],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        yield $em->createQueryBuilder()
            ->select('count(activity.id)')
            ->from(Activity::class, 'activity');

        self::ensureKernelShutdown();
    }
}
