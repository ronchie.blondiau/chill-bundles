<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Filter;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Filter\UserFilter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @internal
 *
 * @coversNothing
 */
final class UserFilterTest extends AbstractFilterTest
{
    private UserFilter $filter;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->filter = self::$container->get(UserFilter::class);
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData(): array
    {
        self::bootKernel();
        $em = self::$container->get(EntityManagerInterface::class);

        $array = $em->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        $data = [];

        foreach ($array as $a) {
            $data[] = [
                'accepted_users' => $a,
            ];
        }

        return $data;
    }

    public function getQueryBuilders(): array
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity'),
        ];
    }
}
