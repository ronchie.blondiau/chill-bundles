<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Aggregator;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Export\Aggregator\ActivityTypeAggregator;
use Chill\MainBundle\Test\Export\AbstractAggregatorTest;
use Doctrine\ORM\EntityManagerInterface;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Add tests for ActivityTypeAggregator.
 *
 * @internal
 *
 * @coversNothing
 */
final class ActivityTypeAggregatorTest extends AbstractAggregatorTest
{
    use ProphecyTrait;

    private ActivityTypeAggregator $aggregator;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->aggregator = self::$container->get(ActivityTypeAggregator::class);

        $request = $this->prophesize()
            ->willExtend(\Symfony\Component\HttpFoundation\Request::class);

        $request->getLocale()->willReturn('fr');

        self::$container->get('request_stack')
            ->push($request->reveal());
    }

    public function getAggregator()
    {
        return $this->aggregator;
    }

    public function getFormData()
    {
        return [
            [],
        ];
    }

    public function getQueryBuilders()
    {
        self::bootKernel();

        $em = self::$container->get(EntityManagerInterface::class);

        return [
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity'),
            $em->createQueryBuilder()
                ->select('count(activity.id)')
                ->from(Activity::class, 'activity')
                ->join('activity.activityType', 'acttype'),
        ];
    }
}
