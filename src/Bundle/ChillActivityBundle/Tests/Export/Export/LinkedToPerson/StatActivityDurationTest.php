<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToPerson;

use Chill\ActivityBundle\Export\Export\LinkedToPerson\StatActivityDuration;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Test\Export\AbstractExportTest;

/**
 * Test the "sum" part of StatActivityDuration.
 *
 * @internal
 *
 * @coversNothing
 */
final class StatActivityDurationTest extends AbstractExportTest
{
    private readonly StatActivityDuration $export;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $activityRepository = self::$container->get(ActivityRepository::class);
        yield new StatActivityDuration($activityRepository, $this->getParameters(true), 'sum');
        yield new StatActivityDuration($activityRepository, $this->getParameters(false), 'sum');
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'person'],
        ];
    }
}
