<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Export\Export\LinkedToACP;

use Chill\ActivityBundle\Export\Export\LinkedToACP\AvgActivityDuration;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Test\Export\AbstractExportTest;

/**
 * @internal
 *
 * @coversNothing
 */
final class AvgActivityDurationTest extends AbstractExportTest
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function getExport()
    {
        $activityRepository = self::$container->get(ActivityRepository::class);

        yield new AvgActivityDuration($activityRepository, $this->getParameters(true));
        yield new AvgActivityDuration($activityRepository, $this->getParameters(false));
    }

    public function getFormData(): array
    {
        return [
            [],
        ];
    }

    public function getModifiersCombination(): array
    {
        return [
            ['activity'],
            ['activity', 'accompanying_period'],
        ];
    }
}
