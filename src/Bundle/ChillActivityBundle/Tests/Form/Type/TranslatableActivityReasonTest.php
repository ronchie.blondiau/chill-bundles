<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Tests\Form\Type;

use Chill\ActivityBundle\Form\Type\PickActivityReasonType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * Test translatableActivityReason.
 *
 * @internal
 *
 * @coversNothing
 */
final class TranslatableActivityReasonTest extends TypeTestCase
{
    /**
     * @var Prophecy\Prophet
     */
    private static $prophet;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testSimple(): never
    {
        $translatableActivityReasonType = new PickActivityReasonType(
            $this->getTranslatableStringHelper()
        );

        $this->markTestSkipped('See issue 651');
    }

    /**
     * @return \Symfony\Bridge\Doctrine\Form\Type\EntityType
     */
    protected function getEntityType()
    {
        $managerRegistry = (new \Prophecy\Prophet())->prophesize();

        $managerRegistry->willImplement('Doctrine\Common\Persistence\ManagerRegistry');

        return new \Symfony\Bridge\Doctrine\Form\Type\EntityType($managerRegistry->reveal());
    }

    protected function getExtensions()
    {
        $entityType = $this->getEntityType();

        return [new PreloadedExtension([
            'entity' => $entityType,
        ], [])];
    }

    /**
     * @param string $locale
     * @param string $fallbackLocale
     *
     * @return TranslatableStringHelper
     */
    protected function getTranslatableStringHelper(
        $locale = 'en',
        $fallbackLocale = 'en'
    ) {
        $prophet = new \Prophecy\Prophet();
        $requestStack = $prophet->prophesize();
        $request = $prophet->prophesize();
        $translator = $prophet->prophesize();

        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn($fallbackLocale);

        $requestStack->willExtend(\Symfony\Component\HttpFoundation\RequestStack::class);
        $requestStack->getCurrentRequest()->will(static fn () => $request);

        $translator->willExtend(\Symfony\Component\Translation\Translator::class);
        $translator->getFallbackLocales()->willReturn($locale);

        return new TranslatableStringHelper(
            $requestStack->reveal(),
            $translator->reveal()
        );
    }
}
