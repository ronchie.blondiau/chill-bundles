<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Service\EntityInfo\AccompanyingPeriodInfoQueryPart;

use Chill\ActivityBundle\Entity\Activity;
use Chill\PersonBundle\Service\EntityInfo\AccompanyingPeriodInfoUnionQueryPartInterface;

class ActivityUsersDateQueryPartForAccompanyingPeriodInfo implements AccompanyingPeriodInfoUnionQueryPartInterface
{
    public function getAccompanyingPeriodIdColumn(): string
    {
        return 'activity.accompanyingperiod_id';
    }

    public function getRelatedEntityColumn(): string
    {
        return Activity::class;
    }

    public function getRelatedEntityIdColumn(): string
    {
        return 'activity.id';
    }

    public function getUserIdColumn(): string
    {
        return 'au.user_id';
    }

    public function getDateTimeColumn(): string
    {
        return 'activity.date';
    }

    public function getDiscriminator(): string
    {
        return 'activity_date';
    }

    public function getMetadataColumn(): string
    {
        return '\'{}\'::jsonb';
    }

    public function getFromStatement(): string
    {
        return 'activity
            LEFT JOIN activity_user au on activity.id = au.activity_id';
    }

    public function getWhereClause(): string
    {
        return 'activity.accompanyingperiod_id IS NOT NULL';
    }
}
