<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\DocStoreBundle\GenericDoc\FetchQuery;
use Chill\DocStoreBundle\GenericDoc\FetchQueryInterface;
use Chill\PersonBundle\Entity\Person;

/**
 * Gives queries usable for fetching documents, with ACL aware.
 */
interface ActivityDocumentACLAwareRepositoryInterface
{
    /**
     * Return a fetch query for querying document's activities for a person.
     *
     * This method must check the rights to see a document: the user must be allowed to see the given activities
     */
    public function buildFetchQueryActivityDocumentLinkedToPersonFromPersonContext(Person $person, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null): FetchQueryInterface;

    /**
     * Return a fetch query for querying document's activities for an activity in accompanying periods, but for a given person.
     *
     * This method must check the rights to see a document: the user must be allowed to see the given accompanying periods
     */
    public function buildFetchQueryActivityDocumentLinkedToAccompanyingPeriodFromPersonContext(Person $person, ?\DateTimeImmutable $startDate = null, ?\DateTimeImmutable $endDate = null, ?string $content = null): FetchQuery;
}
