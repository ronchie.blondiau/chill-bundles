<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\ActivityReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method ActivityReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivityReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivityReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityReasonRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private readonly RequestStack $requestStack
    ) {
        parent::__construct($registry, ActivityReason::class);
    }

    /**
     * @return ActivityReason[]
     */
    public function findAll(): array
    {
        $qb = $this->createQueryBuilder('ar');
        $qb->select(['ar'])
            ->leftJoin('ar.category', 'category')
            ->addOrderBy('JSON_EXTRACT(category.name, :lang)')
            ->addOrderBy('JSON_EXTRACT(ar.name, :lang)')
            ->setParameter('lang', $this->requestStack->getCurrentRequest()->getLocale() ?? 'fr');

        return $qb->getQuery()->getResult();
    }
}
