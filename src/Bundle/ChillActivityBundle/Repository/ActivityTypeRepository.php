<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\ActivityType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class ActivityTypeRepository implements ActivityTypeRepositoryInterface
{
    private readonly EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(ActivityType::class);
    }

    public function find($id): ?ActivityType
    {
        return $this->repository->find($id);
    }

    /**
     * @return array|ActivityType[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @return array|ActivityType[]
     */
    public function findAllActive(): array
    {
        return $this->findBy(['active' => true]);
    }

    /**
     * @return array|ActivityType[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?ActivityType
    {
        return $this->repository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return ActivityType::class;
    }
}
