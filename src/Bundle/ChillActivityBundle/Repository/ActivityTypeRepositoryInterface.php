<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Repository;

use Chill\ActivityBundle\Entity\ActivityType;
use Doctrine\Persistence\ObjectRepository;

interface ActivityTypeRepositoryInterface extends ObjectRepository
{
    /**
     * @return array<ActivityType>
     */
    public function findAllActive(): array;
}
