<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Notification;

use Chill\ActivityBundle\Entity\Activity;
use Chill\ActivityBundle\Repository\ActivityRepository;
use Chill\MainBundle\Entity\Notification;
use Chill\MainBundle\Notification\NotificationHandlerInterface;

final readonly class ActivityNotificationHandler implements NotificationHandlerInterface
{
    public function __construct(private ActivityRepository $activityRepository)
    {
    }

    public function getTemplate(Notification $notification, array $options = []): string
    {
        return '@ChillActivity/Activity/showInNotification.html.twig';
    }

    public function getTemplateData(Notification $notification, array $options = []): array
    {
        return [
            'notification' => $notification,
            'activity' => $this->activityRepository->find($notification->getRelatedEntityId()),
        ];
    }

    public function supports(Notification $notification, array $options = []): bool
    {
        return Activity::class === $notification->getRelatedEntityClass();
    }
}
