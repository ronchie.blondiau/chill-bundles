<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Entity\Activity;
use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Export\Declarations;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Filter accompanying periods to keep only the one without any activity.
 */
class HasNoActivityFilter implements FilterInterface
{
    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->andWhere('
                NOT EXISTS (
                    SELECT 1 FROM '.Activity::class.' activity
                    WHERE activity.accompanyingPeriod = acp
                )
            ');
    }

    public function applyOn(): string
    {
        return Declarations::ACP_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        return ['Filtered acp which has no activities', []];
    }

    public function getTitle(): string
    {
        return 'Filter acp which has no activity';
    }
}
