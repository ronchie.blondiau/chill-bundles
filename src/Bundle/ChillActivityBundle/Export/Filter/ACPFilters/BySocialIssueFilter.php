<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter\ACPFilters;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\PersonBundle\Entity\SocialWork\SocialIssue;
use Chill\PersonBundle\Form\Type\PickSocialIssueType;
use Chill\PersonBundle\Templating\Entity\SocialIssueRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class BySocialIssueFilter implements FilterInterface
{
    public function __construct(private readonly SocialIssueRender $issueRender)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actsocialissue', $qb->getAllAliases(), true)) {
            $qb->join('activity.socialIssues', 'actsocialissue');
        }

        $clause = $qb->expr()->in('actsocialissue.id', ':socialissues');

        $qb->andWhere($clause)
            ->setParameter(
                'socialissues',
                SocialIssue::getDescendantsWithThisForIssues($data['accepted_socialissues'])
            );
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY_ACP;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_socialissues', PickSocialIssueType::class, [
            'multiple' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $issues = [];

        foreach ($data['accepted_socialissues'] as $issue) {
            $issues[] = $this->issueRender->renderString($issue, [
                'show_and_children' => true,
            ]);
        }

        return ['Filtered activity by linked socialissue: only %issues%', [
            '%issues%' => implode(', ', $issues),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by linked socialissue';
    }
}
