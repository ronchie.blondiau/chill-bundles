<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Filter;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickUserDynamicType;
use Chill\MainBundle\Templating\Entity\UserRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ByCreatorFilter implements FilterInterface
{
    public function __construct(private readonly UserRender $userRender)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb
            ->andWhere(
                $qb->expr()->in('activity.createdBy', ':users')
            )
            ->setParameter('users', $data['accepted_users']);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_users', PickUserDynamicType::class, [
            'multiple' => true,
        ]);
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function describeAction($data, $format = 'string'): array
    {
        $users = [];

        foreach ($data['accepted_users'] as $u) {
            $users[] = $this->userRender->renderString($u, []);
        }

        return ['Filtered activity by creator: only %users%', [
            '%users%' => implode(', ', $users),
        ]];
    }

    public function getTitle(): string
    {
        return 'Filter activity by creator';
    }
}
