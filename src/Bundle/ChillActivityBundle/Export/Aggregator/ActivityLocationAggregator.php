<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

final readonly class ActivityLocationAggregator implements AggregatorInterface
{
    public const KEY = 'activity_location_aggregator';

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        if (!\in_array('actloc', $qb->getAllAliases(), true)) {
            $qb->leftJoin('activity.location', 'actloc');
        }
        $qb->addSelect(sprintf('actloc.name AS %s', self::KEY));
        $qb->addGroupBy(self::KEY);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form required for this aggregator
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'export.aggregator.activity.by_location.Activity Location';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            return $value;
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::KEY];
    }

    public function getTitle()
    {
        return 'export.aggregator.activity.by_location.Title';
    }
}
