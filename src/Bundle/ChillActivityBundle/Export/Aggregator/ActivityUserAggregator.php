<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserRepository;
use Chill\MainBundle\Templating\Entity\UserRender;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ActivityUserAggregator implements AggregatorInterface
{
    final public const KEY = 'activity_user_id';

    public function __construct(private readonly UserRepository $userRepository, private readonly UserRender $userRender)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // add select element
        $qb->addSelect(sprintf('IDENTITY(activity.user) AS %s', self::KEY));

        // add the "group by" part
        $qb->addGroupBy(self::KEY);
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // nothing to add
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, $values, $data): \Closure
    {
        return function ($value) {
            if ('_header' === $value) {
                return 'Activity user';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $u = $this->userRepository->find($value);

            return $this->userRender->renderString($u, []);
        };
    }

    public function getQueryKeys($data)
    {
        return [self::KEY];
    }

    public function getTitle(): string
    {
        return 'Aggregate by activity user';
    }
}
