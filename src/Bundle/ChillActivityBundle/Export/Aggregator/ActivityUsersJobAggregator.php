<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export\Aggregator;

use Chill\ActivityBundle\Export\Declarations;
use Chill\MainBundle\Entity\User\UserJobHistory;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Repository\UserJobRepositoryInterface;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ActivityUsersJobAggregator implements AggregatorInterface
{
    private const PREFIX = 'act_agg_user_job';

    public function __construct(
        private readonly UserJobRepositoryInterface $userJobRepository,
        private readonly TranslatableStringHelperInterface $translatableStringHelper
    ) {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $p = self::PREFIX;

        $qb
            ->leftJoin('activity.users', "{$p}_user")
            ->leftJoin(
                UserJobHistory::class,
                "{$p}_history",
                Expr\Join::WITH,
                $qb->expr()->eq("{$p}_history.user", "{$p}_user")
            )
            // job_at based on activity.date
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->lte("{$p}_history.startDate", 'activity.date'),
                    $qb->expr()->orX(
                        $qb->expr()->isNull("{$p}_history.endDate"),
                        $qb->expr()->gt("{$p}_history.endDate", 'activity.date')
                    )
                )
            )
            ->addSelect("IDENTITY({$p}_history.job) AS {$p}_select")
            ->addGroupBy("{$p}_select");
    }

    public function applyOn(): string
    {
        return Declarations::ACTIVITY;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data)
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Users \'s job';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $j = $this->userJobRepository->find($value);

            return $this->translatableStringHelper->localize(
                $j->getLabel()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return [self::PREFIX.'_select'];
    }

    public function getTitle(): string
    {
        return 'export.aggregator.activity.by_user_job.Aggregate by users job';
    }
}
