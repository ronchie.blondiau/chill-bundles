<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Export;

/**
 * This class declare constants used for the export framework.
 */
abstract class Declarations
{
    final public const ACTIVITY = 'activity';

    final public const ACTIVITY_ACP = 'activity_linked_to_acp';

    final public const ACTIVITY_PERSON = 'activity_linked_to_person';
}
