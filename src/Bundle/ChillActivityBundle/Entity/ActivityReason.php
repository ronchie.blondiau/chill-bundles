<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ActivityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ActivityReason.
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="activityreason")
 *
 * @ORM\HasLifecycleCallbacks
 */
class ActivityReason
{
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Chill\ActivityBundle\Entity\ActivityReasonCategory",
     * inversedBy="reasons")
     */
    private ?ActivityReasonCategory $category = null;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="id", type="integer")
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $name;

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get category.
     */
    public function getCategory(): ?ActivityReasonCategory
    {
        return $this->category;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     */
    public function getName(): array
    {
        return $this->name;
    }

    public function isActiveAndParentActive(): bool
    {
        return $this->active && null !== $this->getCategory() && $this->getCategory()->getActive();
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return ActivityReason
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set category of the reason. If you set to the reason an inactive
     * category, the reason will become inactive.
     *
     * @return ActivityReason
     */
    public function setCategory(ActivityReasonCategory $category)
    {
        if ($this->category !== $category && !$category->getActive()) {
            $this->setActive(false);
        }

        $this->category = $category;

        return $this;
    }

    /**
     * Set name.
     *
     * @param array $name
     *
     * @return ActivityReason
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
