// this file loads all assets from the Chill person bundle
module.exports = function(encore, entries)
{
    entries.push(__dirname + '/Resources/public/chill/index.js');

    encore.addAliases({
        ChillActivityAssets: __dirname + '/Resources/public'
    });

    encore.addEntry('page_edit_activity', __dirname + '/Resources/public/page/edit_activity/index.scss');

    encore.addEntry('vue_activity', __dirname + '/Resources/public/vuejs/Activity/index.js');
};
