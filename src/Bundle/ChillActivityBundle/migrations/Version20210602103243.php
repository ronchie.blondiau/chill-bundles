<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add info for socialIssues & socialActions in ActivityType.
 */
final class Version20210602103243 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype DROP socialIssuesVisible');
        $this->addSql('ALTER TABLE activitytype DROP socialIssuesLabel');
        $this->addSql('ALTER TABLE activitytype DROP socialActionsVisible');
        $this->addSql('ALTER TABLE activitytype DROP socialActionsLabel');
    }

    public function getDescription(): string
    {
        return 'Add info for socialIssues & socialActions in ActivityType';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activitytype ADD socialIssuesVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD socialIssuesLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD socialActionsVisible SMALLINT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE activitytype ADD socialActionsLabel VARCHAR(255) DEFAULT \'\' NOT NULL');
    }
}
