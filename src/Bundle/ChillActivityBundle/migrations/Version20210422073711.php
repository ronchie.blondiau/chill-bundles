<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422073711 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE activity DROP emergency');
        $this->addSql('ALTER TABLE activity DROP CONSTRAINT FK_AC74095ABCFD782A');
        $this->addSql('ALTER TABLE activity ADD attendee BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE activity DROP attendee_id');
        $this->addSql('ALTER TABLE activity DROP sentReceived');

        $this->addSql('DROP SEQUENCE activitytpresence_id_seq CASCADE');
        $this->addSql('DROP TABLE activitytpresence');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE activitytpresence_id_seq INCREMENT BY 1 MINVALUE 1 START 6');
        $this->addSql('CREATE TABLE activitytpresence (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');

        $list = [
            'Usager pésent', "Absence de l''usager",
            "Refus de visite ou d''entretien", 'Domicile non trouvé',
            'Domicile erronéee',
        ];

        for ($i = 1; \count($list) >= $i; ++$i) {
            $this->addSql('INSERT INTO activitytpresence VALUES('.$i.", json_build_object('fr', '".$list[$i - 1]."'), true)");
        }

        $this->addSql('ALTER TABLE activity ADD emergency BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('ALTER TABLE activity ADD sentReceived VARCHAR(255) NOT NULL DEFAULT \'\' ');
        $this->addSql('ALTER TABLE activity ALTER attendee TYPE INT USING CASE WHEN attendee is false THEN 2 WHEN attendee is true THEN 1 ELSE null END');
        $this->addSql('ALTER TABLE activity RENAME COLUMN attendee TO attendee_id');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095ABCFD782A FOREIGN KEY (attendee_id) REFERENCES activitytpresence (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
