<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Activity;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506112500 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE activity_document (activity_id INT NOT NULL, document_id INT NOT NULL, PRIMARY KEY(activity_id, document_id))');
        $this->addSql('CREATE INDEX idx_78633a78c33f7837 ON activity_document (document_id)');
        $this->addSql('CREATE INDEX idx_78633a7881c06096 ON activity_document (activity_id)');
        $this->addSql('ALTER TABLE activity_document ADD CONSTRAINT fk_78633a7881c06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE activity_storedobject');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activity_storedobject (activity_id INT NOT NULL, storedobject_id INT NOT NULL, PRIMARY KEY(activity_id, storedobject_id))');
        $this->addSql('CREATE INDEX IDX_6F660E9381C06096 ON activity_storedobject (activity_id)');
        $this->addSql('CREATE INDEX IDX_6F660E93EE684399 ON activity_storedobject (storedobject_id)');
        $this->addSql('ALTER TABLE activity_storedobject ADD CONSTRAINT FK_6F660E9381C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE activity_storedobject ADD CONSTRAINT FK_6F660E93EE684399 FOREIGN KEY (storedobject_id) REFERENCES chill_doc.stored_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE activity_document');
    }
}
