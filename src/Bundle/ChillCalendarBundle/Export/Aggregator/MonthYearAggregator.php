<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Aggregator;

use Chill\CalendarBundle\Export\Declarations;
use Chill\MainBundle\Export\AggregatorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class MonthYearAggregator implements AggregatorInterface
{
    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $qb->addSelect("to_char(cal.startDate, 'MM-YYYY') AS month_year_aggregator");
        // $qb->addSelect("extract(month from age(cal.startDate, cal.endDate)) AS month_aggregator");
        $qb->addGroupBy('month_year_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // No form needed
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return static function ($value): string {
            if ('_header' === $value) {
                return 'by month and year';
            }

            return $value;
        };
    }

    public function getQueryKeys($data): array
    {
        return ['month_year_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group calendars by month and year';
    }
}
