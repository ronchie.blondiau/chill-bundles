<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Export\Aggregator;

use Chill\CalendarBundle\Export\Declarations;
use Chill\CalendarBundle\Repository\CancelReasonRepository;
use Chill\MainBundle\Export\AggregatorInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class CancelReasonAggregator implements AggregatorInterface
{
    public function __construct(private readonly CancelReasonRepository $cancelReasonRepository, private readonly TranslatableStringHelper $translatableStringHelper)
    {
    }

    public function addRole(): ?string
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: still needs to take into account calendars without a cancel reason somehow
        if (!\in_array('calcancel', $qb->getAllAliases(), true)) {
            $qb->join('cal.cancelReason', 'calcancel');
        }

        $qb->addSelect('IDENTITY(cal.cancelReason) as cancel_reason_aggregator');
        $qb->addGroupBy('cancel_reason_aggregator');
    }

    public function applyOn(): string
    {
        return Declarations::CALENDAR_TYPE;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        // no form
    }

    public function getFormDefaultData(): array
    {
        return [];
    }

    public function getLabels($key, array $values, $data): \Closure
    {
        return function ($value): string {
            if ('_header' === $value) {
                return 'Cancel reason';
            }

            if (null === $value || '' === $value) {
                return '';
            }

            $j = $this->cancelReasonRepository->find($value);

            return $this->translatableStringHelper->localize(
                $j->getName()
            );
        };
    }

    public function getQueryKeys($data): array
    {
        return ['cancel_reason_aggregator'];
    }

    public function getTitle(): string
    {
        return 'Group calendars by cancel reason';
    }
}
