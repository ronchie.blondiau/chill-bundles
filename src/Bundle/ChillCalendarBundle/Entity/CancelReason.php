<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Chill\CalendarBundle\Repository\CancelReasonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="chill_calendar.cancel_reason")
 *
 * @ORM\Entity(repositoryClass=CancelReasonRepository::class)
 */
class CancelReason
{
    final public const CANCELEDBY_DONOTCOUNT = 'CANCELEDBY_DONOTCOUNT';

    final public const CANCELEDBY_PERSON = 'CANCELEDBY_PERSON';

    final public const CANCELEDBY_USER = 'CANCELEDBY_USER';

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $active = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $canceledBy = null;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $name = [];

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getCanceledBy(): ?string
    {
        return $this->canceledBy;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?array
    {
        return $this->name;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function setCanceledBy(string $canceledBy): self
    {
        $this->canceledBy = $canceledBy;

        return $this;
    }

    public function setName(array $name): self
    {
        $this->name = $name;

        return $this;
    }
}
