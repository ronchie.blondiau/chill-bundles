<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Entity;

use Chill\CalendarBundle\Entity\CalendarDoc\CalendarDocCreateDTO;
use Chill\CalendarBundle\Entity\CalendarDoc\CalendarDocEditDTO;
use Chill\DocStoreBundle\Entity\StoredObject;
use Chill\MainBundle\Doctrine\Model\TrackCreationInterface;
use Chill\MainBundle\Doctrine\Model\TrackCreationTrait;
use Chill\MainBundle\Doctrine\Model\TrackUpdateInterface;
use Chill\MainBundle\Doctrine\Model\TrackUpdateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="chill_calendar.calendar_doc",
 * )
 */
class CalendarDoc implements TrackCreationInterface, TrackUpdateInterface
{
    use TrackCreationTrait;

    use TrackUpdateTrait;

    /**
     * @ORM\ManyToOne(targetEntity=Calendar::class, inversedBy="documents")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Calendar $calendar;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $datetimeVersion = 0;

    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private bool $trackDateTimeVersion = false;

    public function __construct(Calendar $calendar, /**
     * @ORM\ManyToOne(targetEntity=StoredObject::class, cascade={"persist"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
        private ?StoredObject $storedObject)
    {
        $this->setCalendar($calendar);
        $this->datetimeVersion = $calendar->getDateTimeVersion();
    }

    public function createFromDTO(CalendarDocCreateDTO $calendarDocCreateDTO): void
    {
        $this->storedObject = $calendarDocCreateDTO->doc;
        $this->storedObject->setTitle($calendarDocCreateDTO->title);
    }

    public function editFromDTO(CalendarDocEditDTO $calendarDocEditDTO): void
    {
        if (null !== $calendarDocEditDTO->doc) {
            $calendarDocEditDTO->doc->setTitle($this->getStoredObject()->getTitle());
            $this->setStoredObject($calendarDocEditDTO->doc);
        }

        $this->getStoredObject()->setTitle($calendarDocEditDTO->title);
    }

    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    public function getDatetimeVersion(): int
    {
        return $this->datetimeVersion;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStoredObject(): StoredObject
    {
        return $this->storedObject;
    }

    public function isTrackDateTimeVersion(): bool
    {
        return $this->trackDateTimeVersion;
    }

    /**
     * @internal use @see{Calendar::removeDocument} instead
     */
    public function setCalendar(?Calendar $calendar): CalendarDoc
    {
        if (null === $calendar) {
            $this->calendar->removeDocument($this);
        } else {
            $calendar->addDocument($this);
        }

        $this->calendar = $calendar;

        $this->datetimeVersion = $calendar->getDateTimeVersion();

        return $this;
    }

    public function setDatetimeVersion(int $datetimeVersion): CalendarDoc
    {
        $this->datetimeVersion = $datetimeVersion;

        return $this;
    }

    public function setStoredObject(StoredObject $storedObject): CalendarDoc
    {
        $this->storedObject = $storedObject;

        return $this;
    }

    public function setTrackDateTimeVersion(bool $trackDateTimeVersion): CalendarDoc
    {
        $this->trackDateTimeVersion = $trackDateTimeVersion;

        return $this;
    }
}
