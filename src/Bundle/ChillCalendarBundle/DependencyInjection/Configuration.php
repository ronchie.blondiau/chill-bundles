<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('chill_calendar');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->arrayNode('short_messages')
            ->canBeDisabled()
            ->children()->end()
            ->end() // end for short_messages
            ->arrayNode('remote_calendars_sync')->canBeEnabled()
            ->children()
            ->arrayNode('microsoft_graph')->canBeEnabled()
            ->children()
            ->end() // end of machine_access_token
            ->end() // end of microsoft_graph children
            ->end() // end of array microsoft_graph
            ->end() // end of children's remote_calendars_sync
            ->end() // end of array remote_calendars_sync
            ->end();

        return $treeBuilder;
    }
}
