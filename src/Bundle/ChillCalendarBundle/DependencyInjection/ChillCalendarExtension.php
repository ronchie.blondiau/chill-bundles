<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DependencyInjection;

use Chill\CalendarBundle\Security\Voter\CalendarVoter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillCalendarExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/exports.yaml');
        $loader->load('services/controller.yml');
        $loader->load('services/fixtures.yml');
        $loader->load('services/form.yml');
        $loader->load('services/event.yml');
        $loader->load('services/remote_calendar.yaml');

        $container->setParameter('chill_calendar', $config);

        if ($config['short_messages']['enabled']) {
            $container->setParameter('chill_calendar.short_messages', $config['short_messages']);
        } else {
            $container->setParameter('chill_calendar.short_messages', null);
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->preprendRoutes($container);
        $this->prependCruds($container);
        $this->prependRoleHierarchy($container);
    }

    private function prependCruds(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'cruds' => [
                [
                    'class' => \Chill\CalendarBundle\Entity\CancelReason::class,
                    'name' => 'calendar_cancel-reason',
                    'base_path' => '/admin/calendar/cancel-reason',
                    'form_class' => \Chill\CalendarBundle\Form\CancelReasonType::class,
                    'controller' => \Chill\CalendarBundle\Controller\CancelReasonController::class,
                    'actions' => [
                        'index' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillCalendar/CancelReason/index.html.twig',
                        ],
                        'new' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillCalendar/CancelReason/new.html.twig',
                        ],
                        'edit' => [
                            'role' => 'ROLE_ADMIN',
                            'template' => '@ChillCalendar/CancelReason/edit.html.twig',
                        ],
                    ],
                ],
            ],
            'apis' => [
                [
                    'controller' => \Chill\CalendarBundle\Controller\CalendarRangeAPIController::class,
                    'class' => \Chill\CalendarBundle\Entity\CalendarRange::class,
                    'name' => 'calendar_range',
                    'base_path' => '/api/1.0/calendar/calendar-range',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                                Request::METHOD_POST => true,
                                Request::METHOD_PATCH => true,
                                Request::METHOD_DELETE => true,
                            ],
                        ],
                    ],
                ],
                [
                    'controller' => \Chill\CalendarBundle\Controller\CalendarAPIController::class,
                    'class' => \Chill\CalendarBundle\Entity\Calendar::class,
                    'name' => 'calendar',
                    'base_path' => '/api/1.0/calendar/calendar',
                    'base_role' => 'ROLE_USER',
                    'actions' => [
                        '_index' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                        '_entity' => [
                            'methods' => [
                                Request::METHOD_GET => true,
                                Request::METHOD_HEAD => true,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    private function prependRoleHierarchy(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('security', [
            'role_hierarchy' => [
                CalendarVoter::CREATE => [CalendarVoter::SEE],
                CalendarVoter::EDIT => [CalendarVoter::SEE],
                CalendarVoter::DELETE => [CalendarVoter::SEE],
            ],
        ]);
    }

    private function preprendRoutes(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('chill_main', [
            'routing' => [
                'resources' => [
                    '@ChillCalendarBundle/Resources/config/routing.yml',
                ],
            ],
        ]);
    }
}
