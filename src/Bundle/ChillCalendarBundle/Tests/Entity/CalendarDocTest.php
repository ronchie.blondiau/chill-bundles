<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Entity;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Entity\CalendarDoc;
use Chill\DocStoreBundle\Entity\StoredObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CalendarDocTest extends TestCase
{
    public function testCreateEditFromDTO(): void
    {
        $doc = new CalendarDoc(new Calendar(), null);

        $create = new CalendarDoc\CalendarDocCreateDTO();
        $create->title = 'tagada';
        $create->doc = $obj1 = new StoredObject();

        $doc->createFromDTO($create);

        $this->assertSame($obj1, $doc->getStoredObject());
        $this->assertEquals('tagada', $doc->getStoredObject()->getTitle());

        $edit = new CalendarDoc\CalendarDocEditDTO($doc);
        $edit->title = 'tsointsoin';

        $doc->editFromDTO($edit);

        $this->assertSame($obj1, $doc->getStoredObject());
        $this->assertEquals('tsointsoin', $doc->getStoredObject()->getTitle());

        $edit2 = new CalendarDoc\CalendarDocEditDTO($doc);
        $edit2->doc = $obj2 = new StoredObject();

        $doc->editFromDTO($edit2);

        $this->assertSame($obj2, $doc->getStoredObject());
        $this->assertEquals('tsointsoin', $doc->getStoredObject()->getTitle());

        $edit3 = new CalendarDoc\CalendarDocEditDTO($doc);
        $edit3->doc = $obj3 = new StoredObject();
        $edit3->title = 'tagada';

        $doc->editFromDTO($edit3);

        $this->assertSame($obj3, $doc->getStoredObject());
        $this->assertEquals('tagada', $doc->getStoredObject()->getTitle());
    }
}
