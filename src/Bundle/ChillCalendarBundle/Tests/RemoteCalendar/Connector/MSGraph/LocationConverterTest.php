<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\RemoteCalendar\Connector\MSGraph;

use Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph\AddressConverter;
use Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph\LocationConverter;
use Chill\MainBundle\Doctrine\Model\Point;
use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\AddressReference;
use Chill\MainBundle\Entity\Location;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class LocationConverterTest extends TestCase
{
    use ProphecyTrait;

    public function testConvertToRemoteWithAddressWithoutPoint(): void
    {
        $location = (new Location())->setName('display')->setAddress($address = new Address());
        $address->setAddressReference($reference = new AddressReference());

        $actual = $this->buildLocationConverter()->locationToRemote($location);

        $this->assertArrayHasKey('address', $actual);
        $this->assertArrayNotHasKey('coordinates', $actual);
        $this->assertArrayHasKey('displayName', $actual);
        $this->assertEquals('display', $actual['displayName']);
    }

    public function testConvertToRemoteWithAddressWithPoint(): void
    {
        $location = (new Location())->setName('display')->setAddress($address = new Address());
        $address->setAddressReference($reference = new AddressReference());
        $reference->setPoint($point = Point::fromLonLat(5.3134, 50.3134));

        $actual = $this->buildLocationConverter()->locationToRemote($location);

        $this->assertArrayHasKey('address', $actual);
        $this->assertArrayHasKey('coordinates', $actual);
        $this->assertEquals(['latitude' => 50.3134, 'longitude' => 5.3134], $actual['coordinates']);
        $this->assertArrayHasKey('displayName', $actual);
        $this->assertEquals('display', $actual['displayName']);
    }

    public function testConvertToRemoteWithoutAddressWithoutPoint(): void
    {
        $location = (new Location())->setName('display');

        $actual = $this->buildLocationConverter()->locationToRemote($location);

        $this->assertArrayNotHasKey('address', $actual);
        $this->assertArrayNotHasKey('coordinates', $actual);
        $this->assertArrayHasKey('displayName', $actual);
        $this->assertEquals('display', $actual['displayName']);
    }

    private function buildLocationConverter(): LocationConverter
    {
        $addressConverter = $this->prophesize(AddressConverter::class);
        $addressConverter->addressToRemote(Argument::type(Address::class))->willReturn(['street' => 'dummy']);

        return new LocationConverter($addressConverter->reveal());
    }
}
