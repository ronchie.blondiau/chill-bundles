<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\RemoteCalendar\Connector\MSGraph;

use Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph\AddressConverter;
use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Entity\PostalCode;
use Chill\MainBundle\Templating\Entity\AddressRender;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @internal
 *
 * @coversNothing
 */
final class AddressConverterTest extends TestCase
{
    use ProphecyTrait;

    public function testConvertAddress()
    {
        $country = (new Country())->setName(['fr' => 'Belgique']);
        $postalCode = (new PostalCode())->setName('Houte-Si-Plout')->setCode('4122')
            ->setCountry($country);
        $address = (new Address())->setPostcode($postalCode)->setStreet("Rue de l'Église")
            ->setStreetNumber('15B')->setBuildingName('Résidence de la Truite');

        $actual = $this->buildAddressConverter()->addressToRemote($address);

        $this->assertArrayHasKey('city', $actual);
        $this->assertStringContainsString($actual['city'], 'Houte-Si-Plout');
        $this->assertArrayHasKey('postalCode', $actual);
        $this->assertStringContainsString($actual['postalCode'], '4122');
        $this->assertArrayHasKey('countryOrRegion', $actual);
        $this->assertStringContainsString('Belgique', $actual['countryOrRegion']);
        $this->assertArrayHasKey('street', $actual);
        $this->assertStringContainsString('Rue de l\'Église', $actual['street']);
        $this->assertStringContainsString('15B', $actual['street']);
        $this->assertStringContainsString('Résidence de la Truite', $actual['street']);
    }

    private function buildAddressConverter(): AddressConverter
    {
        $engine = $this->prophesize(\Twig\Environment::class);
        $translatableStringHelper = $this->prophesize(TranslatableStringHelperInterface::class);
        $translatableStringHelper->localize(Argument::type('array'))->will(static fn ($args): string => ($args[0] ?? ['fr' => 'not provided'])['fr'] ?? 'not provided');

        $addressRender = new AddressRender($engine->reveal(), $translatableStringHelper->reveal());

        return new AddressConverter($addressRender, $translatableStringHelper->reveal());
    }
}
