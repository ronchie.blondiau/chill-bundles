<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Tests\Service\ShortMessageNotification;

use Chill\CalendarBundle\Service\ShortMessageNotification\DefaultRangeGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DefaultRangeGeneratorTest extends TestCase
{
    /**
     * * Lundi => Envoi des rdv du mardi et mercredi.
     * * Mardi => Envoi des rdv du jeudi.
     * * Mercredi => Envoi des rdv du vendredi
     * * Jeudi => envoi des rdv du samedi et dimanche
     * * Vendredi => Envoi des rdv du lundi.
     */
    public function generateData(): \Iterator
    {
        yield [
            new \DateTimeImmutable('2022-06-13 10:45:00'),
            new \DateTimeImmutable('2022-06-14 00:00:00'),
            new \DateTimeImmutable('2022-06-16 00:00:00'),
        ];

        yield [
            new \DateTimeImmutable('2022-06-14 15:45:00'),
            new \DateTimeImmutable('2022-06-16 00:00:00'),
            new \DateTimeImmutable('2022-06-17 00:00:00'),
        ];

        yield [
            new \DateTimeImmutable('2022-06-15 13:45:18'),
            new \DateTimeImmutable('2022-06-17 00:00:00'),
            new \DateTimeImmutable('2022-06-18 00:00:00'),
        ];

        yield [
            new \DateTimeImmutable('2022-06-16 01:30:55'),
            new \DateTimeImmutable('2022-06-18 00:00:00'),
            new \DateTimeImmutable('2022-06-20 00:00:00'),
        ];

        yield [
            new \DateTimeImmutable('2022-06-17 21:30:55'),
            new \DateTimeImmutable('2022-06-20 00:00:00'),
            new \DateTimeImmutable('2022-06-21 00:00:00'),
        ];

        yield [
            new \DateTimeImmutable('2022-06-18 21:30:55'),
            null,
            null,
        ];

        yield [
            new \DateTimeImmutable('2022-06-19 21:30:55'),
            null,
            null,
        ];
    }

    /**
     * @dataProvider generateData
     */
    public function testGenerateRange(\DateTimeImmutable $date, ?\DateTimeImmutable $startDate, ?\DateTimeImmutable $endDate)
    {
        $generator = new DefaultRangeGenerator();

        ['startDate' => $actualStartDate, 'endDate' => $actualEndDate] = $generator->generateRange($date);

        if (null === $startDate) {
            $this->assertNull($actualStartDate);
            $this->assertNull($actualEndDate);
        } else {
            $this->assertEquals($startDate->format(\DateTimeImmutable::ATOM), $actualStartDate->format(\DateTimeImmutable::ATOM));
            $this->assertEquals($endDate->format(\DateTimeImmutable::ATOM), $actualEndDate->format(\DateTimeImmutable::ATOM));
        }
    }
}
