<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Service\ShortMessageNotification;

interface RangeGeneratorInterface
{
    /**
     * @return ?array{startDate: \DateTimeImmutable, endDate: \DateTimeImmutable} when return is null, then no ShortMessage must be send
     */
    public function generateRange(\DateTimeImmutable $date): ?array;
}
