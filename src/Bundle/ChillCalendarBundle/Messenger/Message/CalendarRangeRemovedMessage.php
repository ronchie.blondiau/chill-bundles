<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Messenger\Message;

use Chill\CalendarBundle\Entity\CalendarRange;
use Chill\MainBundle\Entity\User;

class CalendarRangeRemovedMessage
{
    private ?int $byUserId = null;

    private readonly int $calendarRangeUserId;

    private readonly array $remoteAttributes;

    private readonly string $remoteId;

    public function __construct(CalendarRange $calendarRange, ?User $byUser)
    {
        $this->remoteId = $calendarRange->getRemoteId();
        $this->remoteAttributes = $calendarRange->getRemoteAttributes();
        $this->calendarRangeUserId = $calendarRange->getUser()->getId();

        if (null !== $byUser) {
            $this->byUserId = $byUser->getId();
        }
    }

    public function getByUserId(): ?int
    {
        return $this->byUserId;
    }

    public function getCalendarRangeUserId(): ?int
    {
        return $this->calendarRangeUserId;
    }

    public function getRemoteAttributes(): array
    {
        return $this->remoteAttributes;
    }

    public function getRemoteId(): string
    {
        return $this->remoteId;
    }
}
