
import { createApp } from 'vue';
import Answer from 'ChillCalendarAssets/vuejs/Invite/Answer';
import { _createI18n } from 'ChillMainAssets/vuejs/_js/i18n';

const i18n = _createI18n({});

document.addEventListener('DOMContentLoaded', function (e) {
  console.log('dom loaded answer');
  document.querySelectorAll('div[invite-answer]').forEach(function (el) {
    console.log('element found', el);

    const app = createApp({
      components: {
        Answer,
      },
      data() {
        return {
          status: el.dataset.status,
          calendarId: Number.parseInt(el.dataset.calendarId),
        }
      },
      template: '<answer :calendarId="calendarId" :status="status" @statusChanged="onStatusChanged"></answer>',
      methods: {
        onStatusChanged: function(newStatus) {
          this.$data.status = newStatus;
        },
      }
    });

    app.use(i18n).mount(el);
  });
});
