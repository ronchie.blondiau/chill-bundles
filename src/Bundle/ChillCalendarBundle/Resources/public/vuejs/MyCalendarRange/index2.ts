import { createApp } from 'vue';
import { _createI18n } from '../../../../../ChillMainBundle/Resources/public/vuejs/_js/i18n'
import { appMessages } from './i18n'
import futureStore, {key} from './store/index'

import App2 from './App2.vue';
import {useI18n} from "vue-i18n";

futureStore().then((store) => {
  const i18n = _createI18n(appMessages, false);

  const app = createApp({
    template: `<app></app>`,
  })
    .use(store, key)
    .use(i18n)
    .component('app', App2)
    .mount('#myCalendar');
});
