import {fetchResults} from '../../../../../ChillMainBundle/Resources/public/lib/api/apiMethods';
import {datetimeToISO} from '../../../../../ChillMainBundle/Resources/public/chill/js/date';
import {User} from '../../../../../ChillMainBundle/Resources/public/types';
import {CalendarLight, CalendarRange, CalendarRemote} from '../../types';

// re-export whoami
export {whoami} from "../../../../../ChillMainBundle/Resources/public/lib/api/user";

/**
 *
 * @param user
 * @param Date start
 * @param Date end
 * @return Promise
 */
export const fetchCalendarRangeForUser = (user: User, start: Date, end: Date): Promise<CalendarRange[]> => {
  const uri = `/api/1.0/calendar/calendar-range-available/${user.id}.json`;
  const dateFrom = datetimeToISO(start);
  const dateTo = datetimeToISO(end);

  return fetchResults<CalendarRange>(uri, {dateFrom, dateTo});
}

export const fetchCalendarRemoteForUser = (user: User, start: Date, end: Date): Promise<CalendarRemote[]> => {
  const uri = `/api/1.0/calendar/proxy/calendar/by-user/${user.id}/events`;
  const dateFrom = datetimeToISO(start);
  const dateTo = datetimeToISO(end);

  return fetchResults<CalendarRemote>(uri, {dateFrom, dateTo});
}

export const fetchCalendarLocalForUser = (user: User, start: Date, end: Date): Promise<CalendarLight[]> => {
  const uri = `/api/1.0/calendar/calendar/by-user/${user.id}.json`;
  const dateFrom = datetimeToISO(start);
  const dateTo = datetimeToISO(end);

  return fetchResults<CalendarLight>(uri, {dateFrom, dateTo});
}
