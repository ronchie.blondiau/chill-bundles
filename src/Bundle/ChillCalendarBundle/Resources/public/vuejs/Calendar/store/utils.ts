import {COLORS} from '../const';
import {ISOToDatetime} from '../../../../../../ChillMainBundle/Resources/public/chill/js/date';
import {DateTime, User} from '../../../../../../ChillMainBundle/Resources/public/types';
import {CalendarLight, CalendarRange, CalendarRemote} from '../../../types';
import type {EventInputCalendarRange} from '../../../types';
import {EventInput} from '@fullcalendar/core';

export interface UserData {
  user: User,
  calendarRanges: CalendarRange[],
  calendarRangesLoaded: {}[],
  remotes: CalendarRemote[],
  remotesLoaded: {}[],
  locals: CalendarRemote[],
  localsLoaded: {}[],
  mainColor: string,
}

export const addIdToValue = (string: string, id: number): string => {
  let array = string ? string.split(',') : [];
  array.push(id.toString());
  let str = array.join();
  return str;
};

export const removeIdFromValue = (string: string, id: number) => {
  let array = string.split(',');
  array = array.filter(el => el !== id.toString());
  let str = array.join();
  return str;
};

/*
* Assign missing keys for the ConcernedGroups component
*/
export const mapEntity = (entity: EventInput): EventInput => {
  let calendar =  { ...entity};
  Object.assign(calendar, {thirdParties: entity.professionals});

  if (entity.startDate !== null ) {
    calendar.startDate = ISOToDatetime(entity.startDate.datetime);
  }
  if (entity.endDate !== null) {
    calendar.endDate = ISOToDatetime(entity.endDate.datetime);
  }

  if (entity.calendarRange !== null) {
    calendar.calendarRange.calendarRangeId = entity.calendarRange.id;
    calendar.calendarRange.id = `range_${entity.calendarRange.id}`;
  }

  return calendar;
};

export const createUserData = (user: User, colorIndex: number): UserData  => {
  const colorId = colorIndex % COLORS.length;

  return {
    user: user,
    calendarRanges: [],
    calendarRangesLoaded: [],
    remotes: [],
    remotesLoaded: [],
    locals: [],
    localsLoaded: [],
    mainColor: COLORS[colorId],
  }
}

// TODO move this function to a more global namespace, as it is also in use in MyCalendarRange app
export const calendarRangeToFullCalendarEvent = (entity: CalendarRange): EventInputCalendarRange => {
  return {
    id: `range_${entity.id}`,
    title: "(" + entity.user.text + ")",
    start: entity.startDate.datetime8601,
    end: entity.endDate.datetime8601,
    allDay: false,
    userId: entity.user.id,
    userLabel: entity.user.label,
    calendarRangeId: entity.id,
    locationId: entity.location.id,
    locationName: entity.location.name,
    is: 'range',
  };
}

export const remoteToFullCalendarEvent = (entity: CalendarRemote): EventInput & {id: string} => {
  return {
    id: `range_${entity.id}`,
    title: entity.title,
    start: entity.startDate.datetime8601,
    end: entity.endDate.datetime8601,
    allDay: entity.isAllDay,
    is: 'remote',
  };
}

export const localsToFullCalendarEvent = (entity: CalendarLight): EventInput & {id: string; originId: number;} => {
  return {
    id: `local_${entity.id}`,
    title: entity.persons.map(p => p.text).join(', '),
    originId: entity.id,
    start: entity.startDate.datetime8601,
    end: entity.endDate.datetime8601,
    allDay: false,
    is: 'local',
  };
}
