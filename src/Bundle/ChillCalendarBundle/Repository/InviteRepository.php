<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Repository;

use Chill\CalendarBundle\Entity\Invite;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;

class InviteRepository implements ObjectRepository
{
    private readonly EntityRepository $entityRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityRepository = $em->getRepository(Invite::class);
    }

    public function find($id): ?Invite
    {
        return $this->entityRepository->find($id);
    }

    /**
     * @return array|Invite[]
     */
    public function findAll(): array
    {
        return $this->entityRepository->findAll();
    }

    /**
     * @return array|Invite[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
    {
        return $this->entityRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): ?Invite
    {
        return $this->entityRepository->findOneBy($criteria);
    }

    public function getClassName(): string
    {
        return Invite::class;
    }
}
