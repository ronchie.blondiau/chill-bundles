<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector;

use Chill\CalendarBundle\Entity\Calendar;
use Chill\CalendarBundle\Entity\CalendarRange;
use Chill\CalendarBundle\Entity\Invite;
use Chill\MainBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class NullRemoteCalendarConnector implements RemoteCalendarConnectorInterface
{
    public function countEventsForUser(User $user, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate): int
    {
        return 0;
    }

    public function getMakeReadyResponse(string $returnPath): Response
    {
        throw new \LogicException('As this connector is always ready, this method should not be called');
    }

    public function isReady(): bool
    {
        return true;
    }

    public function listEventsForUser(User $user, \DateTimeImmutable $startDate, \DateTimeImmutable $endDate, ?int $offset = 0, ?int $limit = 50): array
    {
        return [];
    }

    public function removeCalendar(string $remoteId, array $remoteAttributes, User $user, ?CalendarRange $associatedCalendarRange = null): void
    {
    }

    public function removeCalendarRange(string $remoteId, array $remoteAttributes, User $user): void
    {
    }

    public function syncCalendar(Calendar $calendar, string $action, ?CalendarRange $previousCalendarRange, ?User $previousMainUser, ?array $oldInvites, ?array $newInvites): void
    {
    }

    public function syncCalendarRange(CalendarRange $calendarRange): void
    {
    }

    public function syncInvite(Invite $invite): void
    {
    }
}
