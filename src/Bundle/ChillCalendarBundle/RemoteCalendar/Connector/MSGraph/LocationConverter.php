<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph;

use Chill\MainBundle\Entity\Location;

class LocationConverter
{
    public function __construct(private readonly AddressConverter $addressConverter)
    {
    }

    public function locationToRemote(Location $location): array
    {
        $results = [];

        if ($location->hasAddress()) {
            $results['address'] = $this->addressConverter->addressToRemote($location->getAddress());

            if ($location->getAddress()->hasAddressReference() && $location->getAddress()->getAddressReference()->hasPoint()) {
                $results['coordinates'] = [
                    'latitude' => $location->getAddress()->getAddressReference()->getPoint()->getLat(),
                    'longitude' => $location->getAddress()->getAddressReference()->getPoint()->getLon(),
                ];
            }
        }

        if (null !== $location->getName()) {
            $results['displayName'] = $location->getName();
        }

        return $results;
    }
}
