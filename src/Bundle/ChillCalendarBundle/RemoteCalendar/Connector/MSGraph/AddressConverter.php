<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\RemoteCalendar\Connector\MSGraph;

use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Templating\Entity\AddressRender;
use Chill\MainBundle\Templating\TranslatableStringHelperInterface;

class AddressConverter
{
    public function __construct(private readonly AddressRender $addressRender, private readonly TranslatableStringHelperInterface $translatableStringHelper)
    {
    }

    public function addressToRemote(Address $address): array
    {
        return [
            'city' => $address->getPostcode()->getName(),
            'postalCode' => $address->getPostcode()->getCode(),
            'countryOrRegion' => $this->translatableStringHelper->localize($address->getPostcode()->getCountry()->getName()),
            'street' => $address->isNoAddress() ? '' :
                implode(', ', $this->addressRender->renderLines($address, false, false)),
            'state' => '',
        ];
    }
}
