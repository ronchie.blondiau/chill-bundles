<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Controller;

use Chill\CalendarBundle\Repository\CalendarRepository;
use Chill\MainBundle\CRUD\Controller\ApiController;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Serializer\Model\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CalendarAPIController extends ApiController
{
    public function __construct(private readonly CalendarRepository $calendarRepository)
    {
    }

    /**
     * @Route("/api/1.0/calendar/calendar/by-user/{id}.{_format}",
     *     name="chill_api_single_calendar_list_by-user",
     *     requirements={"_format": "json"}
     * )
     */
    public function listByUser(User $user, Request $request, string $_format): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        if (!$request->query->has('dateFrom')) {
            throw new BadRequestHttpException('You must provide a dateFrom parameter');
        }

        if (false === $dateFrom = \DateTimeImmutable::createFromFormat(
            \DateTimeImmutable::ATOM,
            $request->query->get('dateFrom')
        )) {
            throw new BadRequestHttpException('dateFrom not parsable');
        }

        if (!$request->query->has('dateTo')) {
            throw new BadRequestHttpException('You must provide a dateTo parameter');
        }

        if (false === $dateTo = \DateTimeImmutable::createFromFormat(
            \DateTimeImmutable::ATOM,
            $request->query->get('dateTo')
        )) {
            throw new BadRequestHttpException('dateTo not parsable');
        }

        $total = $this->calendarRepository->countByUser($user, $dateFrom, $dateTo);
        $paginator = $this->getPaginatorFactory()->create($total);
        $ranges = $this->calendarRepository->findByUser(
            $user,
            $dateFrom,
            $dateTo,
            $paginator->getItemsPerPage(),
            $paginator->getCurrentPageFirstItemNumber()
        );

        $collection = new Collection($ranges, $paginator);

        return $this->json($collection, Response::HTTP_OK, [], ['groups' => ['calendar:light']]);
    }

    protected function customizeQuery(string $action, Request $request, $qb): void
    {
        if ($request->query->has('main_user')) {
            $qb->where('e.mainUser = :main_user')
                ->setParameter('main_user', $request->query->get('main_user'));
        }
    }

    protected function getContextForSerialization(string $action, Request $request, string $_format, $entity): array
    {
        switch ($action) {
            case '_index':
                switch ($request->getMethod()) {
                    case Request::METHOD_GET:
                        return ['groups' => ['calendar:read']];
                }
        }

        return parent::getContextForSerialization($action, $request, $_format, $entity);
    }
}
