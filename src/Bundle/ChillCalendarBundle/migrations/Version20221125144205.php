<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221125144205 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Calendar: remove association between scope and calendar';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'UPDATE role_scopes SET scope_id=NULL WHERE role IN (\'%s\', \'%s\', \'%s\', \'%s\')',
                'CHILL_CALENDAR_CALENDAR_CREATE',
                'CHILL_CALENDAR_CALENDAR_DELETE',
                'CHILL_CALENDAR_CALENDAR_EDIT',
                'CHILL_CALENDAR_CALENDAR_SEE'
            )
        );
    }
}
