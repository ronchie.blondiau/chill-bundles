<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220525080633 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.invite ALTER status DROP NOT NULL');
        $this->addSql('DROP INDEX chill_calendar.UNIQ_712315ACC5CB285D');
        $this->addSql('DROP INDEX chill_calendar.idx_calendar_remote');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP remoteAttributes');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP remoteId');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT fk_712315aca76ed395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_712315acc5cb285d ON chill_calendar.calendar (calendarrange_id)');
        $this->addSql('CREATE INDEX idx_712315aca76ed395 ON chill_calendar.calendar (user_id)');
    }

    public function getDescription(): string
    {
        return 'Calendar: add remote infos and fix associations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT fk_712315aca76ed395');
        $this->addSql('DROP INDEX chill_calendar.idx_712315acc5cb285d');
        $this->addSql('DROP INDEX chill_calendar.idx_712315aca76ed395');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD remoteAttributes JSON DEFAULT \'[]\' NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD remoteId TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP user_id');
        $this->addSql('ALTER TABLE chill_calendar.calendar ALTER status SET DEFAULT \'valid\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_712315ACC5CB285D ON chill_calendar.calendar (calendarRange_id)');
        $this->addSql('CREATE INDEX idx_calendar_remote ON chill_calendar.calendar (remoteId)');
        $this->addSql('UPDATE chill_calendar.invite SET status=\'pending\' WHERE status IS NULL');
        $this->addSql('ALTER TABLE chill_calendar.invite ALTER status SET NOT NULL');
    }
}
