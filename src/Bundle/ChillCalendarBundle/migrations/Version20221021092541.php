<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221021092541 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP person_id');
    }

    public function getDescription(): string
    {
        return 'Associate a calendar with a person';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315AC217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_712315AC217BBB47 ON chill_calendar.calendar (person_id)');
    }
}
