<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\Calendar;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create the schema chill_calendar and several calendar entities.
 */
final class Version20210715141731 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_persons DROP CONSTRAINT FK_AEE94715A40A2C8');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_non_professionals DROP CONSTRAINT FK_FADF2C77A40A2C8');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_thirdparties DROP CONSTRAINT FK_2BAB7EFDA40A2C8');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites DROP CONSTRAINT FK_FCBEAAAA40A2C8');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT FK_712315ACC5CB285D');
        $this->addSql('ALTER TABLE chill_calendar.calendar DROP CONSTRAINT FK_712315ACE980772F');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites DROP CONSTRAINT FK_FCBEAAAEA417747');
        $this->addSql('DROP SEQUENCE chill_calendar.calendar_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_calendar.calendar_range_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_calendar.cancel_reason_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_calendar.invite_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_calendar.calendar');
        $this->addSql('DROP TABLE chill_calendar.calendar_to_persons');
        $this->addSql('DROP TABLE chill_calendar.calendar_to_non_professionals');
        $this->addSql('DROP TABLE chill_calendar.calendar_to_thirdparties');
        $this->addSql('DROP TABLE chill_calendar.calendar_to_invites');
        $this->addSql('DROP TABLE chill_calendar.calendar_range');
        $this->addSql('DROP TABLE chill_calendar.cancel_reason');
        $this->addSql('DROP TABLE chill_calendar.invite');
        $this->addSql('DROP SCHEMA chill_calendar');
    }

    public function getDescription(): string
    {
        return 'Create the schema chill_calendar and several calendar entities';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA chill_calendar');
        $this->addSql('CREATE SEQUENCE chill_calendar.calendar_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_calendar.calendar_range_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_calendar.cancel_reason_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_calendar.invite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_calendar.calendar (id INT NOT NULL, user_id INT DEFAULT NULL, activity_id INT DEFAULT NULL, startDate DATE NOT NULL, endDate DATE NOT NULL, status VARCHAR(255) NOT NULL, sendSMS BOOLEAN DEFAULT NULL, comment_comment TEXT DEFAULT NULL, comment_userId INT DEFAULT NULL, comment_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, accompanyingPeriod_id INT DEFAULT NULL, mainUser_id INT DEFAULT NULL, cancelReason_id INT DEFAULT NULL, calendarRange_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_712315ACA76ED395 ON chill_calendar.calendar (user_id)');
        $this->addSql('CREATE INDEX IDX_712315ACD7FA8EF0 ON chill_calendar.calendar (accompanyingPeriod_id)');
        $this->addSql('CREATE INDEX IDX_712315ACEFCB59C ON chill_calendar.calendar (mainUser_id)');
        $this->addSql('CREATE INDEX IDX_712315ACE980772F ON chill_calendar.calendar (cancelReason_id)');
        $this->addSql('CREATE INDEX IDX_712315ACC5CB285D ON chill_calendar.calendar (calendarRange_id)');
        $this->addSql('CREATE INDEX IDX_712315AC81C06096 ON chill_calendar.calendar (activity_id)');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_persons (calendar_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(calendar_id, person_id))');
        $this->addSql('CREATE INDEX IDX_AEE94715A40A2C8 ON chill_calendar.calendar_to_persons (calendar_id)');
        $this->addSql('CREATE INDEX IDX_AEE94715217BBB47 ON chill_calendar.calendar_to_persons (person_id)');
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_non_professionals (calendar_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(calendar_id, person_id))');
        $this->addSql('CREATE INDEX IDX_FADF2C77A40A2C8 ON chill_calendar.calendar_to_non_professionals (calendar_id)');
        $this->addSql('CREATE INDEX IDX_FADF2C77217BBB47 ON chill_calendar.calendar_to_non_professionals (person_id)');
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_thirdparties (calendar_id INT NOT NULL, thirdparty_id INT NOT NULL, PRIMARY KEY(calendar_id, thirdparty_id))');
        $this->addSql('CREATE INDEX IDX_2BAB7EFDA40A2C8 ON chill_calendar.calendar_to_thirdparties (calendar_id)');
        $this->addSql('CREATE INDEX IDX_2BAB7EFDC7D3A8E6 ON chill_calendar.calendar_to_thirdparties (thirdparty_id)');
        $this->addSql('CREATE TABLE chill_calendar.calendar_to_invites (calendar_id INT NOT NULL, invite_id INT NOT NULL, PRIMARY KEY(calendar_id, invite_id))');
        $this->addSql('CREATE INDEX IDX_FCBEAAAA40A2C8 ON chill_calendar.calendar_to_invites (calendar_id)');
        $this->addSql('CREATE INDEX IDX_FCBEAAAEA417747 ON chill_calendar.calendar_to_invites (invite_id)');
        $this->addSql('CREATE TABLE chill_calendar.calendar_range (id INT NOT NULL, user_id INT DEFAULT NULL, startDate DATE NOT NULL, endDate DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_38D57D05A76ED395 ON chill_calendar.calendar_range (user_id)');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar_range.startDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_calendar.calendar_range.endDate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('CREATE TABLE chill_calendar.cancel_reason (id INT NOT NULL, active BOOLEAN NOT NULL, canceledBy JSON NOT NULL, name JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_calendar.invite (id INT NOT NULL, user_id INT DEFAULT NULL, status JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F517FFA7A76ED395 ON chill_calendar.invite (user_id)');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315ACA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315ACD7FA8EF0 FOREIGN KEY (accompanyingPeriod_id) REFERENCES chill_person_accompanying_period (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315ACEFCB59C FOREIGN KEY (mainUser_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315ACE980772F FOREIGN KEY (cancelReason_id) REFERENCES chill_calendar.cancel_reason (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315ACC5CB285D FOREIGN KEY (calendarRange_id) REFERENCES chill_calendar.calendar_range (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar ADD CONSTRAINT FK_712315AC81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_persons ADD CONSTRAINT FK_AEE94715A40A2C8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_persons ADD CONSTRAINT FK_AEE94715217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_non_professionals ADD CONSTRAINT FK_FADF2C77A40A2C8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_non_professionals ADD CONSTRAINT FK_FADF2C77217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_thirdparties ADD CONSTRAINT FK_2BAB7EFDA40A2C8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_thirdparties ADD CONSTRAINT FK_2BAB7EFDC7D3A8E6 FOREIGN KEY (thirdparty_id) REFERENCES chill_3party.third_party (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites ADD CONSTRAINT FK_FCBEAAAA40A2C8 FOREIGN KEY (calendar_id) REFERENCES chill_calendar.calendar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_to_invites ADD CONSTRAINT FK_FCBEAAAEA417747 FOREIGN KEY (invite_id) REFERENCES chill_calendar.invite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.calendar_range ADD CONSTRAINT FK_38D57D05A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_calendar.invite ADD CONSTRAINT FK_F517FFA7A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
