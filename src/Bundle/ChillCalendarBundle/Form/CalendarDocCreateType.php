<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Form;

use Chill\CalendarBundle\Entity\CalendarDoc\CalendarDocCreateDTO;
use Chill\DocStoreBundle\Form\StoredObjectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarDocCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'chill_calendar.Document title',
                'required' => true,
            ])
            ->add('doc', StoredObjectType::class, [
                'label' => 'chill_calendar.Document object',
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CalendarDocCreateDTO::class,
        ]);
    }
}
