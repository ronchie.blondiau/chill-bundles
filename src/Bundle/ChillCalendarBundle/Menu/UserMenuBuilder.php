<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserMenuBuilder implements LocalMenuBuilderInterface
{
    public function __construct(private readonly Security $security, public TranslatorInterface $translator)
    {
    }

    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->security->isGranted('ROLE_USER')) {
            $menu->addChild('My calendar list', [
                'route' => 'chill_calendar_calendar_list_my',
            ])
                ->setExtras([
                    'order' => 9,
                    'icon' => 'tasks',
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return ['user'];
    }
}
