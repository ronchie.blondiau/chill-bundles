<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\CalendarBundle\DataFixtures\ORM;

use Chill\CalendarBundle\Entity\Invite;
use Chill\MainBundle\DataFixtures\ORM\LoadUsers;
use Chill\MainBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class LoadInvite extends Fixture implements FixtureGroupInterface
{
    public static $references = [];

    public static function getGroups(): array
    {
        return ['calendar'];
    }

    public function getOrder(): int
    {
        return 40002;
    }

    public function load(ObjectManager $manager): void
    {
        $arr = [
            [
                'name' => ['fr' => 'Rendez-vous décliné'],
                'status' => Invite::DECLINED,
            ],
            [
                'name' => ['fr' => 'Rendez-vous accepté'],
                'status' => Invite::ACCEPTED,
            ],
        ];

        foreach ($arr as $a) {
            echo 'Creating calendar invite : '.$a['name']['fr']."\n";
            $invite = (new Invite())
                ->setStatus($a['status'])
                ->setUser($this->getRandomUser());
            $manager->persist($invite);
            $reference = 'Invite_'.$a['name']['fr'];
            $this->addReference($reference, $invite);
            static::$references[] = $reference;
        }

        $manager->flush();
    }

    private function getRandomUser(): User
    {
        $userRef = array_rand(LoadUsers::$refs);

        return $this->getReference($userRef);
    }
}
