<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create trigger for canonicalisation on 3party + indexes.
 */
final class Version20211007165001 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TRIGGER canonicalize_fullname_on_update ON chill_3party.third_party');
        $this->addSql('DROP TRIGGER canonicalize_fullname_on_insert ON chill_3party.third_party');
        $this->addSql('DROP FUNCTION chill_3party.canonicalize()');
        $this->addSql('
            DROP INDEX chill_3party.chill_custom_canonicalized_trgm_idx_gist
        ');
    }

    public function getDescription(): string
    {
        return 'Create trigger for canonicalisation on 3party + indexes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            UPDATE chill_3party.third_party
                SET canonicalized =
                    UNACCENT(
                            LOWER(
                                name ||
                                CASE WHEN COALESCE(name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(name_company, '') ||
                                CASE WHEN COALESCE(acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(acronym, '')
                                )
                        )
        ");
        $this->addSql("
            CREATE OR REPLACE FUNCTION chill_3party.canonicalize() RETURNS TRIGGER
                LANGUAGE plpgsql
            AS
                $$
                BEGIN
                    NEW.canonicalized =
                        UNACCENT(
                            LOWER(
                                NEW.name ||
                                CASE WHEN COALESCE(NEW.name_company, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.name_company, '') ||
                                CASE WHEN COALESCE(NEW.acronym, '') <> '' THEN ' ' ELSE '' END ||
                                COALESCE(NEW.acronym, '')
                                )
                            )
                    ;

                    return NEW;
                END
                $$
        ");
        $this->addSql('
            CREATE TRIGGER canonicalize_fullname_on_insert
                BEFORE INSERT
                ON chill_3party.third_party
                FOR EACH ROW
                    EXECUTE procedure chill_3party.canonicalize();
        ');
        $this->addSql('
            CREATE TRIGGER canonicalize_fullname_on_update
                BEFORE UPDATE
                ON chill_3party.third_party
                FOR EACH ROW
                    EXECUTE procedure chill_3party.canonicalize();
        ');
        $this->addSql('
            CREATE INDEX chill_custom_canonicalized_trgm_idx_gist
                ON chill_3party.third_party USING GIST (canonicalized gist_trgm_ops) WHERE active IS TRUE
        ');
    }
}
