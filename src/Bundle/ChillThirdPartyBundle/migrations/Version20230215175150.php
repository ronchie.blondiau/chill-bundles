<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230215175150 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party DROP profession');
    }

    public function getDescription(): string
    {
        return 'Change profession to a string field and transfer values';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party ADD profession TEXT DEFAULT \'\' NOT NULL');
        $this->addSql('UPDATE chill_3party.third_party SET profession = party_profession.name->>\'fr\' FROM chill_3party.party_profession WHERE party_profession.id = third_party.profession_id');
    }
}
