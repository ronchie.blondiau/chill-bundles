<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add anonymous flag for contacts.
 */
final class Version20211007194942 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party DROP contact_data_anonymous');
    }

    public function getDescription(): string
    {
        return 'Add anonymous flag for contacts';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE chill_3party.third_party ADD contact_data_anonymous BOOLEAN DEFAULT \'false\' NOT NULL;');
    }
}
