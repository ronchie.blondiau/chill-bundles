<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\Migrations\ThirdParty;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * migrate data from 3party.civility to chill_main_civility table.
 */
final class Version20211007150459 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Reversible migration not implemented');

        // for reference:
        $this->addSql('CREATE SEQUENCE chill_3party.party_civility_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_3party.party_civility (id INT NOT NULL, name JSON NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467B23D6A298');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT FK_D952467BFDEF8996');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD civility INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD profession INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP civility_id');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP profession_id');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP kind');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP canonicalized');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT fk_d952467b384d4799 FOREIGN KEY (civility) REFERENCES chill_3party.party_civility (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT fk_d952467bba930d69 FOREIGN KEY (profession) REFERENCES chill_3party.party_profession (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_d952467b384d4799 ON chill_3party.third_party (civility)');
        $this->addSql('CREATE UNIQUE INDEX uniq_d952467bba930d69 ON chill_3party.third_party (profession)');
    }

    public function getDescription(): string
    {
        return 'migrate data from 3party.civility to chill_main_civility table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX chill_3party.uniq_d952467b384d4799');
        $this->addSql('DROP INDEX chill_3party.uniq_d952467bba930d69');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD civility_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD profession_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD kind VARCHAR(20) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD canonicalized TEXT NOT NULL DEFAULT \'\'');
        $this->addSql('CREATE TEMPORARY TABLE civility_migration AS SELECT * FROM chill_3party.party_civility');
        $this->addSql('ALTER TABLE civility_migration ADD COLUMN new_id INT DEFAULT NULL');
        $this->addSql('UPDATE civility_migration SET new_id = nextval(\'chill_main_civility_id_seq\')');
        $this->addSql('
            INSERT INTO chill_main_civility (id, name, abbreviation, active)
                SELECT new_id, name, \'{}\'::json, active from civility_migration
        ');
        $this->addSql('UPDATE chill_3party.third_party SET civility_id = new_id
            FROM civility_migration WHERE civility_migration.id = third_party.civility');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT fk_d952467b384d4799');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP CONSTRAINT fk_d952467bba930d69');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP civility');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP profession');
        $this->addSql('DROP SEQUENCE chill_3party.party_civility_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_3party.party_civility');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467B23D6A298 FOREIGN KEY (civility_id) REFERENCES chill_main_civility (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD CONSTRAINT FK_D952467BFDEF8996 FOREIGN KEY (profession_id) REFERENCES chill_3party.party_profession (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D952467B23D6A298 ON chill_3party.third_party (civility_id)');
        $this->addSql('CREATE INDEX IDX_D952467BFDEF8996 ON chill_3party.third_party (profession_id)');
    }
}
