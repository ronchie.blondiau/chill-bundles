<?php

/**
 * Chill is a software for social workers.
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Form\DataTransformer;

use Chill\MainBundle\Form\DataTransformer\IdToEntityDataTransformer;
use Chill\ThirdPartyBundle\Repository\ThirdPartyRepository;

class ThirdPartyToIdDataTransformer extends IdToEntityDataTransformer
{
    public function __construct(ThirdPartyRepository $repository)
    {
        parent::__construct($repository, false);
    }
}
