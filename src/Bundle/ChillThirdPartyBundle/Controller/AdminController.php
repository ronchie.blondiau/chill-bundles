<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * ThirdParty admin.
     *
     * @Route("/{_locale}/admin/thirdparty", name="chill_thirdparty_admin_index")
     */
    public function indexAdminAction()
    {
        return $this->render('@ChillThirdParty/Admin/index.html.twig');
    }
}
