<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\EventListener;

use Chill\ThirdPartyBundle\Entity\ThirdParty;

class ThirdPartyEventListener
{
    public function prePersistThirdParty(ThirdParty $thirdparty): void
    {
        if ('company' !== $thirdparty->getKind()) {
            $firstnameCaps = mb_convert_case(mb_strtolower($thirdparty->getFirstname()), \MB_CASE_TITLE, 'UTF-8');
            $firstnameCaps = ucwords(strtolower($firstnameCaps), " \t\r\n\f\v'-");
            $thirdparty->setFirstName($firstnameCaps);

            $lastnameCaps = mb_strtoupper($thirdparty->getName(), 'UTF-8');
            $thirdparty->setName($lastnameCaps);
        }
    }
}
