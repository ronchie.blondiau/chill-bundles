<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\ThirdPartyType;

/**
 * Provide third party type.
 */
interface ThirdPartyTypeProviderInterface
{
    /**
     * Return an unique key for this type.
     */
    public static function getKey(): string;
}
