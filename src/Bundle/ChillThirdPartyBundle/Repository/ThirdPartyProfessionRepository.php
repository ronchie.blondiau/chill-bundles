<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Repository;

use Chill\ThirdPartyBundle\Entity\ThirdPartyProfession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ThirdPartyProfession|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThirdPartyProfession|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThirdPartyProfession[]    findAll()
 * @method ThirdPartyProfession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThirdPartyProfessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThirdPartyProfession::class);
    }
}
