<?php

declare(strict_types=1);

/*
 * Chill is a software for social workers
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Chill\ThirdPartyBundle\Repository;

use Chill\ThirdPartyBundle\Entity\ThirdParty;

interface ThirdPartyACLAwareRepositoryInterface
{
    public function countThirdParties(string $role, ?string $filterString): int;

    /**
     * @return array|ThirdParty[]
     */
    public function listThirdParties(
        string $role,
        ?string $filterString,
        ?array $orderBy = [],
        ?int $limit = 0,
        ?int $offset = 50
    ): array;
}
