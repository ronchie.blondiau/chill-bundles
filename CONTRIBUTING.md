# Contributing

Chill is an open source, community-driven project.

If you'd like to contribute, please read the following.

## What can you do ?

Chill is an open-source project driven by a community of developers, users and social workers. If you don't feel ready to contribute code or patches, reviewing issues and pull requests (PRs) can be a great start to get involved and give back. 

If you don't have your own instance or don't want to use it, you can try to reproduce bugs using the instance https://demo.chill.social

## Core team

The core team is the group of developers that determine the direction and evolution of the Chill project. Their votes rule if the features and patches proposed by the community are approved or rejected.

All the Chill Core members are long-time contributors with solid technical expertise and they have demonstrated a strong commitment to drive the project forward.

The core team:

- elects his own members;
- merge pull requests;

### members

Project leader: [julienfastre](https://gitlab.com/julienfastre)

Core members:

- [tchama](https://gitlab.com/tchama)
- [LenaertsJ](https://gitlab.com/LenaertsJ)
- [nobohan](https://gitlab.com/nobohan)

### Becoming a project member

About once a year, the core team discusses the opportunity to invite new members. To become a core team member, you must:

- take part on the development for at least 6 month: propose multiple merge requests and participate to the peer review process;
- through this participation, demonstrate your technical skills and your knowledge of the software and any of their dependencies;

### Core Membership Revocation

A Chill Core membership can be revoked for any of the following reasons:

- Refusal to follow the rules and policies stated in this document;
- Lack of activity for the past six months;
- Willful negligence or intent to harm the Chill project;

The decision is taken by the majority of project members.

## Code development rules

### Merge requests

Every merge request must contains:

- one more entries suitable for generating a changelog. This is done using the [changie utility](https://changie.dev);
- a comprehensible description of the changes;
- if applicable, automated tests should be adapted or created;
- the code style must pass the project's rules, and non phpstan errors must be raised nor rector refactoring suggestion.

The pipelines must pass.

In case of emergency, some rules may be temporarily ignored.

### Merge Request Voting Policy

- -1 votes must always be justified by technical and objective reasons;
- +1 (technically: approbation on the merge request) votes do not require justification, unless there is at least one -1 vote;
- Core members can change their votes as many times as they desire during the course of a merge request discussion;
- Core members are not allowed to vote on their own merge requests.

### Merge Request Merging Process

All code must be committed to the repository through merge requests, except for minor changes which can be committed directly to the repository.

### Release Policy

The Core members are also the release manager for every Chill version.

