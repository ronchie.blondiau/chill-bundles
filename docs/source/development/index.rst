.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Development
###########

As Chill relies on the `symfony <http://symfony.com>`_ framework, reading the framework's documentation should answer most of your questions. We are explaining here some tips to work with Chill, and help with things we've encountered.

.. toctree::
    :maxdepth: 2

    Instructions to create a new bundle <create-a-new-bundle.rst>
    CRUD (Create - Update - Delete) for one entity <crud.rst>
    Helpers for building a REST API <api.rst>
    Routing <routing.rst>
    Menus <menus.rst>
    Forms <forms.rst>
    Access control model <access_control_model.rst>
    Messages to users <messages-to-users.rst>
    Pagination <pagination.rst>
    Localisation <localisation.rst>
    Logging <logging.rst>
    Database migrations <migrations.rst>
    Searching <searching.rst>
    Timelines <timelines.rst>
    Exports <exports.rst>
    Embeddable comments <embeddable-comments.rst>
    Run tests <run-tests.rst>
    Useful snippets <useful-snippets.rst>
    manual/index.rst
    Assets <assets.rst>
    Cron Jobs <cronjob.rst>
    Info about entities <entity-info.rst>
    Info about database (in French) <database-principles.rst>
    Developer FAQ <FAQ.rst>

Layout and UI
**************

.. toctree::
   :maxdepth: 2

   Render entities automatically <render-entity.rst>
   Layout / Template usage <user-interface/layout-template-usage.rst>
   Classes and mixins <user-interface/css-classes.rst>
   Widgets <user-interface/widgets.rst>
   Javascript function <user-interface/js-functions.rst>


Help, I am lost !
*****************

Write an email at info@champs-libres.coop, and we will help you !
