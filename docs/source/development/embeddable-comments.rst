
.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


Embeddable comments
###################

Those embeddable comments is a comment with some metadata:

* the one who updated the comment (the comment itself, and not the whole entity);
* the date and time for the last update (again, the comment itself, and not the whole entity).

We make usage of `embeddables <https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/tutorials/embeddables.html>`_.

Embed the comment
=================

The comment may be embedded into the entity:

.. code-block:: php

   namespace Chill\ActivityBundle\Entity;

   use Chill\MainBundle\Entity\Embeddable\CommentEmbeddable;
   use Doctrine\ORM\Mapping as ORM;

   /**
    * Class Activity
    *
    * @ORM\Entity()
    */
   class Activity
   {
       /**
        * @var integer
        *
        * @ORM\Id
        * @ORM\Column(name="id", type="integer")
        * @ORM\GeneratedValue(strategy="AUTO")
        */
       private $id;

       /**
        * @ORM\Embedded(class="Chill\MainBundle\Entity\Embeddable\CommentEmbeddable", columnPrefix="comment_")
        */
       private $comment;


       /**
        * @return \Chill\MainBundle\Entity\Embeddalbe\CommentEmbeddable
        */
       public function getComment()
       {
           return $this->comment;
       }

       /**
        * @param \Chill\MainBundle\Entity\Embeddalbe\CommentEmbeddable $comment
        */
       public function setComment($comment)
       {
           $this->comment = $comment;
       }
   }

Note on relation to :class:`User`
=================================

The embeddable proposed by Doctrine does not support relationship to other entities. The entity Comment is able to render a user's id, but not an User object.


.. code-block:: php

   $activity->getComment()->getUserId(); // return user id of the last author

   $activity->getComment()->getUser(); // does not work !


Usage into form
===============

Use the :class:`Chill\MainBundle\Form\Type\CommentType` to load the form widget:

.. code-block:: php

   namespace Chill\ActivityBundle\Form;

   use Chill\MainBundle\Form\Type\CommentType;
   use Symfony\Component\Form\AbstractType;
   use Symfony\Component\Form\FormBuilderInterface;

   class ActivityType extends AbstractType
   {
       /**
        * @param FormBuilderInterface $builder
        * @param array $options
        */
       public function buildForm(FormBuilderInterface $builder, array $options)
       {
           $builder
               ->add('comment', CommentType::class, [
                   'required' => false,
               ])
           ;
       }
   }

Render the comment
==================

.. code-block:: twig

   {{ activity.comment|chill_entity_render_box }}


