.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


CSS classes and mixins
######################

The stylesheet are based on the framework `ScratchCSS <https://github.com/Champs-Libres/ScratchCSS>`_. 

We added some useful classes and mixins for the Chill usage.

CSS Classes
************


Statement "empty data"
======================

CSS Selector
   :code:`.chill-no-data-statement`
In which case will you use this selector ?
   When a list is empty, and a message fill the list to inform that the data is empty
Example usage
   .. code-block:: html+jinja
      
      <span class="chill-no-data-statement">{{ 'No reason associated'|trans }}</span>


Quotation of user text
=======================

CSS Selector
   :code:`blockquote.chill-user-quote`
In which case will you use this selector ?
   When you quote text that were filled by the user in a form.
Example usage
   .. code-block:: html+jinja

      <blockquote class="chill-user-quote">{{ entity.remark|nl2br }}</blockquote>

Boxes
=====

CSS Selector
   :code:`chill__box`
In which case will you use this selector ?
   When displaying some data in a nice box
Example usage
   .. code-block:: html+twig

      <span class="chill__box green">A nice box with green background</span>
      <span class="chill__box red">A nice box with red background</span>



Mixins
******


Entity decorator
=================

Mixin
   :code:`@mixin entity($background-color, $color: white)`
In which case including this mixin ?
   When you create a `sticker`, a sort of label to represent a text in a way that the user can associate immediatly with a certain type of class / entity.
Example usage
   .. code-block:: sass

      span.entity.entity-activity.activity-reason {
          @include entity($chill-pink, white);
      }
