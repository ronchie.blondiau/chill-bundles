.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Run tests
*********

In reason of the Chill architecture, test should be runnable from the bundle's directory and works correctly: this will allow continuous integration tools to run tests automatically. 

From chill app
==============

This is the most convenient method for developer: run test for chill bundle from the main app.

.. code-block:: bash

   # run into a container
   docker-compose exec --user $(id -u) php bash
   # execute all tests suites
   bin/phpunit
   # .. or execute a single test
   bin/phpunit vendor/chill-project/chill-bundles/src/Bundle/ChillMainBundle/Tests/path/to/FileTest.php

You can also run tests in a single command:

.. code-block:: bash

   docker-compose exec --user $(id -u) php bin/phpunit


Tests from a bundle (chill-bundles)
-----------------------------------

Those tests needs the whole symfony app to execute Application Tests (which test html page).

For ease, the app is cloned using a :code:`git submodule`, which clone the main app into :code:`tests/app`, and tests are bootstrapped to this app. The dependencies are also installed into `tests/app/vendor` to ensure compliance with relative path from this symfony application.

You may boostrap the tests fro the chill bundle this way:

.. code-block:: bash

   # ensure to be located into the environement (provided by docker suits well)
   docker-compose exec --user $(id -u) php bash
   # go to chill subdirectory
   cd vendor/chill-project/chill-bundles
   # install submodule
   git submodule init
   git submodule update
   # install composer and dependencies
   curl -sS https://getcomposer.org/installer | php
   # run tests
   bin/phpunit

.. note::

   If you are on a fresh install, you will need to migrate database schema. 

   The path to console tool must be adapted to the app. To load migration and add fixtures, one can execute the following commands:

   .. code-block:: bash
      
      tests/app/bin/console doctrine:migrations:migrate
      tests/app/bin/console doctrine:fixtures:load

