
.. database-principles:

Principes de la base de données
###############################

Cette page donne une compréhension globale de la base de donnée de Chill, et explique quelques détails d'implémentations qui permettent d'accélérer les traitements à partir de la base de donnée, ou de l'exploiter plus aisément.

Cette page est rédigée en français.

.. note::

    La stabilité du schéma de la base de donnée n'est pas garantie.

    Toutefois, ce dernier évolue relativement peu. Il est rare que des tables ou des colonnes soient supprimées ou renommées. Mais il n'est pas garanti que cela puisse arriver.

Généralités
===========

Une liste commentée de toutes les tables :download:`est disponible au format CSV <./database/table_list.csv`.

Schéma et conventions de nommage
--------------------------------

Au début de l'histoire de Chill, les schémas postgresql n'étaient pas exploités. Les données étaient stockées dans le schéma :code:`public`.

Par la suite, des nouveaux bundles sont apparus, et les tables ont été classées dans des schémas dédiés.

A l'heure actuelle:

- pour les anciens bundle, ceux qui ont déjà des tables dans le schéma public, les nouvelles tables sont ajoutées à ce schéma. Elles sont préfixées par :code:`chill_<nom du bundle>_`;
- pour les bundles plus récents, les tables sont créées dans le schéma dédié

Données avec de l'historicité
-----------------------------

Certaines données sont historisées:

- les référents d'un parcours;
- les statuts d'un parcours;
- la liaison entre les centres et les usagers;
- etc.

Dans ces cas-là, Chill crée généralement deux colonnes, qui sont habituellement nommées :code:`startDate` et :code:`endDate`. Lorsque la colonne :code:`endDate` est à :code:`NULL`, cela signifie que la période n'est pas "fermée". La colonne :code:`startDate` n'est pas nullable.

Dans certains cas, la donnée actuelle (référent d'un parcours, par exemple) est également répétée au niveau de la table en elle-même. Par exemple, la table des parcours :code:`chill_person_accompanying_period` comporte une colonne :code:`step` (le statut du parcours) et :code:`user_id` (id du référent) en plus de l'historique. Bien que redondant, cela simplifie les traitements.

Relations particulières
=======================

Usagers, ménages, adresses
--------------------------

Les usagers ont une adresse au travers des ménages: dans l'interface, l'adresse est inscrite dans le dossier du ménage, et elle est "donnée" aux usagers membres du ménage, **et** qui partagent l'adresse de ce ménage. En effet, il est possible que des usagers "appartiennent" à un ménage sans y être domicilié: c'est le cas, par exemple, des enfants en garde alternée.

L'historique de l'appartenance des usagers au ménage est conservée, de même que l'historique des adresses pour un même ménage.

Les tables en jeu sont les suivantes:

- la table :code:`chill_person_person` liste les usagers;
- la table :code:`chill_person_household_members` liste les appartenances au ménage: il s'agit de la jointure entre les usagers et les ménages:
  - les colonnes :code:`startDate` et :code:`endDate` indiquent la date de début et la date de fin de l'appartenance;
  - la colonne :code:`shareHousehold` indique si l'utilisateur partage l'adresse du ménage (si oui, sa valeur est :code:`TRUE`)
- la table :code:`chill_person_household` liste les ménages
- la table :code:`chill_person_household_to_addresses` associe les ménages aux adresses;
- la table :code:`chill_main_address` contient les adresses, en indiquant la date de début de validité (:code:`validFrom`) et la fin de validité (:code:`validTo`).

Pour simplifier la résolution des adresses et des usagers, deux vues ont été mises en œuvre:

- la vue :code:`view_chill_person_household_address` reprend, pour chaque usager, l'historique des appartenances au ménage découpée par l'historique des adresses d'un ménage.
  Autrement dit, une ligne est créée à chaque fois qu'un usager change de ménage, ou qu'un ménage change d'adresse. Il est donc possible de retrouver l'historique complet des adresses pour un usager donné via cette table.
- la vue :code:`view_chill_person_current_address` reprend l'adresse actuelle des usagers.

Adresses et unités géographiques
--------------------------------

Chill propose des statistiques sur la localisation des adresses par rapport à des zones géographiques (:code:`chill_main_geographical_unit`).

Comme la résolution géographique des adresses est coûteuse en CPU et en temps de traitement, une vue matérialisée a été créée: :code:`view_chill_main_address_geographical_unit`. Elle est rafraichie quotidiennement dans la base de donnée de production.

Liste des tables et commentaires
================================

Une liste commentée de toutes les tables :download:`est disponible au format CSV <./database/table_list.csv`.
