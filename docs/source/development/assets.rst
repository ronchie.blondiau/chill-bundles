
.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _forms:

Assets
#######

The Chill assets (js, css, images, …) can be managed by `Webpack Encore`_, which is a thin wrapper for `Webpack`_ in Symfony applications.

Installation
************

Webpack Encore needs to be run in a Node.js environment, using the Yarn package manager. This Node.js environment can be set up using a node docker image. The bash script `docker-node.sh` set up a Node.js environment with an adequate configuration. Launch this container by typing:


.. code-block:: bash

    $ bash docker-node.sh

In this NodeJS environment, install all the assets required by Chill with:

.. code-block:: bash

    node@b91cab4f7cfc:/app$ yarn install

This command will install all the packages that are listed in `package.json`.

Any further required dependencies can be installed using the Yarn package. For instance, jQuery is installed by:

.. code-block:: bash

   node@b91cab4f7cfc:/app$ yarn add jquery --dev


Usage
*****

Organize your assets
--------------------

Chill assets usually lives under the `/Resources/public` folder of each Chill bundle. The Webpack configuration set up in `webpack.config.js` automatically loads the required assets from the Chill bundles that are used.

For adding your own assets to Webpack, you must add an entry in the `webpack.config.js` file. For instance, the following entry will output a file `main.js` collecting the js code (and possibly css, image, etc.) from `./assets/main.js`.

.. code-block:: js

   	.addEntry('main', './assets/main.js')

To gather the css files, simply connect them to your js file, using `require`. The css file is seen as a dependency of your js file. :

.. code-block:: js

    // assets/js/main.js
    require('../css/app.css');

For finer configuration of webpack encore, we refer to the above-linked documentation.


Compile the assets
------------------

To compile the assets, run this line in the NodeJS container:

.. code-block:: bash

   	node@b91cab4f7cfc:/app$ yarn run encore dev

While developing, you can tell Webpack Encore to continuously watch the files you are modifying:

.. code-block:: bash

    node@b91cab4f7cfc:/app$ yarn run encore dev --watch


Use the assets in the templates
--------------------------------

Any entry defined in the webpack.config.js file can be linked to your application using the symfony `asset` helper:

.. code-block:: html

    <head>
        ...
        <link rel="stylesheet" href="{{ asset('build/app.css') }}">
    </head>
    <body>
        ...
        <script src="{{ asset('build/app.js') }}"></script>
    </body>






.. _Webpack Encore: https://www.npmjs.com/package/@symfony/webpack-encore
.. _Webpack: https://webpack.js.org/
