.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _group-bundle:

Group bundle
############

Allow to group people in a group. This group may be a family, an activity group, ...

.. contents:: Table of content
   :local:

Entities
********

.. figure:: /_static/bundles/group/group_classes_uml.png


.. _group-bundle-macros:

Macros
******

Group sticker
==============

Macro file
   `ChillGroupBundle:Group:macro.html.twig`
Macro name
   :code:`_render`
Macro envelope
   :code:`group`, instance of :class:`Chill\GroupBundle\Entity\CGroup`

When to use this macro ?
   When you want to represent group.
Example usage :
   .. code-block:: html+jinja

      {% import 'ChillGroupBundle:Group:macro.html.twig' as m %}

      {{ m._render(g) }}


