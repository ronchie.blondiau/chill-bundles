
.. _addresses:

Addresses
*********

Chill can store a list of geolocated address references, which are used to suggest address and ensure that the data is correctly stored.

Those addresses may be load from a dedicated source.

In France
=========

The address are loaded from the `BANO <https://bano.openstreetmap.fr/>`_. The postal codes are loaded from `the official list of
postal codes <https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/>`_

.. code-block:: bash

   # first, load postal codes
   bin/console chill:main:postal-code:load:FR
   # then, load all addresses, by departement (multiple departement can be loaded by repeating the departement code
   bin/console chill:main:address-ref-from-bano 57 54 51

In Belgium
==========

Addresses are prepared from the `BeST Address data <https://www.geo.be/catalog/details/ca0fd5c0-8146-11e9-9012-482ae30f98d9>`_.

Postal code are loaded from this database. There is no need to load postal codes from another source (actually, this is strongly discouraged).

The data are prepared for Chill (`See this repository <https://gitea.champs-libres.be/Chill-project/belgian-bestaddresses-transform/releases>`_).
One can select postal code by his first number (:code:`1xxx` for postal codes from 1000 to 1999), or a limited list for development purpose.

.. code-block:: bash

   # load postal code from 1000 to 3999:
   bin/console chill:main:address-ref-from-best-addresse 1xxx 2xxx 3xxx

   # load only an extract (for dev purposes)
   bin/console chill:main:address-ref-from-best-addresse extract

   # load full addresses (discouraged)
   bin/console chill:main:address-ref-from-best-addresse full

.. note::

   There is a possibility to load the full list of addresses is discouraged: the loading is optimized with smaller extracts.

   Once you load the full list, it is not possible to load smaller extract: each extract loaded **after** will not
   delete the addresses loaded with the full extract (and some addresses will be present twice).
