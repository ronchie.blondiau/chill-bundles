.. chill-doc documentation master file, created by
   sphinx-quickstart on Sun Sep 28 22:04:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (C)  2014-2019 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Installation & Usage
####################

.. toctree::
   :maxdepth: 2

   prod.rst
   load-addresses.rst
   prod-calendar-sms-sending.rst
   msgraph-configure.rst


Requirements
************

- This project use `docker  <https://docker.com>`_ to be run. As a developer, use `docker-compose <https://docs.docker.com/compose/overview/>`_ to bootstrap a dev environment in a glance. You do not need any other dependencies ;
- Make is used to automate scripts.

Installation
************

If you plan to run chill in production:

1. install it locally first, and check if everything is ok on your local machine;
2. once ready, build the image from your local machine, and deploy them.

If you want to develop some bundles, the first step is sufficient (until you deploy on production).


1. Get the code
===============

Clone or download the chill-skeleton project and `cd` into the main directory.


.. code-block:: bash

   git clone https://gitea.champs-libres.be/Chill-project/chill-skeleton-basic.git
   cd chill-skeleton-basic


As a developer, the code will stay on your computer and will be executed in docker container. To avoid permission problem, the code should be run with the same uid/gid from your current user. This is why we get your current user id with the command ``id -u`` in each following scripts.

2. Prepare composer to download the sources
===========================================

As you are running in dev, you must configure an auth token for getting the source code.

.. warning

   If you skip this part, the code will be downloaded from dist instead of source (with git repository). You will probably replace the source manually, but the next time you will run ```composer update```, your repository will be replaced and you might loose something.

1. Create a personal access token from https://gitlab.com/-/profile/personal_access_tokens, with the `read_api` scope.
2. add a file called ```.composer/auth.json``` with this content:

   .. code-block:: json

      {
        "gitlab-token": {
            "gitlab.com": "glXXX-XXXXXXXXXXXXXXXXXXXX"
        }
      }

2. Prepare your variables and environment
=========================================

Copy ```docker-compose.override.dev.yml``` into ```docker-compose.override.yml```

.. code-block:: bash

   cp docker-compose.override.dev.template.yml docker-compose.override.yml

2. Prepare your variables and docker-compose
============================================

Have a look at the variable in ``.env`` and check if you need to adapt them. If they do not adapt with your need, or if some are missing:

1. copy the file as ``.env.local``: ``cp .env .env.local``
2. you may replace some variables inside ``.env``

Prepare also you docker-compose installation, and adapt it to your needs:

1. If you plan to deploy on dev, copy the file ``docker-compose.override.dev.template.yml`` to ``docker-compose.override.yml``.
2. adapt to your needs.


**Note**: If you intend to use the bundle ``Chill-Doc-Store``, you will need to configure and install an openstack object storage container with temporary url middleware. You will have to configure `secret keys <https://docs.openstack.org/swift/latest/api/temporary_url_middleware.html#secret-keys>`_.

3. Run the bootstrap script
===========================

This script can be run using `make`

.. code-block:: bash

   make init

This script will :

1. force docker-compose to, eventually, pull the base images and build the image used by this project ;
2. run an install script to download `composer <https://getcomposer.org>`_ ;
3. install the php dependencies
4. build assets

.. warning::

   The script will work only if the binary ``docker-compose`` is located into your ``PATH``. If you use ``compose`` as a docker plugin,
   you can simulate this binary by creating this file at (for instance), ``/usr/local/bin/docker-compose`` (and run ``chmod +x /usr/local/bin/docker-compose``):

   .. code-block:: bash

      #!/bin/bash

      /usr/bin/docker compose "$@"


.. note::

   In some cases it can happen that an old image (chill_base_php82 or chill_php82) stored in the docker cache will make the script fail. To solve this problem you have to delete the image and the container, before the make init :

    .. code-block:: bash

       docker-compose images php
       docker rmi -f chill_php82:prod
       docker-compose rm php


4. Start the project
====================

.. code-block:: bash

   docker-compose up

**On the first run** (and after each upgrade), you must execute *post update commands* and run database migrations. With a container up and running, execute the following commands:

.. code-block:: bash

   # mount into to container
   ./docker-php.sh
   bin/console chill:db:sync-views
   # and load fixtures
   bin/console doctrine:migrations:migrate


Chill will be available at ``http://localhost:8001.`` Currently, there isn't any user or data. To add fixtures, run

.. code-block:: bash

   # mount into to container
   ./docker-php.sh
   # and load fixtures (do not this for production)
   bin/console doctrine:fixtures:load --purge-with-truncate

There are several users available:

- ``center a_social``
- ``center b_social``

The password is always ``password``.

Now, read `Operations` below. For running in production, read `prod_`.


Operations
**********

Build assets
============

run those commands:

.. code-block:: bash

   make build-assets

How to execute the console ?
============================

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   # if not
   docker-compose run --user $(id -u) php bin/console

How to create the database schema (= run migrations) ?
======================================================

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   bin/console doctrine:migrations:migrate
   bin/console chill:db:sync-views
   # if not
   docker-compose run --user $(id -u) php bin/console doctrine:migrations:migrate
   docker-compose run --user $(id -u) php bin/console chill:db:sync-views


How to read the email sent by the program ?
===========================================

Go at ``http://localhost:8005`` and you should have access to mailcatcher.

In case of you should click on a link in the email, be aware that you should remove the "s" from https.

How to load fixtures ? (development mode only)
==============================================

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   bin/console doctrine:fixtures:load
   # if not
   docker-compose run --user $(id -u) php bin/console doctrine:fixtures:load

How to open a terminal in the project
=====================================

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   # if not
   docker-compose run --user $(id -u) php /bin/bash

How to run cron-jobs ?
======================

Some command must be executed in :ref:`cron jobs <cronjob>`. To execute them:

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   bin/console chill:cron-job:execute
   # some of them are executed only during the night. So, we have to force the execution during the day:
   bin/console chill:cron-job:execute 'name-of-the-cron'
   # if not
   docker-compose run --user $(id -u) php bin/console chill:cron-job:execute
   # some of them are executed only during the night. So, we have to force the execution during the day:
   docker-compose run --user $(id -u) php bin/console chill:cron-job:execute 'name-of-the-cron'

How to run composer ?
=====================

.. code-block:: bash

   # if a container is running
   ./docker-php.sh
   composer
   # if not
   docker-compose run --user $(id -u) php composer

How to access to PGADMIN ?
==========================

Pgadmin is installed with docker-compose, and is available **only if you uncomment the appropriate lines into ``docker-compose.override.yml``.

You can access it at ``http://localhost:8002``.

Credentials:

- login: from the variable you set into ``docker-composer.override.yml``
- password: same :-)

How to run tests ?
==================

Tests reside inside the installed bundles. You must `cd` into that directory, download the required packages, and execute them from this place.

**Note**: some bundle require the fixture to be executed. See the dedicated _how-tos_.

Exemple, for running unit test inside `main` bundle:

.. code-block:: bash

   # mount into the php image
   ./docker-php.sh
   # cd into main directory
   cd vendor/chill-project/chill-bundles
   # download deps
   git submodule init
   git submodule update
   composer install
   # run tests
   bin/phpunit src/Bundle/path/to/your/test

Or for running tests to check code style and php conventions with csfixer and phpstan:

.. code-block:: bash

    # run code style fixer
    bin/grumphp run --tasks=phpcsfixer
    # run phpstan
    bin/grumphp run --tasks=phpstan


.. note::

    To avoid phpstan block your commits:

    .. code-block:: bash

        git commit -n ...

    To avoid phpstan block your commits permanently:

    .. code-block:: bash

        ./bin/grumphp git:deinit

How to run webpack interactively
================================

Executing :code:`bash docker-node.sh` will open a terminal in a node container, with volumes mounted.

How to switch the branch for chill-bundles, and get new dependencies
====================================================================

During development, you will switch to new branches for chill-bundles. As long as the dependencies are equals, this does not cause any problem. But sometimes, a new branch introduces a new dependency, and you must download it.

.. warning::

   Ensure that you have gitlab-token ready before updating your branches. See above.

In order to do that without pain, use those steps:

0. Ensuire you have a token, set
1. at the app's root, update the ``composer.json`` to your current branch:

.. code-block:: json

   {
      "require": {
          "chill-bundles": "dev-<my-branch>@dev"
   }

2. mount into the php container (``./docker-php.sh``), and run ``composer update``

Error `An exception has been thrown during the rendering of a template ("Asset manifest file "/var/www/app/web/build/manifest.json" does not exist.").` on first run
====================================================================================================================================================================

Run :code:`make build-assets`

Running in production
*********************

Currently, to run this software in production, the *state of the art* is the following :

1. Run the software locally and tweak the configuration to your needs ;
2. Build the image and store them into a private container registry. This can be done using :code:`make build-and-push-image`.

   To be sure to target the correct container registry, you have to adapt the image names into your ``docker-compose.override.yml`` file.
3. Push the image on your registry, or upload them to the destination machine using ``docker image save`` and ``docker image load``.
3. Run the image on your production server, using docker-compose or eventually docker stack. You have to customize the variable set in docker-compose.

   See also the :ref:`running-production-tips-and-tricks` below.

.. warning::

   In production, you **must** set those variables:

   * ``APP_ENV`` to ``prod``
   * ``APP_DEBUG`` to ``false``

   There are security issues if you keep the same variable than for production.


.. _running-production-tips-and-tricks:

Tips and tricks
===============

Operation on database (backups, running custom sql, replication) are easier to set when run outside of a container. If you run into a container, take care of the volume where data are stored.

The PHP sessions are stored inside redis. This is useful if you distribute the traffic amongst different php server: they will share same sessions if a request goes into a different instance of the container.

When the PHP servers are shared across multiple instances, take care that some data is stored into redis: the same redis server should be reachable by all instances.

It is worth having an eye on the configuration of logstash container.


Design principles
*****************

Why the DB URL is also set in environment, and not in .env file ?
=================================================================

Because, at startup, a script does check the db is up and, if not, wait for a couple of seconds before running ``entrypoint.sh``. For avoiding double configuration, the configuration of the PHP app takes his configuration from environment also (and it will be standard in future releases, with symfony 4.0).

