.. Copyright (C)  2014-2019 Champs Libres Cooperative SCRLFS
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

.. _prod:

Installation for production
###########################

An installation use these services, which are deployed using docker containers:

* a php-fpm image, which run the Php and Symfony code for Chill;
* a nginx image, which serves the assets, and usually proxy the php requests to the fpm image;
* a redis server, which stores the cache, sessions (this is currently hardcoded in the php image), and some useful keys (like wopi locks);
* a postgresql database. The use of postgresql is mandatory;
* a relatorio service, which transform odt templates to full documents (replacing the placeholders);

Some external services:

* (required) an openstack object store, configured with `temporary url <https://docs.openstack.org/swift/latest/api/temporary_url_middleware.html>` configured (no openstack users is required). This is currently the only way to store documents from chill;
* a mailer service (SMTP)
* (optional) a service for verifying phone number. Currently, only Twilio is possible;
* (optional) a service for sending Short Messages (SMS). Currently, only Ovh is possible;

The `docker-compose.yaml` file of chill app is a basis for a production install. The environment variable in the ```.env``` and ```.env.prod``` should be overriden by environment variables, or ```.env.local``` files.

This should be adapted to your needs:

* The image for php and nginx apps are pre-compiled images, with the default configuration and bundle. If they do not fullfill your needs, you should compile your own images.

   .. TODO:

      As the time of writing (2022-07-03) those images are not published yet.

* Think about how you will backup your database. Some adminsys find easier to store database outside of docker, which might be easier to administrate or replicate.

Run migrations on each update
=============================

Every time you start a new version, you should apply update the sql schema:

- running ``bin/console doctrine:migration:migrate`` to run sql migration;
- synchonizing sql views to the last state: ``bin/console chill:db:sync-views``

Cron jobs
=========

The command :code:`chill:cron-job:execute` should be executed every 15 minutes (more or less).

This command should never be executed concurrently. It should be not have more than one process for a single instance.

Post-install tasks
==================

- import addresses. See :ref:`addresses`.


Tweak symfony messenger
=======================

Calendar sync is processed using symfony messenger.

You can tweak the configuration

Going further:

* Configure the saml login and synchronisation with Outlook api
