# Export conventions


Add condition with distinct alias on each export join clauses (Indicators + Filters + Aggregators)

These are alias conventions :

| Entity                                  | Join                                    | Attribute                                  | Alias                                      |
|:----------------------------------------|:----------------------------------------|:-------------------------------------------|:-------------------------------------------|
| AccompanyingPeriodStepHistory::class    |                                         |                                            | acpstephistory (contexte ACP_STEP_HISTORY) |
|                                         | AccompanyingPeriod::class               | acpstephistory.period                      | acp                                        |
| AccompanyingPeriod::class               |                                         |                                            | acp                                        |
|                                         | AccompanyingPeriodWork::class           | acp.works                                  | acpw                                       |
|                                         | AccompanyingPeriodParticipation::class  | acp.participations                         | acppart                                    |
|                                         | Location::class                         | acp.administrativeLocation                 | acploc                                     |
|                                         | ClosingMotive::class                    | acp.closingMotive                          | acpmotive                                  |
|                                         | UserJob::class                          | acp.job                                    | acpjob                                     |
|                                         | Origin::class                           | acp.origin                                 | acporigin                                  |
|                                         | Scope::class                            | acp.scopes                                 | acpscope                                   |
|                                         | SocialIssue::class                      | acp.socialIssues                           | acpsocialissue                             |
|                                         | User::class                             | acp.user                                   | acpuser                                    |
|                                         | AccompanyingPeriopStepHistory::class    | acp.stepHistories                          | acpstephistories                           |
|                                         | AccompanyingPeriodInfo::class           | not existing (using custom WITH clause)    | acpinfo                                    |
| AccompanyingPeriodWork::class           |                                         |                                            | acpw                                       |
|                                         | AccompanyingPeriodWorkEvaluation::class | acpw.accompanyingPeriodWorkEvaluations     | workeval                                   |
|                                         | SocialAction::class                     | acpw.socialAction                          | acpwsocialaction                           |
|                                         | Goal::class                             | acpw.goals                                 | goal                                       |
|                                         | Result::class                           | acpw.results                               | result                                     |
| AccompanyingPeriodParticipation::class  |                                         |                                            | acppart                                    |
|                                         | Person::class                           | acppart.person                             | partperson                                 |
| AccompanyingPeriodWorkEvaluation::class |                                         |                                            | workeval                                   |
|                                         | Evaluation::class                       | workeval.evaluation                        | eval                                       |
| AccompanyingPeriodInfo::class           |                                         |                                            | acpinfo                                    |
|                                         | User::class                             | acpinfo.user                               | acpinfo_user                               |
| Goal::class                             |                                         |                                            | goal                                       |
|                                         | Result::class                           | goal.results                               | goalresult                                 |
| Person::class                           |                                         |                                            | person                                     |
|                                         | Center::class                           | person.center                              | center                                     |
|                                         | HouseholdMember::class                  | partperson.householdParticipations         | householdmember                            |
|                                         | MaritalStatus::class                    | person.maritalStatus                       | personmarital                              |
|                                         | VendeePerson::class                     |                                            | vp                                         |
|                                         | VendeePersonMineur::class               |                                            | vpm                                        |
|                                         | CurrentPersonAddress::class             | person.currentPersonAddress                | currentPersonAddress (on a given date)     |
| ResidentialAddress::class               |                                         |                                            | resaddr                                    |
|                                         | ThirdParty::class                       | resaddr.hostThirdParty                     | tparty                                     |
| ThirdParty::class                       |                                         |                                            | tparty                                     |
|                                         | ThirdPartyCategory::class               | tparty.categories                          | tpartycat                                  |
| HouseholdMember::class                  |                                         |                                            | householdmember                            |
|                                         | Household::class                        | householdmember.household                  | household                                  |
|                                         | Person::class                           | householdmember.person                     | memberperson                               |
|                                         |                                         | memberperson.center                        | membercenter                               |
| Household::class                        |                                         |                                            | household                                  |
|                                         | HouseholdComposition::class             | household.compositions                     | composition                                |
| Activity::class                         |                                         |                                            | activity                                   |
|                                         | Person::class                           | activity.person                            | actperson                                  |
|                                         | AccompanyingPeriod::class               | activity.accompanyingPeriod                | acp                                        |
|                                         | Person::class                           | activity\_person\_having\_activity.person  | person\_person\_having\_activity           |
|                                         | ActivityReason::class                   | activity\_person\_having\_activity.reasons | reasons\_person\_having\_activity          |
|                                         | ActivityType::class                     | activity.activityType                      | acttype                                    |
|                                         | Location::class                         | activity.location                          | actloc                                     |
|                                         | SocialAction::class                     | activity.socialActions                     | actsocialaction                            |
|                                         | SocialIssue::class                      | activity.socialIssues                      | actsocialssue                              |
|                                         | ThirdParty::class                       | activity.thirdParties                      | acttparty                                  |
|                                         | User::class                             | activity.user                              | actuser                                    |
|                                         | User::class                             | activity.users                             | actusers                                   |
|                                         | ActivityReason::class                   | activity.reasons                           | actreasons                                 |
|                                         | Center::class                           | actperson.center                           | actcenter                                  |
|                                         | Person::class                           | activity.createdBy                         | actcreator                                 |
| ActivityReason::class                   |                                         |                                            | actreasons                                 |
|                                         | ActivityReasonCategory::class           | actreason.category                         | actreasoncat                               |
| Calendar::class                         |                                         |                                            | cal                                        |
|                                         | CancelReason::class                     | cal.cancelReason                           | calcancel                                  |
|                                         | Location::class                         | cal.location                               | calloc                                     |
|                                         | User::class                             | cal.user                                   | caluser                                    |
| VendeePerson::class                     |                                         |                                            | vp                                         |
|                                         | SituationProfessionelle::class          | vp.situationProfessionelle                 | vpprof                                     |
|                                         | StatutLogement::class                   | vp.statutLogement                          | vplog                                      |
|                                         | TempsDeTravail::class                   | vp.tempsDeTravail                          | vptt                                       |
