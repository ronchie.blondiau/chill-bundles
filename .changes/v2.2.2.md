## v2.2.2 - 2023-06-26
### Fixed
* [Accompanying period comments]: order comments from the most recent to the oldest, in the list
* Api: filter social action to keep only the currently activated
* ([#82](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/82)) Fix deletion and re-creation of filiation relationship
