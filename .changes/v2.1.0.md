## v2.1.0 - 2023-06-12

### Feature

* [docgen] allow to pick a third party when generating a document in context Activity, AccompanyingPeriod

### Fixed

* ([#111](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/111)) List of "my accompanying periods": separate the active and closed periods in two different lists, and show the inactive_long and inactive_short periods

### Security

* ([#105](https://gitlab.com/Chill-Projet/chill-bundles/-/issues/105)) Rights are checked for display of 'accompanying period' tab in household menu. Rights are also checked for creation of 'accompanying period' from within household context

### DX

* Add methods to RegroupmentRepository and fullfill Center / Regroupment Doctrine mapping
