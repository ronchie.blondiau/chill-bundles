<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements FilterInterface
{
    public function describeAction($data, $format = 'string')
    {
        // TODO: Implement describeAction() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('test', PickRollingDateType::class, [
            'label' => 'Test thing',
            'data' => new RollingDate(RollingDate::T_TODAY)
        ]);
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
-----
<?php

namespace Utils\Rector\Tests\ChillBundleAddFormDefaultDataOnExportFilterAggregatorRector\Fixture;

use Chill\MainBundle\Export\FilterInterface;
use Chill\MainBundle\Form\Type\PickRollingDateType;
use Chill\MainBundle\Service\RollingDate\RollingDate;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class MyClass implements FilterInterface
{
    public function describeAction($data, $format = 'string')
    {
        // TODO: Implement describeAction() method.
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('test', PickRollingDateType::class, [
            'label' => 'Test thing'
        ]);
    }
    public function getFormDefaultData(): array
    {
        return ['test' => new RollingDate(RollingDate::T_TODAY)];
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
    }

    public function addRole(): ?string
    {
        // TODO: Implement addRole() method.
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        // TODO: Implement alterQuery() method.
    }

    public function applyOn()
    {
        // TODO: Implement applyOn() method.
    }
}
?>
